CREATE DATABASE [DIPREMA]
GO
USE [DIPREMA]
GO
/****** Object:  Table [dbo].[AperturaCierre]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AperturaCierre](
	[AperturaCierreID] [bigint] IDENTITY(1,1) NOT NULL,
	[FechaApertura] [datetime] NOT NULL,
	[FechaCierre] [datetime] NULL,
	[Turno] [varchar](50) NOT NULL,
	[PedidoID] [int] NOT NULL,
	[Estado] [bit] NOT NULL default 1,
	[MaquinaID] [int] NULL,
 CONSTRAINT [PK_AperturaCierre] PRIMARY KEY CLUSTERED 
(
	[AperturaCierreID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[AperturaCierreActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*Vistas*/
create VIEW [dbo].[AperturaCierreActivos] AS
select * from AperturaCierre where Estado = 1;
GO
/****** Object:  Table [dbo].[DetalleEntrega]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleEntrega](
	[DetelleEntregaID] [bigint] IDENTITY(1,1) NOT NULL,
	[EntregaID] [int] NOT NULL,
	[ProduccionID] [int] NOT NULL,
	[Paquetes] [int] NOT NULL,
	[Cantidad] [float] NOT NULL,
	[Estado] [bit] NOT NULL default 1,
 CONSTRAINT [PK_DetalleEntrega] PRIMARY KEY CLUSTERED 
(
	[DetelleEntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Entregas]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entregas](
	[EntregaID] [int] IDENTITY(1,1) NOT NULL,
	[NumeroEntrega] [varchar](50) NOT NULL,
	[Fecha] [date] NULL,
	[UnidadesEntregadas] [int] NULL,
	[NumeroFactura] [varchar](50) NULL,
	[Estado] [bit] NOT NULL default 1,
PRIMARY KEY CLUSTERED 
(
	[EntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pedidos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pedidos](
	[PedidoID] [int] IDENTITY(1,1) NOT NULL,
	[NumeroPedido] [varchar](50) NOT NULL,
	[ClienteID] [int] NOT NULL,
	[NumeroOrdenDeCompra] [varchar](50) NULL,
	[ProductoID] [int] NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[UnidadMedidaID] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Fecha] [date] NULL,
	[PrecioTotal] [money] NOT NULL,
	[Estado] [bit] NOT NULL default 1,
PRIMARY KEY CLUSTERED 
(
	[PedidoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productos](
	[ProductoID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProducto] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[TipoProductoID] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Imagen] [varchar](max) NULL,
	[Estado] [bit] NOT NULL default 1,
PRIMARY KEY CLUSTERED 
(
	[ProductoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UnidadMedida]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnidadMedida](
	[UnidadMedidaID] [int] IDENTITY(1,1) NOT NULL,
	[Unidad] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UnidadMedidaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Produccion]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Produccion](
	[ProduccionID] [int] IDENTITY(1,1) NOT NULL,
	[PedidoID] [int] NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaFin] [datetime] NULL,
	[HorasTrabajadas] [varchar](100) NULL,
	[CantidadMerma] [float] NULL,
	[CantidadProduccionTotal] [float] NOT NULL,
	[CantidadPaquetesCajas] [int] NOT NULL,
	[Estado] [bit] NOT NULL default 1,
PRIMARY KEY CLUSTERED 
(
	[ProduccionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[DetalleEntregaActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create VIEW [dbo].[DetalleEntregaActivos] AS

Select Entregas.Fecha as FechaEntrega, Entregas.NumeroFactura as NumeroFactura ,Entregas.NumeroEntrega as NumeroEntrega,
		Pedidos.NumeroPedido as lote, Pedidos.NumeroOrdenDeCompra as NumeroOrdenCompra,
		Productos.Nombre as Producto,
		DetalleEntrega.Paquetes, DetalleEntrega.Cantidad,
		UnidadMedida.Unidad
from DetalleEntrega

join Entregas on DetalleEntrega.EntregaID = Entregas.EntregaID
join Produccion on  DetalleEntrega.ProduccionID = Produccion.ProduccionID
join Pedidos on Produccion.ProduccionID = Pedidos.PedidoID
join UnidadMedida on Pedidos.UnidadMedidaID = UnidadMedida.UnidadMedidaID
join Productos on Pedidos.ProductoID = Productos.ProductoID

where DetalleEntrega.Estado =1
GO
/****** Object:  Table [dbo].[MermaPorTurno]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MermaPorTurno](
	[MermaPorTurnoID] [int] IDENTITY(1,1) NOT NULL,
	[Cantidad] [float] NOT NULL,
	[AperturaCierreID] [bigint] NOT NULL,
	[TipoMermaID] [int] NOT NULL,
	[Estado] [bit] NOT NULL default 1,
PRIMARY KEY CLUSTERED 
(
	[MermaPorTurnoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MermaPorTurnoActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[MermaPorTurnoActivos] as 
SELECT        *
FROM            MermaPorTurno
WHERE        Estado = 1
GO
/****** Object:  Table [dbo].[ProduccionPaquetesCajas]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProduccionPaquetesCajas](
	[EtiquetaID] [int] IDENTITY(1,1) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Cantidad] [float] NOT NULL,
	[AperturaCierreID] [bigint] NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[Turno] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL default 1,
 CONSTRAINT [PK_ProduccionPaquetesCajas] PRIMARY KEY CLUSTERED 
(
	[EtiquetaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ProduccionPaquetesCajaActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[ProduccionPaquetesCajaActivos] as
select * from ProduccionPaquetesCajas where Estado = 1;
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clientes](
	[ClienteID] [int] IDENTITY(1,1) NOT NULL,
	[Identificacion] [varchar](50) NOT NULL,
	[NombreCompleto] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL default 1,
PRIMARY KEY CLUSTERED 
(
	[ClienteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ClientesActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*Vistas*/
create VIEW [dbo].[ClientesActivos] AS
select * from Clientes where Estado = 1;
GO
/****** Object:  View [dbo].[EntregasActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create VIEW [dbo].[EntregasActivos] as
select * from Entregas where Estado = 1;
GO
/****** Object:  Table [dbo].[FichaTecnicaBotella]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FichaTecnicaBotella](
	[FichaTecnicaBotellaID] [int] IDENTITY(1,1) NOT NULL,
	[ProductoID] [int] NOT NULL,
	[Altura] [float] NOT NULL,
	[DiametroMayor] [float] NOT NULL,
	[DiametroMenor] [float] NOT NULL,
	[DiametroCuello] [float] NOT NULL,
	[CapacidadVolumetrica] [float] NOT NULL,
	[Tolerancia] [float] NULL,
	[ImagenCuerpo] [varchar](max) NULL,
	[ImagenFondo] [varchar](max) NULL,
	[Estado] [bit] NOT NULL default 1,
PRIMARY KEY CLUSTERED 
(
	[FichaTecnicaBotellaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[FichaTecnicaBotellaActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[FichaTecnicaBotellaActivos] as
select * from FichaTecnicaBotella where Estado = 1;
GO
/****** Object:  Table [dbo].[FichaTecnicaTapa]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FichaTecnicaTapa](
	[FichaTecnicaTapaID] [int] IDENTITY(1,1) NOT NULL,
	[ProductoID] [int] NOT NULL,
	[PesoPieza] [float] NOT NULL,
	[Altura] [float] NOT NULL,
	[DiametroInterno] [float] NOT NULL,
	[DiametroExternoSelloOliva] [float] NOT NULL,
	[AlturaSelloOliva] [float] NOT NULL,
	[DiametroInternoBandaSeguridad] [float] NOT NULL,
	[EspesorPanel] [float] NOT NULL,
	[NumeroEstrias] [float] NOT NULL,
	[TorqueCierreRecomendado] [float] NOT NULL,
	[TorqueApertura] [float] NOT NULL,
	[Tolerancia] [float] NULL,
	[Imagen] [varchar](max) NULL,
	[Estado] [bit] NOT NULL default 1,
PRIMARY KEY CLUSTERED 
(
	[FichaTecnicaTapaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[FichaTecnicaTapaActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[FichaTecnicaTapaActivos] as
select * from FichaTecnicaTapa where Estado = 1;
GO
/****** Object:  Table [dbo].[Maquinas]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Maquinas](
	[MaquinaID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoMaquina] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[TipoMaquinaID] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[ProduccionHora] [varchar](100) NULL,
	[Estado] [bit] NOT NULL default 1,
PRIMARY KEY CLUSTERED 
(
	[MaquinaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MaquinasActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MaquinasActivos] as
select * from Maquinas where Estado = 1;
GO
/****** Object:  Table [dbo].[MateriaPrima]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MateriaPrima](
	[MateriaPrimaID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoMateriaPrima] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[ProveedorID] [int] NOT NULL,
	[TipoMateriaPrimaID] [int] NOT NULL,
	[UnidadMedidaID] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[PrecioUnitario] [money] NOT NULL,
	[PrecioTotal] [money] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Estado] [bit] NOT NULL default 1,
PRIMARY KEY CLUSTERED 
(
	[MateriaPrimaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MateriaPrimaActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MateriaPrimaActivos] as
select * from MateriaPrima where Estado = 1;
GO
/****** Object:  View [dbo].[PedidosActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[PedidosActivos] as
select * from Pedidos where Estado = 1;
GO
/****** Object:  View [dbo].[ProduccionActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[ProduccionActivos] as
select * from Produccion where Estado = 1;
GO
/****** Object:  View [dbo].[ProductosActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProductosActivos] as
select * from Productos where Estado = 1;
GO
/****** Object:  Table [dbo].[Proveedor]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proveedor](
	[ProveedorID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProveedor] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL default 1,
PRIMARY KEY CLUSTERED 
(
	[ProveedorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ProveedorActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProveedorActivos] as
select * from Proveedor where Estado = 1;
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[UsuarioID] [int] IDENTITY(1,1) NOT NULL,
	[Identificacion] [varchar](50) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [varchar](100) NOT NULL,
	[NombreCompleto] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL default 1,
PRIMARY KEY CLUSTERED 
(
	[UsuarioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[UsuariosActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[UsuariosActivos] as
select * from Usuarios where Estado = 1;
GO
/****** Object:  Table [dbo].[VariablesMaquinaInyectora]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VariablesMaquinaInyectora](
	[VariablesMaquinaInyectoraID] [int] IDENTITY(1,1) NOT NULL,
	[MaquinaID] [int] NOT NULL,
	[ProductoID] [int] NOT NULL,
	[MateriaPrimaPrincipalID] [int] NOT NULL,
	[MateriaPrimaSecundariaID] [int] NULL,
	[MateriaPrimaTerciariaID] [int] NULL,
	[CantidadPrimaPrincipal] [float] NOT NULL,
	[CantidadPrimaSecundaria] [float] NULL,
	[CantidadPrimaTerciaria] [float] NULL,
	[Estado] [bit] NOT NULL default 1,
PRIMARY KEY CLUSTERED 
(
	[VariablesMaquinaInyectoraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VariablesMaquinaInyectoraActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VariablesMaquinaInyectoraActivos] as
select * from VariablesMaquinaInyectora where Estado = 1;
GO
/****** Object:  Table [dbo].[VariablesMaquinaSopladora]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VariablesMaquinaSopladora](
	[VariablesMaquinaSopladoraID] [int] IDENTITY(1,1) NOT NULL,
	[MaquinaID] [int] NOT NULL,
	[ProductoID] [int] NOT NULL,
	[MateriaPrimaID] [int] NOT NULL,
	[RetardoPresoplado] [int] NOT NULL,
	[RetardoSoplado] [int] NOT NULL,
	[TiempoPresoplado] [int] NOT NULL,
	[TiempoRecuperacion] [int] NOT NULL,
	[TiempoSoplado] [int] NOT NULL,
	[TiempoEscape] [int] NOT NULL,
	[TiempoParoCadena] [int] NOT NULL,
	[TiempoSalidaPinzaPreforma] [int] NOT NULL,
	[FrecuenciaVentilador] [float] NOT NULL,
	[PresionEstirado] [float] NOT NULL,
	[Gancho] [bit] NOT NULL,
	[Relay] [bit] NOT NULL,
	[AnillosTornelas] [bit] NOT NULL,
	[Estado] [bit] NOT NULL default 1,
	[Notas] [varchar](100) NULL,
	[CantidadZonas] [int] NOT NULL,
	[CantidadBancos] [int] NOT NULL,
	[PosicionBanco1Zona1] [varchar](100) NULL,
	[PosicionBanco1Zona2] [varchar](100) NULL,
	[PosicionBanco1Zona3] [varchar](100) NULL,
	[PosicionBanco1Zona4] [varchar](100) NULL,
	[PosicionBanco2Zona1] [varchar](100) NULL,
	[PosicionBanco2Zona2] [varchar](100) NULL,
	[PosicionBanco2Zona3] [varchar](100) NULL,
	[PosicionBanco2Zona4] [varchar](100) NULL,
	[ValorBanco1Zona1] [int] NULL,
	[ValorBanco1Zona2] [int] NULL,
	[ValorBanco1Zona3] [int] NULL,
	[ValorBanco1Zona4] [int] NULL,
	[ValorBanco2Zona1] [int] NULL,
	[ValorBanco2Zona2] [int] NULL,
	[ValorBanco2Zona3] [int] NULL,
	[ValorBanco2Zona4] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[VariablesMaquinaSopladoraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VariablesMaquinaSopladoraActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VariablesMaquinaSopladoraActivos] as
select * from VariablesMaquinaSopladora where Estado = 1;
GO
/****** Object:  View [dbo].[MaquinasSopladoraActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MaquinasSopladoraActivos] as
select * from Maquinas where TipoMaquinaID = 1 and Estado = 1;
GO
/****** Object:  View [dbo].[MaquinasInyectoraActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MaquinasInyectoraActivos] as
select * from Maquinas where TipoMaquinaID = 2 and Estado = 1;
GO
/****** Object:  View [dbo].[MateriaPrimaInyectoraActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MateriaPrimaInyectoraActivos] as
select * from MateriaPrima where TipoMateriaPrimaID = 2 or TipoMateriaPrimaID = 3 and Estado = 1
GO
/****** Object:  View [dbo].[MateriaPrimaSopladoraActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MateriaPrimaSopladoraActivos] as
select * from MateriaPrima where TipoMateriaPrimaID = 1 and Estado = 1
GO
/****** Object:  View [dbo].[ProductosBotellaActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProductosBotellaActivos] as
select * from Productos where TipoProductoID = 1 and Estado = 1;
GO
/****** Object:  View [dbo].[ProductosTapaActivos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProductosTapaActivos] as
select * from Productos where TipoProductoID = 2 and Estado = 1;
GO
/****** Object:  Table [dbo].[Movimientos]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movimientos](
	[PedidoID] [int] NOT NULL,
	[EntregaID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Movimientos] PRIMARY KEY CLUSTERED 
(
	[PedidoID] ASC,
	[EntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoMaquina]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoMaquina](
	[TipoMaquinaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMaquinaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoMateriaPrima]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoMateriaPrima](
	[TipoMateriaPrimaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMateriaPrimaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoMerma]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoMerma](
	[TipoMermaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMermaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoProducto]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoProducto](
	[TipoProductoID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoProductoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsersInRoles]    Script Date: 8/9/2020 2:26:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersInRoles](
	[User_UserID] [int] NOT NULL,
	[Role_RoleID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.RoleUsers] PRIMARY KEY CLUSTERED 
(
	[Role_RoleID] ASC,
	[User_UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AperturaCierre] ON 

INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (1, CAST(N'2020-08-23T16:38:54.000' AS DateTime), CAST(N'2020-08-23T21:30:28.823' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (8, CAST(N'2020-08-17T18:06:23.000' AS DateTime), CAST(N'2020-08-25T16:12:12.933' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (9, CAST(N'2020-08-17T18:13:47.000' AS DateTime), CAST(N'2020-08-17T18:14:00.143' AS DateTime), N'Tarde', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (10, CAST(N'2020-08-23T22:06:43.000' AS DateTime), CAST(N'2020-08-25T16:12:11.660' AS DateTime), N'Mañana', 3, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (11, CAST(N'2020-08-23T22:39:58.000' AS DateTime), CAST(N'2020-08-25T16:12:13.990' AS DateTime), N'Mañana', 6, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (12, CAST(N'2020-08-23T22:41:36.000' AS DateTime), CAST(N'2020-08-25T16:12:14.677' AS DateTime), N'Mañana', 5, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (13, CAST(N'2020-08-23T22:43:16.000' AS DateTime), CAST(N'2020-08-25T16:12:15.223' AS DateTime), N'Mañana', 4, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (14, CAST(N'2020-07-30T00:00:00.000' AS DateTime), CAST(N'2020-08-25T16:24:08.840' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (15, CAST(N'2020-08-26T00:00:00.000' AS DateTime), CAST(N'2020-08-25T16:12:15.720' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (16, CAST(N'2020-08-05T00:00:00.000' AS DateTime), CAST(N'2020-08-25T16:24:09.477' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (17, CAST(N'2020-08-05T00:00:00.000' AS DateTime), CAST(N'2020-08-25T16:24:10.017' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (18, CAST(N'2020-08-02T00:00:00.000' AS DateTime), CAST(N'2020-08-25T16:24:07.697' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (21, CAST(N'2020-08-25T16:23:51.000' AS DateTime), CAST(N'2020-08-25T16:24:06.087' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (22, CAST(N'2020-08-25T16:23:57.000' AS DateTime), CAST(N'2020-08-25T16:24:06.937' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (23, CAST(N'2020-08-25T16:24:03.000' AS DateTime), CAST(N'2020-08-25T16:24:08.257' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (24, CAST(N'2020-08-25T16:25:48.000' AS DateTime), CAST(N'2020-08-25T16:30:07.620' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (25, CAST(N'2020-08-25T16:29:04.000' AS DateTime), CAST(N'2020-08-25T16:30:08.713' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (26, CAST(N'2020-08-25T16:30:09.000' AS DateTime), CAST(N'2020-08-25T16:31:28.277' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (27, CAST(N'2020-08-25T16:31:29.000' AS DateTime), CAST(N'2020-08-25T16:32:39.073' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (28, CAST(N'2020-08-25T16:32:39.000' AS DateTime), CAST(N'2020-08-25T16:34:02.547' AS DateTime), N'Mañana', 38, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (29, CAST(N'2020-08-25T16:34:03.000' AS DateTime), CAST(N'2020-08-25T16:38:07.900' AS DateTime), N'Mañana', 38, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (30, CAST(N'2020-08-25T16:37:12.000' AS DateTime), CAST(N'2020-08-25T16:37:18.930' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (31, CAST(N'2020-08-25T16:37:56.000' AS DateTime), CAST(N'2020-08-25T16:38:08.980' AS DateTime), N'Mañana', 38, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (32, CAST(N'2020-08-25T16:38:09.000' AS DateTime), CAST(N'2020-08-25T17:06:44.077' AS DateTime), N'Mañana', 38, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (33, CAST(N'2020-08-25T16:38:43.000' AS DateTime), CAST(N'2020-08-25T16:42:25.217' AS DateTime), N'Tarde', 40, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (34, CAST(N'2020-08-25T17:57:19.970' AS DateTime), CAST(N'2020-08-25T18:58:57.120' AS DateTime), N'Mañana', 38, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (35, CAST(N'2020-08-25T18:59:17.360' AS DateTime), CAST(N'2020-09-03T13:43:54.830' AS DateTime), N'Tarde', 38, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (36, CAST(N'2020-09-03T13:43:56.000' AS DateTime), CAST(N'2020-09-08T12:08:32.683' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (37, CAST(N'2020-09-08T12:28:03.000' AS DateTime), CAST(N'2020-09-08T12:30:26.043' AS DateTime), N'Tarde', 38, 0, 14)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (38, CAST(N'2020-09-08T12:30:27.000' AS DateTime), CAST(N'2020-09-08T12:38:10.937' AS DateTime), N'Noche', 38, 0, 13)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (39, CAST(N'2020-09-08T12:38:11.000' AS DateTime), CAST(N'2020-09-08T13:19:34.973' AS DateTime), N'Mañana', 1, 0, 12)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (40, CAST(N'2020-09-08T13:19:36.000' AS DateTime), NULL, N'Mañana', 47, 1, 12)
SET IDENTITY_INSERT [dbo].[AperturaCierre] OFF
SET IDENTITY_INSERT [dbo].[Clientes] ON 

INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (19, N'610830605', N'Keiko Gordon', N'18476598', N'interdum@CurabiturmassaVestibulum.com', N'Uttar Pradesh', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (20, N'497650452', N'Quyn Velez', N'11975833', N'enim.nisl.elementum@atpretiumaliquet.edu', N'LEN', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (21, N'592833739', N'Micah Kirby', N'62235186', N'consequat.auctor.nunc@odiovelest.org', N'Vienna', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (22, N'520658421', N'Sasha Ratliff', N'44527822', N'adipiscing@dictummagnaUt.com', N'TX', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (23, N'429077125', N'McKenzie Cole', N'48944039', N'dui.in@odioEtiamligula.co.uk', N'Maharastra', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (24, N'902681159', N'Naomi Adams', N'38191805', N'non.dui.nec@scelerisquedui.com', N'SJ', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (25, N'117893803', N'Travis Mccluree', N'89091848', N'cursus@Aliquamadipiscinglobortis.ca', N'RM', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (26, N'380928797', N'Clinton Hines', N'63883206', N'non.ante@velitSed.edu', N'Vienna', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (27, N'783718392', N'Guy Bolton', N'69298809', N'feugiat.non@aliquamarcu.net', N'SMO', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (28, N'642211165', N'Jenna Kerr', N'28189291', N'tortor.at.risus@Nunc.org', N'HH', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (29, N'185552204', N'Forrest Santiago', N'79243979', N'elit.Aliquam.auctor@ProinmiAliquam.edu', N'Cantabria', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (30, N'766323821', N'Griffith Griffin', N'96374699', N'enim@vitaediamProin.ca', N'RJ', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (31, N'675994098', N'Casey Mann', N'75957155', N'rutrum.magna@pretiumaliquet.org', N'Diyarbakir', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (32, N'731865605', N'Marshall Rodriquez', N'14903003', N'eget.mollis.lectus@mattis.co.uk', N'HB', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (33, N'919047907', N'Catherine Pugh', N'90040560', N'inceptos.hymenaeos.Mauris@orciin.co.uk', N'Östergötlands län', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (34, N'691613792', N'Naomi Silva', N'40038551', N'Donec.felis.orci@Aeneanegetmagna.co.uk', N'SI', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (35, N'829472934', N'Stephen Fitzpatrick', N'11021889', N'mollis.vitae@fringilla.org', N'South Kalimantan', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (36, N'451865613', N'Nayda Kelly', N'22472475', N'lacus.Nulla.tincidunt@necurnasuscipit.net', N'Ontario', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (37, N'180400495', N'Bertha Solis', N'25964789', N'sagittis.augue@tellus.net', N'AB', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (38, N'785276376', N'Rudyard Mccarthy', N'41554147', N'et@vitae.ca', N'Ulster', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (39, N'350989313', N'Audrey Stark', N'22461565', N'magnis@orciadipiscing.org', N'AB', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (40, N'463010141', N'Stewart Austin', N'96050284', N'erat.nonummy@felisNullatempor.org', N'Henegouwen', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (41, N'455416293', N'Hamish Randolph', N'93953196', N'molestie@velarcu.ca', N'L', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (42, N'216476790', N'Kylan Baird', N'11175957', N'id.blandit@leoVivamusnibh.co.uk', N'O', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (43, N'452318830', N'Ivor Lyons', N'64788449', N'faucibus@etipsum.ca', N'Bremen', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (44, N'775194566', N'Jesse Gomez', N'56186841', N'Donec.nibh@Donecporttitor.co.uk', N'SP', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (45, N'753974423', N'Sara Thomas', N'95355409', N'nisi.Aenean@duiSuspendisse.edu', N'JUN', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (46, N'658959547', N'Chava Barron', N'86301708', N'ac.risus@placerat.org', N'Adana', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (47, N'470697176', N'Joshua Gaines', N'29278077', N'Vivamus@tortordictumeu.net', N'VLG', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (48, N'402121333', N'Russell Hancock', N'72081220', N'erat@libero.com', N'Mazowieckie', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (49, N'365200530', N'Jakeem Randall', N'53361365', N'est@luctus.edu', N'Noord Holland', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (50, N'764294968', N'Jamal Robinson', N'58457566', N'non.feugiat@ametloremsemper.co.uk', N'YAR', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (51, N'489843663', N'Anika Forbes', N'83439338', N'Nunc.ac.sem@lobortisClassaptent.com', N'RJ', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (52, N'247012451', N'Rhea Fuller', N'51207171', N'ultricies.ornare@sitametdiam.org', N'F', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (53, N'823635737', N'Calvin Wall', N'95170329', N'nulla.magna.malesuada@Sedeu.edu', N'Uttar Pradesh', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (54, N'489848419', N'Indira Merritt', N'67441289', N'aliquam.eros@lectusconvallis.ca', N'New South Wales', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (55, N'284311259', N'Samantha Sharpe', N'84695164', N'dictum.magna@aliquetlobortis.net', N'PO', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (56, N'105036401', N'Rae Nash', N'54354292', N'vitae.semper.egestas@viverra.co.uk', N'IL', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (57, N'488572025', N'Neville Humphrey', N'23216408', N'in.dolor@maurissagittisplacerat.ca', N'M', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (58, N'407089921', N'Kylie Mccoy', N'65078982', N'Cum.sociis@atarcu.co.uk', N'KS', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (59, N'701476367', N'Grady Flowers', N'55959770', N'euismod.in@eratvel.co.uk', N'Rivers', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (60, N'280826669', N'Fritz Roberson', N'27682367', N'massa.rutrum.magna@nonummyacfeugiat.org', N'Emilia-Romagna', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (61, N'570784633', N'Quinn Holt', N'70880555', N'mauris.erat@nulla.com', N'VEN', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (62, N'948831357', N'Micah Snyder', N'63919264', N'aliquam@eutelluseu.org', N'WB', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (63, N'146661975', N'Clio Weber', N'65474174', N'augue@metus.org', N'MA', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (64, N'749458835', N'Phoebe Mathis', N'89467510', N'cursus@Nullamlobortis.ca', N'L', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (65, N'269243494', N'Armand Atkins', N'24070415', N'lorem.ut.aliquam@torquentper.edu', N'LAL', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (66, N'179277770', N'Byron Sherman', N'55645268', N'elit.Etiam.laoreet@urnaNunc.com', N'PUG', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (67, N'406357257', N'Dahlia Summers', N'19450920', N'vitae@feugiat.edu', N'Tyumen Oblast', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (68, N'716168367', N'Aileen Blevins', N'79822534', N'nunc.sed@metusAliquam.com', N'QC', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (69, N'168751844', N'Declan Lott', N'54633878', N'erat@quamdignissim.edu', N'Gyeonggi', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (70, N'369788574', N'Desirae Stokes', N'50020104', N'eu@convallisdolor.net', N'CG', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (71, N'516843405', N'Sawyer Waters', N'14527128', N'Nunc@consectetuereuismod.ca', N'Sindh', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (72, N'866937989', N'Mara Olson', N'16297328', N'diam.Proin@mollisPhaselluslibero.edu', N'KY', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (73, N'776137471', N'Hayley Cummings', N'70779188', N'nunc@ultricesiaculis.ca', N'WA', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (74, N'672313608', N'Sophia Witt', N'20150875', N'lobortis@miDuis.co.uk', N'N.', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (75, N'761250005', N'Hamish Fry', N'72236212', N'tempor@egestasAliquamfringilla.co.uk', N'Ist', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (76, N'680763504', N'Alexis Fernandez', N'48414409', N'blandit@lectus.com', N'Alabama', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (77, N'611206680', N'Ciara Rollins', N'96508536', N'dolor.quam@temporaugueac.ca', N'AB', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (78, N'679176157', N'Kaye Chapman', N'60100725', N'Proin@sociisnatoquepenatibus.ca', N'Comunitat Valenciana', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (79, N'163886784', N'Wanda Booker', N'18835195', N'eu@luctusfelispurus.org', N'CA', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (80, N'884554740', N'Daphne Peck', N'26827001', N'dapibus@ridiculusmusAenean.org', N'Hgo', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (81, N'919755773', N'Bryar Ramos', N'49962399', N'posuere.cubilia@imperdiet.edu', N'NI', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (82, N'644763324', N'Nerea Baker', N'51789314', N'dolor@aceleifendvitae.net', N'OX', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (83, N'666815750', N'Lucius Finch', N'30779391', N'nisl@eu.edu', N'Lombardia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (84, N'466408721', N'Arsenio Vaughn', N'49795835', N'Curae.Phasellus.ornare@a.ca', N'Oaxaca', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (85, N'710352475', N'Chastity Carney', N'30121114', N'fringilla.est@inmolestietortor.org', N'Slaskie', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (86, N'502349354', N'Ryder Vincent', N'95345190', N'faucibus.lectus@tellus.co.uk', N'LA', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (87, N'850514698', N'Garrett Crosby', N'63908742', N'enim@habitant.edu', N'Andalucía', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (88, N'639548679', N'Kirestin Matthews', N'16132639', N'imperdiet@quam.co.uk', N'Chi', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (89, N'499308914', N'Camilla Blevins', N'13352849', N'imperdiet.erat.nonummy@egetdictum.com', N'Malopolskie', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (90, N'539120342', N'Kyle Castillo', N'55543549', N'malesuada.malesuada@tristique.ca', N'Stockholms län', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (91, N'283539010', N'Walker Padilla', N'34207523', N'Nunc@ipsum.com', N'BR', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (92, N'501251413', N'Hollee Wynn', N'80951722', N'auctor.non.feugiat@Nunc.com', N'Antioquia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (93, N'634871311', N'McKenzie Barrett', N'43801703', N'quis.diam@Integer.net', N'Heredia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (94, N'267082196', N'Jordan Fitzpatrick', N'19237708', N'eu.tellus.Phasellus@acrisusMorbi.edu', N'Mexico City', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (95, N'863423651', N'Liberty Massey', N'84792221', N'sem.ut.dolor@id.edu', N'Vienna', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (96, N'743495220', N'Leonard Blackwell', N'93094324', N'tempor.lorem.eget@utsem.co.uk', N'OR', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (97, N'550401800', N'Cyrus Hanson', N'12070362', N'fringilla.porttitor@euaugue.ca', N'North Island', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (98, N'951123733', N'Louis Wilkinson', N'54375356', N'ligula@ipsumportaelit.edu', N'Quebec', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (99, N'742232676', N'Jared Rose', N'42327654', N'conubia@Uttincidunt.edu', N'OH', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (100, N'696218804', N'Melanie Burch', N'32177481', N'turpis@malesuada.com', N'British Columbia', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (101, N'483772942', N'Berk Bartlett', N'87354940', N'nec.enim@Nunc.edu', N'LD', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (102, N'659335304', N'Trevor Levy', N'70786472', N'dolor.elit@lectusjustoeu.net', N'Anambra', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (103, N'894430393', N'Kato Carney', N'90052031', N'velit.justo@urnanecluctus.ca', N'Antioquia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (104, N'236608356', N'Noel Sharpe', N'26398151', N'ipsum@Utsemper.net', N'MD', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (105, N'285447336', N'Rudyard Vance', N'36761113', N'velit.Pellentesque@sit.net', N'AB', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (106, N'252996259', N'Lamar Santiago', N'11976092', N'bibendum.Donec@acliberonec.edu', N'Kaduna', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (107, N'603164410', N'Salvador Herring', N'91067964', N'ut.dolor@dolorNulla.co.uk', N'Vlaams-Brabant', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (108, N'635283914', N'Anika Kinney', N'46393893', N'eros@nasceturridiculusmus.edu', N'C', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (109, N'320451829', N'Nasim Hayes', N'28272354', N'Cras.eu@Etiamligulatortor.net', N'Chiapas', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (110, N'520449488', N'Seth Noel', N'30367174', N'libero@sedpedenec.org', N'West Bengal', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (111, N'778298417', N'Eaton Underwood', N'56386573', N'a.enim@semper.edu', N'L', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (112, N'644466468', N'Vivien Wong', N'88830319', N'semper.dui.lectus@orcitincidunt.co.uk', N'UP', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (113, N'832815276', N'Kenyon Mcmillan', N'29023340', N'quam@euultrices.com', N'UMB', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (114, N'973830179', N'Fuller Crane', N'32218757', N'natoque.penatibus.et@CuraeDonectincidunt.org', N'Antioquia', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (115, N'217090884', N'Megan Hickman', N'10177271', N'pede.nonummy.ut@orci.org', N'Wie', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (116, N'486480302', N'Baxter Durham', N'70148326', N'Sed.pharetra@mauris.edu', N'SO', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (117, N'409183547', N'Stacy Mcneil', N'17881494', N'magna.Suspendisse@cursusluctus.net', N'Leinster', 0)
GO
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (118, N'726656297', N'Bevis Harrell', N'94402165', N'arcu.Nunc@lectusNullam.org', N'Friuli-Venezia Giulia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (120, N'1', N'Bbb', N'123', N'saas', N'asdasd', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (121, N'123456789', N'dsadsasad sdasda', N'4545545454', N'saasdadasd@sadsad.com', N'asdsadaasdsadsad', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (122, N'554454545', N'fddsffdssdsadds', N'54455445', N'sdasdsad@sad.com', N'asdasdasdsadas', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (123, N'12345677554', N'sadadfdsfdsdsf', N'5545454454', N'dsadads@sadas.com', N'asdasdad', 0)
SET IDENTITY_INSERT [dbo].[Clientes] OFF
SET IDENTITY_INSERT [dbo].[DetalleEntrega] ON 

INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (1, 7, 1, 1, 230, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (2, 7, 1, 1, 230, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (3, 8, 1, 1, 230, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (4, 9, 1, 5, 455454, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (5, 10, 6, 2, 123, 1)
SET IDENTITY_INSERT [dbo].[DetalleEntrega] OFF
SET IDENTITY_INSERT [dbo].[Entregas] ON 

INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (3, N'456464', CAST(N'2020-07-15' AS Date), 256, N'4545', 0)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (4, N'565', CAST(N'2020-07-18' AS Date), 566556, N'56656', 0)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (5, N'adads', CAST(N'2020-07-10' AS Date), 45455555, N'566556', 0)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (6, N'45455445', CAST(N'2020-07-24' AS Date), 4545, N'55555', 0)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (7, N'425454', CAST(N'2020-08-23' AS Date), 460, N'455445', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (8, N'4545', CAST(N'2020-08-23' AS Date), 230, N'4545', 0)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (9, N'fdsfdsfds', CAST(N'2020-08-25' AS Date), 455454, N'45545454', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (10, N'12345678', CAST(N'2020-08-25' AS Date), 123, N'12658952', 1)
SET IDENTITY_INSERT [dbo].[Entregas] OFF
SET IDENTITY_INSERT [dbo].[FichaTecnicaBotella] ON 

INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (2, 7, 5445, 55555, 45, 45, 5454, 45, NULL, NULL, 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (3, 10, 555, 55, 55, 55, 55, 55, NULL, NULL, 0)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (4, 3, 5, 5, 5, 5, 0, NULL, NULL, NULL, 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (5, 3, 545, 5, 45, 45, 454, 44545, NULL, NULL, 0)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (6, 5, 5, 5, 45, 454, 454, 5, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[FichaTecnicaBotella] OFF
SET IDENTITY_INSERT [dbo].[FichaTecnicaTapa] ON 

INSERT [dbo].[FichaTecnicaTapa] ([FichaTecnicaTapaID], [ProductoID], [PesoPieza], [Altura], [DiametroInterno], [DiametroExternoSelloOliva], [AlturaSelloOliva], [DiametroInternoBandaSeguridad], [EspesorPanel], [NumeroEstrias], [TorqueCierreRecomendado], [TorqueApertura], [Tolerancia], [Imagen], [Estado]) VALUES (3, 4, 54, 4, 5, 5, 5, 5, 5, 5, 54, 54, 454, NULL, 0)
INSERT [dbo].[FichaTecnicaTapa] ([FichaTecnicaTapaID], [ProductoID], [PesoPieza], [Altura], [DiametroInterno], [DiametroExternoSelloOliva], [AlturaSelloOliva], [DiametroInternoBandaSeguridad], [EspesorPanel], [NumeroEstrias], [TorqueCierreRecomendado], [TorqueApertura], [Tolerancia], [Imagen], [Estado]) VALUES (4, 16, 54, 454, 4554, 44545, 4545, 454, 4545, 545, 4545, 4545, 54554, NULL, 1)
SET IDENTITY_INSERT [dbo].[FichaTecnicaTapa] OFF
SET IDENTITY_INSERT [dbo].[Maquinas] ON 

INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (12, N'SOPLADORA01', N'Sopladora Semiautomatica', 1, N'Sopladora de color rojo', N'1100', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (13, N'SOPLADORA02', N'Sopladora manual', 1, N'Sopladora grande', N'1100', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (14, N'INYECTORA01', N'Inyectora Alajuela', 2, N'Inyectora Alajuela', N'14000', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (15, N'INYECTORA02', N'Inyectora heredia', 2, N'Inyectora Heredia', N'14000', 0)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (16, N'CORTADORA01', N'Cortadora alajuela', 1, N'Cortadora de tapass', N'2000', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (17, N'aaaa', N'bbbbbb', 2, N'sdffsf', N'454554', 0)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (18, N'asasdd', N'dfdsads', 1, N'asddaasd', N'4545', 0)
SET IDENTITY_INSERT [dbo].[Maquinas] OFF
SET IDENTITY_INSERT [dbo].[MateriaPrima] ON 

INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (4, N'PREF01', N'Preforma 410 10g R20mm', 5, 1, 1, 40000, 25.3600, 1200000.0000, N'Preforma nueva', 1)
INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (5, N'PREF02', N'Preforma 410 21g R24mm', 5, 1, 1, 20000, 30.0000, 600000.0000, N'Preforma nueva 24mm', 1)
INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (6, N'PREF03', N'Preforma 19.7 Transp x 15984', 5, 1, 1, 20000, 44.7100, 123456.0000, N'Preforma transp 19.7G', 1)
INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (7, N'PREF04', N'Preforma 23.7 Transp x 15984', 5, 1, 1, 20000, 39.1800, 987654.0000, N'Preforma transp 23.7G', 1)
INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (8, N'PREF04', N'Preforma PET 1881 17.6G Natural', 6, 2, 1, 2, 37.4400, 5445545.0000, N'Preforma 17.6g', 1)
INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (9, N'RESI01', N'HDPE HI-2053', 6, 2, 1, 2, 37.4400, 5445545.0000, N'Resina Polietileno Alta densidad', 1)
INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (10, N'aaaa', N'aaaaaaa', 6, 2, 2, 4554, 454.0000, 4545.0000, N'dsdasdd', 0)
SET IDENTITY_INSERT [dbo].[MateriaPrima] OFF
SET IDENTITY_INSERT [dbo].[MermaPorTurno] ON 

INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (1, 5, 1, 1, 0)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (2, 5, 1, 1, 0)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (3, 55, 1, 1, 1)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (14, 10, 12, 1, 0)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (15, 20, 11, 1, 1)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (16, 20, 8, 2, 1)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (17, 45, 12, 3, 0)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (18, 5, 13, 4, 0)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (19, 5, 1, 1, 0)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (20, 5, 1, 1, 0)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (21, 545, 10, 2, 0)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (22, 12, 33, 1, 1)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (23, 56, 33, 1, 0)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (24, 56, 33, 4, 0)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (28, 123, 34, 1, 1)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (29, 100, 35, 2, 1)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (30, 10, 33, 2, 0)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (31, 5, 39, 2, 0)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (32, 5, 39, 1, 1)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (33, 5, 39, 1, 1)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (34, 0, 40, 3, 1)
SET IDENTITY_INSERT [dbo].[MermaPorTurno] OFF
SET IDENTITY_INSERT [dbo].[Pedidos] ON 

INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (1, N'554554', 20, N'44', 3, 1014, 2, 455454, N'asaddsdsadsa', NULL, 45454545.0200, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (2, N'5456645', 34, N'66665', 5, 1015, 2, 55, N'asdasd', NULL, 7545.0000, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (3, N'54554', 40, N'4545455', 5, 1015, 2, 545454, N'fgggfgf', NULL, 45545.1200, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (4, N'455454', 20, N'544554', 3, 1015, 2, 4, N'xccxcxz', CAST(N'2020-08-19' AS Date), 54554.5445, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (5, N'454', 35, N'4554', 5, 1014, 2, 54, N'cxcx', CAST(N'2020-08-05' AS Date), 4554.5700, 0)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (6, N'455454', 21, N'45454', 3, 1015, 2, 4554, N'cxcxcx', CAST(N'2020-08-04' AS Date), 4554.5600, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (35, N'4565', 25, N'234567', 26, 1014, 1, 2, N'ghjgh', CAST(N'2020-08-20' AS Date), 9000.0000, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (36, N'6765', 36, N'42537384', 35, 1015, 2, 1, N'jckmfmdfngdf', CAST(N'2020-08-23' AS Date), 20000.0000, 0)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (38, N'111', 35, N'45121', 34, 1014, 2, 2, N'dsfdsf', CAST(N'2020-08-30' AS Date), 1000.0000, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (40, N'222', 47, N'5454564', 39, 1015, 5, 1, N'sdfds', CAST(N'2020-08-15' AS Date), 100000.0000, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (41, N'333', 85, N'745452', 30, 1015, 1, 2, N'tyhuyt', CAST(N'2020-08-20' AS Date), 452342.0000, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (43, N'444', 75, N'785458', 24, 1015, 5, 1, N'yhigu', CAST(N'2020-08-30' AS Date), 10.0000, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (44, N'5555', 39, N'4500', 30, 1014, 5, 2, N'prueba', CAST(N'2020-08-19' AS Date), 50000.0000, 0)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (47, N'111', 74, N'44254', 15, 1015, 1, 1, N'dfg', CAST(N'2020-08-25' AS Date), 20000.0000, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (50, N'542', 25, N'45745', 15, 1014, 5, 2, N'aeaeds', CAST(N'2020-08-27' AS Date), 2000000.0000, 1)
SET IDENTITY_INSERT [dbo].[Pedidos] OFF
SET IDENTITY_INSERT [dbo].[Produccion] ON 

INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (1, 1, CAST(N'2020-08-23T16:38:54.000' AS DateTime), CAST(N'2020-09-03T13:44:16.503' AS DateTime), N'10.21:05:22.5039199', 10, 231, 2, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (2, 3, CAST(N'2020-08-23T22:06:43.000' AS DateTime), NULL, NULL, 545, 0, 0, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (3, 6, CAST(N'2020-08-23T22:39:58.000' AS DateTime), NULL, NULL, 0, 0, 0, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (4, 5, CAST(N'2020-08-23T22:41:36.000' AS DateTime), NULL, NULL, 0, 0, 0, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (5, 4, CAST(N'2020-08-23T22:43:16.000' AS DateTime), NULL, NULL, 0, 0, 0, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (6, 38, CAST(N'2020-08-25T16:32:39.000' AS DateTime), CAST(N'2020-08-25T18:59:54.403' AS DateTime), N'02:27:15.4026381', 123, 9, 3, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (7, 40, CAST(N'2020-08-25T16:38:43.000' AS DateTime), CAST(N'2020-08-25T16:39:04.577' AS DateTime), N'00:00:21.5783109', 56, 5, 1, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (11, 1, CAST(N'2020-09-08T12:38:11.000' AS DateTime), NULL, N'0', 10, 0, 0, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (12, 47, CAST(N'2020-09-08T13:19:36.000' AS DateTime), CAST(N'2020-09-08T13:20:40.927' AS DateTime), N'00:01:04.9265131', 0, 10, 4, 1)
SET IDENTITY_INSERT [dbo].[Produccion] OFF
SET IDENTITY_INSERT [dbo].[ProduccionPaquetesCajas] ON 

INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (1, CAST(N'2020-08-23T16:39:34.000' AS DateTime), 230, 1, 1015, N'Mañana', 0)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (5, CAST(N'2020-08-25T16:39:04.577' AS DateTime), 5, 33, 1015, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (6, CAST(N'2020-08-25T16:42:36.430' AS DateTime), 2, 32, 1015, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (7, CAST(N'2020-08-25T17:57:31.453' AS DateTime), 2, 34, 1015, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (8, CAST(N'2020-08-25T18:59:54.403' AS DateTime), 5, 35, 1015, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (9, CAST(N'2020-09-03T13:44:16.503' AS DateTime), 1, 36, 1015, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (10, CAST(N'2020-09-08T13:20:02.187' AS DateTime), 5, 40, 1015, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (11, CAST(N'2020-09-08T13:20:40.927' AS DateTime), 5, 40, 1015, N'Mañana', 1)
SET IDENTITY_INSERT [dbo].[ProduccionPaquetesCajas] OFF
SET IDENTITY_INSERT [dbo].[Productos] ON 

INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (2, N'360MLGEN', N'360ml Generico', 1, N'Botella gen', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (3, N'500MLGEN', N'500ml Generico', 1, N'Botella gen', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (4, N'600MLGEN', N'600ml Generico', 1, N'Botella gen', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (5, N'360MLSPO', N'360ml Sport', 1, N'Botella sport', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (6, N'600MLSPO', N'600ml Sport', 1, N'Botella sport', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (7, N'500MLCUAD', N'500ml Cuadrado', 1, N'Botella cuadrada', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (8, N'600MLDP', N'600ml Cuadrado Dos Pinos', 1, N'Botella cuadrada dos pinos', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (9, N'1LTCUAD', N'1LT Cuadrado Dos Pinos', 1, N'Botella cuadrada dos pinos', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (10, N'TAP01', N'Tapa Negra', 2, N'Tapa negra', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (11, N'TAP02', N'Tapa Blanca', 2, N'Tapa blanca', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (12, N'TAP03', N'Tapa Naranja', 2, N'Tapa naranja', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (13, N'TAP04', N'Tapa Transparente', 2, N'Tapa transparente', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (14, N'wqewqew', N'qweqeqe', 2, N'qqqqqq', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (15, N'4454554', N'ghghgh', 2, N'jhhjhjjh', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (16, N'TAP01', N'Tapa Roja', 2, N'Nose', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (17, N'455445', N'sdasddsa', 1, N'sdssad', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (18, N'dsfsf', N'sdfsfd', 2, N'sdfsd', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (19, N'prub', N'sadad', 1, N'asddsaas', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (20, N'656', N'Botella Locales', 3, N'Lorem Ipsum es ', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (21, N'2', N'Botella Máximo', 2, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (22, N'3', N'áurea Botella', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (23, N'4', N'Resultado Botella', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (24, N'5', N'Botella Tempo', 1, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (25, N'6', N'Tapa Sucualentas', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (26, N'7', N'Tapa Timer', 2, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (27, N'8', N'Tapa Aluminios', 2, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (28, N'9', N'Tapa Flexión', 2, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (29, N'10456', N'Clásica Tapa', 2, N'Lorem Ipsum es simple', N'~/Archivos/Productos/36236401_822453698034397_3111091124572258304_n20205310364.jpg', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (30, N'11', N'óptima Tapa', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (31, N'12', N'Sepia Tapa', 2, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (32, N'13', N'Perfil Galon', 1, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (33, N'14', N'Galon Entornos', 2, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (34, N'15', N'Galon Fashionistas', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (35, N'16', N'Barrera Galon', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (36, N'17', N'Mútuos Galon', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (37, N'18', N'Galon Historia', 1, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (38, N'19', N'Galon Florales', 2, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (39, N'20', N'Galon Burbujas', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (40, N'4454554', N'noseas', 1, N'asdsadds', NULL, 1)
SET IDENTITY_INSERT [dbo].[Productos] OFF
SET IDENTITY_INSERT [dbo].[Proveedor] ON 

INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (5, N'PROV01', N'Polykon', N'253657985', N'servicio@polykon.com', N'Heredia', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (6, N'PROV02', N'Resinas', N'235685695', N'servicio@resinas.com', N'Alajuela', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (7, N'PROV03', N'EPP', N'82632568', N'eppclientes@hotmail.com', N'Alajuela', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (8, N'EMP04', N'Empaques plasticoss', N'5455455', N'empaquesplasticosclientes@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (9, N'ssssssssssssss', N'sssssssssss', N'455454', N'asddaads', N'asdsadsa', 0)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (10, N'44554', N'dsdsasasa', N'44554', N'asdffdsfds@sadas.com', N'adasdasd', 0)
SET IDENTITY_INSERT [dbo].[Proveedor] OFF
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([RoleID], [RoleName]) VALUES (1, N'Administrador')
INSERT [dbo].[Roles] ([RoleID], [RoleName]) VALUES (2, N'Empleado')
SET IDENTITY_INSERT [dbo].[Roles] OFF
SET IDENTITY_INSERT [dbo].[TipoMaquina] ON 

INSERT [dbo].[TipoMaquina] ([TipoMaquinaID], [Tipo]) VALUES (1, N'Sopladora')
INSERT [dbo].[TipoMaquina] ([TipoMaquinaID], [Tipo]) VALUES (2, N'Inyectora')
INSERT [dbo].[TipoMaquina] ([TipoMaquinaID], [Tipo]) VALUES (4, N'Otro')
SET IDENTITY_INSERT [dbo].[TipoMaquina] OFF
SET IDENTITY_INSERT [dbo].[TipoMateriaPrima] ON 

INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (1, N'Preforma')
INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (2, N'Resina')
INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (3, N'Colorante')
INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (4, N'Otro')
SET IDENTITY_INSERT [dbo].[TipoMateriaPrima] OFF
SET IDENTITY_INSERT [dbo].[TipoMerma] ON 

INSERT [dbo].[TipoMerma] ([TipoMermaID], [Tipo]) VALUES (1, N'Descuido operacional')
INSERT [dbo].[TipoMerma] ([TipoMermaID], [Tipo]) VALUES (2, N'Ajuste de máquina')
INSERT [dbo].[TipoMerma] ([TipoMermaID], [Tipo]) VALUES (3, N'Producto fuera especificación')
INSERT [dbo].[TipoMerma] ([TipoMermaID], [Tipo]) VALUES (4, N'Color fuera espeficicación')
SET IDENTITY_INSERT [dbo].[TipoMerma] OFF
SET IDENTITY_INSERT [dbo].[TipoProducto] ON 

INSERT [dbo].[TipoProducto] ([TipoProductoID], [Tipo]) VALUES (1, N'Botella')
INSERT [dbo].[TipoProducto] ([TipoProductoID], [Tipo]) VALUES (2, N'Tapa')
INSERT [dbo].[TipoProducto] ([TipoProductoID], [Tipo]) VALUES (3, N'Otro')
SET IDENTITY_INSERT [dbo].[TipoProducto] OFF
SET IDENTITY_INSERT [dbo].[UnidadMedida] ON 

INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (1, N'Unidades')
INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (2, N'Kilogramos')
INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (3, N'Tarimas')
INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (4, N'Cajas')
INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (5, N'Bolsas')
INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (6, N'Otro')
SET IDENTITY_INSERT [dbo].[UnidadMedida] OFF
INSERT [dbo].[UsersInRoles] ([User_UserID], [Role_RoleID]) VALUES (1014, 1)
INSERT [dbo].[UsersInRoles] ([User_UserID], [Role_RoleID]) VALUES (1015, 1)
INSERT [dbo].[UsersInRoles] ([User_UserID], [Role_RoleID]) VALUES (1027, 2)
SET IDENTITY_INSERT [dbo].[Usuarios] ON 

INSERT [dbo].[Usuarios] ([UsuarioID], [Identificacion], [UserName], [Password], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (1014, N'876543216', N'hansel', N'de4ad303211af5bd002b69d07ce48d1fd8b0b537f7fc34a285eca4d0dea6ac50', N'Hansel Murillo', N'875360236', N'hanse@hotmail.com', N'Heredia', 1)
INSERT [dbo].[Usuarios] ([UsuarioID], [Identificacion], [UserName], [Password], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (1015, N'123456789', N'andres', N'259b77456afddaeb7dc27ce6c5f92cb3cfc6815eadeba5720e5c68f8f82e1d0e', N'Andres Rodriguezz', N'86098503', N'andresrodrig10@hotmail.com', N'Santa', 1)
INSERT [dbo].[Usuarios] ([UsuarioID], [Identificacion], [UserName], [Password], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (1027, N'123456789', N'bryan', N'259b77456afddaeb7dc27ce6c5f92cb3cfc6815eadeba5720e5c68f8f82e1d0e', N'Bryan Montiel', N'883625698', N'bryan@hotmail.com', N'Alajuela', 1)
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
SET IDENTITY_INSERT [dbo].[VariablesMaquinaInyectora] ON 

INSERT [dbo].[VariablesMaquinaInyectora] ([VariablesMaquinaInyectoraID], [MaquinaID], [ProductoID], [MateriaPrimaPrincipalID], [MateriaPrimaSecundariaID], [MateriaPrimaTerciariaID], [CantidadPrimaPrincipal], [CantidadPrimaSecundaria], [CantidadPrimaTerciaria], [Estado]) VALUES (2, 15, 4, 9, NULL, NULL, 55555, NULL, NULL, 0)
INSERT [dbo].[VariablesMaquinaInyectora] ([VariablesMaquinaInyectoraID], [MaquinaID], [ProductoID], [MateriaPrimaPrincipalID], [MateriaPrimaSecundariaID], [MateriaPrimaTerciariaID], [CantidadPrimaPrincipal], [CantidadPrimaSecundaria], [CantidadPrimaTerciaria], [Estado]) VALUES (3, 15, 11, 9, NULL, NULL, 454, 4545, 4545, 0)
INSERT [dbo].[VariablesMaquinaInyectora] ([VariablesMaquinaInyectoraID], [MaquinaID], [ProductoID], [MateriaPrimaPrincipalID], [MateriaPrimaSecundariaID], [MateriaPrimaTerciariaID], [CantidadPrimaPrincipal], [CantidadPrimaSecundaria], [CantidadPrimaTerciaria], [Estado]) VALUES (4, 14, 16, 9, 9, NULL, 1, 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[VariablesMaquinaInyectora] OFF
SET IDENTITY_INSERT [dbo].[VariablesMaquinaSopladora] ON 

INSERT [dbo].[VariablesMaquinaSopladora] ([VariablesMaquinaSopladoraID], [MaquinaID], [ProductoID], [MateriaPrimaID], [RetardoPresoplado], [RetardoSoplado], [TiempoPresoplado], [TiempoRecuperacion], [TiempoSoplado], [TiempoEscape], [TiempoParoCadena], [TiempoSalidaPinzaPreforma], [FrecuenciaVentilador], [PresionEstirado], [Gancho], [Relay], [AnillosTornelas], [Estado], [Notas], [CantidadZonas], [CantidadBancos], [PosicionBanco1Zona1], [PosicionBanco1Zona2], [PosicionBanco1Zona3], [PosicionBanco1Zona4], [PosicionBanco2Zona1], [PosicionBanco2Zona2], [PosicionBanco2Zona3], [PosicionBanco2Zona4], [ValorBanco1Zona1], [ValorBanco1Zona2], [ValorBanco1Zona3], [ValorBanco1Zona4], [ValorBanco2Zona1], [ValorBanco2Zona2], [ValorBanco2Zona3], [ValorBanco2Zona4]) VALUES (2, 13, 6, 7, 4545, 45445445, 4554, 54, 4545, 45, 4545, 54545, 57, 55445, 1, 1, 1, 0, N'fdfdfd', 5, 5, N'5', N'5', N'5', N'5', N'5', N'5', N'5', N'5', 5, 5, 5, 5, 5, 5, 5, 5)
INSERT [dbo].[VariablesMaquinaSopladora] ([VariablesMaquinaSopladoraID], [MaquinaID], [ProductoID], [MateriaPrimaID], [RetardoPresoplado], [RetardoSoplado], [TiempoPresoplado], [TiempoRecuperacion], [TiempoSoplado], [TiempoEscape], [TiempoParoCadena], [TiempoSalidaPinzaPreforma], [FrecuenciaVentilador], [PresionEstirado], [Gancho], [Relay], [AnillosTornelas], [Estado], [Notas], [CantidadZonas], [CantidadBancos], [PosicionBanco1Zona1], [PosicionBanco1Zona2], [PosicionBanco1Zona3], [PosicionBanco1Zona4], [PosicionBanco2Zona1], [PosicionBanco2Zona2], [PosicionBanco2Zona3], [PosicionBanco2Zona4], [ValorBanco1Zona1], [ValorBanco1Zona2], [ValorBanco1Zona3], [ValorBanco1Zona4], [ValorBanco2Zona1], [ValorBanco2Zona2], [ValorBanco2Zona3], [ValorBanco2Zona4]) VALUES (3, 13, 3, 5, 25, 5, 55, 5, 5, 5, 5, 5, 55, 54, 1, 1, 0, 1, N'dffsd', 0, 0, N'5', NULL, N'5', NULL, NULL, NULL, NULL, NULL, 5, NULL, 5, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[VariablesMaquinaSopladora] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [UC_Usuarios]    Script Date: 8/9/2020 2:26:36 p. m. ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [UC_Usuarios] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AperturaCierre]  WITH CHECK ADD  CONSTRAINT [FK_AperturaCierre_PedidoID] FOREIGN KEY([PedidoID])
REFERENCES [dbo].[Pedidos] ([PedidoID])
GO
ALTER TABLE [dbo].[AperturaCierre] CHECK CONSTRAINT [FK_AperturaCierre_PedidoID]
GO
ALTER TABLE [dbo].[DetalleEntrega]  WITH CHECK ADD  CONSTRAINT [FK_DetalleEntrega_Entregas] FOREIGN KEY([EntregaID])
REFERENCES [dbo].[Entregas] ([EntregaID])
GO
ALTER TABLE [dbo].[DetalleEntrega] CHECK CONSTRAINT [FK_DetalleEntrega_Entregas]
GO
ALTER TABLE [dbo].[DetalleEntrega]  WITH CHECK ADD  CONSTRAINT [FK_DetalleEntrega_Produccion] FOREIGN KEY([ProduccionID])
REFERENCES [dbo].[Produccion] ([ProduccionID])
GO
ALTER TABLE [dbo].[DetalleEntrega] CHECK CONSTRAINT [FK_DetalleEntrega_Produccion]
GO
ALTER TABLE [dbo].[FichaTecnicaBotella]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_FichaTecnicaBotella] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[FichaTecnicaBotella] CHECK CONSTRAINT [FK_ProductoID_FichaTecnicaBotella]
GO
ALTER TABLE [dbo].[FichaTecnicaTapa]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_FichaTecnicaTapa] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[FichaTecnicaTapa] CHECK CONSTRAINT [FK_ProductoID_FichaTecnicaTapa]
GO
ALTER TABLE [dbo].[Maquinas]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_Maquinas] FOREIGN KEY([TipoMaquinaID])
REFERENCES [dbo].[TipoMaquina] ([TipoMaquinaID])
GO
ALTER TABLE [dbo].[Maquinas] CHECK CONSTRAINT [FK_TipoID_Maquinas]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_ProveedorID_MateriaPrima] FOREIGN KEY([ProveedorID])
REFERENCES [dbo].[Proveedor] ([ProveedorID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_ProveedorID_MateriaPrima]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_MateriaPrima] FOREIGN KEY([TipoMateriaPrimaID])
REFERENCES [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_TipoID_MateriaPrima]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_UnidadMedidaID_MateriaPrima] FOREIGN KEY([UnidadMedidaID])
REFERENCES [dbo].[UnidadMedida] ([UnidadMedidaID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_UnidadMedidaID_MateriaPrima]
GO
ALTER TABLE [dbo].[MermaPorTurno]  WITH CHECK ADD FOREIGN KEY([AperturaCierreID])
REFERENCES [dbo].[AperturaCierre] ([AperturaCierreID])
GO
ALTER TABLE [dbo].[MermaPorTurno]  WITH CHECK ADD FOREIGN KEY([TipoMermaID])
REFERENCES [dbo].[TipoMerma] ([TipoMermaID])
GO
ALTER TABLE [dbo].[Movimientos]  WITH CHECK ADD  CONSTRAINT [FK_EntregaID_Movimientos] FOREIGN KEY([EntregaID])
REFERENCES [dbo].[Entregas] ([EntregaID])
GO
ALTER TABLE [dbo].[Movimientos] CHECK CONSTRAINT [FK_EntregaID_Movimientos]
GO
ALTER TABLE [dbo].[Movimientos]  WITH CHECK ADD  CONSTRAINT [FK_PedidoID_Movimientos] FOREIGN KEY([PedidoID])
REFERENCES [dbo].[Pedidos] ([PedidoID])
GO
ALTER TABLE [dbo].[Movimientos] CHECK CONSTRAINT [FK_PedidoID_Movimientos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_ClienteID_Pedidos] FOREIGN KEY([ClienteID])
REFERENCES [dbo].[Clientes] ([ClienteID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_ClienteID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_Pedidos] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_ProductoID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_UnidadMedidaID_Pedidos] FOREIGN KEY([UnidadMedidaID])
REFERENCES [dbo].[UnidadMedida] ([UnidadMedidaID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_UnidadMedidaID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioID_Pedidos] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_UsuarioID_Pedidos]
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas]  WITH CHECK ADD  CONSTRAINT [FK_ProduccionPaquetesCajas_ProduccionPaquetesCajas] FOREIGN KEY([AperturaCierreID])
REFERENCES [dbo].[AperturaCierre] ([AperturaCierreID])
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas] CHECK CONSTRAINT [FK_ProduccionPaquetesCajas_ProduccionPaquetesCajas]
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas]  WITH CHECK ADD  CONSTRAINT [FK_ProduccionPaquetesCajas_Usuarios] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas] CHECK CONSTRAINT [FK_ProduccionPaquetesCajas_Usuarios]
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_Productos] FOREIGN KEY([TipoProductoID])
REFERENCES [dbo].[TipoProducto] ([TipoProductoID])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [FK_TipoID_Productos]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK_Role_RoleID_UserInRoles] FOREIGN KEY([Role_RoleID])
REFERENCES [dbo].[Roles] ([RoleID])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [FK_Role_RoleID_UserInRoles]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK_User_UserID_UserInRoles] FOREIGN KEY([User_UserID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [FK_User_UserID_UserInRoles]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MaquinaID_VariablesMaquinaInyectora] FOREIGN KEY([MaquinaID])
REFERENCES [dbo].[Maquinas] ([MaquinaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MaquinaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaPrincipalID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaPrincipalID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaPrincipalID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaSecundariaID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaSecundariaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaSecundariaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaTerciariaID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaTerciariaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaTerciariaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_VariablesMaquinaInyectora] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_ProductoID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_MaquinaID_VariablesMaquinaSopladora] FOREIGN KEY([MaquinaID])
REFERENCES [dbo].[Maquinas] ([MaquinaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_MaquinaID_VariablesMaquinaSopladora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaID_VariablesMaquinaSopladora] FOREIGN KEY([MateriaPrimaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_MateriaPrimaID_VariablesMaquinaSopladora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_VariablesMaquinaSopladora] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_ProductoID_VariablesMaquinaSopladora]
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientesCRUD]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[sp_ClientesCRUD]
@Accion int, 
@ClienteID int null,
@Identificacion varchar(max) null, 
@NombreCompleto varchar(max) null, 
@Telefono varchar(max) null, 
@Correo varchar(max) null, 
@Direccion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin
				INSERT INTO Clientes VALUES (@Identificacion, @NombreCompleto, @Telefono, @Correo, @Direccion, 1);			
			end
		else IF (@Accion = 2) 
			begin
				UPDATE Clientes set Identificacion = @Identificacion, NombreCompleto = @NombreCompleto, Telefono = @Telefono,
				Correo = @Correo, Direccion = @Direccion, Estado = 1 where ClienteID = @ClienteID
			end
		else  IF (@Accion = 3)
			begin
				UPDATE Clientes set Estado = 0 where ClienteID = @ClienteID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_DatosEtiqueta]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_DatosEtiqueta]
@Etiqueta int 

as
begin
		Select produccion.EtiquetaID, produccion.Fecha, produccion.Cantidad, produccion.Turno, usuario.NombreCompleto as Usuario,
		pedido.NumeroPedido, pedido.NumeroOrdenDeCompra, cliente.NombreCompleto as Cliente, cliente.Direccion, producto.Nombre as Producto,
		um.Unidad, maquina.Nombre as maquina

		from ProduccionPaquetesCajas as produccion
		join Usuarios as usuario on produccion.UsuarioID = usuario.UsuarioID
		join AperturaCierre as apertura on produccion.AperturaCierreID = apertura.AperturaCierreID
		join Pedidos as pedido on apertura.PedidoID = pedido.PedidoID
		join Clientes as cliente on pedido.ClienteID = cliente.ClienteID 
		join Productos as producto on pedido.ProductoID = producto.ProductoID		
		join UnidadMedida as um on pedido.UnidadMedidaID = um.UnidadMedidaID
		join Maquinas as maquina on apertura.MaquinaID = maquina.MaquinaID

		where EtiquetaID = @Etiqueta
end
GO
/****** Object:  StoredProcedure [dbo].[sp_EntregasCRUD]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_EntregasCRUD]
@Accion int, 
@EntregaID int null,
@NumeroEntrega varchar(max) null,
@Fecha date null,
@UnidadesEntregadas int null,
@NumeroFactura varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				INSERT INTO Entregas VALUES (@NumeroEntrega, @Fecha, @UnidadesEntregadas, @NumeroFactura, 1);			
			end
		else IF (@Accion = 2) 
			begin
				UPDATE Entregas set NumeroEntrega = @NumeroEntrega, Fecha = @Fecha, UnidadesEntregadas = @UnidadesEntregadas, 
				NumeroFactura = @NumeroFactura, Estado = 1 where EntregaID = @EntregaID
			end
		else  IF (@Accion = 3)
			begin
				UPDATE Entregas set Estado = 0 where EntregaID = @EntregaID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_FichaTecnicaBotellaCRUD]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_FichaTecnicaBotellaCRUD]
@Accion int, 
@FichaTecnicaBotellaID int null,
@ProductoID int null,
@Altura float null,
@DiametroMayor float null,
@DiametroMenor float null,
@DiametroCuello float null,
@CapacidadVolumetrica float null,
@Tolerancia float null,
@ImagenCuerpo varchar(max) null,
@ImagenFondo varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				INSERT INTO FichaTecnicaBotella
				VALUES (@ProductoID, @Altura, @DiametroMayor, @DiametroMenor,
				@DiametroCuello,@CapacidadVolumetrica,@Tolerancia, @ImagenCuerpo, @ImagenFondo ,1);			
			end
		else IF (@Accion = 2) 
			begin
				UPDATE FichaTecnicaBotella set ProductoID = @ProductoID, Altura = @Altura,
				DiametroMayor = @DiametroMayor, DiametroMenor = @DiametroMenor, DiametroCuello = @DiametroCuello, 
				CapacidadVolumetrica = @CapacidadVolumetrica,
				Tolerancia = @Tolerancia, ImagenCuerpo = @ImagenCuerpo, ImagenFondo = @ImagenFondo, Estado = 1 
				where FichaTecnicaBotellaID = @FichaTecnicaBotellaID
			end
		else  IF (@Accion = 3)
			begin
				UPDATE FichaTecnicaBotella set Estado = 0 where FichaTecnicaBotellaID = @FichaTecnicaBotellaID
			end
		/*else  IF (@Accion = 4)
			begin
				select * from FichaTecnicaBotella where Estado = 1;
			end
		else  IF (@Accion = 5)
			begin
				select * from FichaTecnicaBotella where FichaTecnicaBotellaID = @FichaTecnicaBotellaID;
			end*/
end
GO
/****** Object:  StoredProcedure [dbo].[sp_FichaTecnicaTapaCRUD]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_FichaTecnicaTapaCRUD]
@Accion int, 
@FichaTecnicaTapaID int null,
@ProductoID int null,
@PesoPieza float null,
@Altura float null,
@DiametroInterno float null,
@DiametroExternoSelloOliva float null,
@AlturaSelloOliva float null,
@DiametroInternoBandaSeguridad float null,
@EspesorPanel float null,
@NumeroEstrias float null,
@TorqueCierreRecomendado float null,
@TorqueApertura float null,
@Tolerancia float null,
@Imagen varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into FichaTecnicaTapa values (@ProductoID, @PesoPieza, @Altura, @DiametroInterno, @DiametroExternoSelloOliva, @AlturaSelloOliva,
				@DiametroInternoBandaSeguridad, @EspesorPanel, @NumeroEstrias, @TorqueCierreRecomendado, @TorqueApertura, @Tolerancia, @Imagen, 1);	
			end
		else IF (@Accion = 2) 
			begin
				update FichaTecnicaTapa set ProductoID = @ProductoID, PesoPieza = @PesoPieza, Altura = @Altura, DiametroInterno = @DiametroInterno,
				AlturaSelloOliva = @AlturaSelloOliva, DiametroInternoBandaSeguridad = @DiametroInternoBandaSeguridad, EspesorPanel = @EspesorPanel,
				NumeroEstrias = @NumeroEstrias, TorqueCierreRecomendado = @TorqueCierreRecomendado, TorqueApertura = @TorqueApertura, 
				Tolerancia = @Tolerancia, Imagen = @Imagen, Estado = 1 where FichaTecnicaTapaID = @FichaTecnicaTapaID
			end
		else  IF (@Accion = 3)
			begin
				update FichaTecnicaTapa set Estado = 0 where FichaTecnicaTapaID = @FichaTecnicaTapaID;
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_getRolesForUser]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*Procedimientos almacenados*/


create PROCEDURE [dbo].[sp_getRolesForUser]
	@userName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	select r.RoleName from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioID
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where u.UserName=@userName
END
GO
/****** Object:  StoredProcedure [dbo].[sp_getUsuariosRole]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_getUsuariosRole]
	
	@roleName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	select u.UserName from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioID
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where r.RoleName=@roleName 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GraficoParettoMerma]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_GraficoParettoMerma]
as
Select  TipoMerma.Tipo, sum(MermaPorTurno.Cantidad)as Cantidad
from MermaPorTurno
join TipoMerma on TipoMerma.TipoMermaID = MermaPorTurno.TipoMermaID

GROUP BY
TipoMerma.Tipo

order by cantidad desc
GO
/****** Object:  StoredProcedure [dbo].[sp_GraficoPedidos]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_GraficoPedidos] AS

select count(*) as PedidosProceso, (
select count(*) PedidosPendientes from PedidosActivos p 
 left join Produccion on p.PedidoID = Produccion.PedidoID where p.PedidoID not in (select PedidoID from Produccion))
 as PedidosPendientes
 ,(select count(*)

from PedidosActivos
 
join Produccion on PedidosActivos.PedidoID = Produccion.PedidoID
 
where PedidosActivos.PedidoID = Produccion.PedidoID and Produccion.CantidadProduccionTotal >= PedidosActivos.Cantidad
) as PedidosTerminados

from PedidosActivos
 
join Produccion on PedidosActivos.PedidoID = Produccion.PedidoID
 
where PedidosActivos.PedidoID = Produccion.PedidoID and Produccion.CantidadProduccionTotal >= 0 and Produccion.CantidadProduccionTotal < PedidosActivos.Cantidad


GO
/****** Object:  StoredProcedure [dbo].[sp_GraficoProduccion]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_GraficoProduccion] AS


SELECT DATEPART(month, Fecha) as mes, SUM(Cantidad) as total
FROM ProduccionPaquetesCajaActivos

GROUP BY DATEPART(month, Fecha)
GO
/****** Object:  StoredProcedure [dbo].[sp_isUserInRole]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_isUserInRole]
	@userName varchar(20),
	@roleName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	declare @resultado bit =0;
	if exists(
	select * from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioId
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where r.RoleName=@roleName and u.UserName=@userName)
	set @resultado = 1
	 select @resultado
END


GO
/****** Object:  StoredProcedure [dbo].[sp_MaquinasCRUD]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_MaquinasCRUD]
@Accion int, 
@MaquinaID int null,
@CodigoMaquina varchar(max) null,
@Nombre varchar(max) null,
@TipoMaquinaID int null,
@Descripcion varchar(max) null,
@ProduccionHora varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Maquinas values (@CodigoMaquina, @Nombre, @TipoMaquinaID, @Descripcion, @ProduccionHora, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Maquinas set CodigoMaquina = @CodigoMaquina, Nombre = @Nombre, TipoMaquinaID = @TipoMaquinaID, Descripcion = @Descripcion,
				ProduccionHora = @ProduccionHora, Estado = 1 where MaquinaID = @MaquinaID
			end
		else  IF (@Accion = 3)
			begin
				update Maquinas set Estado = 0 where MaquinaID = @MaquinaID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_MateriaPrimaCRUD]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_MateriaPrimaCRUD]
@Accion int, 
@MateriaPrimaID int null,
@CodigoMateriaPrima varchar(max) null,
@Nombre varchar(max) null,
@ProveedorID int null,
@TipoMateriaPrimaID int null,
@UnidadMedidaID int null,
@Cantidad int null,
@PrecioUnitario money null,
@PrecioTotal money null,
@Descripcion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into MateriaPrima values(@CodigoMateriaPrima, @Nombre, @ProveedorID, @TipoMateriaPrimaID, @UnidadMedidaID, @Cantidad,
				@PrecioUnitario, @PrecioTotal, @Descripcion, 1);
			end
		else IF (@Accion = 2) 
			begin
				update MateriaPrima set CodigoMateriaPrima = @CodigoMateriaPrima, Nombre = @Nombre, ProveedorID = @ProveedorID, TipoMateriaPrimaID 
				= @TipoMateriaPrimaID, UnidadMedidaID = @UnidadMedidaID, Cantidad = @Cantidad, PrecioUnitario = @PrecioUnitario, Descripcion = @Descripcion,
				Estado = 1
				where MateriaPrimaID = @MateriaPrimaID
			end
		else  IF (@Accion = 3)
			begin
				update MateriaPrima set Estado = 0 where MateriaPrimaID = @MateriaPrimaID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_PedidosCRUD]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_PedidosCRUD]
@Accion int, 
@PedidoID int null,
@NumeroPedido varchar(max) null,
@ClienteID int null,
@NumeroOrdenDeCompra varchar(max) null,
@ProductoID int null,
@UsuarioID int null,
@UnidadMedidaID int null,
@Cantidad int null,
@Descripcion varchar(max) null,
@Fecha date null,
@PrecioTotal money null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Pedidos values (@NumeroPedido, @ClienteID, @NumeroOrdenDeCompra, @ProductoID, @UsuarioID, @UnidadMedidaID, @Cantidad,
				@Descripcion, @Fecha, @PrecioTotal, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Pedidos set NumeroPedido = @NumeroPedido, ClienteID = @ClienteID, NumeroOrdenDeCompra = @NumeroOrdenDeCompra, ProductoID = @ProductoID,
				UsuarioID = @UsuarioID, UnidadMedidaID = @UnidadMedidaID, Cantidad = @Cantidad, Descripcion = @Descripcion, Fecha = @Fecha, PrecioTotal = @PrecioTotal,
				Estado = 1 where PedidoID = @PedidoID
			end
		else  IF (@Accion = 3)
			begin
				update Pedidos set Estado = 0 where PedidoID = @PedidoID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_ProduccionCRUD]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ProduccionCRUD]
@Accion int, 
@ProduccionID int null,
@PedidoID int null,
@FechaInicio date null,
@FechaFin date null,
@HorasTrabajadas date null,
@CantidadMerma float null,
@CantidadProduccionTotal int null,
@CantidadPaquetesCajas int null


as
begin
		IF (@Accion = 1)
			begin 
				insert into Produccion values (@PedidoID, @FechaInicio, @FechaFin, @HorasTrabajadas, @CantidadMerma, @CantidadProduccionTotal, @CantidadPaquetesCajas, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Produccion set PedidoID = @PedidoID, FechaInicio = @FechaInicio, FechaFin = @FechaFin, HorasTrabajadas = @HorasTrabajadas, 
				CantidadMerma = @CantidadMerma, CantidadProduccionTotal = @CantidadProduccionTotal, CantidadPaquetesCajas = @CantidadPaquetesCajas, Estado = 1
				where ProduccionID = @ProduccionID
			end
		else  IF (@Accion = 3)
			begin
				update Produccion set Estado = 0 where ProduccionID = @ProduccionID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_ProduccionPaquetesCajasCRUD]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ProduccionPaquetesCajasCRUD]
@Accion int, 
@EtiquetaID int null,
@Fecha date null,
@Cantidad int null,
@AperturaCierreID bigint null,
@UsuarioID int null,
@Turno varchar(max) null

as
begin
		IF (@Accion = 1)
			begin 
				insert into ProduccionPaquetesCajas values (@Fecha, @Cantidad,@AperturaCierreID, @UsuarioID, @Turno, 1);

			end
		else IF (@Accion = 2) 
			begin
				update ProduccionPaquetesCajas set Fecha = @Fecha, Cantidad = @Cantidad, AperturaCierreID = @AperturaCierreID, UsuarioID = @UsuarioID, Turno = @Turno, Estado = 1 where EtiquetaID = @EtiquetaID
			end
		else  IF (@Accion = 3)
			begin
				update ProduccionPaquetesCajas set Estado = 0 where EtiquetaID = @EtiquetaID;
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductosCRUD]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ProductosCRUD]
@Accion int, 
@ProductoID int null,
@CodigoProducto varchar(max) null,
@Nombre varchar(max) null,
@TipoProductoID int null,
@Descripcion varchar(max) null,
@Imagen varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Productos values(@CodigoProducto, @Nombre, @TipoProductoID, @Descripcion, @Imagen, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Productos set CodigoProducto = @CodigoProducto, Nombre = @Nombre, TipoProductoID = @TipoProductoID, Descripcion = @Descripcion,
				Imagen = @Imagen, Estado = 1 where ProductoID = @ProductoID
			end
		else  IF (@Accion = 3)
			begin
				update Productos set Estado = 0 where ProductoID = @ProductoID;
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_ProveedorCRUD]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ProveedorCRUD]
@Accion int, 
@ProveedorID int null,
@CodigoProveedor varchar(max) null,
@Nombre varchar(max) null,
@Telefono varchar(max) null,
@Correo varchar(max) null,
@Direccion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Proveedor values(@CodigoProveedor, @Nombre, @Telefono, @Correo, @Direccion, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Proveedor set CodigoProveedor = @CodigoProveedor, Nombre = @Nombre, Telefono = @Telefono, Correo = @Correo,
				Direccion = @Direccion, Estado = 1 where ProveedorID = @ProveedorID;
			end
		else  IF (@Accion = 3)
			begin
				update Proveedor set Estado = 0 where ProveedorID = @ProveedorID;
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_setSpecificRoleForUser]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_setSpecificRoleForUser]
	@UsuarioID int,
	@RoleID int
AS
BEGIN
	SET NOCOUNT ON;
	delete from UsersInRoles where User_UserID = @UsuarioID;
	insert into UsersInRoles values(@UsuarioID,@RoleID);
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UsuariosCRUD]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_UsuariosCRUD]
@Accion int, 
@UsuarioID int null,
@Identificacion varchar(max) null,
@UserName varchar(max) null,
@Password varchar(max) null,
@NombreCompleto varchar(max) null,
@Telefono varchar(max) null,
@Correo varchar(max) null,
@Direccion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Usuarios values(@Identificacion, @UserName, @Password, @NombreCompleto, @Telefono, @Correo, @Direccion,1);
			end
		else IF (@Accion = 2) 
			begin
				update Usuarios set Identificacion = @Identificacion, UserName = @UserName, Password = @Password, NombreCompleto = @NombreCompleto,
				Telefono = @Telefono, Correo = @Correo, Direccion = @Direccion, Estado = 1 where UsuarioID = @UsuarioID
			end
		else  IF (@Accion = 3)
			begin
				update Usuarios set Estado = 0 where UsuarioID = @UsuarioID;
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_VariablesMaquinaInyectoraCRUD]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_VariablesMaquinaInyectoraCRUD]
@Accion int, 
@VariablesMaquinaInyectoraID int null,
@MaquinaID int null,
@ProductoID int null,
@MateriaPrimaPrincipalID int null,
@MateriaPrimaSecundariaID int null,
@MateriaPrimaTerciariaID int null,
@CantidadPrimaPrincipal int null,
@CantidadPrimaSecundaria int null,
@CantidadPrimaTerciaria int null
as
begin
		IF (@Accion = 1)
			begin 
				insert into VariablesMaquinaInyectora values(@MaquinaID, @ProductoID, @MateriaPrimaPrincipalID, @MateriaPrimaSecundariaID,
				@MateriaPrimaTerciariaID, @CantidadPrimaPrincipal, @CantidadPrimaSecundaria, @CantidadPrimaTerciaria, 1);
			end
		else IF (@Accion = 2) 
			begin
				update VariablesMaquinaInyectora set MaquinaID = @MaquinaID, ProductoID = @ProductoID, MateriaPrimaPrincipalID = @MateriaPrimaPrincipalID,
				MateriaPrimaSecundariaID = @MateriaPrimaSecundariaID, MateriaPrimaTerciariaID = @MateriaPrimaTerciariaID, CantidadPrimaPrincipal =
				@CantidadPrimaPrincipal, CantidadPrimaSecundaria = @CantidadPrimaSecundaria, CantidadPrimaTerciaria = @CantidadPrimaTerciaria,
				Estado = 1
				where VariablesMaquinaInyectoraID = @VariablesMaquinaInyectoraID
			end
		else  IF (@Accion = 3)
			begin
				update VariablesMaquinaInyectora set Estado = 0 where VariablesMaquinaInyectoraID = @VariablesMaquinaInyectoraID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_VariablesMaquinaSopladoraCRUD]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_VariablesMaquinaSopladoraCRUD]
@Accion int, 
@VariablesMaquinaSopladoraID int null,
@MaquinaID int null,
@ProductoID int null,
@MateriaPrimaID int null,
@RetardoPresoplado int null,
@RetardoSoplado int null,
@TiempoPresoplado int null,
@TiempoRecuperacion int null,
@TiempoSoplado int null,
@TiempoEscape int null,
@TiempoParoCadena int null,
@TiempoSalidaPinzaPreforma int null,
@FrecuenciaVentilador float null,
@PresionEstirado float null,
@Gancho bit null,
@Relay bit null,
@AnillosTornelas bit null,
@Notas varchar(max) null,
@CantidadZonas int null,
@CantidadBancos int null,
@PosicionBanco1Zona1 varchar(max) null,
@PosicionBanco1Zona2 varchar(max) null,
@PosicionBanco1Zona3 varchar(max) null,
@PosicionBanco1Zona4 varchar(max) null,
@PosicionBanco2Zona1 varchar(max) null,
@PosicionBanco2Zona2 varchar(max) null,
@PosicionBanco2Zona3 varchar(max) null,
@PosicionBanco2Zona4 varchar(max) null,
@ValorBanco1Zona1 int null,
@ValorBanco1Zona2 int null,
@ValorBanco1Zona3 int null,
@ValorBanco1Zona4 int null,
@ValorBanco2Zona1 int null,
@ValorBanco2Zona2 int null,
@ValorBanco2Zona3 int null,
@ValorBanco2Zona4 int null
as
begin
		IF (@Accion = 1)
			begin 
				insert into VariablesMaquinaSopladora values(@MaquinaID,@ProductoID, @MateriaPrimaID, @RetardoPresoplado,
				@RetardoSoplado, @TiempoPresoplado, @TiempoRecuperacion, @TiempoSoplado, @TiempoEscape, @TiempoParoCadena,
				@TiempoSalidaPinzaPreforma, @FrecuenciaVentilador, @PresionEstirado, @Gancho, @Relay, @AnillosTornelas, 1,
				@Notas, @CantidadZonas, @CantidadBancos, @PosicionBanco1Zona1,@PosicionBanco1Zona2,@PosicionBanco1Zona3,@PosicionBanco1Zona4
				,@PosicionBanco2Zona1,@PosicionBanco2Zona2,@PosicionBanco2Zona3,@PosicionBanco2Zona4,@ValorBanco1Zona1,@ValorBanco1Zona2,
				@ValorBanco1Zona3,@ValorBanco1Zona4,@ValorBanco2Zona1,@ValorBanco2Zona2,@ValorBanco2Zona3,@ValorBanco2Zona4);
			end
		else IF (@Accion = 2) 
			begin
				update VariablesMaquinaSopladora set MaquinaID = @MaquinaID, ProductoID = @ProductoID, MateriaPrimaID = @MateriaPrimaID,
				RetardoPresoplado = @RetardoPresoplado, RetardoSoplado = @RetardoSoplado, TiempoPresoplado = @TiempoPresoplado, TiempoRecuperacion =
				@TiempoRecuperacion, TiempoSoplado = @TiempoSoplado, TiempoEscape = @TiempoEscape, TiempoParoCadena = @TiempoParoCadena,
				TiempoSalidaPinzaPreforma = @TiempoSalidaPinzaPreforma, FrecuenciaVentilador = @FrecuenciaVentilador, PresionEstirado = @PresionEstirado,
				Gancho = @Gancho, Relay = @Relay, AnillosTornelas = @AnillosTornelas, Estado = 1, Notas = @Notas, CantidadZonas = @CantidadZonas,
				CantidadBancos = @CantidadBancos, PosicionBanco1Zona1 = @PosicionBanco1Zona1, PosicionBanco1Zona2 = @PosicionBanco1Zona2,
				PosicionBanco1Zona3 = @PosicionBanco1Zona3, PosicionBanco1Zona4 = @PosicionBanco1Zona4, PosicionBanco2Zona1 = @PosicionBanco2Zona1,
				PosicionBanco2Zona2 = @PosicionBanco2Zona2, PosicionBanco2Zona3 = @PosicionBanco2Zona3, PosicionBanco2Zona4 = @PosicionBanco2Zona4,
				ValorBanco1Zona1 = @ValorBanco1Zona1, ValorBanco1Zona2 = @ValorBanco1Zona2, ValorBanco1Zona3 = @ValorBanco1Zona3, ValorBanco1Zona4 =
				@ValorBanco1Zona4, ValorBanco2Zona1 = @ValorBanco2Zona1, ValorBanco2Zona2 = @ValorBanco2Zona2, ValorBanco2Zona3 = @ValorBanco2Zona3,
				ValorBanco2Zona4 = @ValorBanco2Zona4 where VariablesMaquinaSopladoraID = @VariablesMaquinaSopladoraID
			end
		else  IF (@Accion = 3)
			begin
				update VariablesMaquinaSopladora set Estado = 0 where VariablesMaquinaSopladoraID = @VariablesMaquinaSopladoraID;
			end
end
GO
/****** Object:  Trigger [dbo].[TRG_INS_Produccion]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRG_INS_Produccion]
ON [dbo].[AperturaCierre]
AFTER INSERT
AS
BEGIN
		SET NOCOUNT ON;	
		declare @pedidoID int , @fechaApertura datetime;
		select @pedidoID = PedidoID, @fechaApertura = FechaApertura from inserted;
		insert into Produccion 
		([PedidoID]
						   ,[FechaInicio]
						   ,[FechaFin]
						   ,[HorasTrabajadas]
						   ,[CantidadMerma]
						   ,[CantidadProduccionTotal]
						   ,[CantidadPaquetesCajas]
						   ,[Estado])
		
		values (@pedidoID, @fechaApertura, null, 0,0,0,0,1);
		END
GO
ALTER TABLE [dbo].[AperturaCierre] ENABLE TRIGGER [TRG_INS_Produccion]
GO
/****** Object:  Trigger [dbo].[FK_Clientes]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*create procedure [dbo].[sp_ReportEntrega] as
select NumeroPedido, c.Nombre 'NombreCliente', NumeroOrdenDeCompra, pr.Nombre 'NombreProducto', CodigoEntrega, e.Fecha 'FechaEntrega', 
UnidadesEntregadas, NumeroFactura, p.Fecha 'FechaPedido', p.PrecioTotal from Movimientos m inner join Entregas e on m.EntregaID = m.EntregaID
inner join Pedidos p on p.PedidoID = m.PedidoID inner join Productos pr on p.ProductoID = pr.ProductoID inner join Clientes c on c.ClienteID = p.ClienteID
GO
create procedure [dbo].[sp_ReportPedido] as
select PedidoID, NumeroPedido, c.Nombre 'NombreCliente', NumeroOrdenDeCompra, pr.Nombre 'NombreProducto', NombreCompleto 'NombreUsuario',
un.Descripcion 'UnidadMedida', p.Cantidad, p.Descripcion, Fecha, p.PrecioTotal from Pedidos p 
inner join Clientes c on p.ClienteID = c.ClienteID
inner join Productos pr on p.ProductoID = pr.ProductoID 
inner join Usuarios u on u.UsuarioID = p.UsuarioID 
inner join UnidadMedida un on un.UnidadMedidaID = p.UnidadMedidaID
GO*/

/*
Triggers

*/

create trigger [dbo].[FK_Clientes]
on [dbo].[Clientes]
instead of delete
as
begin
set nocount on;
delete from Pedidos where ClienteID in (select ClienteID from deleted);
delete from Clientes where ClienteID in (select ClienteID from deleted);
end
GO
ALTER TABLE [dbo].[Clientes] ENABLE TRIGGER [FK_Clientes]
GO
/****** Object:  Trigger [dbo].[FK_Entregas]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_Entregas]
on [dbo].[Entregas]
instead of delete
as
begin
set nocount on;
delete from Movimientos where EntregaID in (select EntregaID from deleted);
delete from Entregas where EntregaID in (select EntregaID from deleted);
end
GO
ALTER TABLE [dbo].[Entregas] ENABLE TRIGGER [FK_Entregas]
GO
/****** Object:  Trigger [dbo].[FK_Maquinas]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_Maquinas]
on [dbo].[Maquinas]
instead of delete
as
begin
set nocount on;
delete from VariablesMaquinaSopladora where MaquinaID in (select MaquinaID from deleted);
delete from VariablesMaquinaInyectora where MaquinaID in (select MaquinaID from deleted);
delete from Maquinas where MaquinaID in (select MaquinaID from deleted);
end
GO
ALTER TABLE [dbo].[Maquinas] ENABLE TRIGGER [FK_Maquinas]
GO
/****** Object:  Trigger [dbo].[FK_MateriaPrima]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_MateriaPrima]
on [dbo].[MateriaPrima]
instead of delete
as
begin
set nocount on;
delete from VariablesMaquinaSopladora where MateriaPrimaID in (select MateriaPrimaID from deleted);
delete from VariablesMaquinaInyectora where MateriaPrimaPrincipalID in (select MateriaPrimaPrincipalID from deleted);
delete from VariablesMaquinaInyectora where MateriaPrimaSecundariaID in (select MateriaPrimaSecundariaID from deleted);
delete from VariablesMaquinaInyectora where MateriaPrimaTerciariaID in (select MateriaPrimaTerciariaID from deleted);
delete from MateriaPrima where MateriaPrimaID in (select MateriaPrimaID from deleted);
end
GO
ALTER TABLE [dbo].[MateriaPrima] ENABLE TRIGGER [FK_MateriaPrima]
GO
/****** Object:  Trigger [dbo].[TRG_INS_MermaProduccion]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRG_INS_MermaProduccion]
ON [dbo].[MermaPorTurno]
AFTER INSERT
AS
BEGIN
		SET NOCOUNT ON;	
		declare @aperturacierreID int, @pedidoID int, @cantidadNueva float, @cantidadMerma float, @cantidadTotal float;
		select @aperturacierreID = AperturaCierreID, @cantidadNueva = Cantidad from inserted;
		select @pedidoID = PedidoID from AperturaCierre where AperturaCierreID = @aperturacierreID;
		select @cantidadMerma = CantidadMerma from Produccion where PedidoID = @pedidoID;
		set @cantidadTotal = @cantidadMerma + @cantidadNueva;
			update Produccion set CantidadMerma = @cantidadTotal where PedidoID = @pedidoID;	
			END
GO
ALTER TABLE [dbo].[MermaPorTurno] ENABLE TRIGGER [TRG_INS_MermaProduccion]
GO
/****** Object:  Trigger [dbo].[FK_Pedidos]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_Pedidos]
on [dbo].[Pedidos]
instead of delete
as
begin
set nocount on;
delete from Movimientos where PedidoID in (select PedidoID from deleted);
delete from Pedidos where PedidoID in (select PedidoID from deleted);
end
GO
ALTER TABLE [dbo].[Pedidos] ENABLE TRIGGER [FK_Pedidos]
GO
/****** Object:  Trigger [dbo].[TRG_INS_ProduccionPaquetesCajas]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRG_INS_ProduccionPaquetesCajas]
ON [dbo].[ProduccionPaquetesCajas]
AFTER INSERT
AS
BEGIN
		SET NOCOUNT ON;	
		declare @aperturaCierreID int, @pedidoID int , @cantidadProduccion float, @cantidadPaquetesCajas int;
		select @aperturaCierreID = AperturaCierreID, @cantidadProduccion = Cantidad from inserted;
		select @pedidoID = PedidoID from AperturaCierre where AperturaCierreID = @aperturaCierreID;
		select @cantidadPaquetesCajas = CantidadPaquetesCajas from Produccion where PedidoID = @pedidoID;
		set @cantidadPaquetesCajas = @cantidadPaquetesCajas + 1;
		update Produccion set CantidadPaquetesCajas = @cantidadPaquetesCajas where PedidoID = @pedidoID;
		END
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas] ENABLE TRIGGER [TRG_INS_ProduccionPaquetesCajas]
GO
/****** Object:  Trigger [dbo].[FK_Productos]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_Productos]
on [dbo].[Productos]
instead of delete
as
begin
set nocount on;
delete from FichaTecnicaBotella where ProductoID in (select ProductoID from deleted);
delete from VariablesMaquinaSopladora where ProductoID in (select ProductoID from deleted);
delete from VariablesMaquinaInyectora where ProductoID in (select ProductoID from deleted);
delete from Pedidos where ProductoID in (select ProductoID from deleted);
delete from FichaTecnicaTapa where ProductoID in (select ProductoID from deleted);
delete from Productos where ProductoID in (select ProductoID from deleted);
end
GO
ALTER TABLE [dbo].[Productos] ENABLE TRIGGER [FK_Productos]
GO
/****** Object:  Trigger [dbo].[FK_Proveedor]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_Proveedor]
on [dbo].[Proveedor]
instead of delete
as
begin
set nocount on;
delete from MateriaPrima where MateriaPrimaID in (select MateriaPrimaID from deleted);
delete from Proveedor where ProveedorID in (select ProveedorID from deleted);
end
GO
ALTER TABLE [dbo].[Proveedor] ENABLE TRIGGER [FK_Proveedor]
GO
/****** Object:  Trigger [dbo].[FK_Roles]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_Roles]
on [dbo].[Roles]
instead of delete
as
begin
set nocount on;
delete from UsersInRoles where Role_RoleID in (select RoleID from deleted);
delete from Roles where RoleID in (select RoleID from deleted);
end
GO
ALTER TABLE [dbo].[Roles] ENABLE TRIGGER [FK_Roles]
GO
/****** Object:  Trigger [dbo].[FK_TipoMaquina]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_TipoMaquina]
on [dbo].[TipoMaquina]
instead of delete
as
begin
set nocount on;
delete from Maquinas where TipoMaquinaID in (select TipoMaquinaID from deleted);
delete from TipoMaquina where TipoMaquinaID in (select TipoMaquinaID from deleted);
end
GO
ALTER TABLE [dbo].[TipoMaquina] ENABLE TRIGGER [FK_TipoMaquina]
GO
/****** Object:  Trigger [dbo].[FK_TipoMateriaPrima]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_TipoMateriaPrima]
on [dbo].[TipoMateriaPrima]
instead of delete
as
begin
set nocount on;
delete from MateriaPrima where MateriaPrimaID in (select MateriaPrimaID from deleted);
delete from TipoMateriaPrima where TipoMateriaPrimaID in (select TipoMateriaPrimaID from deleted);
end
GO
ALTER TABLE [dbo].[TipoMateriaPrima] ENABLE TRIGGER [FK_TipoMateriaPrima]
GO
/****** Object:  Trigger [dbo].[FK_TipoProducto]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_TipoProducto]
on [dbo].[TipoProducto]
instead of delete
as
begin
set nocount on;
delete from Productos where TipoProductoID in (select TipoProductoID from deleted);
delete from TipoProducto where TipoProductoID in (select TipoProductoID from deleted);
end
GO
ALTER TABLE [dbo].[TipoProducto] ENABLE TRIGGER [FK_TipoProducto]
GO
/****** Object:  Trigger [dbo].[FK_UnidadMedida]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_UnidadMedida]
on [dbo].[UnidadMedida]
instead of delete
as
begin
set nocount on;
delete from Pedidos where UnidadMedidaID in (select UnidadMedidaID from deleted);
delete from MateriaPrima where UnidadMedidaID in (select UnidadMedidaID from deleted);
delete from UnidadMedida where UnidadMedidaID in (select UnidadMedidaID from deleted);
end
GO
ALTER TABLE [dbo].[UnidadMedida] ENABLE TRIGGER [FK_UnidadMedida]
GO
/****** Object:  Trigger [dbo].[FK_Usuarios]    Script Date: 8/9/2020 2:26:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_Usuarios]
on [dbo].[Usuarios]
instead of delete
as
begin
set nocount on;
delete from Pedidos where UsuarioID in (select UsuarioID from deleted);
delete from UsersInRoles where User_UserID in (select UsuarioID from deleted);
delete from ProduccionPaquetesCajas where UsuarioID in (select UsuarioID from deleted);
delete from Usuarios where UsuarioID in (select UsuarioID from deleted);
end
GO
ALTER TABLE [dbo].[Usuarios] ENABLE TRIGGER [FK_Usuarios]
GO
USE [master]
GO
ALTER DATABASE [DIPREMA] SET  READ_WRITE 
GO
