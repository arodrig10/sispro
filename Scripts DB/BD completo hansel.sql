
CREATE DATABASE [DIPREMA]

GO
USE [DIPREMA]
GO
/****** Object:  Table [dbo].[AperturaCierre]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AperturaCierre](
	[AperturaCierreID] [bigint] IDENTITY(1,1) NOT NULL,
	[FechaApertura] [datetime] NOT NULL,
	[FechaCierre] [datetime] NULL,
	[Turno] [varchar](50) NOT NULL,
	[PedidoID] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[MaquinaID] [int] NULL,
 CONSTRAINT [PK_AperturaCierre] PRIMARY KEY CLUSTERED 
(
	[AperturaCierreID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[AperturaCierreActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*Vistas*/
create VIEW [dbo].[AperturaCierreActivos] AS
select * from AperturaCierre where Estado = 1;
GO
/****** Object:  Table [dbo].[DetalleEntrega]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleEntrega](
	[DetelleEntregaID] [bigint] IDENTITY(1,1) NOT NULL,
	[EntregaID] [int] NOT NULL,
	[ProduccionID] [int] NOT NULL,
	[Paquetes] [int] NOT NULL,
	[Cantidad] [float] NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_DetalleEntrega] PRIMARY KEY CLUSTERED 
(
	[DetelleEntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Entregas]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entregas](
	[EntregaID] [int] IDENTITY(1,1) NOT NULL,
	[NumeroEntrega] [varchar](50) NOT NULL,
	[Fecha] [date] NULL,
	[UnidadesEntregadas] [int] NULL,
	[NumeroFactura] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pedidos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pedidos](
	[PedidoID] [int] IDENTITY(1,1) NOT NULL,
	[NumeroPedido] [varchar](50) NOT NULL,
	[ClienteID] [int] NOT NULL,
	[NumeroOrdenDeCompra] [varchar](50) NULL,
	[ProductoID] [int] NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[UnidadMedidaID] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Fecha] [date] NULL,
	[PrecioTotal] [money] NOT NULL,
	[Estado] [bit] NOT NULL,
	[EstadoID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PedidoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productos](
	[ProductoID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProducto] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[TipoProductoID] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Imagen] [varchar](max) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UnidadMedida]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnidadMedida](
	[UnidadMedidaID] [int] IDENTITY(1,1) NOT NULL,
	[Unidad] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UnidadMedidaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Produccion]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Produccion](
	[ProduccionID] [int] IDENTITY(1,1) NOT NULL,
	[PedidoID] [int] NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaFin] [datetime] NULL,
	[HorasTrabajadas] [varchar](100) NULL,
	[CantidadMerma] [float] NULL,
	[CantidadProduccionTotal] [float] NOT NULL,
	[CantidadPaquetesCajas] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProduccionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[DetalleEntregaActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create VIEW [dbo].[DetalleEntregaActivos] AS

Select Entregas.Fecha as FechaEntrega, Entregas.NumeroFactura as NumeroFactura ,Entregas.NumeroEntrega as NumeroEntrega,
		Pedidos.NumeroPedido as lote, Pedidos.NumeroOrdenDeCompra as NumeroOrdenCompra,
		Productos.Nombre as Producto,
		DetalleEntrega.Paquetes, DetalleEntrega.Cantidad,
		UnidadMedida.Unidad
from DetalleEntrega

join Entregas on DetalleEntrega.EntregaID = Entregas.EntregaID
join Produccion on  DetalleEntrega.ProduccionID = Produccion.ProduccionID
join Pedidos on Produccion.ProduccionID = Pedidos.PedidoID
join UnidadMedida on Pedidos.UnidadMedidaID = UnidadMedida.UnidadMedidaID
join Productos on Pedidos.ProductoID = Productos.ProductoID

where DetalleEntrega.Estado =1
GO
/****** Object:  Table [dbo].[MermaPorTurno]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MermaPorTurno](
	[MermaPorTurnoID] [int] IDENTITY(1,1) NOT NULL,
	[Cantidad] [float] NOT NULL,
	[AperturaCierreID] [bigint] NOT NULL,
	[TipoMermaID] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MermaPorTurnoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MermaPorTurnoActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[MermaPorTurnoActivos] as 
SELECT        *
FROM            MermaPorTurno
WHERE        Estado = 1
GO
/****** Object:  Table [dbo].[ProduccionPaquetesCajas]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProduccionPaquetesCajas](
	[EtiquetaID] [int] IDENTITY(1,1) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Cantidad] [float] NOT NULL,
	[AperturaCierreID] [bigint] NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[Turno] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_ProduccionPaquetesCajas] PRIMARY KEY CLUSTERED 
(
	[EtiquetaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ProduccionPaquetesCajaActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[ProduccionPaquetesCajaActivos] as
select * from ProduccionPaquetesCajas where Estado = 1;
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clientes](
	[ClienteID] [int] IDENTITY(1,1) NOT NULL,
	[Identificacion] [varchar](50) NOT NULL,
	[NombreCompleto] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ClienteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ClientesActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*Vistas*/
create VIEW [dbo].[ClientesActivos] AS
select * from Clientes where Estado = 1;
GO
/****** Object:  View [dbo].[EntregasActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create VIEW [dbo].[EntregasActivos] as
select * from Entregas where Estado = 1;
GO
/****** Object:  Table [dbo].[FichaTecnicaBotella]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FichaTecnicaBotella](
	[FichaTecnicaBotellaID] [int] IDENTITY(1,1) NOT NULL,
	[ProductoID] [int] NOT NULL,
	[Altura] [float] NOT NULL,
	[DiametroMayor] [float] NOT NULL,
	[DiametroMenor] [float] NOT NULL,
	[DiametroCuello] [float] NOT NULL,
	[CapacidadVolumetrica] [float] NOT NULL,
	[Tolerancia] [float] NULL,
	[ImagenCuerpo] [varchar](max) NULL,
	[ImagenFondo] [varchar](max) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FichaTecnicaBotellaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[FichaTecnicaBotellaActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[FichaTecnicaBotellaActivos] as
select * from FichaTecnicaBotella where Estado = 1;
GO
/****** Object:  View [dbo].[PedidosActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   view [dbo].[PedidosActivos] as
select * from Pedidos where Estado = 1;
GO
/****** Object:  View [dbo].[ProduccionActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[ProduccionActivos] as
select * from Produccion where Estado = 1;
GO
/****** Object:  View [dbo].[ClientesProduccion]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view 
[dbo].[ClientesProduccion] 
AS
select DISTINCT cliente.ClienteID
      ,cliente.Identificacion
      ,cliente.NombreCompleto
      ,cliente.Telefono
      ,cliente.Correo
      ,cliente.Direccion
      ,cliente.Estado
from ClientesActivos as cliente
join PedidosActivos as pedido on cliente.ClienteID= pedido.ClienteID
join  ProduccionActivos as produccion on pedido.PedidoID= produccion.PedidoID
GO
/****** Object:  Table [dbo].[FichaTecnicaTapa]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FichaTecnicaTapa](
	[FichaTecnicaTapaID] [int] IDENTITY(1,1) NOT NULL,
	[ProductoID] [int] NOT NULL,
	[PesoPieza] [float] NOT NULL,
	[Altura] [float] NOT NULL,
	[DiametroInterno] [float] NOT NULL,
	[DiametroExternoSelloOliva] [float] NOT NULL,
	[AlturaSelloOliva] [float] NOT NULL,
	[DiametroInternoBandaSeguridad] [float] NOT NULL,
	[EspesorPanel] [float] NOT NULL,
	[NumeroEstrias] [float] NOT NULL,
	[TorqueCierreRecomendado] [float] NOT NULL,
	[TorqueApertura] [float] NOT NULL,
	[Tolerancia] [float] NULL,
	[Imagen] [varchar](max) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FichaTecnicaTapaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[FichaTecnicaTapaActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[FichaTecnicaTapaActivos] as
select * from FichaTecnicaTapa where Estado = 1;
GO
/****** Object:  Table [dbo].[Maquinas]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Maquinas](
	[MaquinaID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoMaquina] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[TipoMaquinaID] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[ProduccionHora] [varchar](100) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaquinaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MaquinasActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MaquinasActivos] as
select * from Maquinas where Estado = 1;
GO
/****** Object:  Table [dbo].[MateriaPrima]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MateriaPrima](
	[MateriaPrimaID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoMateriaPrima] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[ProveedorID] [int] NOT NULL,
	[TipoMateriaPrimaID] [int] NOT NULL,
	[UnidadMedidaID] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[PrecioUnitario] [money] NOT NULL,
	[PrecioTotal] [money] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MateriaPrimaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MateriaPrimaActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MateriaPrimaActivos] as
select * from MateriaPrima where Estado = 1;
GO
/****** Object:  View [dbo].[ProductosActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProductosActivos] as
select * from Productos where Estado = 1;
GO
/****** Object:  Table [dbo].[Proveedor]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proveedor](
	[ProveedorID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProveedor] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProveedorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ProveedorActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProveedorActivos] as
select * from Proveedor where Estado = 1;
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[UsuarioID] [int] IDENTITY(1,1) NOT NULL,
	[Identificacion] [varchar](50) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [varchar](100) NOT NULL,
	[NombreCompleto] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UsuarioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[UsuariosActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[UsuariosActivos] as
select * from Usuarios where Estado = 1;
GO
/****** Object:  Table [dbo].[VariablesMaquinaInyectora]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VariablesMaquinaInyectora](
	[VariablesMaquinaInyectoraID] [int] IDENTITY(1,1) NOT NULL,
	[MaquinaID] [int] NOT NULL,
	[ProductoID] [int] NOT NULL,
	[MateriaPrimaPrincipalID] [int] NOT NULL,
	[MateriaPrimaSecundariaID] [int] NULL,
	[MateriaPrimaTerciariaID] [int] NULL,
	[CantidadPrimaPrincipal] [float] NOT NULL,
	[CantidadPrimaSecundaria] [float] NULL,
	[CantidadPrimaTerciaria] [float] NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[VariablesMaquinaInyectoraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VariablesMaquinaInyectoraActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VariablesMaquinaInyectoraActivos] as
select * from VariablesMaquinaInyectora where Estado = 1;
GO
/****** Object:  Table [dbo].[VariablesMaquinaSopladora]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VariablesMaquinaSopladora](
	[VariablesMaquinaSopladoraID] [int] IDENTITY(1,1) NOT NULL,
	[MaquinaID] [int] NOT NULL,
	[ProductoID] [int] NOT NULL,
	[MateriaPrimaID] [int] NOT NULL,
	[RetardoPresoplado] [int] NOT NULL,
	[RetardoSoplado] [int] NOT NULL,
	[TiempoPresoplado] [int] NOT NULL,
	[TiempoRecuperacion] [int] NOT NULL,
	[TiempoSoplado] [int] NOT NULL,
	[TiempoEscape] [int] NOT NULL,
	[TiempoParoCadena] [int] NOT NULL,
	[TiempoSalidaPinzaPreforma] [int] NOT NULL,
	[FrecuenciaVentilador] [float] NOT NULL,
	[PresionEstirado] [float] NOT NULL,
	[Gancho] [bit] NOT NULL,
	[Relay] [bit] NOT NULL,
	[AnillosTornelas] [bit] NOT NULL,
	[Estado] [bit] NOT NULL,
	[Notas] [varchar](100) NULL,
	[CantidadZonas] [int] NOT NULL,
	[CantidadBancos] [int] NOT NULL,
	[PosicionBanco1Zona1] [varchar](100) NULL,
	[PosicionBanco1Zona2] [varchar](100) NULL,
	[PosicionBanco1Zona3] [varchar](100) NULL,
	[PosicionBanco1Zona4] [varchar](100) NULL,
	[PosicionBanco2Zona1] [varchar](100) NULL,
	[PosicionBanco2Zona2] [varchar](100) NULL,
	[PosicionBanco2Zona3] [varchar](100) NULL,
	[PosicionBanco2Zona4] [varchar](100) NULL,
	[ValorBanco1Zona1] [int] NULL,
	[ValorBanco1Zona2] [int] NULL,
	[ValorBanco1Zona3] [int] NULL,
	[ValorBanco1Zona4] [int] NULL,
	[ValorBanco2Zona1] [int] NULL,
	[ValorBanco2Zona2] [int] NULL,
	[ValorBanco2Zona3] [int] NULL,
	[ValorBanco2Zona4] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[VariablesMaquinaSopladoraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VariablesMaquinaSopladoraActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VariablesMaquinaSopladoraActivos] as
select * from VariablesMaquinaSopladora where Estado = 1;
GO
/****** Object:  View [dbo].[MaquinasSopladoraActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MaquinasSopladoraActivos] as
select * from Maquinas where TipoMaquinaID = 1 and Estado = 1;
GO
/****** Object:  View [dbo].[MaquinasInyectoraActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MaquinasInyectoraActivos] as
select * from Maquinas where TipoMaquinaID = 2 and Estado = 1;
GO
/****** Object:  View [dbo].[MateriaPrimaInyectoraActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MateriaPrimaInyectoraActivos] as
select * from MateriaPrima where TipoMateriaPrimaID = 2 or TipoMateriaPrimaID = 3 and Estado = 1
GO
/****** Object:  View [dbo].[MateriaPrimaSopladoraActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MateriaPrimaSopladoraActivos] as
select * from MateriaPrima where TipoMateriaPrimaID = 1 and Estado = 1
GO
/****** Object:  View [dbo].[ProductosBotellaActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProductosBotellaActivos] as
select * from Productos where TipoProductoID = 1 and Estado = 1;
GO
/****** Object:  View [dbo].[ProductosTapaActivos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProductosTapaActivos] as
select * from Productos where TipoProductoID = 2 and Estado = 1;
GO
/****** Object:  Table [dbo].[Estados]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estados](
	[EstadoID] [int] IDENTITY(1,1) NOT NULL,
	[TipoEstado] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Estados] PRIMARY KEY CLUSTERED 
(
	[EstadoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Movimientos]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movimientos](
	[PedidoID] [int] NOT NULL,
	[EntregaID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Movimientos] PRIMARY KEY CLUSTERED 
(
	[PedidoID] ASC,
	[EntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoMaquina]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoMaquina](
	[TipoMaquinaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMaquinaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoMateriaPrima]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoMateriaPrima](
	[TipoMateriaPrimaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMateriaPrimaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoMerma]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoMerma](
	[TipoMermaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMermaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoProducto]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoProducto](
	[TipoProductoID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoProductoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsersInRoles]    Script Date: 12/11/2020 20:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersInRoles](
	[User_UserID] [int] NOT NULL,
	[Role_RoleID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.RoleUsers] PRIMARY KEY CLUSTERED 
(
	[Role_RoleID] ASC,
	[User_UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AperturaCierre] ON 

INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (26, CAST(N'2020-08-17T18:06:23.000' AS DateTime), CAST(N'2020-11-10T09:41:07.363' AS DateTime), N'Mañana', 1, 0, 1)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (27, CAST(N'2020-08-17T18:13:47.000' AS DateTime), CAST(N'2020-08-17T18:14:00.143' AS DateTime), N'Tarde', 1, 0, 1)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (28, CAST(N'2020-08-23T22:06:43.000' AS DateTime), CAST(N'2020-11-10T09:41:05.633' AS DateTime), N'Mañana', 3, 0, 1)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (29, CAST(N'2020-08-23T22:39:58.000' AS DateTime), CAST(N'2020-11-10T09:41:02.663' AS DateTime), N'Mañana', 6, 0, 1)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (30, CAST(N'2020-08-23T22:41:36.000' AS DateTime), CAST(N'2020-08-25T16:22:29.493' AS DateTime), N'Mañana', 5, 0, 1)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (31, CAST(N'2020-08-23T22:43:16.000' AS DateTime), CAST(N'2020-08-25T15:58:36.990' AS DateTime), N'Mañana', 4, 0, 1)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (40, CAST(N'2020-08-25T16:20:18.030' AS DateTime), CAST(N'2020-08-25T16:21:37.477' AS DateTime), N'Mañana', 10, 0, 1)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (41, CAST(N'2020-08-25T16:26:31.080' AS DateTime), CAST(N'2020-08-25T17:09:30.383' AS DateTime), N'Tarde', 10, 0, 1)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (42, CAST(N'2020-11-08T11:33:11.000' AS DateTime), CAST(N'2020-11-10T09:41:15.283' AS DateTime), N'Mañana', 2, 0, 1)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (43, CAST(N'2020-11-08T12:43:13.000' AS DateTime), CAST(N'2020-11-10T09:41:13.503' AS DateTime), N'Mañana', 4, 0, 1)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (44, CAST(N'2020-11-08T13:02:27.000' AS DateTime), CAST(N'2020-11-08T13:02:35.743' AS DateTime), N'Mañana', 5, 0, 1)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (45, CAST(N'2020-11-08T13:02:37.000' AS DateTime), CAST(N'2020-11-08T13:02:44.257' AS DateTime), N'Mañana', 5, 0, 1)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (10042, CAST(N'2020-11-08T13:26:51.000' AS DateTime), CAST(N'2020-11-08T13:28:28.470' AS DateTime), N'Mañana', 5, 0, 1)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (10043, CAST(N'2020-11-08T13:29:49.000' AS DateTime), CAST(N'2020-11-10T09:41:11.913' AS DateTime), N'Mañana', 10, 0, 1)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (10044, CAST(N'2020-11-08T17:29:13.000' AS DateTime), CAST(N'2020-11-10T09:41:09.057' AS DateTime), N'Tarde', 11, 0, 7)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (10045, CAST(N'2020-11-10T09:50:23.000' AS DateTime), NULL, N'Mañana', 11, 1, 1)
SET IDENTITY_INSERT [dbo].[AperturaCierre] OFF
SET IDENTITY_INSERT [dbo].[Clientes] ON 

INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (1, N'282575389', N'Rango Plastico', N'24389455', N'Rango@hotmail.com', N'Heredia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (2, N'645568872', N'Contínua Plastico', N'24400926', N'Contí@hotmail.com', N'Alajuela', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (3, N'508576043', N'Fidelity Plastico', N'24384634', N'Fidel@hotmail.com', N'Heredia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (4, N'386804833', N'Plastico Low', N'24406585', N'Plast@hotmail.com', N'Puntarenas', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (5, N'422603399', N'Anorak Plastico', N'24394613', N'Anora@hotmail.com', N'Limón', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (6, N'334420023', N'Plastico Sorpresas', N'24384475', N'Plast@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (7, N'140604326', N'Público Plastico', N'24392715', N'Públi@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (8, N'566148711', N'Puntos Plastico', N'24406595', N'Punto@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (9, N'435628159', N'Switch Plastico', N'24381348', N'Switc@hotmail.com', N'Guanacaste', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (10, N'264938824', N'Feliz Plastico', N'24392552', N'Feliz@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (11, N'499099817', N'Plastico Prevee', N'24402637', N'Plast@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (12, N'496153743', N'Plastico Cara', N'24392454', N'Plast@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (13, N'524384425', N'Plastico Analítica', N'24401237', N'Plast@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (14, N'545368129', N'Amarillos Plastico', N'24397413', N'Amari@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (15, N'573143265', N'Plastico Ejes', N'24406416', N'Plast@hotmail.com', N'Alajuela', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (16, N'390123888', N'Huellas Plastico', N'24402685', N'Huell@hotmail.com', N'Heredia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (17, N'668481211', N'Plastico Broadcast', N'24394099', N'Plast@hotmail.com', N'Puntarenas', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (18, N'284395720', N'Libre Plastico', N'24380950', N'Libre@hotmail.com', N'Puntarenas', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (19, N'643270140', N'Plastico Hydro', N'24409772', N'Plast@hotmail.com', N'Puntarenas', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (20, N'632802352', N'Plastico índices', N'24382488', N'Plast@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (21, N'28962378', N'Carlos luis', N'55656215615', N'carlos@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (22, N'288943878', N'Fernando Ruiz', N'2154523', N'fer@gmail.com', N'Heredia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (23, N'546455465', N'luis ulloa', N'56415', N'carlos@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (24, N'556y65', N'fgnbgfnb', N'94122141', N'humurillo66@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (25, N'822112', N'Carlos luis', N'56415', N'hmurillo66@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (26, N'28962378', N'Carlos luis', N'56415', N'humurillo66@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (27, N'423', N'sc', N'12445414', N'fer@gmail.com', N'Heredia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (28, N'412041204ytjgunj', N'Luis Carlos', N'86733698iu', N'yuihkmuy@dfhg.com', N'8ytujhhygth', 1)
SET IDENTITY_INSERT [dbo].[Clientes] OFF
SET IDENTITY_INSERT [dbo].[DetalleEntrega] ON 

INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (2, 4, 2, 1, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (8, 11, 2, 12, 12, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (13, 15, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (14, 16, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (15, 17, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (16, 17, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (17, 17, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (18, 17, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (26, 4, 2, 10, 5, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (27, 4, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (29, 4, 2, 9, 9, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (30, 4, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (33, 18, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (34, 19, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (35, 20, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (36, 20, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (37, 21, 2, 1, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (38, 21, 2, 2, 20, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (39, 22, 2, 2, 20, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (40, 22, 2, 1, 1, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (41, 22, 2, 20, 200, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (42, 22, 2, 300, 200, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (43, 23, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (44, 23, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (45, 24, 2, 90, 90, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (46, 24, 2, 100, 100, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (47, 25, 3, 10, 5, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (48, 25, 2, 35, 25, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (49, 4, 2, 10, 20, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (50, 26, 12, 2, 2000, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (51, 4, 1013, 1, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (52, 4, 1013, 1, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (53, 4, 1013, 1, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (54, 4, 1013, 1, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (55, 4, 1013, 1, 10, 1)
SET IDENTITY_INSERT [dbo].[DetalleEntrega] OFF
SET IDENTITY_INSERT [dbo].[Entregas] ON 

INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (4, N'1', CAST(N'2020-08-24' AS Date), 350, N'395', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (5, N'2', CAST(N'2020-08-20' AS Date), 24, N'13', 0)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (6, N'3', CAST(N'2020-08-20' AS Date), 12154, N'53132', 0)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (7, N'4', CAST(N'2020-08-20' AS Date), 20, N'45', 0)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (8, N'5', CAST(N'2020-08-20' AS Date), 125, N'12', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (9, N'6', CAST(N'2020-08-20' AS Date), 20, N'123', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (10, N'4', CAST(N'2020-08-20' AS Date), 300, N'123', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (11, N'78', CAST(N'2020-08-20' AS Date), 12, N'234', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (12, N'79', CAST(N'2020-08-20' AS Date), 20, N'2132', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (13, N'80', CAST(N'2020-08-20' AS Date), 10, N'13', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (14, N'7', CAST(N'2020-08-20' AS Date), 40, N'123', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (15, N'11', CAST(N'2020-08-20' AS Date), 10, N'10', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (16, N'12', CAST(N'2020-08-20' AS Date), 10, N'10', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (17, N'14', CAST(N'2020-08-20' AS Date), 40, N'10', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (18, N'81', CAST(N'2020-08-20' AS Date), 10, N'6567', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (19, N'82', CAST(N'2020-08-20' AS Date), 10, N'6567', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (20, N'83', CAST(N'2020-08-21' AS Date), 20, N'365', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (21, N'84', CAST(N'2020-08-21' AS Date), 30, N'84', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (22, N'90', CAST(N'2020-08-21' AS Date), 421, N'369', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (23, N'91', CAST(N'2020-08-21' AS Date), 20, N'10', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (24, N'93', CAST(N'2020-08-21' AS Date), 190, N'93', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (25, N'235', CAST(N'2020-08-24' AS Date), 30, N'350', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (26, N'99', CAST(N'2020-08-25' AS Date), 2000, N'350', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (27, N'12345', CAST(N'2020-11-12' AS Date), 0, N'12345', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (28, N'123456', CAST(N'2020-11-12' AS Date), 0, N'123456', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (29, N'1234567', CAST(N'2020-11-12' AS Date), 0, N'1234567', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (30, N'123', CAST(N'2020-11-12' AS Date), 0, N'1234567', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (31, N'1234567', CAST(N'2020-11-12' AS Date), 0, N'345', 1)
SET IDENTITY_INSERT [dbo].[Entregas] OFF
SET IDENTITY_INSERT [dbo].[Estados] ON 

INSERT [dbo].[Estados] ([EstadoID], [TipoEstado]) VALUES (1, N'Pendiente')
INSERT [dbo].[Estados] ([EstadoID], [TipoEstado]) VALUES (2, N'Proceso')
INSERT [dbo].[Estados] ([EstadoID], [TipoEstado]) VALUES (3, N'Terminado')
SET IDENTITY_INSERT [dbo].[Estados] OFF
SET IDENTITY_INSERT [dbo].[FichaTecnicaBotella] ON 

INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (16, 30, 11, 25, 22, 13, 26, 0.5, N'73680273', N'74863379', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (20, 34, 23, 23, 19, 20, 23, 0.5, N'52230002', N'75021434', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (21, 35, 10, 11, 6, 11, 29, 0.4, N'58586969', N'14197490', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (23, 30, 25, 17, 13, 17, 15, 0.5, N'81459375', N'26877001', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (27, 34, 26, 24, 22, 23, 29, 0.5, N'80205297', N'45251123', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (28, 35, 28, 13, 10, 29, 26, 0.4, N'54439195', N'53986735', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (29, 36, 26, 27, 22, 20, 14, 0.5, N'34803404', N'48324328', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (30, 37, 26, 21, 16, 14, 16, 0.5, N'69861186', N'39798943', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (31, 38, 13, 11, 10, 22, 29, 0.5, N'45074716', N'67884073', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (32, 39, 23, 30, 26, 25, 18, 0.1, N'79554269', N'77576660', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (33, 40, 22, 13, 9, 15, 24, 0.5, N'59113167', N'24717186', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (34, 41, 17, 27, 23, 17, 21, 0.5, N'22965977', N'43996248', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (35, 42, 14, 17, 15, 13, 17, 0.4, N'55742186', N'21787016', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (36, 43, 14, 19, 15, 23, 23, 0.5, N'9451288', N'27707205', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (37, 44, 12, 10, 9, 18, 23, 0.2, N'66849433', N'83818074', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (38, 45, 12, 13, 10, 22, 16, 0.4, N'8431082', N'69373844', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (39, 46, 17, 23, 18, 15, 29, 0.5, N'70788081', N'15061881', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (40, 47, 25, 19, 16, 27, 25, 0.5, N'52344137', N'13114634', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (41, 48, 10, 15, 13, 13, 12, 0.5, N'36019385', N'25474468', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (42, 49, 24, 13, 10, 18, 16, 0.2, N'78046888', N'74000258', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (43, 50, 27, 16, 11, 11, 19, 0.5, N'32107644', N'53749763', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (44, 51, 30, 24, 23, 17, 29, 0.2, N'67570843', N'7153989', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (45, 52, 19, 12, 10, 17, 29, 0.5, N'70064575', N'15553461', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (46, 41, 3, 2, 3, 2, 20, 3, N'113256', N'52125', 1)
SET IDENTITY_INSERT [dbo].[FichaTecnicaBotella] OFF
SET IDENTITY_INSERT [dbo].[FichaTecnicaTapa] ON 

INSERT [dbo].[FichaTecnicaTapa] ([FichaTecnicaTapaID], [ProductoID], [PesoPieza], [Altura], [DiametroInterno], [DiametroExternoSelloOliva], [AlturaSelloOliva], [DiametroInternoBandaSeguridad], [EspesorPanel], [NumeroEstrias], [TorqueCierreRecomendado], [TorqueApertura], [Tolerancia], [Imagen], [Estado]) VALUES (1, 41, 30, 52, 20, 13, 52, 20, 20, 30, 20, 10, 30, N'1512154231', 1)
SET IDENTITY_INSERT [dbo].[FichaTecnicaTapa] OFF
SET IDENTITY_INSERT [dbo].[Maquinas] ON 

INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (1, N'cod-01', N'Maria', 1, N'Maquina', N'12', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (2, N'cod-2', N'lucia', 2, N'máquina para trabajar preformas', N'12', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (3, N'cod-003', N'Pancha', 2, N'Sopaladora de una boquilla', N'12', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (4, N'cod-04', N'lola', 1, N'inyectora', N'23', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (5, N'34324', N'mar', 1, N'Inyectora nueva', N'122', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (6, N'123', N'cila', 1, N'vieja', N'67', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (7, N'67', N'betty', 2, N'Para tapas', N'56', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (8, N'9089', N'yui', 1, N'iny', N'34', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (9, N'u798', N'toyo', 1, N'Falta  ensamblar', N'234', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (10, N'989', N'prueba', 1, N'prueba', N'89', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (11, N'9087', N'pola', 2, N'China', N'13', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (12, N'y876', N'y789', 1, N'china', N'90', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (13, N'5678', N'yayao', 1, N'Alemana', N'678', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (14, N'878', N'Maria', 1, N'skdjvkv', N'6567', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (15, N'768', N'Maria 2', 1, N'ueuyew', N'78', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (16, N'98786', N'Maria', 1, N'gjf', N'678', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (17, N'83834', N'HP', 1, N'Tica', N'65', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (18, N'23', N'prueba', 2, N'Mensaje', N'47', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (19, N'768', N'prueba', 1, N'rttry', N'4', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (20, N'768', N'Maria', 1, N'rfdg', N'gfd', 0)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (21, N'768', N'Maria', 1, N'rfdg', N'gfd', 0)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (22, N'76876', N'jhgj', 1, N'fgjhfd', N'6787', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (23, N'rthb', N'Maria', 1, N'fdghbgd', N'54', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (24, N'rthb', N'Maria', 1, N'fdghbgd', N'54', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (25, N'5534', N'dfg', 1, N'dfsgd', N'5453', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (26, N'645', N'3465', 1, N'dfrg', N'gsdfgdf', 0)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (27, N'100000', N'gdfg', 1, N'dfg', N'dfg', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (28, N'999999', N'999999', 1, N'999999', N'9999999', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (29, N'prueba de mensaje', N'prueba', 1, N'Prueba de mensaje', N'12', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (30, N'768', N'fghgf', 1, N'hsdahdjuas', N'23', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (31, N'768', N'prueba', 1, N'999999', N'234', 1)
SET IDENTITY_INSERT [dbo].[Maquinas] OFF
SET IDENTITY_INSERT [dbo].[MateriaPrima] ON 

INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (1, N'43543', N'preforma', 1, 1, 2, 15, 2.0000, 30.0000, N'esto lo debe de realizar el sistema o bd Precio total', 1)
SET IDENTITY_INSERT [dbo].[MateriaPrima] OFF
SET IDENTITY_INSERT [dbo].[MermaPorTurno] ON 

INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (1, 10, 26, 1, 1)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (2, 20, 26, 1, 1)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (3, 20, 26, 2, 1)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (4, 45, 26, 3, 1)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (5, 5, 26, 4, 1)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (6, 100, 41, 2, 1)
INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (7, 100, 41, 3, 1)
SET IDENTITY_INSERT [dbo].[MermaPorTurno] OFF
SET IDENTITY_INSERT [dbo].[Pedidos] ON 

INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado], [EstadoID]) VALUES (1, N'4565', 17, N'234567', 44, 1, 1, 100, N'ghjgh', CAST(N'2020-08-20' AS Date), 9000.0000, 1, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado], [EstadoID]) VALUES (2, N'6765', 8, N'42537384', 42, 1, 1, 100, N'jckmfmdfngdf', CAST(N'2020-08-23' AS Date), 20000.0000, 0, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado], [EstadoID]) VALUES (3, N'111', 13, N'45121', 34, 2, 1, 10, N'dsfdsf', CAST(N'2020-08-30' AS Date), 1000.0000, 0, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado], [EstadoID]) VALUES (4, N'222', 17, N'5454564', 41, 1, 2, 1000, N'sdfds', CAST(N'2020-08-15' AS Date), 100000.0000, 1, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado], [EstadoID]) VALUES (5, N'333', 17, N'745452', 30, 1, 2, 452354, N'tyhuyt', CAST(N'2020-08-20' AS Date), 452342.0000, 1, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado], [EstadoID]) VALUES (6, N'444', 15, N'785458', 46, 1, 1, 1452452, N'yhigu', CAST(N'2020-08-30' AS Date), 10.0000, 1, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado], [EstadoID]) VALUES (7, N'5555', 1, N'4500', 30, 2, 2, 100, N'prueba', CAST(N'2020-08-19' AS Date), 50000.0000, 0, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado], [EstadoID]) VALUES (8, N'111', 15, N'44254', 47, 1, 2, 10, N'dfg', CAST(N'2020-08-25' AS Date), 20000.0000, 1, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado], [EstadoID]) VALUES (9, N'542', 10, N'45745', 44, 1, 1, 7527, N'aeaeds', CAST(N'2020-08-27' AS Date), 2000000.0000, 1, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado], [EstadoID]) VALUES (10, N'12345', 3, N'450987654', 38, 1, 2, 2000, N'Botella para fresco freskitop', CAST(N'2020-08-30' AS Date), 90000.0000, 1, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado], [EstadoID]) VALUES (11, N'110', 1, NULL, 40, 1, 2, 300, N'Tapa para botella intima agua 200ml', CAST(N'2020-11-26' AS Date), 500000.0000, 1, 2)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado], [EstadoID]) VALUES (12, N'1111', 1, N'450897654', 38, 1, 2, 100, N'Empacar en paquetes de 10 unds', CAST(N'2020-11-29' AS Date), 100000.0000, 1, 1)
SET IDENTITY_INSERT [dbo].[Pedidos] OFF
SET IDENTITY_INSERT [dbo].[Produccion] ON 

INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (2, 1, CAST(N'2020-08-17T18:06:23.000' AS DateTime), CAST(N'2020-07-24T07:14:37.000' AS DateTime), N'-24.10:51:46', 0, 2481, 14, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (3, 3, CAST(N'2020-08-23T22:06:43.000' AS DateTime), CAST(N'2020-08-23T22:07:36.000' AS DateTime), N'00:00:53', 0, 10, 1, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (4, 6, CAST(N'2020-08-23T22:39:58.000' AS DateTime), NULL, NULL, 0, 0, 0, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (5, 5, CAST(N'2020-08-23T22:41:36.000' AS DateTime), NULL, NULL, 0, 0, 0, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (6, 4, CAST(N'2020-08-23T22:43:16.000' AS DateTime), NULL, NULL, 0, 0, 0, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (7, 1, CAST(N'2020-09-17T18:06:23.000' AS DateTime), CAST(N'2020-07-24T07:14:37.000' AS DateTime), N'-55.10:51:46', 0, 2881, 59, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (8, 1, CAST(N'2020-10-17T18:06:23.000' AS DateTime), CAST(N'2020-07-24T07:14:37.000' AS DateTime), N'-85.10:51:46', 0, 2581, 59, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (9, 1, CAST(N'2020-11-17T18:06:23.000' AS DateTime), CAST(N'2020-07-24T07:14:37.000' AS DateTime), N'-116.10:51:46', 0, 2531, 59, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (10, 1, CAST(N'2020-12-17T18:06:23.000' AS DateTime), CAST(N'2020-07-24T07:14:37.000' AS DateTime), N'-146.10:51:46', 0, 2481, 59, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (11, 1, CAST(N'2020-07-17T18:06:23.000' AS DateTime), CAST(N'2020-07-24T07:14:37.000' AS DateTime), N'6.13:08:14', 0, 2486, 59, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (12, 10, CAST(N'2020-08-25T16:20:18.030' AS DateTime), CAST(N'2020-08-25T17:09:17.313' AS DateTime), N'00:48:59.2837803', 0, 2000, 2, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (13, 2, CAST(N'2020-11-08T11:33:11.000' AS DateTime), NULL, NULL, 0, 0, 0, 1)
INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (1013, 11, CAST(N'2020-11-08T17:29:13.000' AS DateTime), CAST(N'2020-11-08T22:04:14.100' AS DateTime), N'04:35:01.0992652', 0, 210, 16, 1)
SET IDENTITY_INSERT [dbo].[Produccion] OFF
SET IDENTITY_INSERT [dbo].[ProduccionPaquetesCajas] ON 

INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (21, CAST(N'2020-08-17T18:14:11.000' AS DateTime), 10, 26, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (22, CAST(N'2020-08-17T22:31:40.000' AS DateTime), 15, 26, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (23, CAST(N'2020-08-23T22:07:36.000' AS DateTime), 10, 28, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (24, CAST(N'2020-08-24T01:02:04.000' AS DateTime), 25, 26, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (25, CAST(N'2020-08-24T01:02:04.000' AS DateTime), 25, 26, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (26, CAST(N'2020-08-24T01:02:56.000' AS DateTime), 25, 26, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (27, CAST(N'2020-09-24T07:10:32.000' AS DateTime), 200, 26, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (28, CAST(N'2020-01-24T07:10:47.000' AS DateTime), 110, 26, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (29, CAST(N'2020-08-24T07:12:21.000' AS DateTime), 235, 26, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (30, CAST(N'2020-02-24T07:12:31.000' AS DateTime), 235, 26, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (31, CAST(N'2020-03-24T07:13:34.000' AS DateTime), 200, 26, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (32, CAST(N'2020-04-24T07:13:52.000' AS DateTime), 500, 26, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (33, CAST(N'2020-05-24T07:14:09.000' AS DateTime), 356, 26, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (34, CAST(N'2020-06-24T07:14:26.000' AS DateTime), 445, 26, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (35, CAST(N'2020-07-24T07:14:37.000' AS DateTime), 100, 26, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (38, CAST(N'2020-08-25T16:36:40.650' AS DateTime), 1000, 41, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (39, CAST(N'2020-08-25T17:09:17.313' AS DateTime), 1000, 41, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (40, CAST(N'2020-11-08T17:32:47.613' AS DateTime), 10, 10044, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (41, CAST(N'2020-11-08T17:34:36.513' AS DateTime), 10, 10044, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (42, CAST(N'2020-11-08T18:50:16.403' AS DateTime), 5, 10044, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (43, CAST(N'2020-11-08T18:52:51.827' AS DateTime), 5, 10044, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (44, CAST(N'2020-11-08T20:33:55.413' AS DateTime), 10, 10044, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (45, CAST(N'2020-11-08T21:13:49.783' AS DateTime), 20, 10044, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (46, CAST(N'2020-11-08T21:14:41.237' AS DateTime), 40, 10044, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (47, CAST(N'2020-11-08T21:30:38.967' AS DateTime), 10, 10044, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (48, CAST(N'2020-11-08T21:42:49.180' AS DateTime), 10, 10044, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (49, CAST(N'2020-11-08T21:44:49.287' AS DateTime), 30, 10044, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (50, CAST(N'2020-11-08T21:47:25.677' AS DateTime), 10, 10044, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (51, CAST(N'2020-11-08T21:53:31.870' AS DateTime), 10, 10044, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (52, CAST(N'2020-11-08T21:57:29.403' AS DateTime), 10, 10044, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (53, CAST(N'2020-11-08T22:00:10.780' AS DateTime), 10, 10044, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (54, CAST(N'2020-11-08T22:00:55.220' AS DateTime), 10, 10044, 1, N'Tarde', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (55, CAST(N'2020-11-08T22:04:14.100' AS DateTime), 10, 10044, 1, N'Tarde', 1)
SET IDENTITY_INSERT [dbo].[ProduccionPaquetesCajas] OFF
SET IDENTITY_INSERT [dbo].[Productos] ON 

INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (30, N'30', N'prueba', 2, N'descrp prueba', N'6756', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (34, N'656', N'Botella Locales', 3, N'Lorem Ipsum es ', N'~/Archivos/Productos/¡Gokú al ataque! Una particular escena donde se le ve combatiendo con cualquier personaje de la serie ¿Apoco no__20205433949.jfif', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (35, N'2', N'Botella Máximo', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (36, N'3', N'áurea Botella', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (37, N'4', N'Resultado Botella', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (38, N'5', N'Botella Tempo', 1, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (39, N'6', N'Tapa Sucualentas', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (40, N'7', N'Tapa Timer', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (41, N'8', N'Tapa Aluminios', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (42, N'9', N'Tapa Flexión', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (43, N'10', N'Clásica Tapa', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (44, N'11', N'óptima Tapa', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (45, N'12', N'Sepia Tapa', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (46, N'13', N'Perfil Galon', 1, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (47, N'14', N'Galon Entornos', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (48, N'15', N'Galon Fashionistas', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (49, N'16', N'Barrera Galon', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (50, N'17', N'Mútuos Galon', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (51, N'18', N'Galon Historia', 1, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (52, N'19', N'Galon Florales', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (53, N'20', N'Galon Burbujas', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (54, N'645654', N'frdfgd', 1, N'ryttg', N'54654654', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (55, N'1213', N'pruebaSemana10', 1, N'botella 10ml', N'987678', 1)
SET IDENTITY_INSERT [dbo].[Productos] OFF
SET IDENTITY_INSERT [dbo].[Proveedor] ON 

INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (1, N'1', N'3M', N'24389455', N'maeillanes@hotmail.com', N'Heredia', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (2, N'2', N'Abbott Vascular', N'24400926', N'osabarca@hotmail.com', N'Alajuela', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (3, N'3', N'Accenture', N'24384634', N'cabrigo@garmendia.cl', N'Heredia', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (4, N'4', N'Alimentos Prosalud', N'24406585', N'Sb.nashxo.sk8@hotmail.com', N'Puntarenas', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (5, N'5', N'APM Terminals', N'24394613', N'fran.afull@live.cl', N'Limón', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (6, N'6', N'Asesoría Nairí', N'24384475', N'carlosaguileram@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (7, N'7', N'AutoMercado', N'24392715', N'ikis_rojo@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (8, N'8', N'AVON', N'24406595', N'daniela_aguilera_m500@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (9, N'9', N'Azucarera El Viejo', N'24381348', N'vizkala@hotmail.com', N'Guanacaste', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (10, N'10', N'BAC Credomatic', N'24392552', N'alexus3@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (11, N'11', N'Baker Tilly', N'24402637', N'capitanaguilera@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (12, N'12', N'Banco BCT', N'24392454', N'apalamosg@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (13, N'13', N'Banco de Costa Rica', N'24401237', N'niikhox__@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (14, N'14', N'Banco Nacional de Costa Rica', N'24397413', N'luuuuuuci@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (15, N'15', N'Banco Popular y de Desarrollo Comunal', N'24406416', N'kristian_siempre_azul@hotmail.com', N'Alajuela', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (16, N'16', N'Batalla', N'24402685', N'mapuchin@hotmail.com', N'Heredia', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (17, N'17', N'BCR Corredora de Seguros', N'24394099', N'arahuetes@manquehue.net', N'Puntarenas', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (18, N'18', N'BCR Pensiones', N'24380950', N'eduardo.arancibia@grange.cl', N'Puntarenas', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (19, N'19', N'BCR Valores', N'24409772', N'martacam2002@yahoo.com', N'Puntarenas', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (20, N'20', N'BDS ASESORES', N'24382488', N'andrea.geoplanet@gmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (21, N'43543', N'EL gallo mas gallo', N'12445414', N'carlos@gmail.com', N'Limon', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (22, N'43543', N'Maria', N'2154523', N'hmurillo66@gmail.com', N'Heredia', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (23, N'45646', N'lsdlkfvds', N'6566', N'ghghjgjh', N'hjhjkh', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (24, N'43543', N'Maria', N'94122141', N'humurillo66@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (25, N'1532', N'U. Fide', N'56415', N'fide@gmail.com', N'Heredia', 1)
SET IDENTITY_INSERT [dbo].[Proveedor] OFF
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([RoleID], [RoleName]) VALUES (1, N'Administrador')
SET IDENTITY_INSERT [dbo].[Roles] OFF
SET IDENTITY_INSERT [dbo].[TipoMaquina] ON 

INSERT [dbo].[TipoMaquina] ([TipoMaquinaID], [Tipo]) VALUES (1, N'Inyectora')
INSERT [dbo].[TipoMaquina] ([TipoMaquinaID], [Tipo]) VALUES (2, N'Sopladora')
SET IDENTITY_INSERT [dbo].[TipoMaquina] OFF
SET IDENTITY_INSERT [dbo].[TipoMateriaPrima] ON 

INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (1, N'Resina Baja Densidad')
INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (2, N'Resina Alta Densidad')
INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (3, N'Pigmento')
INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (4, N'Colorante')
INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (5, N'Preformas')
SET IDENTITY_INSERT [dbo].[TipoMateriaPrima] OFF
SET IDENTITY_INSERT [dbo].[TipoMerma] ON 

INSERT [dbo].[TipoMerma] ([TipoMermaID], [Tipo]) VALUES (1, N'Descuido operacional')
INSERT [dbo].[TipoMerma] ([TipoMermaID], [Tipo]) VALUES (2, N'Ajuste de máquina')
INSERT [dbo].[TipoMerma] ([TipoMermaID], [Tipo]) VALUES (3, N'Producto fuera especificación')
INSERT [dbo].[TipoMerma] ([TipoMermaID], [Tipo]) VALUES (4, N'Color fuera espeficicación')
SET IDENTITY_INSERT [dbo].[TipoMerma] OFF
SET IDENTITY_INSERT [dbo].[TipoProducto] ON 

INSERT [dbo].[TipoProducto] ([TipoProductoID], [Tipo]) VALUES (1, N'Botella')
INSERT [dbo].[TipoProducto] ([TipoProductoID], [Tipo]) VALUES (2, N'Galon')
INSERT [dbo].[TipoProducto] ([TipoProductoID], [Tipo]) VALUES (3, N'Tapa')
SET IDENTITY_INSERT [dbo].[TipoProducto] OFF
SET IDENTITY_INSERT [dbo].[UnidadMedida] ON 

INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (1, N'Kilogramos')
INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (2, N'Unidades')
SET IDENTITY_INSERT [dbo].[UnidadMedida] OFF
INSERT [dbo].[UsersInRoles] ([User_UserID], [Role_RoleID]) VALUES (1, 1)
INSERT [dbo].[UsersInRoles] ([User_UserID], [Role_RoleID]) VALUES (2, 1)
SET IDENTITY_INSERT [dbo].[Usuarios] ON 

INSERT [dbo].[Usuarios] ([UsuarioID], [Identificacion], [UserName], [Password], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (1, N'112233445', N'hansel', N'26d6a8ad97c75ffc548f6873e5e93ce475479e3e1a1097381e54221fb53ec1d2', N'Hansel Murillo', N'88888888', N'hmuri@fide.com', N'Alajuela', 1)
INSERT [dbo].[Usuarios] ([UsuarioID], [Identificacion], [UserName], [Password], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (2, N'28962378', N'prueba', N'f2dd0b5be829d35a402aa6e9e8938b075d67d810ad2838e47e60d0cb5f96c5d0', N'Jhon salchichon', N'88888888', N'hmurillo66@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Usuarios] ([UsuarioID], [Identificacion], [UserName], [Password], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (3, N'822112', N'andresCarePez', N'e9b32be7b2985cdd46d2d9318aa95a3376c1c9865ff6771629729f605c38033b', N'Andres Rodriguez Alfaro', N'94122141', N'arodrig10@gmail.com', N'Heredia', 1)
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
SET IDENTITY_INSERT [dbo].[VariablesMaquinaInyectora] ON 

INSERT [dbo].[VariablesMaquinaInyectora] ([VariablesMaquinaInyectoraID], [MaquinaID], [ProductoID], [MateriaPrimaPrincipalID], [MateriaPrimaSecundariaID], [MateriaPrimaTerciariaID], [CantidadPrimaPrincipal], [CantidadPrimaSecundaria], [CantidadPrimaTerciaria], [Estado]) VALUES (1, 14, 51, 1, 1, NULL, 500, 2000, 3000, 1)
INSERT [dbo].[VariablesMaquinaInyectora] ([VariablesMaquinaInyectoraID], [MaquinaID], [ProductoID], [MateriaPrimaPrincipalID], [MateriaPrimaSecundariaID], [MateriaPrimaTerciariaID], [CantidadPrimaPrincipal], [CantidadPrimaSecundaria], [CantidadPrimaTerciaria], [Estado]) VALUES (3, 14, 44, 1, 1, 1, 20, 30, 0, 1)
SET IDENTITY_INSERT [dbo].[VariablesMaquinaInyectora] OFF
SET IDENTITY_INSERT [dbo].[VariablesMaquinaSopladora] ON 

INSERT [dbo].[VariablesMaquinaSopladora] ([VariablesMaquinaSopladoraID], [MaquinaID], [ProductoID], [MateriaPrimaID], [RetardoPresoplado], [RetardoSoplado], [TiempoPresoplado], [TiempoRecuperacion], [TiempoSoplado], [TiempoEscape], [TiempoParoCadena], [TiempoSalidaPinzaPreforma], [FrecuenciaVentilador], [PresionEstirado], [Gancho], [Relay], [AnillosTornelas], [Estado], [Notas], [CantidadZonas], [CantidadBancos], [PosicionBanco1Zona1], [PosicionBanco1Zona2], [PosicionBanco1Zona3], [PosicionBanco1Zona4], [PosicionBanco2Zona1], [PosicionBanco2Zona2], [PosicionBanco2Zona3], [PosicionBanco2Zona4], [ValorBanco1Zona1], [ValorBanco1Zona2], [ValorBanco1Zona3], [ValorBanco1Zona4], [ValorBanco2Zona1], [ValorBanco2Zona2], [ValorBanco2Zona3], [ValorBanco2Zona4]) VALUES (1, 15, 50, 1, 20, 30, 20, 23, 23, 654, 45, 456, 45, 415, 1, 0, 1, 1, N'bajar velocidad', 1, 1, N'1', N'3', N'2', N'2', N'3', N'5', N'3', N'2', 2, 3, 2, 1, 33, 55, 23, 23)
INSERT [dbo].[VariablesMaquinaSopladora] ([VariablesMaquinaSopladoraID], [MaquinaID], [ProductoID], [MateriaPrimaID], [RetardoPresoplado], [RetardoSoplado], [TiempoPresoplado], [TiempoRecuperacion], [TiempoSoplado], [TiempoEscape], [TiempoParoCadena], [TiempoSalidaPinzaPreforma], [FrecuenciaVentilador], [PresionEstirado], [Gancho], [Relay], [AnillosTornelas], [Estado], [Notas], [CantidadZonas], [CantidadBancos], [PosicionBanco1Zona1], [PosicionBanco1Zona2], [PosicionBanco1Zona3], [PosicionBanco1Zona4], [PosicionBanco2Zona1], [PosicionBanco2Zona2], [PosicionBanco2Zona3], [PosicionBanco2Zona4], [ValorBanco1Zona1], [ValorBanco1Zona2], [ValorBanco1Zona3], [ValorBanco1Zona4], [ValorBanco2Zona1], [ValorBanco2Zona2], [ValorBanco2Zona3], [ValorBanco2Zona4]) VALUES (2, 10, 34, 1, 20, 20, 30, 20, 30, 20, 20, 30, 20, 30, 1, 0, 1, 1, N'bajar velocidad', 1, 1, N'5', N'5', N'3', N'6', N'26', N'256', N'8', N'52', 30, 61, 512, 20, 20, 952, 30, 20)
SET IDENTITY_INSERT [dbo].[VariablesMaquinaSopladora] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [UC_Usuarios]    Script Date: 12/11/2020 20:44:28 ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [UC_Usuarios] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AperturaCierre] ADD  CONSTRAINT [DF_AperturaCierre_estado]  DEFAULT ((1)) FOR [Estado]
GO
ALTER TABLE [dbo].[DetalleEntrega] ADD  CONSTRAINT [DF_DetalleEntrega_Detalle]  DEFAULT ((1)) FOR [Estado]
GO
ALTER TABLE [dbo].[Entregas] ADD  CONSTRAINT [DF_Entregas_Estado]  DEFAULT ((1)) FOR [Estado]
GO
ALTER TABLE [dbo].[Pedidos] ADD  CONSTRAINT [DF_Pedidos_EstadoID]  DEFAULT ((1)) FOR [EstadoID]
GO
ALTER TABLE [dbo].[AperturaCierre]  WITH CHECK ADD  CONSTRAINT [FK_AperturaCierre_PedidoID] FOREIGN KEY([PedidoID])
REFERENCES [dbo].[Pedidos] ([PedidoID])
GO
ALTER TABLE [dbo].[AperturaCierre] CHECK CONSTRAINT [FK_AperturaCierre_PedidoID]
GO
ALTER TABLE [dbo].[DetalleEntrega]  WITH CHECK ADD  CONSTRAINT [FK_DetalleEntrega_Entregas] FOREIGN KEY([EntregaID])
REFERENCES [dbo].[Entregas] ([EntregaID])
GO
ALTER TABLE [dbo].[DetalleEntrega] CHECK CONSTRAINT [FK_DetalleEntrega_Entregas]
GO
ALTER TABLE [dbo].[DetalleEntrega]  WITH CHECK ADD  CONSTRAINT [FK_DetalleEntrega_Produccion] FOREIGN KEY([ProduccionID])
REFERENCES [dbo].[Produccion] ([ProduccionID])
GO
ALTER TABLE [dbo].[DetalleEntrega] CHECK CONSTRAINT [FK_DetalleEntrega_Produccion]
GO
ALTER TABLE [dbo].[FichaTecnicaBotella]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_FichaTecnicaBotella] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[FichaTecnicaBotella] CHECK CONSTRAINT [FK_ProductoID_FichaTecnicaBotella]
GO
ALTER TABLE [dbo].[FichaTecnicaTapa]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_FichaTecnicaTapa] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[FichaTecnicaTapa] CHECK CONSTRAINT [FK_ProductoID_FichaTecnicaTapa]
GO
ALTER TABLE [dbo].[Maquinas]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_Maquinas] FOREIGN KEY([TipoMaquinaID])
REFERENCES [dbo].[TipoMaquina] ([TipoMaquinaID])
GO
ALTER TABLE [dbo].[Maquinas] CHECK CONSTRAINT [FK_TipoID_Maquinas]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_ProveedorID_MateriaPrima] FOREIGN KEY([ProveedorID])
REFERENCES [dbo].[Proveedor] ([ProveedorID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_ProveedorID_MateriaPrima]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_MateriaPrima] FOREIGN KEY([TipoMateriaPrimaID])
REFERENCES [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_TipoID_MateriaPrima]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_UnidadMedidaID_MateriaPrima] FOREIGN KEY([UnidadMedidaID])
REFERENCES [dbo].[UnidadMedida] ([UnidadMedidaID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_UnidadMedidaID_MateriaPrima]
GO
ALTER TABLE [dbo].[MermaPorTurno]  WITH CHECK ADD FOREIGN KEY([AperturaCierreID])
REFERENCES [dbo].[AperturaCierre] ([AperturaCierreID])
GO
ALTER TABLE [dbo].[MermaPorTurno]  WITH CHECK ADD FOREIGN KEY([TipoMermaID])
REFERENCES [dbo].[TipoMerma] ([TipoMermaID])
GO
ALTER TABLE [dbo].[Movimientos]  WITH CHECK ADD  CONSTRAINT [FK_EntregaID_Movimientos] FOREIGN KEY([EntregaID])
REFERENCES [dbo].[Entregas] ([EntregaID])
GO
ALTER TABLE [dbo].[Movimientos] CHECK CONSTRAINT [FK_EntregaID_Movimientos]
GO
ALTER TABLE [dbo].[Movimientos]  WITH CHECK ADD  CONSTRAINT [FK_PedidoID_Movimientos] FOREIGN KEY([PedidoID])
REFERENCES [dbo].[Pedidos] ([PedidoID])
GO
ALTER TABLE [dbo].[Movimientos] CHECK CONSTRAINT [FK_PedidoID_Movimientos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_ClienteID_Pedidos] FOREIGN KEY([ClienteID])
REFERENCES [dbo].[Clientes] ([ClienteID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_ClienteID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_Pedidos_Estados] FOREIGN KEY([EstadoID])
REFERENCES [dbo].[Estados] ([EstadoID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_Pedidos_Estados]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_Pedidos] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_ProductoID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_UnidadMedidaID_Pedidos] FOREIGN KEY([UnidadMedidaID])
REFERENCES [dbo].[UnidadMedida] ([UnidadMedidaID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_UnidadMedidaID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioID_Pedidos] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_UsuarioID_Pedidos]
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas]  WITH CHECK ADD  CONSTRAINT [FK_ProduccionPaquetesCajas_ProduccionPaquetesCajas] FOREIGN KEY([AperturaCierreID])
REFERENCES [dbo].[AperturaCierre] ([AperturaCierreID])
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas] CHECK CONSTRAINT [FK_ProduccionPaquetesCajas_ProduccionPaquetesCajas]
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas]  WITH CHECK ADD  CONSTRAINT [FK_ProduccionPaquetesCajas_Usuarios] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas] CHECK CONSTRAINT [FK_ProduccionPaquetesCajas_Usuarios]
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_Productos] FOREIGN KEY([TipoProductoID])
REFERENCES [dbo].[TipoProducto] ([TipoProductoID])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [FK_TipoID_Productos]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK_Role_RoleID_UserInRoles] FOREIGN KEY([Role_RoleID])
REFERENCES [dbo].[Roles] ([RoleID])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [FK_Role_RoleID_UserInRoles]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK_User_UserID_UserInRoles] FOREIGN KEY([User_UserID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [FK_User_UserID_UserInRoles]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MaquinaID_VariablesMaquinaInyectora] FOREIGN KEY([MaquinaID])
REFERENCES [dbo].[Maquinas] ([MaquinaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MaquinaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaPrincipalID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaPrincipalID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaPrincipalID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaSecundariaID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaSecundariaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaSecundariaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaTerciariaID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaTerciariaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaTerciariaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_VariablesMaquinaInyectora] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_ProductoID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_MaquinaID_VariablesMaquinaSopladora] FOREIGN KEY([MaquinaID])
REFERENCES [dbo].[Maquinas] ([MaquinaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_MaquinaID_VariablesMaquinaSopladora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaID_VariablesMaquinaSopladora] FOREIGN KEY([MateriaPrimaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_MateriaPrimaID_VariablesMaquinaSopladora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_VariablesMaquinaSopladora] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_ProductoID_VariablesMaquinaSopladora]
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientesCRUD]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[sp_ClientesCRUD]
@Accion int, 
@ClienteID int null,
@Identificacion varchar(max) null, 
@NombreCompleto varchar(max) null, 
@Telefono varchar(max) null, 
@Correo varchar(max) null, 
@Direccion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin
				INSERT INTO Clientes VALUES (@Identificacion, @NombreCompleto, @Telefono, @Correo, @Direccion, 1);			
			end
		else IF (@Accion = 2) 
			begin
				UPDATE Clientes set Identificacion = @Identificacion, NombreCompleto = @NombreCompleto, Telefono = @Telefono,
				Correo = @Correo, Direccion = @Direccion, Estado = 1 where ClienteID = @ClienteID
			end
		else  IF (@Accion = 3)
			begin
				UPDATE Clientes set Estado = 0 where ClienteID = @ClienteID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_DatosEtiqueta]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_DatosEtiqueta]
@Etiqueta int 

as
begin
		Select produccion.EtiquetaID, produccion.Fecha, produccion.Cantidad, produccion.Turno, usuario.NombreCompleto as Usuario,
		pedido.NumeroPedido, pedido.NumeroOrdenDeCompra, cliente.NombreCompleto as Cliente, cliente.Direccion, producto.Nombre as Producto,
		um.Unidad, maquina.Nombre as maquina

		from ProduccionPaquetesCajas as produccion
		join Usuarios as usuario on produccion.UsuarioID = usuario.UsuarioID
		join AperturaCierre as apertura on produccion.AperturaCierreID = apertura.AperturaCierreID
		join Pedidos as pedido on apertura.PedidoID = pedido.PedidoID
		join Clientes as cliente on pedido.ClienteID = cliente.ClienteID 
		join Productos as producto on pedido.ProductoID = producto.ProductoID		
		join UnidadMedida as um on pedido.UnidadMedidaID = um.UnidadMedidaID
		join Maquinas as maquina on apertura.MaquinaID = maquina.MaquinaID

		where EtiquetaID = @Etiqueta
end
GO
/****** Object:  StoredProcedure [dbo].[sp_EntregasCRUD]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_EntregasCRUD]
@Accion int, 
@EntregaID int null,
@NumeroEntrega varchar(max) null,
@Fecha date null,
@UnidadesEntregadas int null,
@NumeroFactura varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				INSERT INTO Entregas VALUES (@NumeroEntrega, @Fecha, @UnidadesEntregadas, @NumeroFactura, 1);			
			end
		else IF (@Accion = 2) 
			begin
				UPDATE Entregas set NumeroEntrega = @NumeroEntrega, Fecha = @Fecha, UnidadesEntregadas = @UnidadesEntregadas, 
				NumeroFactura = @NumeroFactura, Estado = 1 where EntregaID = @EntregaID
			end
		else  IF (@Accion = 3)
			begin
				UPDATE Entregas set Estado = 0 where EntregaID = @EntregaID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_FichaOperacion]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_FichaOperacion]
@Pedido int

as
begin

if (select  p.ProductoID from Pedidos p  join VariablesMaquinaInyectora vi on vi.ProductoID = p.ProductoID where p.PedidoID = @Pedido) > 0

select ('inyectora') as FichaOperacion , p.ProductoID 
from Pedidos p  join VariablesMaquinaInyectora vi on vi.ProductoID = p.ProductoID where p.PedidoID = @Pedido

 


else if (select  p.ProductoID from Pedidos p  join VariablesMaquinaSopladora vs on vs.ProductoID = p.ProductoID where p.PedidoID = @Pedido) > 0

select ('sopladora') as FichaOperacion, p.ProductoID from Pedidos p  join VariablesMaquinaSopladora vs on vs.ProductoID = p.ProductoID where p.PedidoID = @Pedido

else

select ('nada') as FichaOperacion ,p.ProductoID from Pedidos p  join VariablesMaquinaSopladora vs on vs.ProductoID = p.ProductoID where p.PedidoID = @Pedido

end
GO
/****** Object:  StoredProcedure [dbo].[sp_FichaOperacionInyectora]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_FichaOperacionInyectora]
@Pedido int

as
begin



Select p.NumeroPedido, p.NumeroOrdenDeCompra, p.Cantidad, p.Descripcion, p.Fecha, u.NombreCompleto as usuario,c.NombreCompleto, pd.Nombre as producto,
 m.Nombre, vi.CantidadPrimaPrincipal, vi.CantidadPrimaSecundaria, vi.CantidadPrimaTerciaria,
mp.Nombre as mp1, mps.Nombre as mp2, mpt.Nombre as mp3

from Pedidos p
join Usuarios u on p.UsuarioID = u.UsuarioID
join Clientes c on p.ClienteID = c.ClienteID
join Productos pd on p.ProductoID = pd.ProductoID
join VariablesMaquinaInyectora vi on vi.ProductoID = p.ProductoID
join Maquinas m on vi.MaquinaID = m.MaquinaID
join MateriaPrima mp on vi.MateriaPrimaPrincipalID = mp.MateriaPrimaID
join MateriaPrima mps on vi.MateriaPrimaSecundariaID = mps.MateriaPrimaID
join MateriaPrima mpt on vi.MateriaPrimaTerciariaID = mpt.MateriaPrimaID

where p.PedidoID = @Pedido;



end
GO
/****** Object:  StoredProcedure [dbo].[sp_FichaOperacionSopladora]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[sp_FichaOperacionSopladora]
@Pedido int

as
begin

Select p.NumeroPedido, p.NumeroOrdenDeCompra, p.Cantidad, p.Descripcion, p.Fecha, u.NombreCompleto as usuario,c.NombreCompleto as cliente, pd.Nombre as producto,
 m.Nombre as maquina, 
 mp.Nombre as MateriaPrima
      ,[RetardoPresoplado]
      ,[RetardoSoplado] 
      ,[TiempoPresoplado] 
      ,[TiempoRecuperacion] 
      ,[TiempoSoplado]
      ,[TiempoEscape]
      ,[TiempoParoCadena] 
      ,[TiempoSalidaPinzaPreforma] 
      ,[FrecuenciaVentilador] 
      ,[PresionEstirado] 
      ,[Gancho] 
      ,[Relay] 
      ,[AnillosTornelas] 
      ,[Notas] 
      ,[CantidadZonas] 
      ,[CantidadBancos] 
      ,[PosicionBanco1Zona1] 
      ,[PosicionBanco1Zona2]
      ,[PosicionBanco1Zona3]
      ,[PosicionBanco1Zona4] 
      ,[PosicionBanco2Zona1] 
      ,[PosicionBanco2Zona2] 
      ,[PosicionBanco2Zona3] 
      ,[PosicionBanco2Zona4] 
      ,[ValorBanco1Zona1] 
      ,[ValorBanco1Zona2] 
      ,[ValorBanco1Zona3]
      ,[ValorBanco1Zona4] 
      ,[ValorBanco2Zona1] 
      ,[ValorBanco2Zona2] 
      ,[ValorBanco2Zona3] 
      ,[ValorBanco2Zona4] 


from Pedidos p
join Usuarios u on p.UsuarioID = u.UsuarioID
join Clientes c on p.ClienteID = c.ClienteID
join Productos pd on p.ProductoID = pd.ProductoID
join VariablesMaquinaSopladora vs on vs.ProductoID = p.ProductoID
join Maquinas m on vs.MaquinaID = m.MaquinaID
join MateriaPrima mp on vs.MateriaPrimaID = mp.MateriaPrimaID


where p.PedidoID = @Pedido;



end
GO
/****** Object:  StoredProcedure [dbo].[sp_FichaTecnicaBotellaCRUD]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_FichaTecnicaBotellaCRUD]
@Accion int, 
@FichaTecnicaBotellaID int null,
@ProductoID int null,
@Altura float null,
@DiametroMayor float null,
@DiametroMenor float null,
@DiametroCuello float null,
@CapacidadVolumetrica float null,
@Tolerancia float null,
@ImagenCuerpo varchar(max) null,
@ImagenFondo varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				INSERT INTO FichaTecnicaBotella
				VALUES (@ProductoID, @Altura, @DiametroMayor, @DiametroMenor,
				@DiametroCuello,@CapacidadVolumetrica,@Tolerancia, @ImagenCuerpo, @ImagenFondo ,1);			
			end
		else IF (@Accion = 2) 
			begin
				UPDATE FichaTecnicaBotella set ProductoID = @ProductoID, Altura = @Altura,
				DiametroMayor = @DiametroMayor, DiametroMenor = @DiametroMenor, DiametroCuello = @DiametroCuello, 
				CapacidadVolumetrica = @CapacidadVolumetrica,
				Tolerancia = @Tolerancia, ImagenCuerpo = @ImagenCuerpo, ImagenFondo = @ImagenFondo, Estado = 1 
				where FichaTecnicaBotellaID = @FichaTecnicaBotellaID
			end
		else  IF (@Accion = 3)
			begin
				UPDATE FichaTecnicaBotella set Estado = 0 where FichaTecnicaBotellaID = @FichaTecnicaBotellaID
			end
		/*else  IF (@Accion = 4)
			begin
				select * from FichaTecnicaBotella where Estado = 1;
			end
		else  IF (@Accion = 5)
			begin
				select * from FichaTecnicaBotella where FichaTecnicaBotellaID = @FichaTecnicaBotellaID;
			end*/
end
GO
/****** Object:  StoredProcedure [dbo].[sp_FichaTecnicaTapaCRUD]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_FichaTecnicaTapaCRUD]
@Accion int, 
@FichaTecnicaTapaID int null,
@ProductoID int null,
@PesoPieza float null,
@Altura float null,
@DiametroInterno float null,
@DiametroExternoSelloOliva float null,
@AlturaSelloOliva float null,
@DiametroInternoBandaSeguridad float null,
@EspesorPanel float null,
@NumeroEstrias float null,
@TorqueCierreRecomendado float null,
@TorqueApertura float null,
@Tolerancia float null,
@Imagen varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into FichaTecnicaTapa values (@ProductoID, @PesoPieza, @Altura, @DiametroInterno, @DiametroExternoSelloOliva, @AlturaSelloOliva,
				@DiametroInternoBandaSeguridad, @EspesorPanel, @NumeroEstrias, @TorqueCierreRecomendado, @TorqueApertura, @Tolerancia, @Imagen, 1);	
			end
		else IF (@Accion = 2) 
			begin
				update FichaTecnicaTapa set ProductoID = @ProductoID, PesoPieza = @PesoPieza, Altura = @Altura, DiametroInterno = @DiametroInterno,
				AlturaSelloOliva = @AlturaSelloOliva, DiametroInternoBandaSeguridad = @DiametroInternoBandaSeguridad, EspesorPanel = @EspesorPanel,
				NumeroEstrias = @NumeroEstrias, TorqueCierreRecomendado = @TorqueCierreRecomendado, TorqueApertura = @TorqueApertura, 
				Tolerancia = @Tolerancia, Imagen = @Imagen, Estado = 1 where FichaTecnicaTapaID = @FichaTecnicaTapaID
			end
		else  IF (@Accion = 3)
			begin
				update FichaTecnicaTapa set Estado = 0 where FichaTecnicaTapaID = @FichaTecnicaTapaID;
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_getRolesForUser]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*Procedimientos almacenados*/


create PROCEDURE [dbo].[sp_getRolesForUser]
	@userName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	select r.RoleName from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioID
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where u.UserName=@userName
END
GO
/****** Object:  StoredProcedure [dbo].[sp_getUsuariosRole]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_getUsuariosRole]
	
	@roleName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	select u.UserName from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioID
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where r.RoleName=@roleName 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GraficoParettoMerma]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_GraficoParettoMerma]
as
Select  TipoMerma.Tipo, sum(MermaPorTurno.Cantidad)as Cantidad
from MermaPorTurno
join TipoMerma on TipoMerma.TipoMermaID = MermaPorTurno.TipoMermaID

GROUP BY
TipoMerma.Tipo

order by cantidad desc
GO
/****** Object:  StoredProcedure [dbo].[sp_GraficoPedidos]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_GraficoPedidos] AS

select count(*) as PedidosProceso, (
select count(*) PedidosPendientes from PedidosActivos p 
 left join Produccion on p.PedidoID = Produccion.PedidoID where p.PedidoID not in (select PedidoID from Produccion))
 as PedidosPendientes
 ,(select count(*)

from PedidosActivos
 
join Produccion on PedidosActivos.PedidoID = Produccion.PedidoID
 
where PedidosActivos.PedidoID = Produccion.PedidoID and Produccion.CantidadProduccionTotal >= PedidosActivos.Cantidad
) as PedidosTerminados

from PedidosActivos
 
join Produccion on PedidosActivos.PedidoID = Produccion.PedidoID
 
where PedidosActivos.PedidoID = Produccion.PedidoID and Produccion.CantidadProduccionTotal >= 0 and Produccion.CantidadProduccionTotal < PedidosActivos.Cantidad


GO
/****** Object:  StoredProcedure [dbo].[sp_GraficoProduccion]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_GraficoProduccion] AS


SELECT DATEPART(month, Fecha) as mes, SUM(Cantidad) as total
FROM ProduccionPaquetesCajaActivos

GROUP BY DATEPART(month, Fecha)
GO
/****** Object:  StoredProcedure [dbo].[sp_isUserInRole]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_isUserInRole]
	@userName varchar(20),
	@roleName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	declare @resultado bit =0;
	if exists(
	select * from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioId
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where r.RoleName=@roleName and u.UserName=@userName)
	set @resultado = 1
	 select @resultado
END


GO
/****** Object:  StoredProcedure [dbo].[sp_MaquinasCRUD]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_MaquinasCRUD]
@Accion int, 
@MaquinaID int null,
@CodigoMaquina varchar(max) null,
@Nombre varchar(max) null,
@TipoMaquinaID int null,
@Descripcion varchar(max) null,
@ProduccionHora varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Maquinas values (@CodigoMaquina, @Nombre, @TipoMaquinaID, @Descripcion, @ProduccionHora, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Maquinas set CodigoMaquina = @CodigoMaquina, Nombre = @Nombre, TipoMaquinaID = @TipoMaquinaID, Descripcion = @Descripcion,
				ProduccionHora = @ProduccionHora, Estado = 1 where MaquinaID = @MaquinaID
			end
		else  IF (@Accion = 3)
			begin
				update Maquinas set Estado = 0 where MaquinaID = @MaquinaID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_MateriaPrimaCRUD]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_MateriaPrimaCRUD]
@Accion int, 
@MateriaPrimaID int null,
@CodigoMateriaPrima varchar(max) null,
@Nombre varchar(max) null,
@ProveedorID int null,
@TipoMateriaPrimaID int null,
@UnidadMedidaID int null,
@Cantidad int null,
@PrecioUnitario money null,
@PrecioTotal money null,
@Descripcion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into MateriaPrima values(@CodigoMateriaPrima, @Nombre, @ProveedorID, @TipoMateriaPrimaID, @UnidadMedidaID, @Cantidad,
				@PrecioUnitario, @PrecioTotal, @Descripcion, 1);
			end
		else IF (@Accion = 2) 
			begin
				update MateriaPrima set CodigoMateriaPrima = @CodigoMateriaPrima, Nombre = @Nombre, ProveedorID = @ProveedorID, TipoMateriaPrimaID 
				= @TipoMateriaPrimaID, UnidadMedidaID = @UnidadMedidaID, Cantidad = @Cantidad, PrecioUnitario = @PrecioUnitario, Descripcion = @Descripcion,
				Estado = 1
				where MateriaPrimaID = @MateriaPrimaID
			end
		else  IF (@Accion = 3)
			begin
				update MateriaPrima set Estado = 0 where MateriaPrimaID = @MateriaPrimaID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_PedidosCRUD]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_PedidosCRUD]
@Accion int, 
@PedidoID int null,
@NumeroPedido varchar(max) null,
@ClienteID int null,
@NumeroOrdenDeCompra varchar(max) null,
@ProductoID int null,
@UsuarioID int null,
@UnidadMedidaID int null,
@Cantidad int null,
@Descripcion varchar(max) null,
@Fecha date null,
@PrecioTotal money null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Pedidos values (@NumeroPedido, @ClienteID, @NumeroOrdenDeCompra, @ProductoID, @UsuarioID, @UnidadMedidaID, @Cantidad,
				@Descripcion, @Fecha, @PrecioTotal, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Pedidos set NumeroPedido = @NumeroPedido, ClienteID = @ClienteID, NumeroOrdenDeCompra = @NumeroOrdenDeCompra, ProductoID = @ProductoID,
				UsuarioID = @UsuarioID, UnidadMedidaID = @UnidadMedidaID, Cantidad = @Cantidad, Descripcion = @Descripcion, Fecha = @Fecha, PrecioTotal = @PrecioTotal,
				Estado = 1 where PedidoID = @PedidoID
			end
		else  IF (@Accion = 3)
			begin
				update Pedidos set Estado = 0 where PedidoID = @PedidoID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_ProduccionCRUD]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ProduccionCRUD]
@Accion int, 
@ProduccionID int null,
@FechaInicio date null,
@FechaFin date null,
@HorasTrabajadas date null,
@CantidadMerma float null,
@CantidadProduccionTotal int null,
@CantidadPaquetesCajas int null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Produccion values (@FechaInicio, @FechaFin, @HorasTrabajadas, @CantidadMerma, @CantidadProduccionTotal, @CantidadPaquetesCajas, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Produccion set FechaInicio = @FechaInicio, FechaFin = @FechaFin, HorasTrabajadas = @HorasTrabajadas, 
				CantidadMerma = @CantidadMerma, CantidadProduccionTotal = @CantidadProduccionTotal, CantidadPaquetesCajas = @CantidadPaquetesCajas, Estado = 1
				where ProduccionID = @ProduccionID
			end
		else  IF (@Accion = 3)
			begin
				update Produccion set Estado = 0 where ProduccionID = @ProduccionID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_ProduccionPaquetesCajasCRUD]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ProduccionPaquetesCajasCRUD]
@Accion int, 
@EtiquetaID int null,
@Fecha date null,
@Cantidad int null,
@PedidoID int null,
@UsuarioID int null,
@Turno varchar(max) null,
@ProduccionID int null
as
begin
		IF (@Accion = 1)
			begin 
				insert into ProduccionPaquetesCajas values (@Fecha, @Cantidad, @PedidoID, @UsuarioID, @Turno, @ProduccionID, 1);
			end
		else IF (@Accion = 2) 
			begin
				update ProduccionPaquetesCajas set Fecha = @Fecha, Cantidad = @Cantidad, PedidoID = @PedidoID, UsuarioID = @UsuarioID, Turno = @Turno,
				ProduccionID = @ProduccionID, Estado = 1 where EtiquetaID = @EtiquetaID
			end
		else  IF (@Accion = 3)
			begin
				update ProduccionPaquetesCajas set Estado = 0 where EtiquetaID = @EtiquetaID;
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductosCRUD]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ProductosCRUD]
@Accion int, 
@ProductoID int null,
@CodigoProducto varchar(max) null,
@Nombre varchar(max) null,
@TipoProductoID int null,
@Descripcion varchar(max) null,
@Imagen varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Productos values(@CodigoProducto, @Nombre, @TipoProductoID, @Descripcion, @Imagen, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Productos set CodigoProducto = @CodigoProducto, Nombre = @Nombre, TipoProductoID = @TipoProductoID, Descripcion = @Descripcion,
				Imagen = @Imagen, Estado = 1 where ProductoID = @ProductoID
			end
		else  IF (@Accion = 3)
			begin
				update Productos set Estado = 0 where ProductoID = @ProductoID;
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_ProveedorCRUD]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ProveedorCRUD]
@Accion int, 
@ProveedorID int null,
@CodigoProveedor varchar(max) null,
@Nombre varchar(max) null,
@Telefono varchar(max) null,
@Correo varchar(max) null,
@Direccion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Proveedor values(@CodigoProveedor, @Nombre, @Telefono, @Correo, @Direccion, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Proveedor set CodigoProveedor = @CodigoProveedor, Nombre = @Nombre, Telefono = @Telefono, Correo = @Correo,
				Direccion = @Direccion, Estado = 1 where ProveedorID = @ProveedorID;
			end
		else  IF (@Accion = 3)
			begin
				update Proveedor set Estado = 0 where ProveedorID = @ProveedorID;
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_setSpecificRoleForUser]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_setSpecificRoleForUser]
	@UsuarioID int,
	@RoleID int
AS
BEGIN
	SET NOCOUNT ON;
	delete from UsersInRoles where User_UserID = @UsuarioID;
	insert into UsersInRoles values(@UsuarioID,@RoleID);
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UsuariosCRUD]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_UsuariosCRUD]
@Accion int, 
@UsuarioID int null,
@Identificacion varchar(max) null,
@UserName varchar(max) null,
@Password varchar(max) null,
@NombreCompleto varchar(max) null,
@Telefono varchar(max) null,
@Correo varchar(max) null,
@Direccion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Usuarios values(@Identificacion, @UserName, @Password, @NombreCompleto, @Telefono, @Correo, @Direccion,1);
			end
		else IF (@Accion = 2) 
			begin
				update Usuarios set Identificacion = @Identificacion, UserName = @UserName, Password = @Password, NombreCompleto = @NombreCompleto,
				Telefono = @Telefono, Correo = @Correo, Direccion = @Direccion, Estado = 1 where UsuarioID = @UsuarioID
			end
		else  IF (@Accion = 3)
			begin
				update Usuarios set Estado = 0 where UsuarioID = @UsuarioID;
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_VariablesMaquinaInyectoraCRUD]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_VariablesMaquinaInyectoraCRUD]
@Accion int, 
@VariablesMaquinaInyectoraID int null,
@MaquinaID int null,
@ProductoID int null,
@MateriaPrimaPrincipalID int null,
@MateriaPrimaSecundariaID int null,
@MateriaPrimaTerciariaID int null,
@CantidadPrimaPrincipal int null,
@CantidadPrimaSecundaria int null,
@CantidadPrimaTerciaria int null
as
begin
		IF (@Accion = 1)
			begin 
				insert into VariablesMaquinaInyectora values(@MaquinaID, @ProductoID, @MateriaPrimaPrincipalID, @MateriaPrimaSecundariaID,
				@MateriaPrimaTerciariaID, @CantidadPrimaPrincipal, @CantidadPrimaSecundaria, @CantidadPrimaTerciaria, 1);
			end
		else IF (@Accion = 2) 
			begin
				update VariablesMaquinaInyectora set MaquinaID = @MaquinaID, ProductoID = @ProductoID, MateriaPrimaPrincipalID = @MateriaPrimaPrincipalID,
				MateriaPrimaSecundariaID = @MateriaPrimaSecundariaID, MateriaPrimaTerciariaID = @MateriaPrimaTerciariaID, CantidadPrimaPrincipal =
				@CantidadPrimaPrincipal, CantidadPrimaSecundaria = @CantidadPrimaSecundaria, CantidadPrimaTerciaria = @CantidadPrimaTerciaria,
				Estado = 1
				where VariablesMaquinaInyectoraID = @VariablesMaquinaInyectoraID
			end
		else  IF (@Accion = 3)
			begin
				update VariablesMaquinaInyectora set Estado = 0 where VariablesMaquinaInyectoraID = @VariablesMaquinaInyectoraID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_VariablesMaquinaSopladoraCRUD]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_VariablesMaquinaSopladoraCRUD]
@Accion int, 
@VariablesMaquinaSopladoraID int null,
@MaquinaID int null,
@ProductoID int null,
@MateriaPrimaID int null,
@RetardoPresoplado int null,
@RetardoSoplado int null,
@TiempoPresoplado int null,
@TiempoRecuperacion int null,
@TiempoSoplado int null,
@TiempoEscape int null,
@TiempoParoCadena int null,
@TiempoSalidaPinzaPreforma int null,
@FrecuenciaVentilador float null,
@PresionEstirado float null,
@Gancho bit null,
@Relay bit null,
@AnillosTornelas bit null,
@Notas varchar(max) null,
@CantidadZonas int null,
@CantidadBancos int null,
@PosicionBanco1Zona1 varchar(max) null,
@PosicionBanco1Zona2 varchar(max) null,
@PosicionBanco1Zona3 varchar(max) null,
@PosicionBanco1Zona4 varchar(max) null,
@PosicionBanco2Zona1 varchar(max) null,
@PosicionBanco2Zona2 varchar(max) null,
@PosicionBanco2Zona3 varchar(max) null,
@PosicionBanco2Zona4 varchar(max) null,
@ValorBanco1Zona1 int null,
@ValorBanco1Zona2 int null,
@ValorBanco1Zona3 int null,
@ValorBanco1Zona4 int null,
@ValorBanco2Zona1 int null,
@ValorBanco2Zona2 int null,
@ValorBanco2Zona3 int null,
@ValorBanco2Zona4 int null
as
begin
		IF (@Accion = 1)
			begin 
				insert into VariablesMaquinaSopladora values(@MaquinaID,@ProductoID, @MateriaPrimaID, @RetardoPresoplado,
				@RetardoSoplado, @TiempoPresoplado, @TiempoRecuperacion, @TiempoSoplado, @TiempoEscape, @TiempoParoCadena,
				@TiempoSalidaPinzaPreforma, @FrecuenciaVentilador, @PresionEstirado, @Gancho, @Relay, @AnillosTornelas, 1,
				@Notas, @CantidadZonas, @CantidadBancos, @PosicionBanco1Zona1,@PosicionBanco1Zona2,@PosicionBanco1Zona3,@PosicionBanco1Zona4
				,@PosicionBanco2Zona1,@PosicionBanco2Zona2,@PosicionBanco2Zona3,@PosicionBanco2Zona4,@ValorBanco1Zona1,@ValorBanco1Zona2,
				@ValorBanco1Zona3,@ValorBanco1Zona4,@ValorBanco2Zona1,@ValorBanco2Zona2,@ValorBanco2Zona3,@ValorBanco2Zona4);
			end
		else IF (@Accion = 2) 
			begin
				update VariablesMaquinaSopladora set MaquinaID = @MaquinaID, ProductoID = @ProductoID, MateriaPrimaID = @MateriaPrimaID,
				RetardoPresoplado = @RetardoPresoplado, RetardoSoplado = @RetardoSoplado, TiempoPresoplado = @TiempoPresoplado, TiempoRecuperacion =
				@TiempoRecuperacion, TiempoSoplado = @TiempoSoplado, TiempoEscape = @TiempoEscape, TiempoParoCadena = @TiempoParoCadena,
				TiempoSalidaPinzaPreforma = @TiempoSalidaPinzaPreforma, FrecuenciaVentilador = @FrecuenciaVentilador, PresionEstirado = @PresionEstirado,
				Gancho = @Gancho, Relay = @Relay, AnillosTornelas = @AnillosTornelas, Estado = 1, Notas = @Notas, CantidadZonas = @CantidadZonas,
				CantidadBancos = @CantidadBancos, PosicionBanco1Zona1 = @PosicionBanco1Zona1, PosicionBanco1Zona2 = @PosicionBanco1Zona2,
				PosicionBanco1Zona3 = @PosicionBanco1Zona3, PosicionBanco1Zona4 = @PosicionBanco1Zona4, PosicionBanco2Zona1 = @PosicionBanco2Zona1,
				PosicionBanco2Zona2 = @PosicionBanco2Zona2, PosicionBanco2Zona3 = @PosicionBanco2Zona3, PosicionBanco2Zona4 = @PosicionBanco2Zona4,
				ValorBanco1Zona1 = @ValorBanco1Zona1, ValorBanco1Zona2 = @ValorBanco1Zona2, ValorBanco1Zona3 = @ValorBanco1Zona3, ValorBanco1Zona4 =
				@ValorBanco1Zona4, ValorBanco2Zona1 = @ValorBanco2Zona1, ValorBanco2Zona2 = @ValorBanco2Zona2, ValorBanco2Zona3 = @ValorBanco2Zona3,
				ValorBanco2Zona4 = @ValorBanco2Zona4 where VariablesMaquinaSopladoraID = @VariablesMaquinaSopladoraID
			end
		else  IF (@Accion = 3)
			begin
				update VariablesMaquinaSopladora set Estado = 0 where VariablesMaquinaSopladoraID = @VariablesMaquinaSopladoraID;
			end
end
GO
/****** Object:  Trigger [dbo].[TRG_INS_Produccion]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRG_INS_Produccion]
ON [dbo].[AperturaCierre]
FOR INSERT
AS
BEGIN
		SET NOCOUNT ON;	

		IF NOT EXISTS( SELECT PedidoID FROM [Produccion] WHERE PedidoID =  (select top 1 PedidoID from AperturaCierre order by AperturaCierreID desc))
			begin
				INSERT INTO [dbo].[Produccion]
						   ([PedidoID]
						   ,[FechaInicio]
						   ,[FechaFin]
						   ,[HorasTrabajadas]
						   ,[CantidadMerma]
						   ,[CantidadProduccionTotal]
						   ,[CantidadPaquetesCajas]
						   ,[Estado])
					 VALUES
						   ((select top 1 PedidoID from AperturaCierre order by AperturaCierreID desc)
						   ,(select top 1 FechaApertura from AperturaCierre order by AperturaCierreID desc)
						   ,null
						   ,null
						   ,0
						   ,0
						   ,0
						   ,1)
			end


		END
GO
ALTER TABLE [dbo].[AperturaCierre] ENABLE TRIGGER [TRG_INS_Produccion]
GO
/****** Object:  Trigger [dbo].[FK_Clientes]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*create procedure [dbo].[sp_ReportEntrega] as
select NumeroPedido, c.Nombre 'NombreCliente', NumeroOrdenDeCompra, pr.Nombre 'NombreProducto', CodigoEntrega, e.Fecha 'FechaEntrega', 
UnidadesEntregadas, NumeroFactura, p.Fecha 'FechaPedido', p.PrecioTotal from Movimientos m inner join Entregas e on m.EntregaID = m.EntregaID
inner join Pedidos p on p.PedidoID = m.PedidoID inner join Productos pr on p.ProductoID = pr.ProductoID inner join Clientes c on c.ClienteID = p.ClienteID
GO
create procedure [dbo].[sp_ReportPedido] as
select PedidoID, NumeroPedido, c.Nombre 'NombreCliente', NumeroOrdenDeCompra, pr.Nombre 'NombreProducto', NombreCompleto 'NombreUsuario',
un.Descripcion 'UnidadMedida', p.Cantidad, p.Descripcion, Fecha, p.PrecioTotal from Pedidos p 
inner join Clientes c on p.ClienteID = c.ClienteID
inner join Productos pr on p.ProductoID = pr.ProductoID 
inner join Usuarios u on u.UsuarioID = p.UsuarioID 
inner join UnidadMedida un on un.UnidadMedidaID = p.UnidadMedidaID
GO*/

/*
Triggers

*/

create trigger [dbo].[FK_Clientes]
on [dbo].[Clientes]
instead of delete
as
begin
set nocount on;
delete from Pedidos where ClienteID in (select ClienteID from deleted);
delete from Clientes where ClienteID in (select ClienteID from deleted);
end
GO
ALTER TABLE [dbo].[Clientes] ENABLE TRIGGER [FK_Clientes]
GO
/****** Object:  Trigger [dbo].[FK_Entregas]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_Entregas]
on [dbo].[Entregas]
instead of delete
as
begin
set nocount on;
delete from Movimientos where EntregaID in (select EntregaID from deleted);
delete from Entregas where EntregaID in (select EntregaID from deleted);
end
GO
ALTER TABLE [dbo].[Entregas] ENABLE TRIGGER [FK_Entregas]
GO
/****** Object:  Trigger [dbo].[FK_Maquinas]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_Maquinas]
on [dbo].[Maquinas]
instead of delete
as
begin
set nocount on;
delete from VariablesMaquinaSopladora where MaquinaID in (select MaquinaID from deleted);
delete from VariablesMaquinaInyectora where MaquinaID in (select MaquinaID from deleted);
delete from Maquinas where MaquinaID in (select MaquinaID from deleted);
end
GO
ALTER TABLE [dbo].[Maquinas] ENABLE TRIGGER [FK_Maquinas]
GO
/****** Object:  Trigger [dbo].[FK_MateriaPrima]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_MateriaPrima]
on [dbo].[MateriaPrima]
instead of delete
as
begin
set nocount on;
delete from VariablesMaquinaSopladora where MateriaPrimaID in (select MateriaPrimaID from deleted);
delete from VariablesMaquinaInyectora where MateriaPrimaPrincipalID in (select MateriaPrimaPrincipalID from deleted);
delete from VariablesMaquinaInyectora where MateriaPrimaSecundariaID in (select MateriaPrimaSecundariaID from deleted);
delete from VariablesMaquinaInyectora where MateriaPrimaTerciariaID in (select MateriaPrimaTerciariaID from deleted);
delete from MateriaPrima where MateriaPrimaID in (select MateriaPrimaID from deleted);
end
GO
ALTER TABLE [dbo].[MateriaPrima] ENABLE TRIGGER [FK_MateriaPrima]
GO
/****** Object:  Trigger [dbo].[TRG_INS_MermaProduccion]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create TRIGGER [dbo].[TRG_INS_MermaProduccion]
ON [dbo].[MermaPorTurno]
FOR INSERT
AS
BEGIN
		SET NOCOUNT ON;	

			begin
			
					UPDATE [dbo].[Produccion]
					   SET 
						  [CantidadMerma] = (select top 1 Cantidad from MermaPorTurno order by MermaPorTurnoID desc)
						  
					 WHERE Produccion.PedidoID = (

						select top 1 AperturaCierre.PedidoID 
						from AperturaCierre 
							join MermaPorTurno on  AperturaCierre.AperturaCierreID = MermaPorTurno.AperturaCierreID
						where AperturaCierre.AperturaCierreID = MermaPorTurno.AperturaCierreID
						 order by  AperturaCierre.AperturaCierreID desc
						)
				
end


	END
GO
ALTER TABLE [dbo].[MermaPorTurno] ENABLE TRIGGER [TRG_INS_MermaProduccion]
GO
/****** Object:  Trigger [dbo].[FK_Pedidos]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_Pedidos]
on [dbo].[Pedidos]
instead of delete
as
begin
set nocount on;
delete from Movimientos where PedidoID in (select PedidoID from deleted);
delete from ProduccionPaquetesCajas where PedidoID in (select PedidoID from deleted);
delete from Pedidos where PedidoID in (select PedidoID from deleted);
end
GO
ALTER TABLE [dbo].[Pedidos] ENABLE TRIGGER [FK_Pedidos]
GO
/****** Object:  Trigger [dbo].[FK_Productos]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_Productos]
on [dbo].[Productos]
instead of delete
as
begin
set nocount on;
delete from FichaTecnicaBotella where ProductoID in (select ProductoID from deleted);
delete from VariablesMaquinaSopladora where ProductoID in (select ProductoID from deleted);
delete from VariablesMaquinaInyectora where ProductoID in (select ProductoID from deleted);
delete from Pedidos where ProductoID in (select ProductoID from deleted);
delete from FichaTecnicaTapa where ProductoID in (select ProductoID from deleted);
delete from Productos where ProductoID in (select ProductoID from deleted);
end
GO
ALTER TABLE [dbo].[Productos] ENABLE TRIGGER [FK_Productos]
GO
/****** Object:  Trigger [dbo].[FK_Proveedor]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_Proveedor]
on [dbo].[Proveedor]
instead of delete
as
begin
set nocount on;
delete from MateriaPrima where MateriaPrimaID in (select MateriaPrimaID from deleted);
delete from Proveedor where ProveedorID in (select ProveedorID from deleted);
end
GO
ALTER TABLE [dbo].[Proveedor] ENABLE TRIGGER [FK_Proveedor]
GO
/****** Object:  Trigger [dbo].[FK_Roles]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_Roles]
on [dbo].[Roles]
instead of delete
as
begin
set nocount on;
delete from UsersInRoles where Role_RoleID in (select RoleID from deleted);
delete from Roles where RoleID in (select RoleID from deleted);
end
GO
ALTER TABLE [dbo].[Roles] ENABLE TRIGGER [FK_Roles]
GO
/****** Object:  Trigger [dbo].[FK_TipoMaquina]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_TipoMaquina]
on [dbo].[TipoMaquina]
instead of delete
as
begin
set nocount on;
delete from Maquinas where TipoMaquinaID in (select TipoMaquinaID from deleted);
delete from TipoMaquina where TipoMaquinaID in (select TipoMaquinaID from deleted);
end
GO
ALTER TABLE [dbo].[TipoMaquina] ENABLE TRIGGER [FK_TipoMaquina]
GO
/****** Object:  Trigger [dbo].[FK_TipoMateriaPrima]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_TipoMateriaPrima]
on [dbo].[TipoMateriaPrima]
instead of delete
as
begin
set nocount on;
delete from MateriaPrima where MateriaPrimaID in (select MateriaPrimaID from deleted);
delete from TipoMateriaPrima where TipoMateriaPrimaID in (select TipoMateriaPrimaID from deleted);
end
GO
ALTER TABLE [dbo].[TipoMateriaPrima] ENABLE TRIGGER [FK_TipoMateriaPrima]
GO
/****** Object:  Trigger [dbo].[FK_TipoProducto]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_TipoProducto]
on [dbo].[TipoProducto]
instead of delete
as
begin
set nocount on;
delete from Productos where TipoProductoID in (select TipoProductoID from deleted);
delete from TipoProducto where TipoProductoID in (select TipoProductoID from deleted);
end
GO
ALTER TABLE [dbo].[TipoProducto] ENABLE TRIGGER [FK_TipoProducto]
GO
/****** Object:  Trigger [dbo].[FK_UnidadMedida]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_UnidadMedida]
on [dbo].[UnidadMedida]
instead of delete
as
begin
set nocount on;
delete from Pedidos where UnidadMedidaID in (select UnidadMedidaID from deleted);
delete from MateriaPrima where UnidadMedidaID in (select UnidadMedidaID from deleted);
delete from UnidadMedida where UnidadMedidaID in (select UnidadMedidaID from deleted);
end
GO
ALTER TABLE [dbo].[UnidadMedida] ENABLE TRIGGER [FK_UnidadMedida]
GO
/****** Object:  Trigger [dbo].[FK_Usuarios]    Script Date: 12/11/2020 20:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[FK_Usuarios]
on [dbo].[Usuarios]
instead of delete
as
begin
set nocount on;
delete from Pedidos where UsuarioID in (select UsuarioID from deleted);
delete from UsersInRoles where User_UserID in (select UsuarioID from deleted);
delete from ProduccionPaquetesCajas where UsuarioID in (select UsuarioID from deleted);
delete from Usuarios where UsuarioID in (select UsuarioID from deleted);
end
GO
ALTER TABLE [dbo].[Usuarios] ENABLE TRIGGER [FK_Usuarios]
GO
