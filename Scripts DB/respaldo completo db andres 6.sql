USE [master]
GO
/****** Object:  Database [DIPREMA]    Script Date: 25/11/2020 5:15:00 p. m. ******/
CREATE DATABASE [DIPREMA]
GO
USE [DIPREMA]
GO
/****** Object:  Table [dbo].[AperturaCierre]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AperturaCierre](
	[AperturaCierreID] [bigint] IDENTITY(1,1) NOT NULL,
	[FechaApertura] [datetime] NOT NULL,
	[FechaCierre] [datetime] NULL,
	[Turno] [varchar](50) NOT NULL,
	[PedidoID] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[MaquinaID] [int] NULL,
 CONSTRAINT [PK_AperturaCierre] PRIMARY KEY CLUSTERED 
(
	[AperturaCierreID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[AperturaCierreActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*Vistas*/
create VIEW [dbo].[AperturaCierreActivos] AS
select * from AperturaCierre where Estado = 1;
GO
/****** Object:  Table [dbo].[DetalleEntrega]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleEntrega](
	[DetelleEntregaID] [bigint] IDENTITY(1,1) NOT NULL,
	[EntregaID] [int] NOT NULL,
	[ProduccionID] [int] NOT NULL,
	[Paquetes] [int] NOT NULL,
	[Cantidad] [float] NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_DetalleEntrega] PRIMARY KEY CLUSTERED 
(
	[DetelleEntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Entregas]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entregas](
	[EntregaID] [int] IDENTITY(1,1) NOT NULL,
	[NumeroEntrega] [varchar](50) NOT NULL,
	[Fecha] [date] NULL,
	[UnidadesEntregadas] [int] NULL,
	[NumeroFactura] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pedidos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pedidos](
	[PedidoID] [int] IDENTITY(1,1) NOT NULL,
	[NumeroPedido] [varchar](50) NOT NULL,
	[ClienteID] [int] NOT NULL,
	[NumeroOrdenDeCompra] [varchar](50) NULL,
	[ProductoID] [int] NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[UnidadMedidaID] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Fecha] [date] NULL,
	[PrecioTotal] [money] NOT NULL,
	[Estado] [bit] NOT NULL,
	[EstadoID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PedidoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productos](
	[ProductoID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProducto] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[TipoProductoID] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Imagen] [varchar](max) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UnidadMedida]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnidadMedida](
	[UnidadMedidaID] [int] IDENTITY(1,1) NOT NULL,
	[Unidad] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UnidadMedidaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Produccion]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Produccion](
	[ProduccionID] [int] IDENTITY(1,1) NOT NULL,
	[PedidoID] [int] NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaFin] [datetime] NULL,
	[HorasTrabajadas] [varchar](100) NULL,
	[CantidadMerma] [float] NULL,
	[CantidadProduccionTotal] [float] NOT NULL,
	[CantidadPaquetesCajas] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProduccionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[DetalleEntregaActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create VIEW [dbo].[DetalleEntregaActivos] AS

Select Entregas.Fecha as FechaEntrega, Entregas.NumeroFactura as NumeroFactura ,Entregas.NumeroEntrega as NumeroEntrega,
		Pedidos.NumeroPedido as lote, Pedidos.NumeroOrdenDeCompra as NumeroOrdenCompra,
		Productos.Nombre as Producto,
		DetalleEntrega.Paquetes, DetalleEntrega.Cantidad,
		UnidadMedida.Unidad
from DetalleEntrega

join Entregas on DetalleEntrega.EntregaID = Entregas.EntregaID
join Produccion on  DetalleEntrega.ProduccionID = Produccion.ProduccionID
join Pedidos on Produccion.ProduccionID = Pedidos.PedidoID
join UnidadMedida on Pedidos.UnidadMedidaID = UnidadMedida.UnidadMedidaID
join Productos on Pedidos.ProductoID = Productos.ProductoID

where DetalleEntrega.Estado =1
GO
/****** Object:  Table [dbo].[MermaPorTurno]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MermaPorTurno](
	[MermaPorTurnoID] [int] IDENTITY(1,1) NOT NULL,
	[Cantidad] [float] NOT NULL,
	[AperturaCierreID] [bigint] NOT NULL,
	[TipoMermaID] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MermaPorTurnoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MermaPorTurnoActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[MermaPorTurnoActivos] as 
SELECT        *
FROM            MermaPorTurno
WHERE        Estado = 1
GO
/****** Object:  Table [dbo].[ProduccionPaquetesCajas]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProduccionPaquetesCajas](
	[EtiquetaID] [int] IDENTITY(1,1) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Cantidad] [float] NOT NULL,
	[AperturaCierreID] [bigint] NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[Turno] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_ProduccionPaquetesCajas] PRIMARY KEY CLUSTERED 
(
	[EtiquetaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ProduccionPaquetesCajaActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[ProduccionPaquetesCajaActivos] as
select * from ProduccionPaquetesCajas where Estado = 1;
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clientes](
	[ClienteID] [int] IDENTITY(1,1) NOT NULL,
	[Identificacion] [varchar](50) NOT NULL,
	[NombreCompleto] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ClienteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ClientesActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*Vistas*/
create VIEW [dbo].[ClientesActivos] AS
select * from Clientes where Estado = 1;
GO
/****** Object:  View [dbo].[EntregasActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create VIEW [dbo].[EntregasActivos] as
select * from Entregas where Estado = 1;
GO
/****** Object:  Table [dbo].[FichaTecnicaBotella]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FichaTecnicaBotella](
	[FichaTecnicaBotellaID] [int] IDENTITY(1,1) NOT NULL,
	[ProductoID] [int] NOT NULL,
	[Altura] [float] NOT NULL,
	[DiametroMayor] [float] NOT NULL,
	[DiametroMenor] [float] NOT NULL,
	[DiametroCuello] [float] NOT NULL,
	[CapacidadVolumetrica] [float] NOT NULL,
	[Tolerancia] [float] NULL,
	[ImagenCuerpo] [varchar](max) NULL,
	[ImagenFondo] [varchar](max) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FichaTecnicaBotellaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[FichaTecnicaBotellaActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[FichaTecnicaBotellaActivos] as
select * from FichaTecnicaBotella where Estado = 1;
GO
/****** Object:  View [dbo].[PedidosActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   view [dbo].[PedidosActivos] as
select * from Pedidos where Estado = 1;
GO
/****** Object:  View [dbo].[ProduccionActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[ProduccionActivos] as
select * from Produccion where Estado = 1;
GO
/****** Object:  View [dbo].[ClientesProduccion]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view 
[dbo].[ClientesProduccion] 
AS
select DISTINCT cliente.ClienteID
      ,cliente.Identificacion
      ,cliente.NombreCompleto
      ,cliente.Telefono
      ,cliente.Correo
      ,cliente.Direccion
      ,cliente.Estado
from ClientesActivos as cliente
join PedidosActivos as pedido on cliente.ClienteID= pedido.ClienteID
join  ProduccionActivos as produccion on pedido.PedidoID= produccion.PedidoID
GO
/****** Object:  Table [dbo].[FichaTecnicaTapa]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FichaTecnicaTapa](
	[FichaTecnicaTapaID] [int] IDENTITY(1,1) NOT NULL,
	[ProductoID] [int] NOT NULL,
	[PesoPieza] [float] NOT NULL,
	[Altura] [float] NOT NULL,
	[DiametroInterno] [float] NOT NULL,
	[DiametroExternoSelloOliva] [float] NOT NULL,
	[AlturaSelloOliva] [float] NOT NULL,
	[DiametroInternoBandaSeguridad] [float] NOT NULL,
	[EspesorPanel] [float] NOT NULL,
	[NumeroEstrias] [float] NOT NULL,
	[TorqueCierreRecomendado] [float] NOT NULL,
	[TorqueApertura] [float] NOT NULL,
	[Tolerancia] [float] NULL,
	[Imagen] [varchar](max) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FichaTecnicaTapaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[FichaTecnicaTapaActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[FichaTecnicaTapaActivos] as
select * from FichaTecnicaTapa where Estado = 1;
GO
/****** Object:  Table [dbo].[Maquinas]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Maquinas](
	[MaquinaID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoMaquina] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[TipoMaquinaID] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[ProduccionHora] [varchar](100) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaquinaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MaquinasActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MaquinasActivos] as
select * from Maquinas where Estado = 1;
GO
/****** Object:  Table [dbo].[MateriaPrima]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MateriaPrima](
	[MateriaPrimaID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoMateriaPrima] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[ProveedorID] [int] NOT NULL,
	[TipoMateriaPrimaID] [int] NOT NULL,
	[UnidadMedidaID] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[PrecioUnitario] [money] NOT NULL,
	[PrecioTotal] [money] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MateriaPrimaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MateriaPrimaActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MateriaPrimaActivos] as
select * from MateriaPrima where Estado = 1;
GO
/****** Object:  View [dbo].[ProductosActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProductosActivos] as
select * from Productos where Estado = 1;
GO
/****** Object:  Table [dbo].[Proveedor]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proveedor](
	[ProveedorID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProveedor] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProveedorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ProveedorActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProveedorActivos] as
select * from Proveedor where Estado = 1;
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[UsuarioID] [int] IDENTITY(1,1) NOT NULL,
	[Identificacion] [varchar](50) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [varchar](100) NOT NULL,
	[NombreCompleto] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UsuarioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[UsuariosActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[UsuariosActivos] as
select * from Usuarios where Estado = 1;
GO
/****** Object:  Table [dbo].[VariablesMaquinaInyectora]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VariablesMaquinaInyectora](
	[VariablesMaquinaInyectoraID] [int] IDENTITY(1,1) NOT NULL,
	[MaquinaID] [int] NOT NULL,
	[ProductoID] [int] NOT NULL,
	[MateriaPrimaPrincipalID] [int] NOT NULL,
	[MateriaPrimaSecundariaID] [int] NULL,
	[MateriaPrimaTerciariaID] [int] NULL,
	[CantidadPrimaPrincipal] [float] NOT NULL,
	[CantidadPrimaSecundaria] [float] NULL,
	[CantidadPrimaTerciaria] [float] NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[VariablesMaquinaInyectoraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VariablesMaquinaInyectoraActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VariablesMaquinaInyectoraActivos] as
select * from VariablesMaquinaInyectora where Estado = 1;
GO
/****** Object:  Table [dbo].[VariablesMaquinaSopladora]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VariablesMaquinaSopladora](
	[VariablesMaquinaSopladoraID] [int] IDENTITY(1,1) NOT NULL,
	[MaquinaID] [int] NOT NULL,
	[ProductoID] [int] NOT NULL,
	[MateriaPrimaID] [int] NOT NULL,
	[RetardoPresoplado] [int] NOT NULL,
	[RetardoSoplado] [int] NOT NULL,
	[TiempoPresoplado] [int] NOT NULL,
	[TiempoRecuperacion] [int] NOT NULL,
	[TiempoSoplado] [int] NOT NULL,
	[TiempoEscape] [int] NOT NULL,
	[TiempoParoCadena] [int] NOT NULL,
	[TiempoSalidaPinzaPreforma] [int] NOT NULL,
	[FrecuenciaVentilador] [float] NOT NULL,
	[PresionEstirado] [float] NOT NULL,
	[Gancho] [bit] NOT NULL,
	[Relay] [bit] NOT NULL,
	[AnillosTornelas] [bit] NOT NULL,
	[Estado] [bit] NOT NULL,
	[Notas] [varchar](100) NULL,
	[CantidadZonas] [int] NOT NULL,
	[CantidadBancos] [int] NOT NULL,
	[PosicionBanco1Zona1] [varchar](100) NULL,
	[PosicionBanco1Zona2] [varchar](100) NULL,
	[PosicionBanco1Zona3] [varchar](100) NULL,
	[PosicionBanco1Zona4] [varchar](100) NULL,
	[PosicionBanco2Zona1] [varchar](100) NULL,
	[PosicionBanco2Zona2] [varchar](100) NULL,
	[PosicionBanco2Zona3] [varchar](100) NULL,
	[PosicionBanco2Zona4] [varchar](100) NULL,
	[ValorBanco1Zona1] [int] NULL,
	[ValorBanco1Zona2] [int] NULL,
	[ValorBanco1Zona3] [int] NULL,
	[ValorBanco1Zona4] [int] NULL,
	[ValorBanco2Zona1] [int] NULL,
	[ValorBanco2Zona2] [int] NULL,
	[ValorBanco2Zona3] [int] NULL,
	[ValorBanco2Zona4] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[VariablesMaquinaSopladoraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VariablesMaquinaSopladoraActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VariablesMaquinaSopladoraActivos] as
select * from VariablesMaquinaSopladora where Estado = 1;
GO
/****** Object:  View [dbo].[MaquinasSopladoraActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MaquinasSopladoraActivos] as
select * from Maquinas where TipoMaquinaID = 1 and Estado = 1;
GO
/****** Object:  View [dbo].[MaquinasInyectoraActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MaquinasInyectoraActivos] as
select * from Maquinas where TipoMaquinaID = 2 and Estado = 1;
GO
/****** Object:  View [dbo].[MateriaPrimaInyectoraActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MateriaPrimaInyectoraActivos] as
select * from MateriaPrima where TipoMateriaPrimaID = 2 or TipoMateriaPrimaID = 3 and Estado = 1
GO
/****** Object:  View [dbo].[MateriaPrimaSopladoraActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MateriaPrimaSopladoraActivos] as
select * from MateriaPrima where TipoMateriaPrimaID = 1 and Estado = 1
GO
/****** Object:  View [dbo].[ProductosBotellaActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProductosBotellaActivos] as
select * from Productos where TipoProductoID = 1 and Estado = 1;
GO
/****** Object:  View [dbo].[ProductosTapaActivos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProductosTapaActivos] as
select * from Productos where TipoProductoID = 2 and Estado = 1;
GO
/****** Object:  Table [dbo].[Estados]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estados](
	[EstadoID] [int] IDENTITY(1,1) NOT NULL,
	[TipoEstado] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Estados] PRIMARY KEY CLUSTERED 
(
	[EstadoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Movimientos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movimientos](
	[PedidoID] [int] NOT NULL,
	[EntregaID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Movimientos] PRIMARY KEY CLUSTERED 
(
	[PedidoID] ASC,
	[EntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoMaquina]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoMaquina](
	[TipoMaquinaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMaquinaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoMateriaPrima]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoMateriaPrima](
	[TipoMateriaPrimaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMateriaPrimaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoMerma]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoMerma](
	[TipoMermaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMermaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoProducto]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoProducto](
	[TipoProductoID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoProductoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsersInRoles]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersInRoles](
	[User_UserID] [int] NOT NULL,
	[Role_RoleID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.RoleUsers] PRIMARY KEY CLUSTERED 
(
	[Role_RoleID] ASC,
	[User_UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AperturaCierre] ON 

INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (1, CAST(N'2020-11-25T16:53:33.000' AS DateTime), NULL, N'Mañana', 52, 1, 12)
SET IDENTITY_INSERT [dbo].[AperturaCierre] OFF
SET IDENTITY_INSERT [dbo].[Clientes] ON 

INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (19, N'610830605', N'Keiko Gordon', N'18476598', N'interdum@CurabiturmassaVestibulum.com', N'Uttar Pradesh', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (20, N'497650452', N'Quyn Velez', N'11975833', N'enim.nisl.elementum@atpretiumaliquet.edu', N'LEN', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (21, N'592833739', N'Micah Kirby', N'62235186', N'consequat.auctor.nunc@odiovelest.org', N'Vienna', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (22, N'520658421', N'Sasha Ratliff', N'44527822', N'adipiscing@dictummagnaUt.com', N'TX', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (23, N'429077125', N'McKenzie Cole', N'48944039', N'dui.in@odioEtiamligula.co.uk', N'Maharastra', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (24, N'902681159', N'Naomi Adams', N'38191805', N'non.dui.nec@scelerisquedui.com', N'SJ', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (25, N'117893803', N'Travis Mccluree', N'89091848', N'cursus@Aliquamadipiscinglobortis.ca', N'RM', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (26, N'380928797', N'Clinton Hines', N'63883206', N'non.ante@velitSed.edu', N'Vienna', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (27, N'783718392', N'Guy Bolton', N'69298809', N'feugiat.non@aliquamarcu.net', N'SMO', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (28, N'642211165', N'Jenna Kerr', N'28189291', N'tortor.at.risus@Nunc.org', N'HH', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (29, N'185552204', N'Forrest Santiago', N'79243979', N'elit.Aliquam.auctor@ProinmiAliquam.edu', N'Cantabria', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (30, N'766323821', N'Griffith Griffin', N'96374699', N'enim@vitaediamProin.ca', N'RJ', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (31, N'675994098', N'Casey Mann', N'75957155', N'rutrum.magna@pretiumaliquet.org', N'Diyarbakir', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (32, N'731865605', N'Marshall Rodriquez', N'14903003', N'eget.mollis.lectus@mattis.co.uk', N'HB', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (33, N'919047907', N'Catherine Pugh', N'90040560', N'inceptos.hymenaeos.Mauris@orciin.co.uk', N'Östergötlands län', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (34, N'691613792', N'Naomi Silva', N'40038551', N'Donec.felis.orci@Aeneanegetmagna.co.uk', N'SI', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (35, N'829472934', N'Stephen Fitzpatrick', N'11021889', N'mollis.vitae@fringilla.org', N'South Kalimantan', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (36, N'451865613', N'Nayda Kelly', N'22472475', N'lacus.Nulla.tincidunt@necurnasuscipit.net', N'Ontario', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (37, N'180400495', N'Bertha Solis', N'25964789', N'sagittis.augue@tellus.net', N'AB', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (38, N'785276376', N'Rudyard Mccarthy', N'41554147', N'et@vitae.ca', N'Ulster', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (39, N'350989313', N'Audrey Stark', N'22461565', N'magnis@orciadipiscing.org', N'AB', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (40, N'463010141', N'Stewart Austin', N'96050284', N'erat.nonummy@felisNullatempor.org', N'Henegouwen', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (41, N'455416293', N'Hamish Randolph', N'93953196', N'molestie@velarcu.ca', N'L', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (42, N'216476790', N'Kylan Baird', N'11175957', N'id.blandit@leoVivamusnibh.co.uk', N'O', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (43, N'452318830', N'Ivor Lyons', N'64788449', N'faucibus@etipsum.ca', N'Bremen', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (44, N'775194566', N'Jesse Gomez', N'56186841', N'Donec.nibh@Donecporttitor.co.uk', N'SP', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (45, N'753974423', N'Sara Thomas', N'95355409', N'nisi.Aenean@duiSuspendisse.edu', N'JUN', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (46, N'658959547', N'Chava Barron', N'86301708', N'ac.risus@placerat.org', N'Adana', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (47, N'470697176', N'Joshua Gaines', N'29278077', N'Vivamus@tortordictumeu.net', N'VLG', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (48, N'402121333', N'Russell Hancock', N'72081220', N'erat@libero.com', N'Mazowieckie', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (49, N'365200530', N'Jakeem Randall', N'53361365', N'est@luctus.edu', N'Noord Holland', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (50, N'764294968', N'Jamal Robinson', N'58457566', N'non.feugiat@ametloremsemper.co.uk', N'YAR', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (51, N'489843663', N'Anika Forbes', N'83439338', N'Nunc.ac.sem@lobortisClassaptent.com', N'RJ', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (52, N'247012451', N'Rhea Fuller', N'51207171', N'ultricies.ornare@sitametdiam.org', N'F', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (53, N'823635737', N'Calvin Wall', N'95170329', N'nulla.magna.malesuada@Sedeu.edu', N'Uttar Pradesh', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (54, N'489848419', N'Indira Merritt', N'67441289', N'aliquam.eros@lectusconvallis.ca', N'New South Wales', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (55, N'284311259', N'Samantha Sharpe', N'84695164', N'dictum.magna@aliquetlobortis.net', N'PO', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (56, N'105036401', N'Rae Nash', N'54354292', N'vitae.semper.egestas@viverra.co.uk', N'IL', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (57, N'488572025', N'Neville Humphrey', N'23216408', N'in.dolor@maurissagittisplacerat.ca', N'M', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (58, N'407089921', N'Kylie Mccoy', N'65078982', N'Cum.sociis@atarcu.co.uk', N'KS', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (59, N'701476367', N'Grady Flowers', N'55959770', N'euismod.in@eratvel.co.uk', N'Rivers', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (60, N'280826669', N'Fritz Roberson', N'27682367', N'massa.rutrum.magna@nonummyacfeugiat.org', N'Emilia-Romagna', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (61, N'570784633', N'Quinn Holt', N'70880555', N'mauris.erat@nulla.com', N'VEN', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (62, N'948831357', N'Micah Snyder', N'63919264', N'aliquam@eutelluseu.org', N'WB', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (63, N'146661975', N'Clio Weber', N'65474174', N'augue@metus.org', N'MA', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (64, N'749458835', N'Phoebe Mathis', N'89467510', N'cursus@Nullamlobortis.ca', N'L', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (65, N'269243494', N'Armand Atkins', N'24070415', N'lorem.ut.aliquam@torquentper.edu', N'LAL', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (66, N'179277770', N'Byron Sherman', N'55645268', N'elit.Etiam.laoreet@urnaNunc.com', N'PUG', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (67, N'406357257', N'Dahlia Summers', N'19450920', N'vitae@feugiat.edu', N'Tyumen Oblast', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (68, N'716168367', N'Aileen Blevins', N'79822534', N'nunc.sed@metusAliquam.com', N'QC', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (69, N'168751844', N'Declan Lott', N'54633878', N'erat@quamdignissim.edu', N'Gyeonggi', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (70, N'369788574', N'Desirae Stokes', N'50020104', N'eu@convallisdolor.net', N'CG', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (71, N'516843405', N'Sawyer Waters', N'14527128', N'Nunc@consectetuereuismod.ca', N'Sindh', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (72, N'866937989', N'Mara Olson', N'16297328', N'diam.Proin@mollisPhaselluslibero.edu', N'KY', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (73, N'776137471', N'Hayley Cummings', N'70779188', N'nunc@ultricesiaculis.ca', N'WA', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (74, N'672313608', N'Sophia Witt', N'20150875', N'lobortis@miDuis.co.uk', N'N.', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (75, N'761250005', N'Hamish Fry', N'72236212', N'tempor@egestasAliquamfringilla.co.uk', N'Ist', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (76, N'680763504', N'Alexis Fernandez', N'48414409', N'blandit@lectus.com', N'Alabama', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (77, N'611206680', N'Ciara Rollins', N'96508536', N'dolor.quam@temporaugueac.ca', N'AB', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (78, N'679176157', N'Kaye Chapman', N'60100725', N'Proin@sociisnatoquepenatibus.ca', N'Comunitat Valenciana', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (79, N'163886784', N'Wanda Booker', N'18835195', N'eu@luctusfelispurus.org', N'CA', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (80, N'884554740', N'Daphne Peck', N'26827001', N'dapibus@ridiculusmusAenean.org', N'Hgo', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (81, N'919755773', N'Bryar Ramos', N'49962399', N'posuere.cubilia@imperdiet.edu', N'NI', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (82, N'644763324', N'Nerea Baker', N'51789314', N'dolor@aceleifendvitae.net', N'OX', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (83, N'666815750', N'Lucius Finch', N'30779391', N'nisl@eu.edu', N'Lombardia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (84, N'466408721', N'Arsenio Vaughn', N'49795835', N'Curae.Phasellus.ornare@a.ca', N'Oaxaca', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (85, N'710352475', N'Chastity Carney', N'30121114', N'fringilla.est@inmolestietortor.org', N'Slaskie', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (86, N'502349354', N'Ryder Vincent', N'95345190', N'faucibus.lectus@tellus.co.uk', N'LA', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (87, N'850514698', N'Garrett Crosby', N'63908742', N'enim@habitant.edu', N'Andalucía', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (88, N'639548679', N'Kirestin Matthews', N'16132639', N'imperdiet@quam.co.uk', N'Chi', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (89, N'499308914', N'Camilla Blevins', N'13352849', N'imperdiet.erat.nonummy@egetdictum.com', N'Malopolskie', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (90, N'539120342', N'Kyle Castillo', N'55543549', N'malesuada.malesuada@tristique.ca', N'Stockholms län', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (91, N'283539010', N'Walker Padilla', N'34207523', N'Nunc@ipsum.com', N'BR', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (92, N'501251413', N'Hollee Wynn', N'80951722', N'auctor.non.feugiat@Nunc.com', N'Antioquia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (93, N'634871311', N'McKenzie Barrett', N'43801703', N'quis.diam@Integer.net', N'Heredia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (94, N'267082196', N'Jordan Fitzpatrick', N'19237708', N'eu.tellus.Phasellus@acrisusMorbi.edu', N'Mexico City', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (95, N'863423651', N'Liberty Massey', N'84792221', N'sem.ut.dolor@id.edu', N'Vienna', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (96, N'743495220', N'Leonard Blackwell', N'93094324', N'tempor.lorem.eget@utsem.co.uk', N'OR', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (97, N'550401800', N'Cyrus Hanson', N'12070362', N'fringilla.porttitor@euaugue.ca', N'North Island', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (98, N'951123733', N'Louis Wilkinson', N'54375356', N'ligula@ipsumportaelit.edu', N'Quebec', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (99, N'742232676', N'Jared Rose', N'42327654', N'conubia@Uttincidunt.edu', N'OH', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (100, N'696218804', N'Melanie Burch', N'32177481', N'turpis@malesuada.com', N'British Columbia', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (101, N'483772942', N'Berk Bartlett', N'87354940', N'nec.enim@Nunc.edu', N'LD', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (102, N'659335304', N'Trevor Levy', N'70786472', N'dolor.elit@lectusjustoeu.net', N'Anambra', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (103, N'894430393', N'Kato Carney', N'90052031', N'velit.justo@urnanecluctus.ca', N'Antioquia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (104, N'236608356', N'Noel Sharpe', N'26398151', N'ipsum@Utsemper.net', N'MD', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (105, N'285447336', N'Rudyard Vance', N'36761113', N'velit.Pellentesque@sit.net', N'AB', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (106, N'252996259', N'Lamar Santiago', N'11976092', N'bibendum.Donec@acliberonec.edu', N'Kaduna', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (107, N'603164410', N'Salvador Herring', N'91067964', N'ut.dolor@dolorNulla.co.uk', N'Vlaams-Brabant', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (108, N'635283914', N'Anika Kinney', N'46393893', N'eros@nasceturridiculusmus.edu', N'C', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (109, N'320451829', N'Nasim Hayes', N'28272354', N'Cras.eu@Etiamligulatortor.net', N'Chiapas', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (110, N'520449488', N'Seth Noel', N'30367174', N'libero@sedpedenec.org', N'West Bengal', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (111, N'778298417', N'Eaton Underwood', N'56386573', N'a.enim@semper.edu', N'L', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (112, N'644466468', N'Vivien Wong', N'88830319', N'semper.dui.lectus@orcitincidunt.co.uk', N'UP', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (113, N'832815276', N'Kenyon Mcmillan', N'29023340', N'quam@euultrices.com', N'UMB', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (114, N'973830179', N'Fuller Crane', N'32218757', N'natoque.penatibus.et@CuraeDonectincidunt.org', N'Antioquia', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (115, N'217090884', N'Megan Hickman', N'10177271', N'pede.nonummy.ut@orci.org', N'Wie', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (116, N'486480302', N'Baxter Durham', N'70148326', N'Sed.pharetra@mauris.edu', N'SO', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (117, N'409183547', N'Stacy Mcneil', N'17881494', N'magna.Suspendisse@cursusluctus.net', N'Leinster', 0)
GO
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (118, N'726656297', N'Bevis Harrell', N'94402165', N'arcu.Nunc@lectusNullam.org', N'Friuli-Venezia Giulia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (120, N'1', N'Bbb', N'123', N'saas', N'asdasd', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (121, N'123456789', N'dsadsasad sdasda', N'4545545454', N'saasdadasd@sadsad.com', N'asdsadaasdsadsad', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (122, N'554454545', N'fddsffdssdsadds', N'54455445', N'sdasdsad@sad.com', N'asdasdasdsadas', 0)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (123, N'12345677554', N'sadadfdsfdsdsf', N'5545454454', N'dsadads@sadas.com', N'asdasdad', 0)
SET IDENTITY_INSERT [dbo].[Clientes] OFF
SET IDENTITY_INSERT [dbo].[DetalleEntrega] ON 

INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (1, 1, 1, 1, 10, 1)
SET IDENTITY_INSERT [dbo].[DetalleEntrega] OFF
SET IDENTITY_INSERT [dbo].[Entregas] ON 

INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (1, N'123456', CAST(N'2020-11-25' AS Date), 10, N'2325', 1)
SET IDENTITY_INSERT [dbo].[Entregas] OFF
SET IDENTITY_INSERT [dbo].[Estados] ON 

INSERT [dbo].[Estados] ([EstadoID], [TipoEstado]) VALUES (1, N'Pendiente')
INSERT [dbo].[Estados] ([EstadoID], [TipoEstado]) VALUES (2, N'Proceso')
INSERT [dbo].[Estados] ([EstadoID], [TipoEstado]) VALUES (3, N'Terminado')
SET IDENTITY_INSERT [dbo].[Estados] OFF
SET IDENTITY_INSERT [dbo].[FichaTecnicaBotella] ON 

INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (2, 7, 5445, 55555, 45, 45, 5454, 45, NULL, NULL, 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (3, 10, 555, 55, 55, 55, 55, 55, NULL, NULL, 0)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (4, 3, 5, 5, 5, 5, 0, NULL, NULL, NULL, 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (5, 3, 545, 5, 45, 45, 454, 44545, NULL, NULL, 0)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (6, 5, 5, 5, 45, 454, 454, 5, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[FichaTecnicaBotella] OFF
SET IDENTITY_INSERT [dbo].[FichaTecnicaTapa] ON 

INSERT [dbo].[FichaTecnicaTapa] ([FichaTecnicaTapaID], [ProductoID], [PesoPieza], [Altura], [DiametroInterno], [DiametroExternoSelloOliva], [AlturaSelloOliva], [DiametroInternoBandaSeguridad], [EspesorPanel], [NumeroEstrias], [TorqueCierreRecomendado], [TorqueApertura], [Tolerancia], [Imagen], [Estado]) VALUES (3, 4, 54, 4, 5, 5, 5, 5, 5, 5, 54, 54, 454, NULL, 0)
INSERT [dbo].[FichaTecnicaTapa] ([FichaTecnicaTapaID], [ProductoID], [PesoPieza], [Altura], [DiametroInterno], [DiametroExternoSelloOliva], [AlturaSelloOliva], [DiametroInternoBandaSeguridad], [EspesorPanel], [NumeroEstrias], [TorqueCierreRecomendado], [TorqueApertura], [Tolerancia], [Imagen], [Estado]) VALUES (4, 16, 54, 454, 4554, 44545, 4545, 454, 4545, 545, 4545, 4545, 54554, NULL, 1)
SET IDENTITY_INSERT [dbo].[FichaTecnicaTapa] OFF
SET IDENTITY_INSERT [dbo].[Maquinas] ON 

INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (12, N'SOPLADORA01', N'Sopladora Semiautomatica', 1, N'Sopladora de color rojo', N'1100', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (13, N'SOPLADORA02', N'Sopladora manual', 1, N'Sopladora grande', N'1100', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (14, N'INYECTORA01', N'Inyectora Alajuela', 2, N'Inyectora Alajuela', N'14000', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (15, N'INYECTORA02', N'Inyectora heredia', 2, N'Inyectora Heredia', N'14000', 0)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (16, N'CORTADORA01', N'Cortadora alajuela', 1, N'Cortadora de tapass', N'2000', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (17, N'aaaa', N'bbbbbb', 2, N'sdffsf', N'454554', 0)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (18, N'asasdd', N'dfdsads', 1, N'asddaasd', N'4545', 0)
SET IDENTITY_INSERT [dbo].[Maquinas] OFF
SET IDENTITY_INSERT [dbo].[MateriaPrima] ON 

INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (4, N'PREF01', N'Preforma 410 10g R20mm', 5, 1, 1, 40000, 25.3600, 1200000.0000, N'Preforma nueva', 1)
INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (5, N'PREF02', N'Preforma 410 21g R24mm', 5, 1, 1, 20000, 30.0000, 600000.0000, N'Preforma nueva 24mm', 1)
INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (6, N'PREF03', N'Preforma 19.7 Transp x 15984', 5, 1, 1, 20000, 44.7100, 123456.0000, N'Preforma transp 19.7G', 1)
INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (7, N'PREF04', N'Preforma 23.7 Transp x 15984', 5, 1, 1, 20000, 39.1800, 987654.0000, N'Preforma transp 23.7G', 1)
INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (8, N'PREF04', N'Preforma PET 1881 17.6G Natural', 6, 2, 1, 2, 37.4400, 5445545.0000, N'Preforma 17.6g', 1)
INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (9, N'RESI01', N'HDPE HI-2053', 6, 2, 1, 2, 37.4400, 5445545.0000, N'Resina Polietileno Alta densidad', 1)
INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (10, N'aaaa', N'aaaaaaa', 6, 2, 2, 4554, 454.0000, 4545.0000, N'dsdasdd', 0)
SET IDENTITY_INSERT [dbo].[MateriaPrima] OFF
SET IDENTITY_INSERT [dbo].[MermaPorTurno] ON 

INSERT [dbo].[MermaPorTurno] ([MermaPorTurnoID], [Cantidad], [AperturaCierreID], [TipoMermaID], [Estado]) VALUES (1, 5, 1, 2, 1)
SET IDENTITY_INSERT [dbo].[MermaPorTurno] OFF
SET IDENTITY_INSERT [dbo].[Pedidos] ON 

INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado], [EstadoID]) VALUES (52, N'123456', 21, N'4444', 23, 1015, 2, 300, N'fdsfds', CAST(N'2020-11-25' AS Date), 55445.0000, 1, 2)
SET IDENTITY_INSERT [dbo].[Pedidos] OFF
SET IDENTITY_INSERT [dbo].[Produccion] ON 

INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (1, 52, CAST(N'2020-11-25T16:53:33.000' AS DateTime), CAST(N'2020-11-25T16:54:18.503' AS DateTime), N'00:00:45.5036410', 5, 15, 2, 1)
SET IDENTITY_INSERT [dbo].[Produccion] OFF
SET IDENTITY_INSERT [dbo].[ProduccionPaquetesCajas] ON 

INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (1, CAST(N'2020-11-25T16:54:00.357' AS DateTime), 5, 1, 1015, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (2, CAST(N'2020-11-25T16:54:18.503' AS DateTime), 10, 1, 1015, N'Mañana', 1)
SET IDENTITY_INSERT [dbo].[ProduccionPaquetesCajas] OFF
SET IDENTITY_INSERT [dbo].[Productos] ON 

INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (2, N'360MLGEN', N'360ml Generico', 1, N'Botella gen', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (3, N'500MLGEN', N'500ml Generico', 1, N'Botella gen', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (4, N'600MLGEN', N'600ml Generico', 1, N'Botella gen', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (5, N'360MLSPO', N'360ml Sport', 1, N'Botella sport', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (6, N'600MLSPO', N'600ml Sport', 1, N'Botella sport', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (7, N'500MLCUAD', N'500ml Cuadrado', 1, N'Botella cuadrada', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (8, N'600MLDP', N'600ml Cuadrado Dos Pinos', 1, N'Botella cuadrada dos pinos', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (9, N'1LTCUAD', N'1LT Cuadrado Dos Pinos', 1, N'Botella cuadrada dos pinos', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (10, N'TAP01', N'Tapa Negra', 2, N'Tapa negra', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (11, N'TAP02', N'Tapa Blanca', 2, N'Tapa blanca', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (12, N'TAP03', N'Tapa Naranja', 2, N'Tapa naranja', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (13, N'TAP04', N'Tapa Transparente', 2, N'Tapa transparente', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (14, N'wqewqew', N'qweqeqe', 2, N'qqqqqq', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (15, N'4454554', N'ghghgh', 2, N'jhhjhjjh', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (16, N'TAP01', N'Tapa Roja', 2, N'Nose', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (17, N'455445', N'sdasddsa', 1, N'sdssad', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (18, N'dsfsf', N'sdfsfd', 2, N'sdfsd', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (19, N'prub', N'sadad', 1, N'asddsaas', NULL, 0)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (20, N'6565', N'Botella Locales', 3, N'Lorem Ipsum es ', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (21, N'25545', N'Botella Máximo', 2, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (22, N'3445', N'áurea Botella', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (23, N'44545', N'Resultado Botella', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (24, N'5545', N'Botella Tempo', 1, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (25, N'6554', N'Tapa Sucualentas', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (26, N'7554', N'Tapa Timer', 2, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (27, N'85445', N'Tapa Aluminios', 2, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (28, N'9554', N'Tapa Flexión', 2, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (29, N'10456', N'Clásica Tapa', 2, N'Lorem Ipsum es simple', N'~/Archivos/Productos/dragon-1721875_960_72020205153815.png', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (30, N'114545', N'óptima Tapa', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (31, N'125445', N'Sepia Tapa', 2, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (32, N'135454', N'Perfil Galon', 1, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (33, N'145445', N'Galon Entornos', 2, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (34, N'1554545', N'Galon Fashionistas', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (35, N'164545', N'Barrera Galon', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (36, N'17555', N'Mútuos Galon', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (37, N'185445', N'Galon Historia', 1, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (38, N'194554', N'Galon Florales', 2, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (39, N'205445', N'Galon Burbujas', 3, N'Lorem Ipsum es simple', NULL, 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (40, N'4454554', N'noseas', 1, N'asdsadds', NULL, 1)
SET IDENTITY_INSERT [dbo].[Productos] OFF
SET IDENTITY_INSERT [dbo].[Proveedor] ON 

INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (5, N'PROV01', N'Polykon', N'253657985', N'servicio@polykon.com', N'Heredia', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (6, N'PROV02', N'Resinas', N'235685695', N'servicio@resinas.com', N'Alajuela', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (7, N'PROV03', N'EPP', N'82632568', N'eppclientes@hotmail.com', N'Alajuela', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (8, N'EMP04', N'Empaques plasticoss', N'5455455', N'empaquesplasticosclientes@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (9, N'ssssssssssssss', N'sssssssssss', N'455454', N'asddaads', N'asdsadsa', 0)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (10, N'44554', N'dsdsasasa', N'44554', N'asdffdsfds@sadas.com', N'adasdasd', 0)
SET IDENTITY_INSERT [dbo].[Proveedor] OFF
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([RoleID], [RoleName]) VALUES (1, N'Administrador')
INSERT [dbo].[Roles] ([RoleID], [RoleName]) VALUES (2, N'Empleado')
SET IDENTITY_INSERT [dbo].[Roles] OFF
SET IDENTITY_INSERT [dbo].[TipoMaquina] ON 

INSERT [dbo].[TipoMaquina] ([TipoMaquinaID], [Tipo]) VALUES (1, N'Sopladora')
INSERT [dbo].[TipoMaquina] ([TipoMaquinaID], [Tipo]) VALUES (2, N'Inyectora')
INSERT [dbo].[TipoMaquina] ([TipoMaquinaID], [Tipo]) VALUES (4, N'Otro')
SET IDENTITY_INSERT [dbo].[TipoMaquina] OFF
SET IDENTITY_INSERT [dbo].[TipoMateriaPrima] ON 

INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (1, N'Preforma')
INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (2, N'Resina')
INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (3, N'Colorante')
INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (4, N'Otro')
SET IDENTITY_INSERT [dbo].[TipoMateriaPrima] OFF
SET IDENTITY_INSERT [dbo].[TipoMerma] ON 

INSERT [dbo].[TipoMerma] ([TipoMermaID], [Tipo]) VALUES (1, N'Descuido operacional')
INSERT [dbo].[TipoMerma] ([TipoMermaID], [Tipo]) VALUES (2, N'Ajuste de máquina')
INSERT [dbo].[TipoMerma] ([TipoMermaID], [Tipo]) VALUES (3, N'Producto fuera especificación')
INSERT [dbo].[TipoMerma] ([TipoMermaID], [Tipo]) VALUES (4, N'Color fuera espeficicación')
SET IDENTITY_INSERT [dbo].[TipoMerma] OFF
SET IDENTITY_INSERT [dbo].[TipoProducto] ON 

INSERT [dbo].[TipoProducto] ([TipoProductoID], [Tipo]) VALUES (1, N'Botella')
INSERT [dbo].[TipoProducto] ([TipoProductoID], [Tipo]) VALUES (2, N'Tapa')
INSERT [dbo].[TipoProducto] ([TipoProductoID], [Tipo]) VALUES (3, N'Otro')
SET IDENTITY_INSERT [dbo].[TipoProducto] OFF
SET IDENTITY_INSERT [dbo].[UnidadMedida] ON 

INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (1, N'Unidades')
INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (2, N'Kilogramos')
INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (3, N'Tarimas')
INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (4, N'Cajas')
INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (5, N'Bolsas')
INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (6, N'Otro')
SET IDENTITY_INSERT [dbo].[UnidadMedida] OFF
INSERT [dbo].[UsersInRoles] ([User_UserID], [Role_RoleID]) VALUES (1014, 1)
INSERT [dbo].[UsersInRoles] ([User_UserID], [Role_RoleID]) VALUES (1015, 1)
INSERT [dbo].[UsersInRoles] ([User_UserID], [Role_RoleID]) VALUES (1027, 2)
SET IDENTITY_INSERT [dbo].[Usuarios] ON 

INSERT [dbo].[Usuarios] ([UsuarioID], [Identificacion], [UserName], [Password], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (1014, N'876543216', N'hansel', N'de4ad303211af5bd002b69d07ce48d1fd8b0b537f7fc34a285eca4d0dea6ac50', N'Hansel Murillo', N'875360236', N'hanse@hotmail.com', N'Heredia', 1)
INSERT [dbo].[Usuarios] ([UsuarioID], [Identificacion], [UserName], [Password], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (1015, N'123456789', N'andres', N'259b77456afddaeb7dc27ce6c5f92cb3cfc6815eadeba5720e5c68f8f82e1d0e', N'Andres Rodriguezz', N'86098503', N'andresrodrig10@hotmail.com', N'Santa', 1)
INSERT [dbo].[Usuarios] ([UsuarioID], [Identificacion], [UserName], [Password], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (1027, N'123456789', N'bryan', N'259b77456afddaeb7dc27ce6c5f92cb3cfc6815eadeba5720e5c68f8f82e1d0e', N'Bryan Montiel', N'883625698', N'bryan@hotmail.com', N'Alajuela', 1)
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
SET IDENTITY_INSERT [dbo].[VariablesMaquinaInyectora] ON 

INSERT [dbo].[VariablesMaquinaInyectora] ([VariablesMaquinaInyectoraID], [MaquinaID], [ProductoID], [MateriaPrimaPrincipalID], [MateriaPrimaSecundariaID], [MateriaPrimaTerciariaID], [CantidadPrimaPrincipal], [CantidadPrimaSecundaria], [CantidadPrimaTerciaria], [Estado]) VALUES (2, 15, 4, 9, NULL, NULL, 55555, NULL, NULL, 0)
INSERT [dbo].[VariablesMaquinaInyectora] ([VariablesMaquinaInyectoraID], [MaquinaID], [ProductoID], [MateriaPrimaPrincipalID], [MateriaPrimaSecundariaID], [MateriaPrimaTerciariaID], [CantidadPrimaPrincipal], [CantidadPrimaSecundaria], [CantidadPrimaTerciaria], [Estado]) VALUES (3, 15, 11, 9, NULL, NULL, 454, 4545, 4545, 0)
INSERT [dbo].[VariablesMaquinaInyectora] ([VariablesMaquinaInyectoraID], [MaquinaID], [ProductoID], [MateriaPrimaPrincipalID], [MateriaPrimaSecundariaID], [MateriaPrimaTerciariaID], [CantidadPrimaPrincipal], [CantidadPrimaSecundaria], [CantidadPrimaTerciaria], [Estado]) VALUES (4, 14, 16, 9, 9, NULL, 1, 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[VariablesMaquinaInyectora] OFF
SET IDENTITY_INSERT [dbo].[VariablesMaquinaSopladora] ON 

INSERT [dbo].[VariablesMaquinaSopladora] ([VariablesMaquinaSopladoraID], [MaquinaID], [ProductoID], [MateriaPrimaID], [RetardoPresoplado], [RetardoSoplado], [TiempoPresoplado], [TiempoRecuperacion], [TiempoSoplado], [TiempoEscape], [TiempoParoCadena], [TiempoSalidaPinzaPreforma], [FrecuenciaVentilador], [PresionEstirado], [Gancho], [Relay], [AnillosTornelas], [Estado], [Notas], [CantidadZonas], [CantidadBancos], [PosicionBanco1Zona1], [PosicionBanco1Zona2], [PosicionBanco1Zona3], [PosicionBanco1Zona4], [PosicionBanco2Zona1], [PosicionBanco2Zona2], [PosicionBanco2Zona3], [PosicionBanco2Zona4], [ValorBanco1Zona1], [ValorBanco1Zona2], [ValorBanco1Zona3], [ValorBanco1Zona4], [ValorBanco2Zona1], [ValorBanco2Zona2], [ValorBanco2Zona3], [ValorBanco2Zona4]) VALUES (2, 13, 6, 7, 4545, 45445445, 4554, 54, 4545, 45, 4545, 54545, 57, 55445, 1, 1, 1, 0, N'fdfdfd', 5, 5, N'5', N'5', N'5', N'5', N'5', N'5', N'5', N'5', 5, 5, 5, 5, 5, 5, 5, 5)
INSERT [dbo].[VariablesMaquinaSopladora] ([VariablesMaquinaSopladoraID], [MaquinaID], [ProductoID], [MateriaPrimaID], [RetardoPresoplado], [RetardoSoplado], [TiempoPresoplado], [TiempoRecuperacion], [TiempoSoplado], [TiempoEscape], [TiempoParoCadena], [TiempoSalidaPinzaPreforma], [FrecuenciaVentilador], [PresionEstirado], [Gancho], [Relay], [AnillosTornelas], [Estado], [Notas], [CantidadZonas], [CantidadBancos], [PosicionBanco1Zona1], [PosicionBanco1Zona2], [PosicionBanco1Zona3], [PosicionBanco1Zona4], [PosicionBanco2Zona1], [PosicionBanco2Zona2], [PosicionBanco2Zona3], [PosicionBanco2Zona4], [ValorBanco1Zona1], [ValorBanco1Zona2], [ValorBanco1Zona3], [ValorBanco1Zona4], [ValorBanco2Zona1], [ValorBanco2Zona2], [ValorBanco2Zona3], [ValorBanco2Zona4]) VALUES (3, 13, 3, 5, 25, 5, 55, 5, 5, 5, 5, 5, 55, 54, 1, 1, 0, 1, N'dffsd', 0, 0, N'5', NULL, N'5', NULL, NULL, NULL, NULL, NULL, 5, NULL, 5, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[VariablesMaquinaSopladora] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [UC_Usuarios]    Script Date: 25/11/2020 5:15:00 p. m. ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [UC_Usuarios] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AperturaCierre] ADD  CONSTRAINT [DF_AperturaCierre_estado]  DEFAULT ((1)) FOR [Estado]
GO
ALTER TABLE [dbo].[DetalleEntrega] ADD  CONSTRAINT [DF_DetalleEntrega_Detalle]  DEFAULT ((1)) FOR [Estado]
GO
ALTER TABLE [dbo].[Entregas] ADD  CONSTRAINT [DF_Entregas_Estado]  DEFAULT ((1)) FOR [Estado]
GO
ALTER TABLE [dbo].[Pedidos] ADD  CONSTRAINT [DF_Pedidos_EstadoID]  DEFAULT ((1)) FOR [EstadoID]
GO
ALTER TABLE [dbo].[AperturaCierre]  WITH CHECK ADD  CONSTRAINT [FK_AperturaCierre_PedidoID] FOREIGN KEY([PedidoID])
REFERENCES [dbo].[Pedidos] ([PedidoID])
GO
ALTER TABLE [dbo].[AperturaCierre] CHECK CONSTRAINT [FK_AperturaCierre_PedidoID]
GO
ALTER TABLE [dbo].[DetalleEntrega]  WITH CHECK ADD  CONSTRAINT [FK_DetalleEntrega_Entregas] FOREIGN KEY([EntregaID])
REFERENCES [dbo].[Entregas] ([EntregaID])
GO
ALTER TABLE [dbo].[DetalleEntrega] CHECK CONSTRAINT [FK_DetalleEntrega_Entregas]
GO
ALTER TABLE [dbo].[DetalleEntrega]  WITH CHECK ADD  CONSTRAINT [FK_DetalleEntrega_Produccion] FOREIGN KEY([ProduccionID])
REFERENCES [dbo].[Produccion] ([ProduccionID])
GO
ALTER TABLE [dbo].[DetalleEntrega] CHECK CONSTRAINT [FK_DetalleEntrega_Produccion]
GO
ALTER TABLE [dbo].[FichaTecnicaBotella]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_FichaTecnicaBotella] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[FichaTecnicaBotella] CHECK CONSTRAINT [FK_ProductoID_FichaTecnicaBotella]
GO
ALTER TABLE [dbo].[FichaTecnicaTapa]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_FichaTecnicaTapa] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[FichaTecnicaTapa] CHECK CONSTRAINT [FK_ProductoID_FichaTecnicaTapa]
GO
ALTER TABLE [dbo].[Maquinas]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_Maquinas] FOREIGN KEY([TipoMaquinaID])
REFERENCES [dbo].[TipoMaquina] ([TipoMaquinaID])
GO
ALTER TABLE [dbo].[Maquinas] CHECK CONSTRAINT [FK_TipoID_Maquinas]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_ProveedorID_MateriaPrima] FOREIGN KEY([ProveedorID])
REFERENCES [dbo].[Proveedor] ([ProveedorID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_ProveedorID_MateriaPrima]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_MateriaPrima] FOREIGN KEY([TipoMateriaPrimaID])
REFERENCES [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_TipoID_MateriaPrima]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_UnidadMedidaID_MateriaPrima] FOREIGN KEY([UnidadMedidaID])
REFERENCES [dbo].[UnidadMedida] ([UnidadMedidaID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_UnidadMedidaID_MateriaPrima]
GO
ALTER TABLE [dbo].[MermaPorTurno]  WITH CHECK ADD FOREIGN KEY([AperturaCierreID])
REFERENCES [dbo].[AperturaCierre] ([AperturaCierreID])
GO
ALTER TABLE [dbo].[MermaPorTurno]  WITH CHECK ADD FOREIGN KEY([TipoMermaID])
REFERENCES [dbo].[TipoMerma] ([TipoMermaID])
GO
ALTER TABLE [dbo].[Movimientos]  WITH CHECK ADD  CONSTRAINT [FK_EntregaID_Movimientos] FOREIGN KEY([EntregaID])
REFERENCES [dbo].[Entregas] ([EntregaID])
GO
ALTER TABLE [dbo].[Movimientos] CHECK CONSTRAINT [FK_EntregaID_Movimientos]
GO
ALTER TABLE [dbo].[Movimientos]  WITH CHECK ADD  CONSTRAINT [FK_PedidoID_Movimientos] FOREIGN KEY([PedidoID])
REFERENCES [dbo].[Pedidos] ([PedidoID])
GO
ALTER TABLE [dbo].[Movimientos] CHECK CONSTRAINT [FK_PedidoID_Movimientos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_ClienteID_Pedidos] FOREIGN KEY([ClienteID])
REFERENCES [dbo].[Clientes] ([ClienteID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_ClienteID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_Pedidos_Estados] FOREIGN KEY([EstadoID])
REFERENCES [dbo].[Estados] ([EstadoID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_Pedidos_Estados]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_Pedidos] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_ProductoID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_UnidadMedidaID_Pedidos] FOREIGN KEY([UnidadMedidaID])
REFERENCES [dbo].[UnidadMedida] ([UnidadMedidaID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_UnidadMedidaID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioID_Pedidos] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_UsuarioID_Pedidos]
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas]  WITH CHECK ADD  CONSTRAINT [FK_ProduccionPaquetesCajas_ProduccionPaquetesCajas] FOREIGN KEY([AperturaCierreID])
REFERENCES [dbo].[AperturaCierre] ([AperturaCierreID])
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas] CHECK CONSTRAINT [FK_ProduccionPaquetesCajas_ProduccionPaquetesCajas]
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas]  WITH CHECK ADD  CONSTRAINT [FK_ProduccionPaquetesCajas_Usuarios] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas] CHECK CONSTRAINT [FK_ProduccionPaquetesCajas_Usuarios]
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_Productos] FOREIGN KEY([TipoProductoID])
REFERENCES [dbo].[TipoProducto] ([TipoProductoID])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [FK_TipoID_Productos]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK_Role_RoleID_UserInRoles] FOREIGN KEY([Role_RoleID])
REFERENCES [dbo].[Roles] ([RoleID])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [FK_Role_RoleID_UserInRoles]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK_User_UserID_UserInRoles] FOREIGN KEY([User_UserID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [FK_User_UserID_UserInRoles]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MaquinaID_VariablesMaquinaInyectora] FOREIGN KEY([MaquinaID])
REFERENCES [dbo].[Maquinas] ([MaquinaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MaquinaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaPrincipalID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaPrincipalID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaPrincipalID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaSecundariaID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaSecundariaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaSecundariaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaTerciariaID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaTerciariaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaTerciariaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_VariablesMaquinaInyectora] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_ProductoID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_MaquinaID_VariablesMaquinaSopladora] FOREIGN KEY([MaquinaID])
REFERENCES [dbo].[Maquinas] ([MaquinaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_MaquinaID_VariablesMaquinaSopladora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaID_VariablesMaquinaSopladora] FOREIGN KEY([MateriaPrimaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_MateriaPrimaID_VariablesMaquinaSopladora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_VariablesMaquinaSopladora] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_ProductoID_VariablesMaquinaSopladora]
GO
/****** Object:  StoredProcedure [dbo].[sp_DatosEtiqueta]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_DatosEtiqueta]
@Etiqueta int 

as
begin
		Select produccion.EtiquetaID, produccion.Fecha, produccion.Cantidad, produccion.Turno, usuario.NombreCompleto as Usuario,
		pedido.NumeroPedido, pedido.NumeroOrdenDeCompra, cliente.NombreCompleto as Cliente, cliente.Direccion, producto.Nombre as Producto,
		um.Unidad, maquina.Nombre as maquina

		from ProduccionPaquetesCajas as produccion
		join Usuarios as usuario on produccion.UsuarioID = usuario.UsuarioID
		join AperturaCierre as apertura on produccion.AperturaCierreID = apertura.AperturaCierreID
		join Pedidos as pedido on apertura.PedidoID = pedido.PedidoID
		join Clientes as cliente on pedido.ClienteID = cliente.ClienteID 
		join Productos as producto on pedido.ProductoID = producto.ProductoID		
		join UnidadMedida as um on pedido.UnidadMedidaID = um.UnidadMedidaID
		join Maquinas as maquina on apertura.MaquinaID = maquina.MaquinaID

		where EtiquetaID = @Etiqueta
end
GO
/****** Object:  StoredProcedure [dbo].[sp_FichaOperacion]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_FichaOperacion]
@Pedido int

as
begin

if (select  p.ProductoID from Pedidos p  join VariablesMaquinaInyectora vi on vi.ProductoID = p.ProductoID where p.PedidoID = @Pedido) > 0

select ('inyectora') as FichaOperacion , p.ProductoID 
from Pedidos p  join VariablesMaquinaInyectora vi on vi.ProductoID = p.ProductoID where p.PedidoID = @Pedido

 


else if (select  p.ProductoID from Pedidos p  join VariablesMaquinaSopladora vs on vs.ProductoID = p.ProductoID where p.PedidoID = @Pedido) > 0

select ('sopladora') as FichaOperacion, p.ProductoID from Pedidos p  join VariablesMaquinaSopladora vs on vs.ProductoID = p.ProductoID where p.PedidoID = @Pedido

else

select ('nada') as FichaOperacion ,p.ProductoID from Pedidos p  join VariablesMaquinaSopladora vs on vs.ProductoID = p.ProductoID where p.PedidoID = @Pedido

end
GO
/****** Object:  StoredProcedure [dbo].[sp_FichaOperacionInyectora]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_FichaOperacionInyectora]
@Pedido int

as
begin



Select p.NumeroPedido, p.NumeroOrdenDeCompra, p.Cantidad, p.Descripcion, p.Fecha, u.NombreCompleto as usuario,c.NombreCompleto, pd.Nombre as producto,
 m.Nombre, vi.CantidadPrimaPrincipal, vi.CantidadPrimaSecundaria, vi.CantidadPrimaTerciaria,
mp.Nombre as mp1, mps.Nombre as mp2, mpt.Nombre as mp3

from Pedidos p
join Usuarios u on p.UsuarioID = u.UsuarioID
join Clientes c on p.ClienteID = c.ClienteID
join Productos pd on p.ProductoID = pd.ProductoID
join VariablesMaquinaInyectora vi on vi.ProductoID = p.ProductoID
join Maquinas m on vi.MaquinaID = m.MaquinaID
join MateriaPrima mp on vi.MateriaPrimaPrincipalID = mp.MateriaPrimaID
join MateriaPrima mps on vi.MateriaPrimaSecundariaID = mps.MateriaPrimaID
join MateriaPrima mpt on vi.MateriaPrimaTerciariaID = mpt.MateriaPrimaID

where p.PedidoID = @Pedido;



end
GO
/****** Object:  StoredProcedure [dbo].[sp_FichaOperacionSopladora]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[sp_FichaOperacionSopladora]
@Pedido int

as
begin

Select p.NumeroPedido, p.NumeroOrdenDeCompra, p.Cantidad, p.Descripcion, p.Fecha, u.NombreCompleto as usuario,c.NombreCompleto as cliente, pd.Nombre as producto,
 m.Nombre as maquina, 
 mp.Nombre as MateriaPrima
      ,[RetardoPresoplado]
      ,[RetardoSoplado] 
      ,[TiempoPresoplado] 
      ,[TiempoRecuperacion] 
      ,[TiempoSoplado]
      ,[TiempoEscape]
      ,[TiempoParoCadena] 
      ,[TiempoSalidaPinzaPreforma] 
      ,[FrecuenciaVentilador] 
      ,[PresionEstirado] 
      ,[Gancho] 
      ,[Relay] 
      ,[AnillosTornelas] 
      ,[Notas] 
      ,[CantidadZonas] 
      ,[CantidadBancos] 
      ,[PosicionBanco1Zona1] 
      ,[PosicionBanco1Zona2]
      ,[PosicionBanco1Zona3]
      ,[PosicionBanco1Zona4] 
      ,[PosicionBanco2Zona1] 
      ,[PosicionBanco2Zona2] 
      ,[PosicionBanco2Zona3] 
      ,[PosicionBanco2Zona4] 
      ,[ValorBanco1Zona1] 
      ,[ValorBanco1Zona2] 
      ,[ValorBanco1Zona3]
      ,[ValorBanco1Zona4] 
      ,[ValorBanco2Zona1] 
      ,[ValorBanco2Zona2] 
      ,[ValorBanco2Zona3] 
      ,[ValorBanco2Zona4] 


from Pedidos p
join Usuarios u on p.UsuarioID = u.UsuarioID
join Clientes c on p.ClienteID = c.ClienteID
join Productos pd on p.ProductoID = pd.ProductoID
join VariablesMaquinaSopladora vs on vs.ProductoID = p.ProductoID
join Maquinas m on vs.MaquinaID = m.MaquinaID
join MateriaPrima mp on vs.MateriaPrimaID = mp.MateriaPrimaID


where p.PedidoID = @Pedido;



end
GO
/****** Object:  StoredProcedure [dbo].[sp_getRolesForUser]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*Procedimientos almacenados*/


create PROCEDURE [dbo].[sp_getRolesForUser]
	@userName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	select r.RoleName from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioID
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where u.UserName=@userName
END
GO
/****** Object:  StoredProcedure [dbo].[sp_getUsuariosRole]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_getUsuariosRole]
	
	@roleName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	select u.UserName from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioID
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where r.RoleName=@roleName 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GraficoParettoMerma]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_GraficoParettoMerma]
as
Select  TipoMerma.Tipo, sum(MermaPorTurno.Cantidad)as Cantidad
from MermaPorTurno
join TipoMerma on TipoMerma.TipoMermaID = MermaPorTurno.TipoMermaID

GROUP BY
TipoMerma.Tipo

order by cantidad desc
GO
/****** Object:  StoredProcedure [dbo].[sp_GraficoPedidos]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_GraficoPedidos] AS

select count(*) as PedidosProceso, (
select count(*) PedidosPendientes from PedidosActivos p 
 left join Produccion on p.PedidoID = Produccion.PedidoID where p.PedidoID not in (select PedidoID from Produccion))
 as PedidosPendientes
 ,(select count(*)

from PedidosActivos
 
join Produccion on PedidosActivos.PedidoID = Produccion.PedidoID
 
where PedidosActivos.PedidoID = Produccion.PedidoID and Produccion.CantidadProduccionTotal >= PedidosActivos.Cantidad
) as PedidosTerminados

from PedidosActivos
 
join Produccion on PedidosActivos.PedidoID = Produccion.PedidoID
 
where PedidosActivos.PedidoID = Produccion.PedidoID and Produccion.CantidadProduccionTotal >= 0 and Produccion.CantidadProduccionTotal < PedidosActivos.Cantidad


GO
/****** Object:  StoredProcedure [dbo].[sp_GraficoProduccion]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_GraficoProduccion] AS


SELECT DATEPART(month, Fecha) as mes, SUM(Cantidad) as total
FROM ProduccionPaquetesCajaActivos

GROUP BY DATEPART(month, Fecha)
GO
/****** Object:  StoredProcedure [dbo].[sp_isUserInRole]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_isUserInRole]
	@userName varchar(20),
	@roleName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	declare @resultado bit =0;
	if exists(
	select * from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioId
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where r.RoleName=@roleName and u.UserName=@userName)
	set @resultado = 1
	 select @resultado
END


GO
/****** Object:  StoredProcedure [dbo].[sp_setSpecificRoleForUser]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_setSpecificRoleForUser]
	@UsuarioID int,
	@RoleID int
AS
BEGIN
	SET NOCOUNT ON;
	delete from UsersInRoles where User_UserID = @UsuarioID;
	insert into UsersInRoles values(@UsuarioID,@RoleID);
END

GO
/****** Object:  Trigger [dbo].[TRG_INS_Produccion]    Script Date: 25/11/2020 5:15:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRG_INS_Produccion]
ON [dbo].[AperturaCierre]
FOR INSERT
AS
BEGIN
		SET NOCOUNT ON;	

		IF NOT EXISTS( SELECT PedidoID FROM [Produccion] WHERE PedidoID =  (select top 1 PedidoID from AperturaCierre order by AperturaCierreID desc))
			begin
				INSERT INTO [dbo].[Produccion]
						   ([PedidoID]
						   ,[FechaInicio]
						   ,[FechaFin]
						   ,[HorasTrabajadas]
						   ,[CantidadMerma]
						   ,[CantidadProduccionTotal]
						   ,[CantidadPaquetesCajas]
						   ,[Estado])
					 VALUES
						   ((select top 1 PedidoID from AperturaCierre order by AperturaCierreID desc)
						   ,(select top 1 FechaApertura from AperturaCierre order by AperturaCierreID desc)
						   ,null
						   ,null
						   ,0
						   ,0
						   ,0
						   ,1)
			end


		END
GO
ALTER TABLE [dbo].[AperturaCierre] ENABLE TRIGGER [TRG_INS_Produccion]
GO
/****** Object:  Trigger [dbo].[TRG_INS_MermaProduccion]    Script Date: 25/11/2020 5:15:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create TRIGGER [dbo].[TRG_INS_MermaProduccion]
ON [dbo].[MermaPorTurno]
FOR INSERT
AS
BEGIN
		SET NOCOUNT ON;	

			begin
			
					UPDATE [dbo].[Produccion]
					   SET 
						  [CantidadMerma] = (select top 1 Cantidad from MermaPorTurno order by MermaPorTurnoID desc)
						  
					 WHERE Produccion.PedidoID = (

						select top 1 AperturaCierre.PedidoID 
						from AperturaCierre 
							join MermaPorTurno on  AperturaCierre.AperturaCierreID = MermaPorTurno.AperturaCierreID
						where AperturaCierre.AperturaCierreID = MermaPorTurno.AperturaCierreID
						 order by  AperturaCierre.AperturaCierreID desc
						)
				
end


	END
GO
ALTER TABLE [dbo].[MermaPorTurno] ENABLE TRIGGER [TRG_INS_MermaProduccion]
GO
USE [master]
GO
ALTER DATABASE [DIPREMA] SET  READ_WRITE 
GO
