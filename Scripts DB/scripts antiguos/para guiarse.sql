USE [master]
GO
DROP database if exists [SISPRO]
GO
CREATE database [SISPRO]
GO
USE [SISPRO]
GO
CREATE TABLE [dbo].[Clientes](
	[ClienteID] int IDENTITY(1,1) NOT NULL,
	[CodigoCliente] varchar(50) NOT NULL,
	[Nombre] varchar(50) NOT NULL,
	[Telefono] varchar(50) null,
	[Correo] varchar(50) null,
	[Direccion] varchar(50) null,
	[Estatus] bit not null,
PRIMARY KEY CLUSTERED 
(
	[ClienteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Usuarios](
	[UsuarioID] int IDENTITY(1,1) NOT NULL,
	[CodigoUsuario] varchar(50) NOT NULL,
	[UserName] varchar(50) NOT NULL,
	[Password] varchar(100) NOT NULL,
	[NombreCompleto] varchar(50) NOT NULL,
	[Telefono] varchar(50) null,
	[Correo] varchar(50) null,
	[Direccion] varchar(50) null,
	[Estatus] bit not null,
PRIMARY KEY CLUSTERED 
(
	[UsuarioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Roles](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [dbo].[UsersInRoles](
	[User_UserID] [int] NOT NULL,
	[Role_RoleID] [int] NOT NULL,
	CONSTRAINT FK_User_UserID_UserInRoles FOREIGN KEY (User_UserID)
    REFERENCES Usuarios(UsuarioID),
	CONSTRAINT FK_Role_RoleID_UserInRoles FOREIGN KEY (Role_RoleID)
    REFERENCES Roles(RoleID),
 CONSTRAINT [PK_dbo.RoleUsers] PRIMARY KEY CLUSTERED 
(
	[Role_RoleID] ASC,
	[User_UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Proveedor](
	[ProveedorID] int IDENTITY(1,1) NOT NULL,
	[CodigoProveedor] varchar(50) NOT NULL,
	[Nombre] varchar(50) NOT NULL,
	[Telefono] varchar(50) null,
	[Correo] varchar(50) null,
	[Direccion] varchar(50) null,
	[Estatus] bit not null,
PRIMARY KEY CLUSTERED 
(
	[ProveedorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[TipoMateriaPrima](
	[TipoMateriaPrimaID] int IDENTITY(1,1) NOT NULL,
	[Descripcion] varchar(100) not null,
PRIMARY KEY CLUSTERED 
(
	[TipoMateriaPrimaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[TipoProducto](
	[TipoProductoID] int IDENTITY(1,1) NOT NULL,
	[Descripcion] varchar(100) not null,
PRIMARY KEY CLUSTERED 
(
	[TipoProductoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[TipoMaquina](
	[TipoMaquinaID] int IDENTITY(1,1) NOT NULL,
	[Descripcion] varchar(100) not null,
PRIMARY KEY CLUSTERED 
(
	[TipoMaquinaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[UnidadMedida](
	[UnidadMedidaID] int IDENTITY(1,1) NOT NULL,
	[Descripcion] varchar(100) not null,
PRIMARY KEY CLUSTERED 
(
	[UnidadMedidaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[MateriaPrima](
	[MateriaPrimaID] int IDENTITY(1,1) NOT NULL,
	[CodigoMateriaPrima] varchar(50) NOT NULL,
	[Nombre] varchar(50) NOT NULL,
	[ProveedorID] int not null,
	[TipoMateriaPrimaID] int not null,
	[UnidadMedidaID] int not null,
	[Cantidad] int not null,
	[PrecioUnitario] money not null,
	[PrecioTotal] money not null,
	[Descripcion] varchar(100) null,
	[Estatus] bit not null,
	CONSTRAINT FK_ProveedorID_MateriaPrima FOREIGN KEY (ProveedorID)
    REFERENCES Proveedor(ProveedorID),
	CONSTRAINT FK_TipoID_MateriaPrima FOREIGN KEY (TipoMateriaPrimaID)
    REFERENCES TipoMateriaPrima(TipoMateriaPrimaID),
	CONSTRAINT FK_UnidadMedidaID_MateriaPrima FOREIGN KEY (UnidadMedidaID)
    REFERENCES UnidadMedida(UnidadMedidaID),
PRIMARY KEY CLUSTERED 
(
	[MateriaPrimaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Maquinas](
	[MaquinaID] int IDENTITY(1,1) NOT NULL,
	[CodigoMaquina] varchar(50) NOT NULL,
	[Nombre] varchar(50) NOT NULL,
	[TipoMaquinaID] int not null,
	[Descripcion] varchar(100) null,
	[ProduccionHora] varchar(100) null,
	[Estatus] bit not null,
	CONSTRAINT FK_TipoID_Maquinas FOREIGN KEY (TipoMaquinaID)
    REFERENCES TipoMaquina(TipoMaquinaID),
PRIMARY KEY CLUSTERED 
(
	[MaquinaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Productos](
	[ProductoID] int IDENTITY(1,1) NOT NULL,
	[CodigoProducto] varchar(50) NOT NULL,
	[Nombre] varchar(50) NOT NULL,
	[TipoProductoID] int not null,
	[UnidadMedidaID] int not null,
	[Cantidad] int not null,
	[PrecioUnitario] money not null,
	[PrecioTotal] money not null,
	[Descripcion] varchar(100) null,
	[Imagen] image null,
	[Estatus] bit not null,
	CONSTRAINT FK_TipoID_Productos FOREIGN KEY (TipoProductoID)
    REFERENCES TipoProducto(TipoProductoID),
	CONSTRAINT FK_UnidadMedidaID_Productos FOREIGN KEY (UnidadMedidaID)
    REFERENCES UnidadMedida(UnidadMedidaID),
PRIMARY KEY CLUSTERED 
(
	[ProductoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Pedidos](
	[PedidoID] int IDENTITY(1,1) NOT NULL,
	[CodigoPedido] varchar(50) NOT NULL,
	[ClienteID] int NOT NULL,
	[NumeroOrdenDeCompra] varchar(50) null,
	[ProductoID] int not null,
	[UsuarioID] int not null,
	[UnidadMedidaID] int not null,
	[Cantidad] int not null,
	[Descripcion] varchar(100) null,
	[Fecha] date null,
	[PrecioTotal] money not null,
	[Estatus] bit not null,
	CONSTRAINT FK_ClienteID_Pedidos FOREIGN KEY (ClienteID)
    REFERENCES Clientes(ClienteID),
	CONSTRAINT FK_UsuarioID_Pedidos FOREIGN KEY (UsuarioID)
    REFERENCES Usuarios(UsuarioID),
	CONSTRAINT FK_ProductoID_Pedidos FOREIGN KEY (ProductoID)
    REFERENCES Productos(ProductoID),
	CONSTRAINT FK_UnidadMedidaID_Pedidos FOREIGN KEY (UnidadMedidaID)
    REFERENCES UnidadMedida(UnidadMedidaID),
PRIMARY KEY CLUSTERED 
(
	[PedidoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Entregas](
	[EntregaID] int IDENTITY(1,1) NOT NULL,
	[CodigoEntrega] varchar(50) not null,
	[Fecha] date null,
	[UnidadesEntregadas] int null,
	[NumeroFactura] varchar(50),
	[Estatus] bit not null,
PRIMARY KEY CLUSTERED 
(
	[EntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Movimientos](
	[PedidoID] int not null,
	[EntregaID] int not null,
	CONSTRAINT FK_EntregaID_Movimientos FOREIGN KEY (EntregaID)
    REFERENCES Entregas(EntregaID),
	CONSTRAINT FK_PedidoID_Movimientos FOREIGN KEY (PedidoID)
    REFERENCES Pedidos(PedidoID),
CONSTRAINT [PK_dbo.Movimientos] PRIMARY KEY CLUSTERED 
(
	[PedidoID] ASC,
	[EntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[FichaTecnicaBotella](
	[FichaTecnicaBotellaID] int IDENTITY(1,1) NOT NULL,
	[ProductoID] int not null,
	[Altura] float not null,
	[DiametroMayor] float not null,
	[DiametroMenor] float not null,
	[DiametroCuello] float not null,
	[CapacidadVolumetrica] float not null,
	[Tolerancia] float null,
	[ImagenCuerpo] image null,
	[ImagenFondo] image null,
	[Estatus] bit not null,
	CONSTRAINT FK_ProductoID_FichaTecnicaBotella FOREIGN KEY (ProductoID)
    REFERENCES Productos(ProductoID),
PRIMARY KEY CLUSTERED 
(
	[FichaTecnicaBotellaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[FichaTecnicaTapa](
	[FichaTecnicaTapaID] int IDENTITY(1,1) NOT NULL,
	[ProductoID] int not null,
	[PesoPieza] float not null,
	[Altura] float not null,
	[DiametroInterno] float not null,
	[DiametroExternoSelloOliva] float not null,
	[AlturaSelloOliva] float not null,
	[DiametroInternoBandaSeguridad] float not null,
	[EspesorPanel] float not null,
	[NumeroEstrias] float not null,
	[TorqueCierreRecomendado] float not null,
	[TorqueApertura] float not null,
	[Tolerancia] float null,
	[Imagen] image null,
	[Estatus] bit not null,
	CONSTRAINT FK_ProductoID_FichaTecnicaTapa FOREIGN KEY (ProductoID)
    REFERENCES Productos(ProductoID),
PRIMARY KEY CLUSTERED 
(
	[FichaTecnicaTapaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[VariablesMaquinaSopladora](
	[VariablesMaquinaSopladoraID] int IDENTITY(1,1) NOT NULL,
	[MaquinaID] int not null,
	[ProductoID] int not null,
	[MateriaPrimaID] int not null,
	[RetardoPresoplado] int not null,
	[RetardoSoplado] int not null,
	[TiempoPresoplado] int not null,
	[TiempoRecuperacion] int not null,
	[TiempoSoplado] int not null,
	[TiempoEscape] int not null,
	[TiempoParoCadena] int not null,
	[TiempoSalidaPinzaPreforma] int not null,
	[FrecuenciaVentilador] float not null,
	[PresionEstirado] float not null,
	[PosicionLamparas] varchar(100) not null,
	[Gancho] bit not null,
	[Relay] bit not null,
	[AnillosTornelas] bit not null,
	[Estatus] bit not null,
	[Notas] varchar(100) null,
	[CantidadZonas] int not null,
	[CantidadBancos] int not null,
	[Banco1Zona1] int null,
	[Banco1Zona2] int null,
	[Banco1Zona3] int null,
	[Banco1Zona4] int null,
	[Banco2Zona1] int null,
	[Banco2Zona2] int null,
	[Banco2Zona3] int null,
	[Banco2Zona4] int null,
	CONSTRAINT FK_MaquinaID_VariablesMaquinaSopladora FOREIGN KEY (MaquinaID)
    REFERENCES Maquinas(MaquinaID),
	CONSTRAINT FK_ProductoID_VariablesMaquinaSopladora FOREIGN KEY (ProductoID)
    REFERENCES Productos(ProductoID),
	CONSTRAINT FK_MateriaPrimaID_VariablesMaquinaSopladora FOREIGN KEY (MateriaPrimaID)
    REFERENCES MateriaPrima(MateriaPrimaID),
PRIMARY KEY CLUSTERED 
(
	[VariablesMaquinaSopladoraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[VariablesMaquinaInyectora](
	[VariablesMaquinaInyectoraID] int IDENTITY(1,1) NOT NULL,
	[MaquinaID] int not null,
	[ProductoID] int not null,
	[MateriaPrimaPrincipalID] int not null,
	[MateriaPrimaSecundariaID] int null,
	[MateriaPrimaTerciariaID] int null,
	[CantidadPrimaPrincipal] float not null,
	[CantidadPrimaSecundaria] float null,
	[CantidadPrimaTerciaria] float null,
	[Estatus] bit not null,
	CONSTRAINT FK_MaquinaID_VariablesMaquinaInyectora FOREIGN KEY (MaquinaID)
    REFERENCES Maquinas(MaquinaID),
	CONSTRAINT FK_ProductoID_VariablesMaquinaInyectora FOREIGN KEY (ProductoID)
    REFERENCES Productos(ProductoID),
	CONSTRAINT FK_MateriaPrimaPrincipalID_VariablesMaquinaInyectora FOREIGN KEY (MateriaPrimaPrincipalID)
    REFERENCES MateriaPrima(MateriaPrimaID),
	CONSTRAINT FK_MateriaPrimaSecundariaID_VariablesMaquinaInyectora FOREIGN KEY (MateriaPrimaSecundariaID)
    REFERENCES MateriaPrima(MateriaPrimaID),
	CONSTRAINT FK_MateriaPrimaTerciariaID_VariablesMaquinaInyectora FOREIGN KEY (MateriaPrimaTerciariaID)
    REFERENCES MateriaPrima(MateriaPrimaID),
PRIMARY KEY CLUSTERED 
(
	[VariablesMaquinaInyectoraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


insert into Roles values('Administrador');
insert into Roles values('Empleado');
insert into TipoMaquina values ('Sopladora');
insert into TipoMaquina values ('Inyectora');
insert into TipoMaquina values ('Otro');
insert into TipoMateriaPrima values ('Preforma');
insert into TipoMateriaPrima values ('Resina');
insert into TipoMateriaPrima values ('Colorante');
insert into TipoMateriaPrima values ('Otro');
insert into TipoProducto values ('Botella');
insert into TipoProducto values ('Tapa');
insert into TipoProducto values ('Otro');
insert into UnidadMedida values ('Kilogramo');
insert into UnidadMedida values ('Unidades');
insert into UnidadMedida values ('Tarimas');
insert into UnidadMedida values ('Bolsas');
insert into UnidadMedida values ('Otro');
insert into Usuarios values ('abc','andres','202cb962ac59075b964b07152d234b70','Andres Rodriguez Alfaro','888888888','andres@hotmail.com','Heredia',0)
insert into Usuarios values ('nose','prueba','202cb962ac59075b964b07152d234b70','Quiros Salas','888888888','jordy@hotmail.com','Heredia',0)
insert into UsersInRoles values(1,1);
insert into UsersInRoles values(2,2);
INSERT INTO Productos VALUES('360MLGEN','360ml Generico' ,1,4,231,13,20000,'Botella gen',null,0)
INSERT INTO Productos VALUES('500MLGEN','500ml Generico' ,1,4,181,25,13650,'Botella gen',null,1)
INSERT INTO Productos VALUES('600MLGEN','600ml Generico' ,1,4,162,45,32234,'Botella gen',null,0)
INSERT INTO Productos VALUES('360MLSPO','360ml Sport' ,1,4,213,35,5645,'Botella sport',null,1)
INSERT INTO Productos VALUES('600MLSPO','600ml Sport' ,1,4,231,42,65656,'Botella sport',null,0)
INSERT INTO Productos VALUES('500MLCUAD','500ml Cuadrado' ,1,4,153,65,5656,'Botella cuadrada',null,1)
INSERT INTO Productos VALUES('600MLDP','600ml Cuadrado Dos Pinos' ,1,4,160,25,5545,'Botella cuadrada dos pinos',null,1)
INSERT INTO Productos VALUES('1LTCUAD','1LT Cuadrado Dos Pinos' ,1,4,160,56,566565,'Botella cuadrada dos pinos',null,0)
INSERT INTO Productos VALUES('TAP01','Tapa Negra' ,2,2,1000,65,566,'Tapa negra',null,0)
INSERT INTO Productos VALUES('TAP02','Tapa Blanca' ,2,2,1000,65,566,'Tapa blanca',null,0)
INSERT INTO Productos VALUES('TAP03','Tapa Naranja' ,2,2,1000,65,566,'Tapa naranja',null,0)
INSERT INTO Productos VALUES('TAP04','Tapa Transparente' ,2,2,1000,65,566,'Tapa transparente',null,0)
INSERT INTO [dbo].[Clientes] VALUES
           ('DP'
           ,'Cooperativa de Productores de Leche'
           ,'26589856'
           ,'servicio@dospinos.com'
           ,'Alajuela'
           ,0)
GO

INSERT INTO [dbo].[Clientes] VALUES
           ('CB'
           ,'Cruz Blanca'
           ,'254545'
           ,'cruzblanca@gmail.com'
           ,'San Jose'
           ,0)
GO

INSERT INTO [dbo].[Clientes] VALUES
           ('SERLMD'
           ,'Inversion Servicios LMD'
           ,'544554'
           ,'lmd@hotmail.com'
           ,'Heredia'
           ,0)
GO

INSERT INTO [dbo].[Clientes] VALUES
           ('TRANS'
           ,'Transcontinental Packaging'
           ,'26589636'
           ,'transpacking@gmail.com'
           ,'Cartago'
           ,0)
GO
INSERT INTO [dbo].[Clientes] VALUES
           ('COA'
           ,'Coansa S.A'
           ,'43256975'
           ,'coansa@coansa.com'
           ,'Heredia'
           ,0)
GO
INSERT INTO [dbo].[Maquinas]
           ([CodigoMaquina]
           ,[Nombre]
           ,[TipoMaquinaID]
           ,[Descripcion]
           ,[ProduccionHora]
           ,[Estatus])
     VALUES
           (
		   'SOPLADORA01'
           ,'Sopladora Semiautomatica'
           ,1
           ,'Sopladora de color rojo'
           ,1100
           ,0)
GO
INSERT INTO [dbo].[Maquinas]
           ([CodigoMaquina]
           ,[Nombre]
           ,[TipoMaquinaID]
           ,[Descripcion]
           ,[ProduccionHora]
           ,[Estatus])
     VALUES
           (
		   'SOPLADORA02'
           ,'Sopladora manual'
           ,1
           ,'Sopladora grande'
           ,1100
           ,0)
GO

INSERT INTO [dbo].[Maquinas]
           ([CodigoMaquina]
           ,[Nombre]
           ,[TipoMaquinaID]
           ,[Descripcion]
           ,[ProduccionHora]
           ,[Estatus])
     VALUES
           (
		   'INYECTORA01'
           ,'Inyectora Alajuela'
           ,2
           ,'Inyectora Alajuela'
           ,14000
           ,0)
		   INSERT INTO [dbo].[Maquinas]
           ([CodigoMaquina]
           ,[Nombre]
           ,[TipoMaquinaID]
           ,[Descripcion]
           ,[ProduccionHora]
           ,[Estatus])
     VALUES
           (
		   'INYECTORA02'
           ,'Inyectora heredia'
           ,2
           ,'Inyectora Heredia'
           ,14000
           ,0)

		   INSERT INTO [dbo].[Maquinas]
           ([CodigoMaquina]
           ,[Nombre]
           ,[TipoMaquinaID]
           ,[Descripcion]
           ,[ProduccionHora]
           ,[Estatus])
     VALUES
           (
		   'CORTADORA01'
           ,'Cortadora alajuea'
           ,1
           ,'Cortadora de tapas'
           ,2000
           ,1)
		   GO

		   INSERT INTO [dbo].[Proveedor]
           ([CodigoProveedor]
           ,[Nombre]
           ,[Telefono]
           ,[Correo]
           ,[Direccion]
           ,[Estatus])
     VALUES
           ('PROV01'
           ,'Polykon'
           ,'253657985'
           ,'servicio@polykon.com'
           ,'Heredia'
           ,0)
GO
INSERT INTO [dbo].[Proveedor]
           ([CodigoProveedor]
           ,[Nombre]
           ,[Telefono]
           ,[Correo]
           ,[Direccion]
           ,[Estatus])
     VALUES
           ('PROV02'
           ,'Resinas'
           ,'235685695'
           ,'servicio@resinas.com'
           ,'Alajuela'
           ,0)
GO
INSERT INTO [dbo].[Proveedor]
           ([CodigoProveedor]
           ,[Nombre]
           ,[Telefono]
           ,[Correo]
           ,[Direccion]
           ,[Estatus])
     VALUES
           ('PROV03'
           ,'EPP'
           ,'82632568'
           ,'eppclientes@hotmail.com'
           ,'Alajuela'
           ,0)
GO
INSERT INTO [dbo].[Proveedor]
           ([CodigoProveedor]
           ,[Nombre]
           ,[Telefono]
           ,[Correo]
           ,[Direccion]
           ,[Estatus])
     VALUES
           ('EMP04'
           ,'Empaques plasticos'
           ,'545545'
           ,'empaquesplasticosclientes@gmail.com'
           ,'Alajuela'
           ,0)
GO

create procedure sp_ReportPedido as
select PedidoID, CodigoPedido, c.Nombre 'NombreCliente', NumeroOrdenDeCompra, pr.Nombre 'NombreProducto', NombreCompleto 'NombreUsuario',
un.Descripcion 'UnidadMedida', p.Cantidad, p.Descripcion, Fecha, p.PrecioTotal from Pedidos p 
inner join Clientes c on p.ClienteID = c.ClienteID
inner join Productos pr on p.ProductoID = pr.ProductoID 
inner join Usuarios u on u.UsuarioID = p.UsuarioID 
inner join UnidadMedida un on un.UnidadMedidaID = p.UnidadMedidaID
GO

create procedure sp_ReportEntrega as
select CodigoPedido, c.Nombre 'NombreCliente', NumeroOrdenDeCompra, pr.Nombre 'NombreProducto', CodigoEntrega, e.Fecha 'FechaEntrega', 
UnidadesEntregadas, NumeroFactura, p.Fecha 'FechaPedido', p.PrecioTotal from Movimientos m inner join Entregas e on m.EntregaID = m.EntregaID
inner join Pedidos p on p.PedidoID = m.PedidoID inner join Productos pr on p.ProductoID = pr.ProductoID inner join Clientes c on c.ClienteID = p.ClienteID
GO

create PROCEDURE [dbo].[sp_getRolesForUser]
	@userName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	select r.RoleName from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioID
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where u.UserName=@userName
END
go

create PROCEDURE [dbo].[sp_getUsuariosRole]
	
	@roleName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	select u.UserName from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioID
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where r.RoleName=@roleName 
END
go

create PROCEDURE [dbo].[sp_isUserInRole]
	@userName varchar(20),
	@roleName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	declare @resultado bit =0;
	if exists(
	select * from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioId
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where r.RoleName=@roleName and u.UserName=@userName)
	set @resultado = 1
	 select @resultado
END
GO

create procedure sp_ProductosBotella as
select * from Productos where TipoProductoID = 1;
GO

create procedure sp_ProductosTapa as
select * from Productos where TipoProductoID = 2;
GO

create procedure sp_MaquinasInyectora as
select * from Maquinas where TipoMaquinaID = 2;
GO

create procedure sp_MaquinasSopladora as
select * from Maquinas where TipoMaquinaID = 1;
GO

create procedure sp_MateriaPrimaSopladora as
select * from MateriaPrima where TipoMateriaPrimaID = 1
GO

create procedure sp_MateriaPrimaInyectora as
select * from MateriaPrima where TipoMateriaPrimaID = 2 or TipoMateriaPrimaID = 3
GO


create trigger FK_Clientes
on Clientes
instead of delete
as
begin
set nocount on;
delete from Pedidos where ClienteID in (select ClienteID from deleted);
delete from Clientes where ClienteID in (select ClienteID from deleted);
end
GO

create trigger FK_Entregas
on Entregas
instead of delete
as
begin
set nocount on;
delete from Movimientos where EntregaID in (select EntregaID from deleted);
delete from Entregas where EntregaID in (select EntregaID from deleted);
end
GO

create trigger FK_Maquinas
on Maquinas
instead of delete
as
begin
set nocount on;
delete from VariablesMaquinaSopladora where MaquinaID in (select MaquinaID from deleted);
delete from VariablesMaquinaInyectora where MaquinaID in (select MaquinaID from deleted);
delete from Maquinas where MaquinaID in (select MaquinaID from deleted);
end
GO

create trigger FK_MateriaPrima
on MateriaPrima
instead of delete
as
begin
set nocount on;
delete from VariablesMaquinaSopladora where MateriaPrimaID in (select MateriaPrimaID from deleted);
delete from VariablesMaquinaInyectora where MateriaPrimaPrincipalID in (select MateriaPrimaPrincipalID from deleted);
delete from VariablesMaquinaInyectora where MateriaPrimaSecundariaID in (select MateriaPrimaSecundariaID from deleted);
delete from VariablesMaquinaInyectora where MateriaPrimaTerciariaID in (select MateriaPrimaTerciariaID from deleted);
delete from MateriaPrima where MateriaPrimaID in (select MateriaPrimaID from deleted);
end
GO

create trigger FK_Pedidos
on Pedidos
instead of delete
as
begin
set nocount on;
delete from Movimientos where PedidoID in (select PedidoID from deleted);
delete from Pedidos where PedidoID in (select PedidoID from deleted);
end
GO

create trigger FK_Productos
on Productos
instead of delete
as
begin
set nocount on;
delete from FichaTecnicaBotella where ProductoID in (select ProductoID from deleted);
delete from VariablesMaquinaSopladora where ProductoID in (select ProductoID from deleted);
delete from VariablesMaquinaInyectora where ProductoID in (select ProductoID from deleted);
delete from Pedidos where ProductoID in (select ProductoID from deleted);
delete from FichaTecnicaTapa where ProductoID in (select ProductoID from deleted);
delete from Productos where ProductoID in (select ProductoID from deleted);
end
GO

create trigger FK_Proveedor
on Proveedor
instead of delete
as
begin
set nocount on;
delete from MateriaPrima where MateriaPrimaID in (select MateriaPrimaID from deleted);
delete from Proveedor where ProveedorID in (select ProveedorID from deleted);
end
GO

create trigger FK_Roles
on Roles
instead of delete
as
begin
set nocount on;
delete from UsersInRoles where Role_RoleID in (select RoleID from deleted);
delete from Roles where RoleID in (select RoleID from deleted);
end
GO

create trigger FK_TipoMaquina
on TipoMaquina
instead of delete
as
begin
set nocount on;
delete from Maquinas where TipoMaquinaID in (select TipoMaquinaID from deleted);
delete from TipoMaquina where TipoMaquinaID in (select TipoMaquinaID from deleted);
end
GO

create trigger FK_TipoMateriaPrima
on TipoMateriaPrima
instead of delete
as
begin
set nocount on;
delete from MateriaPrima where MateriaPrimaID in (select MateriaPrimaID from deleted);
delete from TipoMateriaPrima where TipoMateriaPrimaID in (select TipoMateriaPrimaID from deleted);
end
GO

create trigger FK_TipoProducto
on TipoProducto
instead of delete
as
begin
set nocount on;
delete from Productos where TipoProductoID in (select TipoProductoID from deleted);
delete from TipoProducto where TipoProductoID in (select TipoProductoID from deleted);
end
GO

create trigger FK_UnidadMedida
on UnidadMedida
instead of delete
as
begin
set nocount on;
delete from Pedidos where UnidadMedidaID in (select UnidadMedidaID from deleted);
delete from Productos where UnidadMedidaID in (select UnidadMedidaID from deleted);
delete from MateriaPrima where UnidadMedidaID in (select UnidadMedidaID from deleted);
delete from UnidadMedida where UnidadMedidaID in (select UnidadMedidaID from deleted);
end
GO

create trigger FK_Usuarios
on Usuarios
instead of delete
as
begin
set nocount on;
delete from Pedidos where UsuarioID in (select UsuarioID from deleted);
delete from UsersInRoles where User_UserID in (select UsuarioID from deleted);
delete from Usuarios where UsuarioID in (select UsuarioID from deleted);
end
GO


