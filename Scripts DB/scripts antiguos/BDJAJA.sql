USE [master]
GO
/****** Object:  Database [DIPREMA]    Script Date: 22/08/2020 10:15:35 ******/
CREATE DATABASE [DIPREMA]
GO
USE [DIPREMA]
GO
/****** Object:  Table [dbo].[AperturaCierre]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AperturaCierre](
	[AperturaCierreID] [bigint] IDENTITY(1,1) NOT NULL,
	[FechaApertura] [datetime] NOT NULL,
	[FechaCierre] [datetime] NULL,
	[Turno] [varchar](50) NOT NULL,
	[PedidoID] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[MaquinaID] [int] NULL,
 CONSTRAINT [PK_AperturaCierre] PRIMARY KEY CLUSTERED 
(
	[AperturaCierreID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[AperturaCierreActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*Vistas*/
create VIEW [dbo].[AperturaCierreActivos] AS
select * from AperturaCierre where Estado = 1;
GO
/****** Object:  Table [dbo].[DetalleEntrega]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleEntrega](
	[DetelleEntregaID] [bigint] IDENTITY(1,1) NOT NULL,
	[EntregaID] [int] NOT NULL,
	[ProduccionID] [int] NOT NULL,
	[Paquetes] [int] NOT NULL,
	[Cantidad] [float] NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_DetalleEntrega] PRIMARY KEY CLUSTERED 
(
	[DetelleEntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Entregas]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entregas](
	[EntregaID] [int] IDENTITY(1,1) NOT NULL,
	[NumeroEntrega] [varchar](50) NOT NULL,
	[Fecha] [date] NULL,
	[UnidadesEntregadas] [int] NULL,
	[NumeroFactura] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pedidos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pedidos](
	[PedidoID] [int] IDENTITY(1,1) NOT NULL,
	[NumeroPedido] [varchar](50) NOT NULL,
	[ClienteID] [int] NOT NULL,
	[NumeroOrdenDeCompra] [varchar](50) NULL,
	[ProductoID] [int] NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[UnidadMedidaID] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Fecha] [date] NULL,
	[PrecioTotal] [money] NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PedidoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productos](
	[ProductoID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProducto] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[TipoProductoID] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Imagen] [varchar](max) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UnidadMedida]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnidadMedida](
	[UnidadMedidaID] [int] IDENTITY(1,1) NOT NULL,
	[Unidad] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UnidadMedidaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Produccion]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Produccion](
	[ProduccionID] [int] IDENTITY(1,1) NOT NULL,
	[PedidoID] [int] NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaFin] [datetime] NULL,
	[HorasTrabajadas] [varchar](100) NULL,
	[CantidadMerma] [float] NULL,
	[CantidadProduccionTotal] [float] NOT NULL,
	[CantidadPaquetesCajas] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProduccionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[DetalleEntregaActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create VIEW [dbo].[DetalleEntregaActivos] AS

Select Entregas.Fecha as FechaEntrega, Entregas.NumeroFactura as NumeroFactura ,Entregas.NumeroEntrega as NumeroEntrega,
		Pedidos.NumeroPedido as lote, Pedidos.NumeroOrdenDeCompra as NumeroOrdenCompra,
		Productos.Nombre as Producto,
		DetalleEntrega.Paquetes, DetalleEntrega.Cantidad,
		UnidadMedida.Unidad
from DetalleEntrega

join Entregas on DetalleEntrega.EntregaID = Entregas.EntregaID
join Produccion on  DetalleEntrega.ProduccionID = Produccion.ProduccionID
join Pedidos on Produccion.ProduccionID = Pedidos.PedidoID
join UnidadMedida on Pedidos.UnidadMedidaID = UnidadMedida.UnidadMedidaID
join Productos on Pedidos.ProductoID = Productos.ProductoID

where DetalleEntrega.Estado =1
GO
/****** Object:  Table [dbo].[ProduccionPaquetesCajas]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProduccionPaquetesCajas](
	[EtiquetaID] [int] IDENTITY(1,1) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Cantidad] [float] NOT NULL,
	[AperturaCierreID] [bigint] NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[Turno] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_ProduccionPaquetesCajas] PRIMARY KEY CLUSTERED 
(
	[EtiquetaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ProduccionPaquetesCajaActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[ProduccionPaquetesCajaActivos] as
select * from ProduccionPaquetesCajas where Estado = 1;
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clientes](
	[ClienteID] [int] IDENTITY(1,1) NOT NULL,
	[Identificacion] [varchar](50) NOT NULL,
	[NombreCompleto] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ClienteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ClientesActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*Vistas*/
create VIEW [dbo].[ClientesActivos] AS
select * from Clientes where Estado = 1;
GO
/****** Object:  View [dbo].[EntregasActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create VIEW [dbo].[EntregasActivos] as
select * from Entregas where Estado = 1;
GO
/****** Object:  Table [dbo].[FichaTecnicaBotella]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FichaTecnicaBotella](
	[FichaTecnicaBotellaID] [int] IDENTITY(1,1) NOT NULL,
	[ProductoID] [int] NOT NULL,
	[Altura] [float] NOT NULL,
	[DiametroMayor] [float] NOT NULL,
	[DiametroMenor] [float] NOT NULL,
	[DiametroCuello] [float] NOT NULL,
	[CapacidadVolumetrica] [float] NOT NULL,
	[Tolerancia] [float] NULL,
	[ImagenCuerpo] [varchar](max) NULL,
	[ImagenFondo] [varchar](max) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FichaTecnicaBotellaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[FichaTecnicaBotellaActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[FichaTecnicaBotellaActivos] as
select * from FichaTecnicaBotella where Estado = 1;
GO
/****** Object:  Table [dbo].[FichaTecnicaTapa]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FichaTecnicaTapa](
	[FichaTecnicaTapaID] [int] IDENTITY(1,1) NOT NULL,
	[ProductoID] [int] NOT NULL,
	[PesoPieza] [float] NOT NULL,
	[Altura] [float] NOT NULL,
	[DiametroInterno] [float] NOT NULL,
	[DiametroExternoSelloOliva] [float] NOT NULL,
	[AlturaSelloOliva] [float] NOT NULL,
	[DiametroInternoBandaSeguridad] [float] NOT NULL,
	[EspesorPanel] [float] NOT NULL,
	[NumeroEstrias] [float] NOT NULL,
	[TorqueCierreRecomendado] [float] NOT NULL,
	[TorqueApertura] [float] NOT NULL,
	[Tolerancia] [float] NULL,
	[Imagen] [varchar](max) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FichaTecnicaTapaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[FichaTecnicaTapaActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[FichaTecnicaTapaActivos] as
select * from FichaTecnicaTapa where Estado = 1;
GO
/****** Object:  Table [dbo].[Maquinas]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Maquinas](
	[MaquinaID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoMaquina] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[TipoMaquinaID] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[ProduccionHora] [varchar](100) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaquinaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MaquinasActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MaquinasActivos] as
select * from Maquinas where Estado = 1;
GO
/****** Object:  Table [dbo].[MateriaPrima]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MateriaPrima](
	[MateriaPrimaID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoMateriaPrima] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[ProveedorID] [int] NOT NULL,
	[TipoMateriaPrimaID] [int] NOT NULL,
	[UnidadMedidaID] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[PrecioUnitario] [money] NOT NULL,
	[PrecioTotal] [money] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MateriaPrimaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MateriaPrimaActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MateriaPrimaActivos] as
select * from MateriaPrima where Estado = 1;
GO
/****** Object:  View [dbo].[PedidosActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[PedidosActivos] as
select * from Pedidos where Estado = 1;
GO
/****** Object:  View [dbo].[ProduccionActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[ProduccionActivos] as
select * from Produccion where Estado = 1;
GO
/****** Object:  View [dbo].[ProductosActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProductosActivos] as
select * from Productos where Estado = 1;
GO
/****** Object:  Table [dbo].[Proveedor]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proveedor](
	[ProveedorID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProveedor] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProveedorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ProveedorActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProveedorActivos] as
select * from Proveedor where Estado = 1;
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[UsuarioID] [int] IDENTITY(1,1) NOT NULL,
	[Identificacion] [varchar](50) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [varchar](100) NOT NULL,
	[NombreCompleto] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UsuarioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UC_Usuarios] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[UsuariosActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[UsuariosActivos] as
select * from Usuarios where Estado = 1;
GO
/****** Object:  Table [dbo].[VariablesMaquinaInyectora]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VariablesMaquinaInyectora](
	[VariablesMaquinaInyectoraID] [int] IDENTITY(1,1) NOT NULL,
	[MaquinaID] [int] NOT NULL,
	[ProductoID] [int] NOT NULL,
	[MateriaPrimaPrincipalID] [int] NOT NULL,
	[MateriaPrimaSecundariaID] [int] NULL,
	[MateriaPrimaTerciariaID] [int] NULL,
	[CantidadPrimaPrincipal] [float] NOT NULL,
	[CantidadPrimaSecundaria] [float] NULL,
	[CantidadPrimaTerciaria] [float] NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[VariablesMaquinaInyectoraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VariablesMaquinaInyectoraActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VariablesMaquinaInyectoraActivos] as
select * from VariablesMaquinaInyectora where Estado = 1;
GO
/****** Object:  Table [dbo].[VariablesMaquinaSopladora]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VariablesMaquinaSopladora](
	[VariablesMaquinaSopladoraID] [int] IDENTITY(1,1) NOT NULL,
	[MaquinaID] [int] NOT NULL,
	[ProductoID] [int] NOT NULL,
	[MateriaPrimaID] [int] NOT NULL,
	[RetardoPresoplado] [int] NOT NULL,
	[RetardoSoplado] [int] NOT NULL,
	[TiempoPresoplado] [int] NOT NULL,
	[TiempoRecuperacion] [int] NOT NULL,
	[TiempoSoplado] [int] NOT NULL,
	[TiempoEscape] [int] NOT NULL,
	[TiempoParoCadena] [int] NOT NULL,
	[TiempoSalidaPinzaPreforma] [int] NOT NULL,
	[FrecuenciaVentilador] [float] NOT NULL,
	[PresionEstirado] [float] NOT NULL,
	[Gancho] [bit] NOT NULL,
	[Relay] [bit] NOT NULL,
	[AnillosTornelas] [bit] NOT NULL,
	[Estado] [bit] NOT NULL,
	[Notas] [varchar](100) NULL,
	[CantidadZonas] [int] NOT NULL,
	[CantidadBancos] [int] NOT NULL,
	[PosicionBanco1Zona1] [varchar](100) NULL,
	[PosicionBanco1Zona2] [varchar](100) NULL,
	[PosicionBanco1Zona3] [varchar](100) NULL,
	[PosicionBanco1Zona4] [varchar](100) NULL,
	[PosicionBanco2Zona1] [varchar](100) NULL,
	[PosicionBanco2Zona2] [varchar](100) NULL,
	[PosicionBanco2Zona3] [varchar](100) NULL,
	[PosicionBanco2Zona4] [varchar](100) NULL,
	[ValorBanco1Zona1] [int] NULL,
	[ValorBanco1Zona2] [int] NULL,
	[ValorBanco1Zona3] [int] NULL,
	[ValorBanco1Zona4] [int] NULL,
	[ValorBanco2Zona1] [int] NULL,
	[ValorBanco2Zona2] [int] NULL,
	[ValorBanco2Zona3] [int] NULL,
	[ValorBanco2Zona4] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[VariablesMaquinaSopladoraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VariablesMaquinaSopladoraActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VariablesMaquinaSopladoraActivos] as
select * from VariablesMaquinaSopladora where Estado = 1;
GO
/****** Object:  View [dbo].[MaquinasSopladoraActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MaquinasSopladoraActivos] as
select * from Maquinas where TipoMaquinaID = 1 and Estado = 1;
GO
/****** Object:  View [dbo].[MaquinasInyectoraActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MaquinasInyectoraActivos] as
select * from Maquinas where TipoMaquinaID = 2 and Estado = 1;
GO
/****** Object:  View [dbo].[MateriaPrimaInyectoraActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MateriaPrimaInyectoraActivos] as
select * from MateriaPrima where TipoMateriaPrimaID = 2 or TipoMateriaPrimaID = 3 and Estado = 1
GO
/****** Object:  View [dbo].[MateriaPrimaSopladoraActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MateriaPrimaSopladoraActivos] as
select * from MateriaPrima where TipoMateriaPrimaID = 1 and Estado = 1
GO
/****** Object:  View [dbo].[ProductosBotellaActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProductosBotellaActivos] as
select * from Productos where TipoProductoID = 1 and Estado = 1;
GO
/****** Object:  View [dbo].[ProductosTapaActivos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProductosTapaActivos] as
select * from Productos where TipoProductoID = 2 and Estado = 1;
GO
/****** Object:  Table [dbo].[MermaPorTurno]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MermaPorTurno](
	[idMermaTurno] [bigint] IDENTITY(1,1) NOT NULL,
	[cantidad] [float] NOT NULL,
	[AperturaCierreID] [bigint] NOT NULL,
	[MermaId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Movimientos]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movimientos](
	[PedidoID] [int] NOT NULL,
	[EntregaID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Movimientos] PRIMARY KEY CLUSTERED 
(
	[PedidoID] ASC,
	[EntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoMaquina]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoMaquina](
	[TipoMaquinaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMaquinaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoMateriaPrima]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoMateriaPrima](
	[TipoMateriaPrimaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMateriaPrimaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoMerma]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoMerma](
	[idMerma] [bigint] NOT NULL,
	[tipo_Merma] [varchar](200) NOT NULL,
 CONSTRAINT [PK_TipoMerma] PRIMARY KEY CLUSTERED 
(
	[idMerma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoProducto]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoProducto](
	[TipoProductoID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoProductoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsersInRoles]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersInRoles](
	[User_UserID] [int] NOT NULL,
	[Role_RoleID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.RoleUsers] PRIMARY KEY CLUSTERED 
(
	[Role_RoleID] ASC,
	[User_UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AperturaCierre] ADD  CONSTRAINT [DF_AperturaCierre_estado]  DEFAULT ((1)) FOR [Estado]
GO
ALTER TABLE [dbo].[DetalleEntrega] ADD  CONSTRAINT [DF_DetalleEntrega_Detalle]  DEFAULT ((1)) FOR [Estado]
GO
ALTER TABLE [dbo].[Entregas] ADD  CONSTRAINT [DF_Entregas_Estado]  DEFAULT ((1)) FOR [Estado]
GO
ALTER TABLE [dbo].[AperturaCierre]  WITH CHECK ADD  CONSTRAINT [FK_AperturaCierre_PedidoID] FOREIGN KEY([PedidoID])
REFERENCES [dbo].[Pedidos] ([PedidoID])
GO
ALTER TABLE [dbo].[AperturaCierre] CHECK CONSTRAINT [FK_AperturaCierre_PedidoID]
GO
ALTER TABLE [dbo].[DetalleEntrega]  WITH CHECK ADD  CONSTRAINT [FK_DetalleEntrega_Entregas] FOREIGN KEY([EntregaID])
REFERENCES [dbo].[Entregas] ([EntregaID])
GO
ALTER TABLE [dbo].[DetalleEntrega] CHECK CONSTRAINT [FK_DetalleEntrega_Entregas]
GO
ALTER TABLE [dbo].[DetalleEntrega]  WITH CHECK ADD  CONSTRAINT [FK_DetalleEntrega_Produccion] FOREIGN KEY([ProduccionID])
REFERENCES [dbo].[Produccion] ([ProduccionID])
GO
ALTER TABLE [dbo].[DetalleEntrega] CHECK CONSTRAINT [FK_DetalleEntrega_Produccion]
GO
ALTER TABLE [dbo].[FichaTecnicaBotella]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_FichaTecnicaBotella] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[FichaTecnicaBotella] CHECK CONSTRAINT [FK_ProductoID_FichaTecnicaBotella]
GO
ALTER TABLE [dbo].[FichaTecnicaTapa]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_FichaTecnicaTapa] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[FichaTecnicaTapa] CHECK CONSTRAINT [FK_ProductoID_FichaTecnicaTapa]
GO
ALTER TABLE [dbo].[Maquinas]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_Maquinas] FOREIGN KEY([TipoMaquinaID])
REFERENCES [dbo].[TipoMaquina] ([TipoMaquinaID])
GO
ALTER TABLE [dbo].[Maquinas] CHECK CONSTRAINT [FK_TipoID_Maquinas]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_ProveedorID_MateriaPrima] FOREIGN KEY([ProveedorID])
REFERENCES [dbo].[Proveedor] ([ProveedorID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_ProveedorID_MateriaPrima]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_MateriaPrima] FOREIGN KEY([TipoMateriaPrimaID])
REFERENCES [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_TipoID_MateriaPrima]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_UnidadMedidaID_MateriaPrima] FOREIGN KEY([UnidadMedidaID])
REFERENCES [dbo].[UnidadMedida] ([UnidadMedidaID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_UnidadMedidaID_MateriaPrima]
GO
ALTER TABLE [dbo].[Movimientos]  WITH CHECK ADD  CONSTRAINT [FK_EntregaID_Movimientos] FOREIGN KEY([EntregaID])
REFERENCES [dbo].[Entregas] ([EntregaID])
GO
ALTER TABLE [dbo].[Movimientos] CHECK CONSTRAINT [FK_EntregaID_Movimientos]
GO
ALTER TABLE [dbo].[Movimientos]  WITH CHECK ADD  CONSTRAINT [FK_PedidoID_Movimientos] FOREIGN KEY([PedidoID])
REFERENCES [dbo].[Pedidos] ([PedidoID])
GO
ALTER TABLE [dbo].[Movimientos] CHECK CONSTRAINT [FK_PedidoID_Movimientos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_ClienteID_Pedidos] FOREIGN KEY([ClienteID])
REFERENCES [dbo].[Clientes] ([ClienteID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_ClienteID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_Pedidos] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_ProductoID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_UnidadMedidaID_Pedidos] FOREIGN KEY([UnidadMedidaID])
REFERENCES [dbo].[UnidadMedida] ([UnidadMedidaID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_UnidadMedidaID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioID_Pedidos] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_UsuarioID_Pedidos]
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas]  WITH CHECK ADD  CONSTRAINT [FK_ProduccionPaquetesCajas_ProduccionPaquetesCajas] FOREIGN KEY([AperturaCierreID])
REFERENCES [dbo].[AperturaCierre] ([AperturaCierreID])
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas] CHECK CONSTRAINT [FK_ProduccionPaquetesCajas_ProduccionPaquetesCajas]
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas]  WITH CHECK ADD  CONSTRAINT [FK_ProduccionPaquetesCajas_Usuarios] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas] CHECK CONSTRAINT [FK_ProduccionPaquetesCajas_Usuarios]
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_Productos] FOREIGN KEY([TipoProductoID])
REFERENCES [dbo].[TipoProducto] ([TipoProductoID])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [FK_TipoID_Productos]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK_Role_RoleID_UserInRoles] FOREIGN KEY([Role_RoleID])
REFERENCES [dbo].[Roles] ([RoleID])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [FK_Role_RoleID_UserInRoles]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK_User_UserID_UserInRoles] FOREIGN KEY([User_UserID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [FK_User_UserID_UserInRoles]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MaquinaID_VariablesMaquinaInyectora] FOREIGN KEY([MaquinaID])
REFERENCES [dbo].[Maquinas] ([MaquinaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MaquinaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaPrincipalID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaPrincipalID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaPrincipalID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaSecundariaID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaSecundariaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaSecundariaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaTerciariaID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaTerciariaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaTerciariaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_VariablesMaquinaInyectora] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_ProductoID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_MaquinaID_VariablesMaquinaSopladora] FOREIGN KEY([MaquinaID])
REFERENCES [dbo].[Maquinas] ([MaquinaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_MaquinaID_VariablesMaquinaSopladora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaID_VariablesMaquinaSopladora] FOREIGN KEY([MateriaPrimaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_MateriaPrimaID_VariablesMaquinaSopladora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_VariablesMaquinaSopladora] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_ProductoID_VariablesMaquinaSopladora]
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientesCRUD]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[sp_ClientesCRUD]
@Accion int, 
@ClienteID int null,
@Identificacion varchar(max) null, 
@NombreCompleto varchar(max) null, 
@Telefono varchar(max) null, 
@Correo varchar(max) null, 
@Direccion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin
				INSERT INTO Clientes VALUES (@Identificacion, @NombreCompleto, @Telefono, @Correo, @Direccion, 1);			
			end
		else IF (@Accion = 2) 
			begin
				UPDATE Clientes set Identificacion = @Identificacion, NombreCompleto = @NombreCompleto, Telefono = @Telefono,
				Correo = @Correo, Direccion = @Direccion, Estado = 1 where ClienteID = @ClienteID
			end
		else  IF (@Accion = 3)
			begin
				UPDATE Clientes set Estado = 0 where ClienteID = @ClienteID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_DatosEtiqueta]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_DatosEtiqueta]
@Etiqueta int 

as
begin
		Select produccion.EtiquetaID, produccion.Fecha, produccion.Cantidad, produccion.Turno, usuario.NombreCompleto as Usuario,
		pedido.NumeroPedido, pedido.NumeroOrdenDeCompra, cliente.NombreCompleto as Cliente, cliente.Direccion, producto.Nombre as Producto,
		um.Unidad, maquina.Nombre as maquina

		from ProduccionPaquetesCajas as produccion
		join Usuarios as usuario on produccion.UsuarioID = usuario.UsuarioID
		join AperturaCierre as apertura on produccion.AperturaCierreID = apertura.AperturaCierreID
		join Pedidos as pedido on apertura.PedidoID = pedido.PedidoID
		join Clientes as cliente on pedido.ClienteID = cliente.ClienteID 
		join Productos as producto on pedido.ProductoID = producto.ProductoID		
		join UnidadMedida as um on pedido.UnidadMedidaID = um.UnidadMedidaID
		join Maquinas as maquina on apertura.MaquinaID = maquina.MaquinaID

		where EtiquetaID = @Etiqueta
end
GO
/****** Object:  StoredProcedure [dbo].[sp_EntregasCRUD]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_EntregasCRUD]
@Accion int, 
@EntregaID int null,
@NumeroEntrega varchar(max) null,
@Fecha date null,
@UnidadesEntregadas int null,
@NumeroFactura varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				INSERT INTO Entregas VALUES (@NumeroEntrega, @Fecha, @UnidadesEntregadas, @NumeroFactura, 1);			
			end
		else IF (@Accion = 2) 
			begin
				UPDATE Entregas set NumeroEntrega = @NumeroEntrega, Fecha = @Fecha, UnidadesEntregadas = @UnidadesEntregadas, 
				NumeroFactura = @NumeroFactura, Estado = 1 where EntregaID = @EntregaID
			end
		else  IF (@Accion = 3)
			begin
				UPDATE Entregas set Estado = 0 where EntregaID = @EntregaID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_FichaTecnicaBotellaCRUD]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_FichaTecnicaBotellaCRUD]
@Accion int, 
@FichaTecnicaBotellaID int null,
@ProductoID int null,
@Altura float null,
@DiametroMayor float null,
@DiametroMenor float null,
@DiametroCuello float null,
@CapacidadVolumetrica float null,
@Tolerancia float null,
@ImagenCuerpo varchar(max) null,
@ImagenFondo varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				INSERT INTO FichaTecnicaBotella
				VALUES (@ProductoID, @Altura, @DiametroMayor, @DiametroMenor,
				@DiametroCuello,@CapacidadVolumetrica,@Tolerancia, @ImagenCuerpo, @ImagenFondo ,1);			
			end
		else IF (@Accion = 2) 
			begin
				UPDATE FichaTecnicaBotella set ProductoID = @ProductoID, Altura = @Altura,
				DiametroMayor = @DiametroMayor, DiametroMenor = @DiametroMenor, DiametroCuello = @DiametroCuello, 
				CapacidadVolumetrica = @CapacidadVolumetrica,
				Tolerancia = @Tolerancia, ImagenCuerpo = @ImagenCuerpo, ImagenFondo = @ImagenFondo, Estado = 1 
				where FichaTecnicaBotellaID = @FichaTecnicaBotellaID
			end
		else  IF (@Accion = 3)
			begin
				UPDATE FichaTecnicaBotella set Estado = 0 where FichaTecnicaBotellaID = @FichaTecnicaBotellaID
			end
		/*else  IF (@Accion = 4)
			begin
				select * from FichaTecnicaBotella where Estado = 1;
			end
		else  IF (@Accion = 5)
			begin
				select * from FichaTecnicaBotella where FichaTecnicaBotellaID = @FichaTecnicaBotellaID;
			end*/
end
GO
/****** Object:  StoredProcedure [dbo].[sp_FichaTecnicaTapaCRUD]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_FichaTecnicaTapaCRUD]
@Accion int, 
@FichaTecnicaTapaID int null,
@ProductoID int null,
@PesoPieza float null,
@Altura float null,
@DiametroInterno float null,
@DiametroExternoSelloOliva float null,
@AlturaSelloOliva float null,
@DiametroInternoBandaSeguridad float null,
@EspesorPanel float null,
@NumeroEstrias float null,
@TorqueCierreRecomendado float null,
@TorqueApertura float null,
@Tolerancia float null,
@Imagen varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into FichaTecnicaTapa values (@ProductoID, @PesoPieza, @Altura, @DiametroInterno, @DiametroExternoSelloOliva, @AlturaSelloOliva,
				@DiametroInternoBandaSeguridad, @EspesorPanel, @NumeroEstrias, @TorqueCierreRecomendado, @TorqueApertura, @Tolerancia, @Imagen, 1);	
			end
		else IF (@Accion = 2) 
			begin
				update FichaTecnicaTapa set ProductoID = @ProductoID, PesoPieza = @PesoPieza, Altura = @Altura, DiametroInterno = @DiametroInterno,
				AlturaSelloOliva = @AlturaSelloOliva, DiametroInternoBandaSeguridad = @DiametroInternoBandaSeguridad, EspesorPanel = @EspesorPanel,
				NumeroEstrias = @NumeroEstrias, TorqueCierreRecomendado = @TorqueCierreRecomendado, TorqueApertura = @TorqueApertura, 
				Tolerancia = @Tolerancia, Imagen = @Imagen, Estado = 1 where FichaTecnicaTapaID = @FichaTecnicaTapaID
			end
		else  IF (@Accion = 3)
			begin
				update FichaTecnicaTapa set Estado = 0 where FichaTecnicaTapaID = @FichaTecnicaTapaID;
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_getRolesForUser]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*Procedimientos almacenados*/


create PROCEDURE [dbo].[sp_getRolesForUser]
	@userName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	select r.RoleName from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioID
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where u.UserName=@userName
END
GO
/****** Object:  StoredProcedure [dbo].[sp_getUsuariosRole]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_getUsuariosRole]
	
	@roleName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	select u.UserName from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioID
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where r.RoleName=@roleName 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_isUserInRole]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_isUserInRole]
	@userName varchar(20),
	@roleName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	declare @resultado bit =0;
	if exists(
	select * from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioId
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where r.RoleName=@roleName and u.UserName=@userName)
	set @resultado = 1
	 select @resultado
END


GO
/****** Object:  StoredProcedure [dbo].[sp_MaquinasCRUD]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_MaquinasCRUD]
@Accion int, 
@MaquinaID int null,
@CodigoMaquina varchar(max) null,
@Nombre varchar(max) null,
@TipoMaquinaID int null,
@Descripcion varchar(max) null,
@ProduccionHora varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Maquinas values (@CodigoMaquina, @Nombre, @TipoMaquinaID, @Descripcion, @ProduccionHora, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Maquinas set CodigoMaquina = @CodigoMaquina, Nombre = @Nombre, TipoMaquinaID = @TipoMaquinaID, Descripcion = @Descripcion,
				ProduccionHora = @ProduccionHora, Estado = 1 where MaquinaID = @MaquinaID
			end
		else  IF (@Accion = 3)
			begin
				update Maquinas set Estado = 0 where MaquinaID = @MaquinaID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_MateriaPrimaCRUD]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_MateriaPrimaCRUD]
@Accion int, 
@MateriaPrimaID int null,
@CodigoMateriaPrima varchar(max) null,
@Nombre varchar(max) null,
@ProveedorID int null,
@TipoMateriaPrimaID int null,
@UnidadMedidaID int null,
@Cantidad int null,
@PrecioUnitario money null,
@PrecioTotal money null,
@Descripcion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into MateriaPrima values(@CodigoMateriaPrima, @Nombre, @ProveedorID, @TipoMateriaPrimaID, @UnidadMedidaID, @Cantidad,
				@PrecioUnitario, @PrecioTotal, @Descripcion, 1);
			end
		else IF (@Accion = 2) 
			begin
				update MateriaPrima set CodigoMateriaPrima = @CodigoMateriaPrima, Nombre = @Nombre, ProveedorID = @ProveedorID, TipoMateriaPrimaID 
				= @TipoMateriaPrimaID, UnidadMedidaID = @UnidadMedidaID, Cantidad = @Cantidad, PrecioUnitario = @PrecioUnitario, Descripcion = @Descripcion,
				Estado = 1
				where MateriaPrimaID = @MateriaPrimaID
			end
		else  IF (@Accion = 3)
			begin
				update MateriaPrima set Estado = 0 where MateriaPrimaID = @MateriaPrimaID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_PedidosCRUD]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_PedidosCRUD]
@Accion int, 
@PedidoID int null,
@NumeroPedido varchar(max) null,
@ClienteID int null,
@NumeroOrdenDeCompra varchar(max) null,
@ProductoID int null,
@UsuarioID int null,
@UnidadMedidaID int null,
@Cantidad int null,
@Descripcion varchar(max) null,
@Fecha date null,
@PrecioTotal money null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Pedidos values (@NumeroPedido, @ClienteID, @NumeroOrdenDeCompra, @ProductoID, @UsuarioID, @UnidadMedidaID, @Cantidad,
				@Descripcion, @Fecha, @PrecioTotal, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Pedidos set NumeroPedido = @NumeroPedido, ClienteID = @ClienteID, NumeroOrdenDeCompra = @NumeroOrdenDeCompra, ProductoID = @ProductoID,
				UsuarioID = @UsuarioID, UnidadMedidaID = @UnidadMedidaID, Cantidad = @Cantidad, Descripcion = @Descripcion, Fecha = @Fecha, PrecioTotal = @PrecioTotal,
				Estado = 1 where PedidoID = @PedidoID
			end
		else  IF (@Accion = 3)
			begin
				update Pedidos set Estado = 0 where PedidoID = @PedidoID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_ProduccionCRUD]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ProduccionCRUD]
@Accion int, 
@ProduccionID int null,
@PedidoID int null,
@FechaInicio date null,
@FechaFin date null,
@HorasTrabajadas date null,
@CantidadMerma float null,
@CantidadProduccionTotal int null,
@CantidadPaquetesCajas int null


as
begin
		IF (@Accion = 1)
			begin 
				insert into Produccion values (@PedidoID, @FechaInicio, @FechaFin, @HorasTrabajadas, @CantidadMerma, @CantidadProduccionTotal, @CantidadPaquetesCajas, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Produccion set PedidoID = @PedidoID, FechaInicio = @FechaInicio, FechaFin = @FechaFin, HorasTrabajadas = @HorasTrabajadas, 
				CantidadMerma = @CantidadMerma, CantidadProduccionTotal = @CantidadProduccionTotal, CantidadPaquetesCajas = @CantidadPaquetesCajas, Estado = 1
				where ProduccionID = @ProduccionID
			end
		else  IF (@Accion = 3)
			begin
				update Produccion set Estado = 0 where ProduccionID = @ProduccionID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_ProduccionPaquetesCajasCRUD]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ProduccionPaquetesCajasCRUD]
@Accion int, 
@EtiquetaID int null,
@Fecha date null,
@Cantidad int null,
@AperturaCierreID bigint null,
@UsuarioID int null,
@Turno varchar(max) null

as
begin
		IF (@Accion = 1)
			begin 
				insert into ProduccionPaquetesCajas values (@Fecha, @Cantidad,@AperturaCierreID, @UsuarioID, @Turno, 1);

			end
		else IF (@Accion = 2) 
			begin
				update ProduccionPaquetesCajas set Fecha = @Fecha, Cantidad = @Cantidad, AperturaCierreID = @AperturaCierreID, UsuarioID = @UsuarioID, Turno = @Turno, Estado = 1 where EtiquetaID = @EtiquetaID
			end
		else  IF (@Accion = 3)
			begin
				update ProduccionPaquetesCajas set Estado = 0 where EtiquetaID = @EtiquetaID;
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductosCRUD]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ProductosCRUD]
@Accion int, 
@ProductoID int null,
@CodigoProducto varchar(max) null,
@Nombre varchar(max) null,
@TipoProductoID int null,
@Descripcion varchar(max) null,
@Imagen varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Productos values(@CodigoProducto, @Nombre, @TipoProductoID, @Descripcion, @Imagen, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Productos set CodigoProducto = @CodigoProducto, Nombre = @Nombre, TipoProductoID = @TipoProductoID, Descripcion = @Descripcion,
				Imagen = @Imagen, Estado = 1 where ProductoID = @ProductoID
			end
		else  IF (@Accion = 3)
			begin
				update Productos set Estado = 0 where ProductoID = @ProductoID;
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_ProveedorCRUD]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ProveedorCRUD]
@Accion int, 
@ProveedorID int null,
@CodigoProveedor varchar(max) null,
@Nombre varchar(max) null,
@Telefono varchar(max) null,
@Correo varchar(max) null,
@Direccion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Proveedor values(@CodigoProveedor, @Nombre, @Telefono, @Correo, @Direccion, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Proveedor set CodigoProveedor = @CodigoProveedor, Nombre = @Nombre, Telefono = @Telefono, Correo = @Correo,
				Direccion = @Direccion, Estado = 1 where ProveedorID = @ProveedorID;
			end
		else  IF (@Accion = 3)
			begin
				update Proveedor set Estado = 0 where ProveedorID = @ProveedorID;
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_setSpecificRoleForUser]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_setSpecificRoleForUser]
	@UsuarioID int,
	@RoleID int
AS
BEGIN
	SET NOCOUNT ON;
	delete from UsersInRoles where User_UserID = @UsuarioID;
	insert into UsersInRoles values(@UsuarioID,@RoleID);
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UsuariosCRUD]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_UsuariosCRUD]
@Accion int, 
@UsuarioID int null,
@Identificacion varchar(max) null,
@UserName varchar(max) null,
@Password varchar(max) null,
@NombreCompleto varchar(max) null,
@Telefono varchar(max) null,
@Correo varchar(max) null,
@Direccion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Usuarios values(@Identificacion, @UserName, @Password, @NombreCompleto, @Telefono, @Correo, @Direccion,1);
			end
		else IF (@Accion = 2) 
			begin
				update Usuarios set Identificacion = @Identificacion, UserName = @UserName, Password = @Password, NombreCompleto = @NombreCompleto,
				Telefono = @Telefono, Correo = @Correo, Direccion = @Direccion, Estado = 1 where UsuarioID = @UsuarioID
			end
		else  IF (@Accion = 3)
			begin
				update Usuarios set Estado = 0 where UsuarioID = @UsuarioID;
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_VariablesMaquinaInyectoraCRUD]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_VariablesMaquinaInyectoraCRUD]
@Accion int, 
@VariablesMaquinaInyectoraID int null,
@MaquinaID int null,
@ProductoID int null,
@MateriaPrimaPrincipalID int null,
@MateriaPrimaSecundariaID int null,
@MateriaPrimaTerciariaID int null,
@CantidadPrimaPrincipal int null,
@CantidadPrimaSecundaria int null,
@CantidadPrimaTerciaria int null
as
begin
		IF (@Accion = 1)
			begin 
				insert into VariablesMaquinaInyectora values(@MaquinaID, @ProductoID, @MateriaPrimaPrincipalID, @MateriaPrimaSecundariaID,
				@MateriaPrimaTerciariaID, @CantidadPrimaPrincipal, @CantidadPrimaSecundaria, @CantidadPrimaTerciaria, 1);
			end
		else IF (@Accion = 2) 
			begin
				update VariablesMaquinaInyectora set MaquinaID = @MaquinaID, ProductoID = @ProductoID, MateriaPrimaPrincipalID = @MateriaPrimaPrincipalID,
				MateriaPrimaSecundariaID = @MateriaPrimaSecundariaID, MateriaPrimaTerciariaID = @MateriaPrimaTerciariaID, CantidadPrimaPrincipal =
				@CantidadPrimaPrincipal, CantidadPrimaSecundaria = @CantidadPrimaSecundaria, CantidadPrimaTerciaria = @CantidadPrimaTerciaria,
				Estado = 1
				where VariablesMaquinaInyectoraID = @VariablesMaquinaInyectoraID
			end
		else  IF (@Accion = 3)
			begin
				update VariablesMaquinaInyectora set Estado = 0 where VariablesMaquinaInyectoraID = @VariablesMaquinaInyectoraID
			end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_VariablesMaquinaSopladoraCRUD]    Script Date: 22/08/2020 10:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_VariablesMaquinaSopladoraCRUD]
@Accion int, 
@VariablesMaquinaSopladoraID int null,
@MaquinaID int null,
@ProductoID int null,
@MateriaPrimaID int null,
@RetardoPresoplado int null,
@RetardoSoplado int null,
@TiempoPresoplado int null,
@TiempoRecuperacion int null,
@TiempoSoplado int null,
@TiempoEscape int null,
@TiempoParoCadena int null,
@TiempoSalidaPinzaPreforma int null,
@FrecuenciaVentilador float null,
@PresionEstirado float null,
@Gancho bit null,
@Relay bit null,
@AnillosTornelas bit null,
@Notas varchar(max) null,
@CantidadZonas int null,
@CantidadBancos int null,
@PosicionBanco1Zona1 varchar(max) null,
@PosicionBanco1Zona2 varchar(max) null,
@PosicionBanco1Zona3 varchar(max) null,
@PosicionBanco1Zona4 varchar(max) null,
@PosicionBanco2Zona1 varchar(max) null,
@PosicionBanco2Zona2 varchar(max) null,
@PosicionBanco2Zona3 varchar(max) null,
@PosicionBanco2Zona4 varchar(max) null,
@ValorBanco1Zona1 int null,
@ValorBanco1Zona2 int null,
@ValorBanco1Zona3 int null,
@ValorBanco1Zona4 int null,
@ValorBanco2Zona1 int null,
@ValorBanco2Zona2 int null,
@ValorBanco2Zona3 int null,
@ValorBanco2Zona4 int null
as
begin
		IF (@Accion = 1)
			begin 
				insert into VariablesMaquinaSopladora values(@MaquinaID,@ProductoID, @MateriaPrimaID, @RetardoPresoplado,
				@RetardoSoplado, @TiempoPresoplado, @TiempoRecuperacion, @TiempoSoplado, @TiempoEscape, @TiempoParoCadena,
				@TiempoSalidaPinzaPreforma, @FrecuenciaVentilador, @PresionEstirado, @Gancho, @Relay, @AnillosTornelas, 1,
				@Notas, @CantidadZonas, @CantidadBancos, @PosicionBanco1Zona1,@PosicionBanco1Zona2,@PosicionBanco1Zona3,@PosicionBanco1Zona4
				,@PosicionBanco2Zona1,@PosicionBanco2Zona2,@PosicionBanco2Zona3,@PosicionBanco2Zona4,@ValorBanco1Zona1,@ValorBanco1Zona2,
				@ValorBanco1Zona3,@ValorBanco1Zona4,@ValorBanco2Zona1,@ValorBanco2Zona2,@ValorBanco2Zona3,@ValorBanco2Zona4);
			end
		else IF (@Accion = 2) 
			begin
				update VariablesMaquinaSopladora set MaquinaID = @MaquinaID, ProductoID = @ProductoID, MateriaPrimaID = @MateriaPrimaID,
				RetardoPresoplado = @RetardoPresoplado, RetardoSoplado = @RetardoSoplado, TiempoPresoplado = @TiempoPresoplado, TiempoRecuperacion =
				@TiempoRecuperacion, TiempoSoplado = @TiempoSoplado, TiempoEscape = @TiempoEscape, TiempoParoCadena = @TiempoParoCadena,
				TiempoSalidaPinzaPreforma = @TiempoSalidaPinzaPreforma, FrecuenciaVentilador = @FrecuenciaVentilador, PresionEstirado = @PresionEstirado,
				Gancho = @Gancho, Relay = @Relay, AnillosTornelas = @AnillosTornelas, Estado = 1, Notas = @Notas, CantidadZonas = @CantidadZonas,
				CantidadBancos = @CantidadBancos, PosicionBanco1Zona1 = @PosicionBanco1Zona1, PosicionBanco1Zona2 = @PosicionBanco1Zona2,
				PosicionBanco1Zona3 = @PosicionBanco1Zona3, PosicionBanco1Zona4 = @PosicionBanco1Zona4, PosicionBanco2Zona1 = @PosicionBanco2Zona1,
				PosicionBanco2Zona2 = @PosicionBanco2Zona2, PosicionBanco2Zona3 = @PosicionBanco2Zona3, PosicionBanco2Zona4 = @PosicionBanco2Zona4,
				ValorBanco1Zona1 = @ValorBanco1Zona1, ValorBanco1Zona2 = @ValorBanco1Zona2, ValorBanco1Zona3 = @ValorBanco1Zona3, ValorBanco1Zona4 =
				@ValorBanco1Zona4, ValorBanco2Zona1 = @ValorBanco2Zona1, ValorBanco2Zona2 = @ValorBanco2Zona2, ValorBanco2Zona3 = @ValorBanco2Zona3,
				ValorBanco2Zona4 = @ValorBanco2Zona4 where VariablesMaquinaSopladoraID = @VariablesMaquinaSopladoraID
			end
		else  IF (@Accion = 3)
			begin
				update VariablesMaquinaSopladora set Estado = 0 where VariablesMaquinaSopladoraID = @VariablesMaquinaSopladoraID;
			end
end
GO
USE [master]
GO
ALTER DATABASE [DIPREMA] SET  READ_WRITE 
GO
