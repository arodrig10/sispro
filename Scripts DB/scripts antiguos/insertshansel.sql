USE [DIPREMA]
GO
SET IDENTITY_INSERT [dbo].[Clientes] ON 

INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (1, N'282575389', N'Rango Plastico', N'24389455', N'Rango@hotmail.com', N'Heredia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (2, N'645568872', N'Contínua Plastico', N'24400926', N'Contí@hotmail.com', N'Alajuela', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (3, N'508576043', N'Fidelity Plastico', N'24384634', N'Fidel@hotmail.com', N'Heredia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (4, N'386804833', N'Plastico Low', N'24406585', N'Plast@hotmail.com', N'Puntarenas', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (5, N'422603399', N'Anorak Plastico', N'24394613', N'Anora@hotmail.com', N'Limón', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (6, N'334420023', N'Plastico Sorpresas', N'24384475', N'Plast@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (7, N'140604326', N'Público Plastico', N'24392715', N'Públi@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (8, N'566148711', N'Puntos Plastico', N'24406595', N'Punto@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (9, N'435628159', N'Switch Plastico', N'24381348', N'Switc@hotmail.com', N'Guanacaste', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (10, N'264938824', N'Feliz Plastico', N'24392552', N'Feliz@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (11, N'499099817', N'Plastico Prevee', N'24402637', N'Plast@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (12, N'496153743', N'Plastico Cara', N'24392454', N'Plast@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (13, N'524384425', N'Plastico Analítica', N'24401237', N'Plast@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (14, N'545368129', N'Amarillos Plastico', N'24397413', N'Amari@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (15, N'573143265', N'Plastico Ejes', N'24406416', N'Plast@hotmail.com', N'Alajuela', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (16, N'390123888', N'Huellas Plastico', N'24402685', N'Huell@hotmail.com', N'Heredia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (17, N'668481211', N'Plastico Broadcast', N'24394099', N'Plast@hotmail.com', N'Puntarenas', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (18, N'284395720', N'Libre Plastico', N'24380950', N'Libre@hotmail.com', N'Puntarenas', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (19, N'643270140', N'Plastico Hydro', N'24409772', N'Plast@hotmail.com', N'Puntarenas', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (20, N'632802352', N'Plastico índices', N'24382488', N'Plast@hotmail.com', N'San José', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (21, N'28962378', N'Carlos luis', N'55656215615', N'carlos@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (22, N'288943878', N'Fernando Ruiz', N'2154523', N'fer@gmail.com', N'Heredia', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (23, N'546455465', N'luis ulloa', N'56415', N'carlos@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (24, N'556y65', N'fgnbgfnb', N'94122141', N'humurillo66@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (25, N'822112', N'Carlos luis', N'56415', N'hmurillo66@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (26, N'28962378', N'Carlos luis', N'56415', N'humurillo66@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Clientes] ([ClienteID], [Identificacion], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (27, N'423', N'sc', N'12445414', N'fer@gmail.com', N'Heredia', 1)
SET IDENTITY_INSERT [dbo].[Clientes] OFF
SET IDENTITY_INSERT [dbo].[TipoProducto] ON 

INSERT [dbo].[TipoProducto] ([TipoProductoID], [Tipo]) VALUES (1, N'Botella')
INSERT [dbo].[TipoProducto] ([TipoProductoID], [Tipo]) VALUES (2, N'Galon')
INSERT [dbo].[TipoProducto] ([TipoProductoID], [Tipo]) VALUES (3, N'Tapa')
SET IDENTITY_INSERT [dbo].[TipoProducto] OFF
SET IDENTITY_INSERT [dbo].[Productos] ON 

INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (30, N'30', N'prueba', 2, N'descrp prueba', N'6756', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (34, N'1', N'Botella Locales', 3, N'Lorem Ipsum es ', N'76567', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (35, N'2', N'Botella Máximo', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (36, N'3', N'áurea Botella', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (37, N'4', N'Resultado Botella', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (38, N'5', N'Botella Tempo', 1, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (39, N'6', N'Tapa Sucualentas', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (40, N'7', N'Tapa Timer', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (41, N'8', N'Tapa Aluminios', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (42, N'9', N'Tapa Flexión', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (43, N'10', N'Clásica Tapa', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (44, N'11', N'óptima Tapa', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (45, N'12', N'Sepia Tapa', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (46, N'13', N'Perfil Galon', 1, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (47, N'14', N'Galon Entornos', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (48, N'15', N'Galon Fashionistas', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (49, N'16', N'Barrera Galon', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (50, N'17', N'Mútuos Galon', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (51, N'18', N'Galon Historia', 1, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (52, N'19', N'Galon Florales', 2, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (53, N'20', N'Galon Burbujas', 3, N'Lorem Ipsum es simple', N'1', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (54, N'645654', N'frdfgd', 1, N'ryttg', N'54654654', 1)
INSERT [dbo].[Productos] ([ProductoID], [CodigoProducto], [Nombre], [TipoProductoID], [Descripcion], [Imagen], [Estado]) VALUES (55, N'1213', N'pruebaSemana10', 1, N'botella 10ml', N'987678', 1)
SET IDENTITY_INSERT [dbo].[Productos] OFF
SET IDENTITY_INSERT [dbo].[UnidadMedida] ON 

INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (1, N'Kilogramos')
INSERT [dbo].[UnidadMedida] ([UnidadMedidaID], [Unidad]) VALUES (2, N'Unidades')
SET IDENTITY_INSERT [dbo].[UnidadMedida] OFF
SET IDENTITY_INSERT [dbo].[Usuarios] ON 

INSERT [dbo].[Usuarios] ([UsuarioID], [Identificacion], [UserName], [Password], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (1, N'112233445', N'hansel', N'26d6a8ad97c75ffc548f6873e5e93ce475479e3e1a1097381e54221fb53ec1d2', N'Hansel Murillo', N'88888888', N'hmuri@fide.com', N'Alajuela', 1)
INSERT [dbo].[Usuarios] ([UsuarioID], [Identificacion], [UserName], [Password], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (2, N'28962378', N'prueba', N'f2dd0b5be829d35a402aa6e9e8938b075d67d810ad2838e47e60d0cb5f96c5d0', N'Jhon salchichon', N'88888888', N'hmurillo66@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Usuarios] ([UsuarioID], [Identificacion], [UserName], [Password], [NombreCompleto], [Telefono], [Correo], [Direccion], [Estado]) VALUES (3, N'822112', N'andresCarePez', N'e9b32be7b2985cdd46d2d9318aa95a3376c1c9865ff6771629729f605c38033b', N'Andres Rodriguez Alfaro', N'94122141', N'arodrig10@gmail.com', N'Heredia', 1)
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
SET IDENTITY_INSERT [dbo].[Pedidos] ON 

INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (1, N'4565', 17, N'234567', 44, 1, 1, 100, N'ghjgh', CAST(N'2020-08-20' AS Date), 9000.0000, 1)
INSERT [dbo].[Pedidos] ([PedidoID], [NumeroPedido], [ClienteID], [NumeroOrdenDeCompra], [ProductoID], [UsuarioID], [UnidadMedidaID], [Cantidad], [Descripcion], [Fecha], [PrecioTotal], [Estado]) VALUES (2, N'6765', 8, N'42537384', 42, 1, 1, 100, N'jckmfmdfngdf', CAST(N'2020-08-23' AS Date), 20000.0000, 1)
SET IDENTITY_INSERT [dbo].[Pedidos] OFF
SET IDENTITY_INSERT [dbo].[AperturaCierre] ON 

INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (26, CAST(N'2020-08-17T18:06:23.000' AS DateTime), NULL, N'Mañana', 1, 1, 1)
INSERT [dbo].[AperturaCierre] ([AperturaCierreID], [FechaApertura], [FechaCierre], [Turno], [PedidoID], [Estado], [MaquinaID]) VALUES (27, CAST(N'2020-08-17T18:13:47.000' AS DateTime), CAST(N'2020-08-17T18:14:00.143' AS DateTime), N'Tarde', 1, 0, 1)
SET IDENTITY_INSERT [dbo].[AperturaCierre] OFF
SET IDENTITY_INSERT [dbo].[Entregas] ON 

INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (4, N'1', CAST(N'2020-08-19' AS Date), 280, N'395', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (5, N'2', CAST(N'2020-08-20' AS Date), 24, N'13', 0)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (6, N'3', CAST(N'2020-08-20' AS Date), 12154, N'53132', 0)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (7, N'4', CAST(N'2020-08-20' AS Date), 20, N'45', 0)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (8, N'5', CAST(N'2020-08-20' AS Date), 125, N'12', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (9, N'6', CAST(N'2020-08-20' AS Date), 20, N'123', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (10, N'4', CAST(N'2020-08-20' AS Date), 300, N'123', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (11, N'78', CAST(N'2020-08-20' AS Date), 12, N'234', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (12, N'79', CAST(N'2020-08-20' AS Date), 20, N'2132', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (13, N'80', CAST(N'2020-08-20' AS Date), 10, N'13', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (14, N'7', CAST(N'2020-08-20' AS Date), 40, N'123', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (15, N'11', CAST(N'2020-08-20' AS Date), 10, N'10', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (16, N'12', CAST(N'2020-08-20' AS Date), 10, N'10', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (17, N'14', CAST(N'2020-08-20' AS Date), 40, N'10', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (18, N'81', CAST(N'2020-08-20' AS Date), 10, N'6567', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (19, N'82', CAST(N'2020-08-20' AS Date), 10, N'6567', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (20, N'83', CAST(N'2020-08-21' AS Date), 20, N'365', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (21, N'84', CAST(N'2020-08-21' AS Date), 30, N'84', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (22, N'90', CAST(N'2020-08-21' AS Date), 421, N'369', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (23, N'91', CAST(N'2020-08-21' AS Date), 20, N'10', 1)
INSERT [dbo].[Entregas] ([EntregaID], [NumeroEntrega], [Fecha], [UnidadesEntregadas], [NumeroFactura], [Estado]) VALUES (24, N'93', CAST(N'2020-08-21' AS Date), 190, N'93', 1)
SET IDENTITY_INSERT [dbo].[Entregas] OFF
SET IDENTITY_INSERT [dbo].[Produccion] ON 

INSERT [dbo].[Produccion] ([ProduccionID], [PedidoID], [FechaInicio], [FechaFin], [HorasTrabajadas], [CantidadMerma], [CantidadProduccionTotal], [CantidadPaquetesCajas], [Estado]) VALUES (2, 1, CAST(N'2020-08-17T18:06:23.000' AS DateTime), CAST(N'2020-08-17T22:31:40.000' AS DateTime), N'04:25:17', 0, 25, 2, 1)
SET IDENTITY_INSERT [dbo].[Produccion] OFF
SET IDENTITY_INSERT [dbo].[DetalleEntrega] ON 

INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (2, 4, 2, 1, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (8, 11, 2, 12, 12, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (13, 15, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (14, 16, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (15, 17, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (16, 17, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (17, 17, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (18, 17, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (26, 4, 2, 10, 5, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (27, 4, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (29, 4, 2, 9, 9, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (30, 4, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (33, 18, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (34, 19, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (35, 20, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (36, 20, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (37, 21, 2, 1, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (38, 21, 2, 2, 20, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (39, 22, 2, 2, 20, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (40, 22, 2, 1, 1, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (41, 22, 2, 20, 200, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (42, 22, 2, 300, 200, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (43, 23, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (44, 23, 2, 10, 10, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (45, 24, 2, 90, 90, 1)
INSERT [dbo].[DetalleEntrega] ([DetelleEntregaID], [EntregaID], [ProduccionID], [Paquetes], [Cantidad], [Estado]) VALUES (46, 24, 2, 100, 100, 1)
SET IDENTITY_INSERT [dbo].[DetalleEntrega] OFF
SET IDENTITY_INSERT [dbo].[TipoMaquina] ON 

INSERT [dbo].[TipoMaquina] ([TipoMaquinaID], [Tipo]) VALUES (1, N'Inyectora')
INSERT [dbo].[TipoMaquina] ([TipoMaquinaID], [Tipo]) VALUES (2, N'Sopladora')
SET IDENTITY_INSERT [dbo].[TipoMaquina] OFF
SET IDENTITY_INSERT [dbo].[Maquinas] ON 

INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (1, N'cod-01', N'Maria', 1, N'Maquina', N'12', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (2, N'cod-2', N'lucia', 2, N'máquina para trabajar preformas', N'12', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (3, N'cod-003', N'Pancha', 2, N'Sopaladora de una boquilla', N'12', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (4, N'cod-04', N'lola', 1, N'inyectora', N'23', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (5, N'34324', N'mar', 1, N'Inyectora nueva', N'122', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (6, N'123', N'cila', 1, N'vieja', N'67', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (7, N'67', N'betty', 2, N'Para tapas', N'56', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (8, N'9089', N'yui', 1, N'iny', N'34', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (9, N'u798', N'toyo', 1, N'Falta  ensamblar', N'234', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (10, N'989', N'prueba', 1, N'prueba', N'89', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (11, N'9087', N'pola', 2, N'China', N'13', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (12, N'y876', N'y789', 1, N'china', N'90', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (13, N'5678', N'yayao', 1, N'Alemana', N'678', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (14, N'878', N'Maria', 1, N'skdjvkv', N'6567', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (15, N'768', N'Maria 2', 1, N'ueuyew', N'78', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (16, N'98786', N'Maria', 1, N'gjf', N'678', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (17, N'83834', N'HP', 1, N'Tica', N'65', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (18, N'23', N'prueba', 2, N'Mensaje', N'47', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (19, N'768', N'prueba', 1, N'rttry', N'4', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (20, N'768', N'Maria', 1, N'rfdg', N'gfd', 0)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (21, N'768', N'Maria', 1, N'rfdg', N'gfd', 0)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (22, N'76876', N'jhgj', 1, N'fgjhfd', N'6787', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (23, N'rthb', N'Maria', 1, N'fdghbgd', N'54', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (24, N'rthb', N'Maria', 1, N'fdghbgd', N'54', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (25, N'5534', N'dfg', 1, N'dfsgd', N'5453', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (26, N'645', N'3465', 1, N'dfrg', N'gsdfgdf', 0)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (27, N'100000', N'gdfg', 1, N'dfg', N'dfg', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (28, N'999999', N'999999', 1, N'999999', N'9999999', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (29, N'prueba de mensaje', N'prueba', 1, N'Prueba de mensaje', N'12', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (30, N'768', N'fghgf', 1, N'hsdahdjuas', N'23', 1)
INSERT [dbo].[Maquinas] ([MaquinaID], [CodigoMaquina], [Nombre], [TipoMaquinaID], [Descripcion], [ProduccionHora], [Estado]) VALUES (31, N'768', N'prueba', 1, N'999999', N'234', 1)
SET IDENTITY_INSERT [dbo].[Maquinas] OFF
SET IDENTITY_INSERT [dbo].[Proveedor] ON 

INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (1, N'1', N'3M', N'24389455', N'maeillanes@hotmail.com', N'Heredia', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (2, N'2', N'Abbott Vascular', N'24400926', N'osabarca@hotmail.com', N'Alajuela', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (3, N'3', N'Accenture', N'24384634', N'cabrigo@garmendia.cl', N'Heredia', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (4, N'4', N'Alimentos Prosalud', N'24406585', N'Sb.nashxo.sk8@hotmail.com', N'Puntarenas', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (5, N'5', N'APM Terminals', N'24394613', N'fran.afull@live.cl', N'Limón', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (6, N'6', N'Asesoría Nairí', N'24384475', N'carlosaguileram@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (7, N'7', N'AutoMercado', N'24392715', N'ikis_rojo@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (8, N'8', N'AVON', N'24406595', N'daniela_aguilera_m500@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (9, N'9', N'Azucarera El Viejo', N'24381348', N'vizkala@hotmail.com', N'Guanacaste', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (10, N'10', N'BAC Credomatic', N'24392552', N'alexus3@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (11, N'11', N'Baker Tilly', N'24402637', N'capitanaguilera@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (12, N'12', N'Banco BCT', N'24392454', N'apalamosg@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (13, N'13', N'Banco de Costa Rica', N'24401237', N'niikhox__@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (14, N'14', N'Banco Nacional de Costa Rica', N'24397413', N'luuuuuuci@hotmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (15, N'15', N'Banco Popular y de Desarrollo Comunal', N'24406416', N'kristian_siempre_azul@hotmail.com', N'Alajuela', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (16, N'16', N'Batalla', N'24402685', N'mapuchin@hotmail.com', N'Heredia', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (17, N'17', N'BCR Corredora de Seguros', N'24394099', N'arahuetes@manquehue.net', N'Puntarenas', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (18, N'18', N'BCR Pensiones', N'24380950', N'eduardo.arancibia@grange.cl', N'Puntarenas', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (19, N'19', N'BCR Valores', N'24409772', N'martacam2002@yahoo.com', N'Puntarenas', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (20, N'20', N'BDS ASESORES', N'24382488', N'andrea.geoplanet@gmail.com', N'San José', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (21, N'43543', N'EL gallo mas gallo', N'12445414', N'carlos@gmail.com', N'Limon', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (22, N'43543', N'Maria', N'2154523', N'hmurillo66@gmail.com', N'Heredia', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (23, N'45646', N'lsdlkfvds', N'6566', N'ghghjgjh', N'hjhjkh', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (24, N'43543', N'Maria', N'94122141', N'humurillo66@gmail.com', N'Alajuela', 1)
INSERT [dbo].[Proveedor] ([ProveedorID], [CodigoProveedor], [Nombre], [Telefono], [Correo], [Direccion], [Estado]) VALUES (25, N'1532', N'U. Fide', N'56415', N'fide@gmail.com', N'Heredia', 1)
SET IDENTITY_INSERT [dbo].[Proveedor] OFF
SET IDENTITY_INSERT [dbo].[TipoMateriaPrima] ON 

INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (1, N'Resina Baja Densidad')
INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (2, N'Resina Alta Densidad')
INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (3, N'Pigmento')
INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (4, N'Colorante')
INSERT [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID], [Tipo]) VALUES (5, N'Preformas')
SET IDENTITY_INSERT [dbo].[TipoMateriaPrima] OFF
SET IDENTITY_INSERT [dbo].[MateriaPrima] ON 

INSERT [dbo].[MateriaPrima] ([MateriaPrimaID], [CodigoMateriaPrima], [Nombre], [ProveedorID], [TipoMateriaPrimaID], [UnidadMedidaID], [Cantidad], [PrecioUnitario], [PrecioTotal], [Descripcion], [Estado]) VALUES (1, N'43543', N'preforma', 1, 1, 2, 15, 2.0000, 30.0000, N'esto lo debe de realizar el sistema o bd Precio total', 1)
SET IDENTITY_INSERT [dbo].[MateriaPrima] OFF
SET IDENTITY_INSERT [dbo].[VariablesMaquinaInyectora] ON 

INSERT [dbo].[VariablesMaquinaInyectora] ([VariablesMaquinaInyectoraID], [MaquinaID], [ProductoID], [MateriaPrimaPrincipalID], [MateriaPrimaSecundariaID], [MateriaPrimaTerciariaID], [CantidadPrimaPrincipal], [CantidadPrimaSecundaria], [CantidadPrimaTerciaria], [Estado]) VALUES (1, 14, 51, 1, 1, NULL, 500, 2000, 3000, 1)
INSERT [dbo].[VariablesMaquinaInyectora] ([VariablesMaquinaInyectoraID], [MaquinaID], [ProductoID], [MateriaPrimaPrincipalID], [MateriaPrimaSecundariaID], [MateriaPrimaTerciariaID], [CantidadPrimaPrincipal], [CantidadPrimaSecundaria], [CantidadPrimaTerciaria], [Estado]) VALUES (2, 16, 34, 1, 1, NULL, 30, 50, 20, 1)
INSERT [dbo].[VariablesMaquinaInyectora] ([VariablesMaquinaInyectoraID], [MaquinaID], [ProductoID], [MateriaPrimaPrincipalID], [MateriaPrimaSecundariaID], [MateriaPrimaTerciariaID], [CantidadPrimaPrincipal], [CantidadPrimaSecundaria], [CantidadPrimaTerciaria], [Estado]) VALUES (3, 14, 44, 1, 1, NULL, 20, 30, 20, 1)
SET IDENTITY_INSERT [dbo].[VariablesMaquinaInyectora] OFF
SET IDENTITY_INSERT [dbo].[VariablesMaquinaSopladora] ON 

INSERT [dbo].[VariablesMaquinaSopladora] ([VariablesMaquinaSopladoraID], [MaquinaID], [ProductoID], [MateriaPrimaID], [RetardoPresoplado], [RetardoSoplado], [TiempoPresoplado], [TiempoRecuperacion], [TiempoSoplado], [TiempoEscape], [TiempoParoCadena], [TiempoSalidaPinzaPreforma], [FrecuenciaVentilador], [PresionEstirado], [Gancho], [Relay], [AnillosTornelas], [Estado], [Notas], [CantidadZonas], [CantidadBancos], [PosicionBanco1Zona1], [PosicionBanco1Zona2], [PosicionBanco1Zona3], [PosicionBanco1Zona4], [PosicionBanco2Zona1], [PosicionBanco2Zona2], [PosicionBanco2Zona3], [PosicionBanco2Zona4], [ValorBanco1Zona1], [ValorBanco1Zona2], [ValorBanco1Zona3], [ValorBanco1Zona4], [ValorBanco2Zona1], [ValorBanco2Zona2], [ValorBanco2Zona3], [ValorBanco2Zona4]) VALUES (1, 15, 50, 1, 20, 30, 20, 23, 23, 654, 45, 456, 45, 415, 1, 0, 1, 1, N'bajar velocidad', 1, 1, N'1', N'3', N'2', N'2', N'3', N'5', N'3', N'2', 2, 3, 2, 1, 33, 55, 23, 23)
INSERT [dbo].[VariablesMaquinaSopladora] ([VariablesMaquinaSopladoraID], [MaquinaID], [ProductoID], [MateriaPrimaID], [RetardoPresoplado], [RetardoSoplado], [TiempoPresoplado], [TiempoRecuperacion], [TiempoSoplado], [TiempoEscape], [TiempoParoCadena], [TiempoSalidaPinzaPreforma], [FrecuenciaVentilador], [PresionEstirado], [Gancho], [Relay], [AnillosTornelas], [Estado], [Notas], [CantidadZonas], [CantidadBancos], [PosicionBanco1Zona1], [PosicionBanco1Zona2], [PosicionBanco1Zona3], [PosicionBanco1Zona4], [PosicionBanco2Zona1], [PosicionBanco2Zona2], [PosicionBanco2Zona3], [PosicionBanco2Zona4], [ValorBanco1Zona1], [ValorBanco1Zona2], [ValorBanco1Zona3], [ValorBanco1Zona4], [ValorBanco2Zona1], [ValorBanco2Zona2], [ValorBanco2Zona3], [ValorBanco2Zona4]) VALUES (2, 10, 34, 1, 20, 20, 30, 20, 30, 20, 20, 30, 20, 30, 1, 0, 1, 1, N'bajar velocidad', 1, 1, N'5', N'5', N'3', N'6', N'26', N'256', N'8', N'52', 30, 61, 512, 20, 20, 952, 30, 20)
SET IDENTITY_INSERT [dbo].[VariablesMaquinaSopladora] OFF
SET IDENTITY_INSERT [dbo].[FichaTecnicaBotella] ON 

INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (16, 30, 11, 25, 22, 13, 26, 0.5, N'73680273', N'74863379', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (20, 34, 23, 23, 19, 20, 23, 0.5, N'52230002', N'75021434', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (21, 35, 10, 11, 6, 11, 29, 0.4, N'58586969', N'14197490', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (23, 30, 25, 17, 13, 17, 15, 0.5, N'81459375', N'26877001', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (27, 34, 26, 24, 22, 23, 29, 0.5, N'80205297', N'45251123', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (28, 35, 28, 13, 10, 29, 26, 0.4, N'54439195', N'53986735', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (29, 36, 26, 27, 22, 20, 14, 0.5, N'34803404', N'48324328', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (30, 37, 26, 21, 16, 14, 16, 0.5, N'69861186', N'39798943', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (31, 38, 13, 11, 10, 22, 29, 0.5, N'45074716', N'67884073', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (32, 39, 23, 30, 26, 25, 18, 0.1, N'79554269', N'77576660', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (33, 40, 22, 13, 9, 15, 24, 0.5, N'59113167', N'24717186', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (34, 41, 17, 27, 23, 17, 21, 0.5, N'22965977', N'43996248', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (35, 42, 14, 17, 15, 13, 17, 0.4, N'55742186', N'21787016', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (36, 43, 14, 19, 15, 23, 23, 0.5, N'9451288', N'27707205', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (37, 44, 12, 10, 9, 18, 23, 0.2, N'66849433', N'83818074', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (38, 45, 12, 13, 10, 22, 16, 0.4, N'8431082', N'69373844', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (39, 46, 17, 23, 18, 15, 29, 0.5, N'70788081', N'15061881', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (40, 47, 25, 19, 16, 27, 25, 0.5, N'52344137', N'13114634', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (41, 48, 10, 15, 13, 13, 12, 0.5, N'36019385', N'25474468', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (42, 49, 24, 13, 10, 18, 16, 0.2, N'78046888', N'74000258', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (43, 50, 27, 16, 11, 11, 19, 0.5, N'32107644', N'53749763', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (44, 51, 30, 24, 23, 17, 29, 0.2, N'67570843', N'7153989', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (45, 52, 19, 12, 10, 17, 29, 0.5, N'70064575', N'15553461', 1)
INSERT [dbo].[FichaTecnicaBotella] ([FichaTecnicaBotellaID], [ProductoID], [Altura], [DiametroMayor], [DiametroMenor], [DiametroCuello], [CapacidadVolumetrica], [Tolerancia], [ImagenCuerpo], [ImagenFondo], [Estado]) VALUES (46, 41, 3, 2, 3, 2, 20, 3, N'113256', N'52125', 1)
SET IDENTITY_INSERT [dbo].[FichaTecnicaBotella] OFF
SET IDENTITY_INSERT [dbo].[FichaTecnicaTapa] ON 

INSERT [dbo].[FichaTecnicaTapa] ([FichaTecnicaTapaID], [ProductoID], [PesoPieza], [Altura], [DiametroInterno], [DiametroExternoSelloOliva], [AlturaSelloOliva], [DiametroInternoBandaSeguridad], [EspesorPanel], [NumeroEstrias], [TorqueCierreRecomendado], [TorqueApertura], [Tolerancia], [Imagen], [Estado]) VALUES (1, 41, 30, 52, 20, 13, 52, 20, 20, 30, 20, 10, 30, N'1512154231', 1)
SET IDENTITY_INSERT [dbo].[FichaTecnicaTapa] OFF
SET IDENTITY_INSERT [dbo].[ProduccionPaquetesCajas] ON 

INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (21, CAST(N'2020-08-17T18:14:11.000' AS DateTime), 10, 26, 1, N'Mañana', 1)
INSERT [dbo].[ProduccionPaquetesCajas] ([EtiquetaID], [Fecha], [Cantidad], [AperturaCierreID], [UsuarioID], [Turno], [Estado]) VALUES (22, CAST(N'2020-08-17T22:31:40.000' AS DateTime), 15, 26, 1, N'Mañana', 1)
SET IDENTITY_INSERT [dbo].[ProduccionPaquetesCajas] OFF
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([RoleID], [RoleName]) VALUES (1, N'Administrador')
SET IDENTITY_INSERT [dbo].[Roles] OFF
INSERT [dbo].[UsersInRoles] ([User_UserID], [Role_RoleID]) VALUES (2, 1)
