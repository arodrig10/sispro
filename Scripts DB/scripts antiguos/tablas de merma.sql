CREATE TABLE [dbo].[TipoMerma](
	[TipoMermaID] [int] primary key IDENTITY(1,1)NOT NULL,
	[Tipo] [varchar](200) NOT NULL,
)
GO


CREATE TABLE [dbo].[MermaPorTurno](
	[MermaPorTurnoID] [int] primary key IDENTITY(1,1) NOT NULL,
	[Cantidad] [float] NOT NULL,
	[AperturaCierreID] [bigint] NOT NULL,
	[TipoMermaID] [int] NOT NULL,
	[Estado] [bit] not null,
	foreign key (TipoMermaID) references TipoMerma(TipoMermaID),
	foreign key (AperturaCierreID) references AperturaCierre(AperturaCierreID)
)
GO

create view MermaPorTurnoActivos as 
SELECT        *
FROM            MermaPorTurno
WHERE        Estado = 1
