USE DIPREMA
GO

/*Creacion de las tablas*/

CREATE TABLE [dbo].[Clientes](
	[ClienteID] [int] IDENTITY(1,1) NOT NULL,
	[Identificacion] [varchar](50) NOT NULL,
	[NombreCompleto] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ClienteID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Entregas](
	[EntregaID] [int] IDENTITY(1,1) NOT NULL,
	[NumeroEntrega] [varchar](50) NOT NULL,
	[Fecha] [date] NULL,
	[UnidadesEntregadas] [int] NULL,
	[NumeroFactura] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EntregaID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[FichaTecnicaBotella](
	[FichaTecnicaBotellaID] [int] IDENTITY(1,1) NOT NULL,
	[ProductoID] [int] NOT NULL,
	[Altura] [float] NOT NULL,
	[DiametroMayor] [float] NOT NULL,
	[DiametroMenor] [float] NOT NULL,
	[DiametroCuello] [float] NOT NULL,
	[CapacidadVolumetrica] [float] NOT NULL,
	[Tolerancia] [float] NULL,
	[ImagenCuerpo] [varchar](max) NULL,
	[ImagenFondo] [varchar](max) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FichaTecnicaBotellaID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE TABLE [dbo].[FichaTecnicaTapa](
	[FichaTecnicaTapaID] [int] IDENTITY(1,1) NOT NULL,
	[ProductoID] [int] NOT NULL,
	[PesoPieza] [float] NOT NULL,
	[Altura] [float] NOT NULL,
	[DiametroInterno] [float] NOT NULL,
	[DiametroExternoSelloOliva] [float] NOT NULL,
	[AlturaSelloOliva] [float] NOT NULL,
	[DiametroInternoBandaSeguridad] [float] NOT NULL,
	[EspesorPanel] [float] NOT NULL,
	[NumeroEstrias] [float] NOT NULL,
	[TorqueCierreRecomendado] [float] NOT NULL,
	[TorqueApertura] [float] NOT NULL,
	[Tolerancia] [float] NULL,
	[Imagen] [varchar](max) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FichaTecnicaTapaID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE TABLE [dbo].[Maquinas](
	[MaquinaID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoMaquina] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[TipoMaquinaID] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[ProduccionHora] [varchar](100) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaquinaID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[MateriaPrima](
	[MateriaPrimaID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoMateriaPrima] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[ProveedorID] [int] NOT NULL,
	[TipoMateriaPrimaID] [int] NOT NULL,
	[UnidadMedidaID] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[PrecioUnitario] [money] NOT NULL,
	[PrecioTotal] [money] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MateriaPrimaID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Movimientos](
	[PedidoID] [int] NOT NULL,
	[EntregaID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Movimientos] PRIMARY KEY CLUSTERED 
(
	[PedidoID] ASC,
	[EntregaID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Pedidos](
	[PedidoID] [int] IDENTITY(1,1) NOT NULL,
	[NumeroPedido] [varchar](50) NOT NULL,
	[ClienteID] [int] NOT NULL,
	[NumeroOrdenDeCompra] [varchar](50) NULL,
	[ProductoID] [int] NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[UnidadMedidaID] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Fecha] [date] NULL,
	[PrecioTotal] [money] NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PedidoID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Productos](
	[ProductoID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProducto] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[TipoProductoID] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Imagen] [varchar](max) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductoID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE TABLE [dbo].[Proveedor](
	[ProveedorID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProveedor] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProveedorID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Roles](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE TABLE [dbo].[TipoMaquina](
	[TipoMaquinaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMaquinaID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[TipoMateriaPrima](
	[TipoMateriaPrimaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMateriaPrimaID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[TipoProducto](
	[TipoProductoID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoProductoID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[UnidadMedida](
	[UnidadMedidaID] [int] IDENTITY(1,1) NOT NULL,
	[Unidad] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UnidadMedidaID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[UsersInRoles](
	[User_UserID] [int] NOT NULL,
	[Role_RoleID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.RoleUsers] PRIMARY KEY CLUSTERED 
(
	[Role_RoleID] ASC,
	[User_UserID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Usuarios](
	[UsuarioID] [int] IDENTITY(1,1) NOT NULL,
	[Identificacion] [varchar](50) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [varchar](100) NOT NULL,
	[NombreCompleto] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UsuarioID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[VariablesMaquinaInyectora](
	[VariablesMaquinaInyectoraID] [int] IDENTITY(1,1) NOT NULL,
	[MaquinaID] [int] NOT NULL,
	[ProductoID] [int] NOT NULL,
	[MateriaPrimaPrincipalID] [int] NOT NULL,
	[MateriaPrimaSecundariaID] [int] NULL,
	[MateriaPrimaTerciariaID] [int] NULL,
	[CantidadPrimaPrincipal] [float] NOT NULL,
	[CantidadPrimaSecundaria] [float] NULL,
	[CantidadPrimaTerciaria] [float] NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[VariablesMaquinaInyectoraID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[VariablesMaquinaSopladora](
	[VariablesMaquinaSopladoraID] [int] IDENTITY(1,1) NOT NULL,
	[MaquinaID] [int] NOT NULL,
	[ProductoID] [int] NOT NULL,
	[MateriaPrimaID] [int] NOT NULL,
	[RetardoPresoplado] [int] NOT NULL,
	[RetardoSoplado] [int] NOT NULL,
	[TiempoPresoplado] [int] NOT NULL,
	[TiempoRecuperacion] [int] NOT NULL,
	[TiempoSoplado] [int] NOT NULL,
	[TiempoEscape] [int] NOT NULL,
	[TiempoParoCadena] [int] NOT NULL,
	[TiempoSalidaPinzaPreforma] [int] NOT NULL,
	[FrecuenciaVentilador] [float] NOT NULL,
	[PresionEstirado] [float] NOT NULL,
	[Gancho] [bit] NOT NULL,
	[Relay] [bit] NOT NULL,
	[AnillosTornelas] [bit] NOT NULL,
	[Estado] [bit] NOT NULL,
	[Notas] [varchar](100) NULL,
	[CantidadZonas] [int] NOT NULL,
	[CantidadBancos] [int] NOT NULL,
	[PosicionBanco1Zona1] [varchar](100) NULL,
	[PosicionBanco1Zona2] [varchar](100) NULL,
	[PosicionBanco1Zona3] [varchar](100) NULL,
	[PosicionBanco1Zona4] [varchar](100) NULL,
	[PosicionBanco2Zona1] [varchar](100) NULL,
	[PosicionBanco2Zona2] [varchar](100) NULL,
	[PosicionBanco2Zona3] [varchar](100) NULL,
	[PosicionBanco2Zona4] [varchar](100) NULL,
	[ValorBanco1Zona1] [int] NULL,
	[ValorBanco1Zona2] [int] NULL,
	[ValorBanco1Zona3] [int] NULL,
	[ValorBanco1Zona4] [int] NULL,
	[ValorBanco2Zona1] [int] NULL,
	[ValorBanco2Zona2] [int] NULL,
	[ValorBanco2Zona3] [int] NULL,
	[ValorBanco2Zona4] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[VariablesMaquinaSopladoraID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[ProduccionPaquetesCajas](
	[EtiquetaID] [int] IDENTITY(1,1) NOT NULL,
	[Fecha] [date] not NULL,
	[Cantidad] [int] NULL,
	[PedidoID] [int]  NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[Turno] [varchar](10) NOT NULL,
	[ProduccionID] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EtiquetaID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Produccion](
	[ProduccionID] [int] IDENTITY(1,1) NOT NULL,
	[FechaInicio] [date] not NULL,
	[FechaFin] [date] not NULL,
	[HorasTrabajadas] [date] not NULL,
	[CantidadMerma] [float] NULL,
	[CantidadProduccionTotal] [int] NULL,
	[CantidadPaquetesCajas] [int]  NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProduccionID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

/*Creacion de Foreign keys*/

ALTER TABLE [dbo].[FichaTecnicaBotella]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_FichaTecnicaBotella] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[FichaTecnicaBotella] CHECK CONSTRAINT [FK_ProductoID_FichaTecnicaBotella]
GO
ALTER TABLE [dbo].[FichaTecnicaTapa]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_FichaTecnicaTapa] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[FichaTecnicaTapa] CHECK CONSTRAINT [FK_ProductoID_FichaTecnicaTapa]
GO
ALTER TABLE [dbo].[Maquinas]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_Maquinas] FOREIGN KEY([TipoMaquinaID])
REFERENCES [dbo].[TipoMaquina] ([TipoMaquinaID])
GO
ALTER TABLE [dbo].[Maquinas] CHECK CONSTRAINT [FK_TipoID_Maquinas]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_ProveedorID_MateriaPrima] FOREIGN KEY([ProveedorID])
REFERENCES [dbo].[Proveedor] ([ProveedorID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_ProveedorID_MateriaPrima]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_MateriaPrima] FOREIGN KEY([TipoMateriaPrimaID])
REFERENCES [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_TipoID_MateriaPrima]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_UnidadMedidaID_MateriaPrima] FOREIGN KEY([UnidadMedidaID])
REFERENCES [dbo].[UnidadMedida] ([UnidadMedidaID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_UnidadMedidaID_MateriaPrima]
GO
ALTER TABLE [dbo].[Movimientos]  WITH CHECK ADD  CONSTRAINT [FK_EntregaID_Movimientos] FOREIGN KEY([EntregaID])
REFERENCES [dbo].[Entregas] ([EntregaID])
GO
ALTER TABLE [dbo].[Movimientos] CHECK CONSTRAINT [FK_EntregaID_Movimientos]
GO
ALTER TABLE [dbo].[Movimientos]  WITH CHECK ADD  CONSTRAINT [FK_PedidoID_Movimientos] FOREIGN KEY([PedidoID])
REFERENCES [dbo].[Pedidos] ([PedidoID])
GO
ALTER TABLE [dbo].[Movimientos] CHECK CONSTRAINT [FK_PedidoID_Movimientos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_ClienteID_Pedidos] FOREIGN KEY([ClienteID])
REFERENCES [dbo].[Clientes] ([ClienteID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_ClienteID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_Pedidos] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_ProductoID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_UnidadMedidaID_Pedidos] FOREIGN KEY([UnidadMedidaID])
REFERENCES [dbo].[UnidadMedida] ([UnidadMedidaID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_UnidadMedidaID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioID_Pedidos] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_UsuarioID_Pedidos]
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_Productos] FOREIGN KEY([TipoProductoID])
REFERENCES [dbo].[TipoProducto] ([TipoProductoID])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [FK_TipoID_Productos]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK_Role_RoleID_UserInRoles] FOREIGN KEY([Role_RoleID])
REFERENCES [dbo].[Roles] ([RoleID])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [FK_Role_RoleID_UserInRoles]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK_User_UserID_UserInRoles] FOREIGN KEY([User_UserID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [FK_User_UserID_UserInRoles]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MaquinaID_VariablesMaquinaInyectora] FOREIGN KEY([MaquinaID])
REFERENCES [dbo].[Maquinas] ([MaquinaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MaquinaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaPrincipalID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaPrincipalID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaPrincipalID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaSecundariaID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaSecundariaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaSecundariaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaTerciariaID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaTerciariaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaTerciariaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_VariablesMaquinaInyectora] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_ProductoID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_MaquinaID_VariablesMaquinaSopladora] FOREIGN KEY([MaquinaID])
REFERENCES [dbo].[Maquinas] ([MaquinaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_MaquinaID_VariablesMaquinaSopladora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaID_VariablesMaquinaSopladora] FOREIGN KEY([MateriaPrimaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_MateriaPrimaID_VariablesMaquinaSopladora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_VariablesMaquinaSopladora] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_ProductoID_VariablesMaquinaSopladora]
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas]  WITH CHECK ADD  CONSTRAINT [FK_PedidoID_ProduccionPaquetesCajas] FOREIGN KEY([PedidoID])
REFERENCES [dbo].[Pedidos] ([PedidoID])
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas] CHECK CONSTRAINT [FK_PedidoID_ProduccionPaquetesCajas]
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioID_ProduccionPaquetesCajas] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas] CHECK CONSTRAINT [FK_UsuarioID_ProduccionPaquetesCajas]
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas]  WITH CHECK ADD  CONSTRAINT [FK_ProduccionID_ProduccionPaquetesCajas] FOREIGN KEY([ProduccionID])
REFERENCES [dbo].[Produccion] ([ProduccionID])
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas] CHECK CONSTRAINT [FK_ProduccionID_ProduccionPaquetesCajas]
GO
ALTER TABLE Usuarios
ADD CONSTRAINT UC_Usuarios UNIQUE (UserName);
GO
/*Vistas*/
create VIEW ClientesActivos AS
select * from Clientes where Estado = 1;
GO
create VIEW EntregasActivos as
select * from Entregas where Estado = 1;
GO
create view FichaTecnicaBotellaActivos as
select * from FichaTecnicaBotella where Estado = 1;
GO
create view FichaTecnicaTapaActivos as
select * from FichaTecnicaTapa where Estado = 1;
GO
create view MaquinasActivos as
select * from Maquinas where Estado = 1;
GO
create view MateriaPrimaActivos as
select * from MateriaPrima where Estado = 1;
GO
create view PedidosActivos as
select * from Pedidos where Estado = 1;
GO
create view ProduccionActivos as
select * from Produccion where Estado = 1;
GO
create view ProduccionPaquetesCajaActivos as
select * from ProduccionPaquetesCajas where Estado = 1;
GO
create view ProductosActivos as
select * from Productos where Estado = 1;
GO
create view ProveedorActivos as
select * from Proveedor where Estado = 1;
GO
create view UsuariosActivos as
select * from Usuarios where Estado = 1;
GO
create view VariablesMaquinaInyectoraActivos as
select * from VariablesMaquinaInyectora where Estado = 1;
GO
create view VariablesMaquinaSopladoraActivos as
select * from VariablesMaquinaSopladora where Estado = 1;
GO
create view MaquinasSopladoraActivos as
select * from Maquinas where TipoMaquinaID = 1 and Estado = 1;
GO
create view MaquinasInyectoraActivos as
select * from Maquinas where TipoMaquinaID = 2 and Estado = 1;
go
create view MateriaPrimaInyectoraActivos as
select * from MateriaPrima where TipoMateriaPrimaID = 2 or TipoMateriaPrimaID = 3 and Estado = 1
GO
create view MateriaPrimaSopladoraActivos as
select * from MateriaPrima where TipoMateriaPrimaID = 1 and Estado = 1
GO
create view ProductosBotellaActivos as
select * from Productos where TipoProductoID = 1 and Estado = 1;
GO
create view ProductosTapaActivos as
select * from Productos where TipoProductoID = 2 and Estado = 1;
GO



/*Procedimientos almacenados*/


create PROCEDURE [dbo].[sp_getRolesForUser]
	@userName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	select r.RoleName from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioID
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where u.UserName=@userName
END
GO
create PROCEDURE [dbo].[sp_getUsuariosRole]
	
	@roleName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	select u.UserName from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioID
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where r.RoleName=@roleName 
END
GO
create PROCEDURE [dbo].[sp_isUserInRole]
	@userName varchar(20),
	@roleName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	declare @resultado bit =0;
	if exists(
	select * from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioId
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where r.RoleName=@roleName and u.UserName=@userName)
	set @resultado = 1
	 select @resultado
END


GO



create procedure sp_ClientesCRUD
@Accion int, 
@ClienteID int null,
@Identificacion varchar(max) null, 
@NombreCompleto varchar(max) null, 
@Telefono varchar(max) null, 
@Correo varchar(max) null, 
@Direccion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin
				INSERT INTO Clientes VALUES (@Identificacion, @NombreCompleto, @Telefono, @Correo, @Direccion, 1);			
			end
		else IF (@Accion = 2) 
			begin
				UPDATE Clientes set Identificacion = @Identificacion, NombreCompleto = @NombreCompleto, Telefono = @Telefono,
				Correo = @Correo, Direccion = @Direccion, Estado = 1 where ClienteID = @ClienteID
			end
		else  IF (@Accion = 3)
			begin
				UPDATE Clientes set Estado = 0 where ClienteID = @ClienteID
			end
end
GO
create procedure sp_EntregasCRUD
@Accion int, 
@EntregaID int null,
@NumeroEntrega varchar(max) null,
@Fecha date null,
@UnidadesEntregadas int null,
@NumeroFactura varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				INSERT INTO Entregas VALUES (@NumeroEntrega, @Fecha, @UnidadesEntregadas, @NumeroFactura, 1);			
			end
		else IF (@Accion = 2) 
			begin
				UPDATE Entregas set NumeroEntrega = @NumeroEntrega, Fecha = @Fecha, UnidadesEntregadas = @UnidadesEntregadas, 
				NumeroFactura = @NumeroFactura, Estado = 1 where EntregaID = @EntregaID
			end
		else  IF (@Accion = 3)
			begin
				UPDATE Entregas set Estado = 0 where EntregaID = @EntregaID
			end
end
GO
create procedure sp_FichaTecnicaBotellaCRUD
@Accion int, 
@FichaTecnicaBotellaID int null,
@ProductoID int null,
@Altura float null,
@DiametroMayor float null,
@DiametroMenor float null,
@DiametroCuello float null,
@CapacidadVolumetrica float null,
@Tolerancia float null,
@ImagenCuerpo varchar(max) null,
@ImagenFondo varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				INSERT INTO FichaTecnicaBotella
				VALUES (@ProductoID, @Altura, @DiametroMayor, @DiametroMenor,
				@DiametroCuello,@CapacidadVolumetrica,@Tolerancia, @ImagenCuerpo, @ImagenFondo ,1);			
			end
		else IF (@Accion = 2) 
			begin
				UPDATE FichaTecnicaBotella set ProductoID = @ProductoID, Altura = @Altura,
				DiametroMayor = @DiametroMayor, DiametroMenor = @DiametroMenor, DiametroCuello = @DiametroCuello, 
				CapacidadVolumetrica = @CapacidadVolumetrica,
				Tolerancia = @Tolerancia, ImagenCuerpo = @ImagenCuerpo, ImagenFondo = @ImagenFondo, Estado = 1 
				where FichaTecnicaBotellaID = @FichaTecnicaBotellaID
			end
		else  IF (@Accion = 3)
			begin
				UPDATE FichaTecnicaBotella set Estado = 0 where FichaTecnicaBotellaID = @FichaTecnicaBotellaID
			end
		/*else  IF (@Accion = 4)
			begin
				select * from FichaTecnicaBotella where Estado = 1;
			end
		else  IF (@Accion = 5)
			begin
				select * from FichaTecnicaBotella where FichaTecnicaBotellaID = @FichaTecnicaBotellaID;
			end*/
end
GO
create procedure sp_FichaTecnicaTapaCRUD
@Accion int, 
@FichaTecnicaTapaID int null,
@ProductoID int null,
@PesoPieza float null,
@Altura float null,
@DiametroInterno float null,
@DiametroExternoSelloOliva float null,
@AlturaSelloOliva float null,
@DiametroInternoBandaSeguridad float null,
@EspesorPanel float null,
@NumeroEstrias float null,
@TorqueCierreRecomendado float null,
@TorqueApertura float null,
@Tolerancia float null,
@Imagen varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into FichaTecnicaTapa values (@ProductoID, @PesoPieza, @Altura, @DiametroInterno, @DiametroExternoSelloOliva, @AlturaSelloOliva,
				@DiametroInternoBandaSeguridad, @EspesorPanel, @NumeroEstrias, @TorqueCierreRecomendado, @TorqueApertura, @Tolerancia, @Imagen, 1);	
			end
		else IF (@Accion = 2) 
			begin
				update FichaTecnicaTapa set ProductoID = @ProductoID, PesoPieza = @PesoPieza, Altura = @Altura, DiametroInterno = @DiametroInterno,
				AlturaSelloOliva = @AlturaSelloOliva, DiametroInternoBandaSeguridad = @DiametroInternoBandaSeguridad, EspesorPanel = @EspesorPanel,
				NumeroEstrias = @NumeroEstrias, TorqueCierreRecomendado = @TorqueCierreRecomendado, TorqueApertura = @TorqueApertura, 
				Tolerancia = @Tolerancia, Imagen = @Imagen, Estado = 1 where FichaTecnicaTapaID = @FichaTecnicaTapaID
			end
		else  IF (@Accion = 3)
			begin
				update FichaTecnicaTapa set Estado = 0 where FichaTecnicaTapaID = @FichaTecnicaTapaID;
			end
end
GO
create procedure sp_MaquinasCRUD
@Accion int, 
@MaquinaID int null,
@CodigoMaquina varchar(max) null,
@Nombre varchar(max) null,
@TipoMaquinaID int null,
@Descripcion varchar(max) null,
@ProduccionHora varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Maquinas values (@CodigoMaquina, @Nombre, @TipoMaquinaID, @Descripcion, @ProduccionHora, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Maquinas set CodigoMaquina = @CodigoMaquina, Nombre = @Nombre, TipoMaquinaID = @TipoMaquinaID, Descripcion = @Descripcion,
				ProduccionHora = @ProduccionHora, Estado = 1 where MaquinaID = @MaquinaID
			end
		else  IF (@Accion = 3)
			begin
				update Maquinas set Estado = 0 where MaquinaID = @MaquinaID
			end
end
GO
create procedure sp_MateriaPrimaCRUD
@Accion int, 
@MateriaPrimaID int null,
@CodigoMateriaPrima varchar(max) null,
@Nombre varchar(max) null,
@ProveedorID int null,
@TipoMateriaPrimaID int null,
@UnidadMedidaID int null,
@Cantidad int null,
@PrecioUnitario money null,
@PrecioTotal money null,
@Descripcion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into MateriaPrima values(@CodigoMateriaPrima, @Nombre, @ProveedorID, @TipoMateriaPrimaID, @UnidadMedidaID, @Cantidad,
				@PrecioUnitario, @PrecioTotal, @Descripcion, 1);
			end
		else IF (@Accion = 2) 
			begin
				update MateriaPrima set CodigoMateriaPrima = @CodigoMateriaPrima, Nombre = @Nombre, ProveedorID = @ProveedorID, TipoMateriaPrimaID 
				= @TipoMateriaPrimaID, UnidadMedidaID = @UnidadMedidaID, Cantidad = @Cantidad, PrecioUnitario = @PrecioUnitario, Descripcion = @Descripcion,
				Estado = 1
				where MateriaPrimaID = @MateriaPrimaID
			end
		else  IF (@Accion = 3)
			begin
				update MateriaPrima set Estado = 0 where MateriaPrimaID = @MateriaPrimaID
			end
end
GO
create procedure sp_PedidosCRUD
@Accion int, 
@PedidoID int null,
@NumeroPedido varchar(max) null,
@ClienteID int null,
@NumeroOrdenDeCompra varchar(max) null,
@ProductoID int null,
@UsuarioID int null,
@UnidadMedidaID int null,
@Cantidad int null,
@Descripcion varchar(max) null,
@Fecha date null,
@PrecioTotal money null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Pedidos values (@NumeroPedido, @ClienteID, @NumeroOrdenDeCompra, @ProductoID, @UsuarioID, @UnidadMedidaID, @Cantidad,
				@Descripcion, @Fecha, @PrecioTotal, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Pedidos set NumeroPedido = @NumeroPedido, ClienteID = @ClienteID, NumeroOrdenDeCompra = @NumeroOrdenDeCompra, ProductoID = @ProductoID,
				UsuarioID = @UsuarioID, UnidadMedidaID = @UnidadMedidaID, Cantidad = @Cantidad, Descripcion = @Descripcion, Fecha = @Fecha, PrecioTotal = @PrecioTotal,
				Estado = 1 where PedidoID = @PedidoID
			end
		else  IF (@Accion = 3)
			begin
				update Pedidos set Estado = 0 where PedidoID = @PedidoID
			end
end
GO
create procedure sp_ProduccionCRUD
@Accion int, 
@ProduccionID int null,
@FechaInicio date null,
@FechaFin date null,
@HorasTrabajadas date null,
@CantidadMerma float null,
@CantidadProduccionTotal int null,
@CantidadPaquetesCajas int null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Produccion values (@FechaInicio, @FechaFin, @HorasTrabajadas, @CantidadMerma, @CantidadProduccionTotal, @CantidadPaquetesCajas, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Produccion set FechaInicio = @FechaInicio, FechaFin = @FechaFin, HorasTrabajadas = @HorasTrabajadas, 
				CantidadMerma = @CantidadMerma, CantidadProduccionTotal = @CantidadProduccionTotal, CantidadPaquetesCajas = @CantidadPaquetesCajas, Estado = 1
				where ProduccionID = @ProduccionID
			end
		else  IF (@Accion = 3)
			begin
				update Produccion set Estado = 0 where ProduccionID = @ProduccionID
			end
end
GO
create procedure sp_ProduccionPaquetesCajasCRUD
@Accion int, 
@EtiquetaID int null,
@Fecha date null,
@Cantidad int null,
@PedidoID int null,
@UsuarioID int null,
@Turno varchar(max) null,
@ProduccionID int null
as
begin
		IF (@Accion = 1)
			begin 
				insert into ProduccionPaquetesCajas values (@Fecha, @Cantidad, @PedidoID, @UsuarioID, @Turno, @ProduccionID, 1);
			end
		else IF (@Accion = 2) 
			begin
				update ProduccionPaquetesCajas set Fecha = @Fecha, Cantidad = @Cantidad, PedidoID = @PedidoID, UsuarioID = @UsuarioID, Turno = @Turno,
				ProduccionID = @ProduccionID, Estado = 1 where EtiquetaID = @EtiquetaID
			end
		else  IF (@Accion = 3)
			begin
				update ProduccionPaquetesCajas set Estado = 0 where EtiquetaID = @EtiquetaID;
			end
end
GO
create procedure sp_ProductosCRUD
@Accion int, 
@ProductoID int null,
@CodigoProducto varchar(max) null,
@Nombre varchar(max) null,
@TipoProductoID int null,
@Descripcion varchar(max) null,
@Imagen varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Productos values(@CodigoProducto, @Nombre, @TipoProductoID, @Descripcion, @Imagen, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Productos set CodigoProducto = @CodigoProducto, Nombre = @Nombre, TipoProductoID = @TipoProductoID, Descripcion = @Descripcion,
				Imagen = @Imagen, Estado = 1 where ProductoID = @ProductoID
			end
		else  IF (@Accion = 3)
			begin
				update Productos set Estado = 0 where ProductoID = @ProductoID;
			end
end
GO
create procedure sp_ProveedorCRUD
@Accion int, 
@ProveedorID int null,
@CodigoProveedor varchar(max) null,
@Nombre varchar(max) null,
@Telefono varchar(max) null,
@Correo varchar(max) null,
@Direccion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Proveedor values(@CodigoProveedor, @Nombre, @Telefono, @Correo, @Direccion, 1);
			end
		else IF (@Accion = 2) 
			begin
				update Proveedor set CodigoProveedor = @CodigoProveedor, Nombre = @Nombre, Telefono = @Telefono, Correo = @Correo,
				Direccion = @Direccion, Estado = 1 where ProveedorID = @ProveedorID;
			end
		else  IF (@Accion = 3)
			begin
				update Proveedor set Estado = 0 where ProveedorID = @ProveedorID;
			end
end
GO
create procedure sp_UsuariosCRUD
@Accion int, 
@UsuarioID int null,
@Identificacion varchar(max) null,
@UserName varchar(max) null,
@Password varchar(max) null,
@NombreCompleto varchar(max) null,
@Telefono varchar(max) null,
@Correo varchar(max) null,
@Direccion varchar(max) null
as
begin
		IF (@Accion = 1)
			begin 
				insert into Usuarios values(@Identificacion, @UserName, @Password, @NombreCompleto, @Telefono, @Correo, @Direccion,1);
			end
		else IF (@Accion = 2) 
			begin
				update Usuarios set Identificacion = @Identificacion, UserName = @UserName, Password = @Password, NombreCompleto = @NombreCompleto,
				Telefono = @Telefono, Correo = @Correo, Direccion = @Direccion, Estado = 1 where UsuarioID = @UsuarioID
			end
		else  IF (@Accion = 3)
			begin
				update Usuarios set Estado = 0 where UsuarioID = @UsuarioID;
			end
end
GO
create procedure sp_VariablesMaquinaInyectoraCRUD
@Accion int, 
@VariablesMaquinaInyectoraID int null,
@MaquinaID int null,
@ProductoID int null,
@MateriaPrimaPrincipalID int null,
@MateriaPrimaSecundariaID int null,
@MateriaPrimaTerciariaID int null,
@CantidadPrimaPrincipal int null,
@CantidadPrimaSecundaria int null,
@CantidadPrimaTerciaria int null
as
begin
		IF (@Accion = 1)
			begin 
				insert into VariablesMaquinaInyectora values(@MaquinaID, @ProductoID, @MateriaPrimaPrincipalID, @MateriaPrimaSecundariaID,
				@MateriaPrimaTerciariaID, @CantidadPrimaPrincipal, @CantidadPrimaSecundaria, @CantidadPrimaTerciaria, 1);
			end
		else IF (@Accion = 2) 
			begin
				update VariablesMaquinaInyectora set MaquinaID = @MaquinaID, ProductoID = @ProductoID, MateriaPrimaPrincipalID = @MateriaPrimaPrincipalID,
				MateriaPrimaSecundariaID = @MateriaPrimaSecundariaID, MateriaPrimaTerciariaID = @MateriaPrimaTerciariaID, CantidadPrimaPrincipal =
				@CantidadPrimaPrincipal, CantidadPrimaSecundaria = @CantidadPrimaSecundaria, CantidadPrimaTerciaria = @CantidadPrimaTerciaria,
				Estado = 1
				where VariablesMaquinaInyectoraID = @VariablesMaquinaInyectoraID
			end
		else  IF (@Accion = 3)
			begin
				update VariablesMaquinaInyectora set Estado = 0 where VariablesMaquinaInyectoraID = @VariablesMaquinaInyectoraID
			end
end
go
create procedure sp_VariablesMaquinaSopladoraCRUD
@Accion int, 
@VariablesMaquinaSopladoraID int null,
@MaquinaID int null,
@ProductoID int null,
@MateriaPrimaID int null,
@RetardoPresoplado int null,
@RetardoSoplado int null,
@TiempoPresoplado int null,
@TiempoRecuperacion int null,
@TiempoSoplado int null,
@TiempoEscape int null,
@TiempoParoCadena int null,
@TiempoSalidaPinzaPreforma int null,
@FrecuenciaVentilador float null,
@PresionEstirado float null,
@Gancho bit null,
@Relay bit null,
@AnillosTornelas bit null,
@Notas varchar(max) null,
@CantidadZonas int null,
@CantidadBancos int null,
@PosicionBanco1Zona1 varchar(max) null,
@PosicionBanco1Zona2 varchar(max) null,
@PosicionBanco1Zona3 varchar(max) null,
@PosicionBanco1Zona4 varchar(max) null,
@PosicionBanco2Zona1 varchar(max) null,
@PosicionBanco2Zona2 varchar(max) null,
@PosicionBanco2Zona3 varchar(max) null,
@PosicionBanco2Zona4 varchar(max) null,
@ValorBanco1Zona1 int null,
@ValorBanco1Zona2 int null,
@ValorBanco1Zona3 int null,
@ValorBanco1Zona4 int null,
@ValorBanco2Zona1 int null,
@ValorBanco2Zona2 int null,
@ValorBanco2Zona3 int null,
@ValorBanco2Zona4 int null
as
begin
		IF (@Accion = 1)
			begin 
				insert into VariablesMaquinaSopladora values(@MaquinaID,@ProductoID, @MateriaPrimaID, @RetardoPresoplado,
				@RetardoSoplado, @TiempoPresoplado, @TiempoRecuperacion, @TiempoSoplado, @TiempoEscape, @TiempoParoCadena,
				@TiempoSalidaPinzaPreforma, @FrecuenciaVentilador, @PresionEstirado, @Gancho, @Relay, @AnillosTornelas, 1,
				@Notas, @CantidadZonas, @CantidadBancos, @PosicionBanco1Zona1,@PosicionBanco1Zona2,@PosicionBanco1Zona3,@PosicionBanco1Zona4
				,@PosicionBanco2Zona1,@PosicionBanco2Zona2,@PosicionBanco2Zona3,@PosicionBanco2Zona4,@ValorBanco1Zona1,@ValorBanco1Zona2,
				@ValorBanco1Zona3,@ValorBanco1Zona4,@ValorBanco2Zona1,@ValorBanco2Zona2,@ValorBanco2Zona3,@ValorBanco2Zona4);
			end
		else IF (@Accion = 2) 
			begin
				update VariablesMaquinaSopladora set MaquinaID = @MaquinaID, ProductoID = @ProductoID, MateriaPrimaID = @MateriaPrimaID,
				RetardoPresoplado = @RetardoPresoplado, RetardoSoplado = @RetardoSoplado, TiempoPresoplado = @TiempoPresoplado, TiempoRecuperacion =
				@TiempoRecuperacion, TiempoSoplado = @TiempoSoplado, TiempoEscape = @TiempoEscape, TiempoParoCadena = @TiempoParoCadena,
				TiempoSalidaPinzaPreforma = @TiempoSalidaPinzaPreforma, FrecuenciaVentilador = @FrecuenciaVentilador, PresionEstirado = @PresionEstirado,
				Gancho = @Gancho, Relay = @Relay, AnillosTornelas = @AnillosTornelas, Estado = 1, Notas = @Notas, CantidadZonas = @CantidadZonas,
				CantidadBancos = @CantidadBancos, PosicionBanco1Zona1 = @PosicionBanco1Zona1, PosicionBanco1Zona2 = @PosicionBanco1Zona2,
				PosicionBanco1Zona3 = @PosicionBanco1Zona3, PosicionBanco1Zona4 = @PosicionBanco1Zona4, PosicionBanco2Zona1 = @PosicionBanco2Zona1,
				PosicionBanco2Zona2 = @PosicionBanco2Zona2, PosicionBanco2Zona3 = @PosicionBanco2Zona3, PosicionBanco2Zona4 = @PosicionBanco2Zona4,
				ValorBanco1Zona1 = @ValorBanco1Zona1, ValorBanco1Zona2 = @ValorBanco1Zona2, ValorBanco1Zona3 = @ValorBanco1Zona3, ValorBanco1Zona4 =
				@ValorBanco1Zona4, ValorBanco2Zona1 = @ValorBanco2Zona1, ValorBanco2Zona2 = @ValorBanco2Zona2, ValorBanco2Zona3 = @ValorBanco2Zona3,
				ValorBanco2Zona4 = @ValorBanco2Zona4 where VariablesMaquinaSopladoraID = @VariablesMaquinaSopladoraID
			end
		else  IF (@Accion = 3)
			begin
				update VariablesMaquinaSopladora set Estado = 0 where VariablesMaquinaSopladoraID = @VariablesMaquinaSopladoraID;
			end
end
GO
create procedure [sp_setSpecificRoleForUser]
	@UsuarioID int,
	@RoleID int
AS
BEGIN
	SET NOCOUNT ON;
	delete from UsersInRoles where User_UserID = @UsuarioID;
	insert into UsersInRoles values(@UsuarioID,@RoleID);
END
GO




/*create procedure [dbo].[sp_ReportEntrega] as
select NumeroPedido, c.Nombre 'NombreCliente', NumeroOrdenDeCompra, pr.Nombre 'NombreProducto', CodigoEntrega, e.Fecha 'FechaEntrega', 
UnidadesEntregadas, NumeroFactura, p.Fecha 'FechaPedido', p.PrecioTotal from Movimientos m inner join Entregas e on m.EntregaID = m.EntregaID
inner join Pedidos p on p.PedidoID = m.PedidoID inner join Productos pr on p.ProductoID = pr.ProductoID inner join Clientes c on c.ClienteID = p.ClienteID
GO
create procedure [dbo].[sp_ReportPedido] as
select PedidoID, NumeroPedido, c.Nombre 'NombreCliente', NumeroOrdenDeCompra, pr.Nombre 'NombreProducto', NombreCompleto 'NombreUsuario',
un.Descripcion 'UnidadMedida', p.Cantidad, p.Descripcion, Fecha, p.PrecioTotal from Pedidos p 
inner join Clientes c on p.ClienteID = c.ClienteID
inner join Productos pr on p.ProductoID = pr.ProductoID 
inner join Usuarios u on u.UsuarioID = p.UsuarioID 
inner join UnidadMedida un on un.UnidadMedidaID = p.UnidadMedidaID
GO*/

/*
Triggers

*/

create trigger FK_Clientes
on Clientes
instead of delete
as
begin
set nocount on;
delete from Pedidos where ClienteID in (select ClienteID from deleted);
delete from Clientes where ClienteID in (select ClienteID from deleted);
end
GO

create trigger FK_Entregas
on Entregas
instead of delete
as
begin
set nocount on;
delete from Movimientos where EntregaID in (select EntregaID from deleted);
delete from Entregas where EntregaID in (select EntregaID from deleted);
end
GO

create trigger FK_Maquinas
on Maquinas
instead of delete
as
begin
set nocount on;
delete from VariablesMaquinaSopladora where MaquinaID in (select MaquinaID from deleted);
delete from VariablesMaquinaInyectora where MaquinaID in (select MaquinaID from deleted);
delete from Maquinas where MaquinaID in (select MaquinaID from deleted);
end
GO

create trigger FK_MateriaPrima
on MateriaPrima
instead of delete
as
begin
set nocount on;
delete from VariablesMaquinaSopladora where MateriaPrimaID in (select MateriaPrimaID from deleted);
delete from VariablesMaquinaInyectora where MateriaPrimaPrincipalID in (select MateriaPrimaPrincipalID from deleted);
delete from VariablesMaquinaInyectora where MateriaPrimaSecundariaID in (select MateriaPrimaSecundariaID from deleted);
delete from VariablesMaquinaInyectora where MateriaPrimaTerciariaID in (select MateriaPrimaTerciariaID from deleted);
delete from MateriaPrima where MateriaPrimaID in (select MateriaPrimaID from deleted);
end
GO

create trigger FK_Pedidos
on Pedidos
instead of delete
as
begin
set nocount on;
delete from Movimientos where PedidoID in (select PedidoID from deleted);
delete from ProduccionPaquetesCajas where PedidoID in (select PedidoID from deleted);
delete from Pedidos where PedidoID in (select PedidoID from deleted);
end
GO

create trigger FK_Productos
on Productos
instead of delete
as
begin
set nocount on;
delete from FichaTecnicaBotella where ProductoID in (select ProductoID from deleted);
delete from VariablesMaquinaSopladora where ProductoID in (select ProductoID from deleted);
delete from VariablesMaquinaInyectora where ProductoID in (select ProductoID from deleted);
delete from Pedidos where ProductoID in (select ProductoID from deleted);
delete from FichaTecnicaTapa where ProductoID in (select ProductoID from deleted);
delete from Productos where ProductoID in (select ProductoID from deleted);
end
GO

create trigger FK_Proveedor
on Proveedor
instead of delete
as
begin
set nocount on;
delete from MateriaPrima where MateriaPrimaID in (select MateriaPrimaID from deleted);
delete from Proveedor where ProveedorID in (select ProveedorID from deleted);
end
GO

create trigger FK_Roles
on Roles
instead of delete
as
begin
set nocount on;
delete from UsersInRoles where Role_RoleID in (select RoleID from deleted);
delete from Roles where RoleID in (select RoleID from deleted);
end
GO

create trigger FK_TipoMaquina
on TipoMaquina
instead of delete
as
begin
set nocount on;
delete from Maquinas where TipoMaquinaID in (select TipoMaquinaID from deleted);
delete from TipoMaquina where TipoMaquinaID in (select TipoMaquinaID from deleted);
end
GO

create trigger FK_TipoMateriaPrima
on TipoMateriaPrima
instead of delete
as
begin
set nocount on;
delete from MateriaPrima where MateriaPrimaID in (select MateriaPrimaID from deleted);
delete from TipoMateriaPrima where TipoMateriaPrimaID in (select TipoMateriaPrimaID from deleted);
end
GO

create trigger FK_TipoProducto
on TipoProducto
instead of delete
as
begin
set nocount on;
delete from Productos where TipoProductoID in (select TipoProductoID from deleted);
delete from TipoProducto where TipoProductoID in (select TipoProductoID from deleted);
end
GO

create trigger FK_UnidadMedida
on UnidadMedida
instead of delete
as
begin
set nocount on;
delete from Pedidos where UnidadMedidaID in (select UnidadMedidaID from deleted);
delete from MateriaPrima where UnidadMedidaID in (select UnidadMedidaID from deleted);
delete from UnidadMedida where UnidadMedidaID in (select UnidadMedidaID from deleted);
end
GO

create trigger FK_Usuarios
on Usuarios
instead of delete
as
begin
set nocount on;
delete from Pedidos where UsuarioID in (select UsuarioID from deleted);
delete from UsersInRoles where User_UserID in (select UsuarioID from deleted);
delete from ProduccionPaquetesCajas where UsuarioID in (select UsuarioID from deleted);
delete from Usuarios where UsuarioID in (select UsuarioID from deleted);
end
GO

create trigger FK_Produccion
on Produccion
instead of delete
as
begin
set nocount on;
delete from ProduccionPaquetesCajas where ProduccionID in (select ProduccionID from deleted);
delete from Produccion where ProduccionID in (select ProduccionID from deleted);
end
GO





