USE [master]
GO
/****** Object:  Database [DIPREMA]    Script Date: 25/11/2020 3:52:01 p. m. ******/
CREATE DATABASE [DIPREMA]
GO
USE [DIPREMA]
GO
/****** Object:  Table [dbo].[AperturaCierre]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AperturaCierre](
	[AperturaCierreID] [bigint] IDENTITY(1,1) NOT NULL,
	[FechaApertura] [datetime] NOT NULL,
	[FechaCierre] [datetime] NULL,
	[Turno] [varchar](50) NOT NULL,
	[PedidoID] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[MaquinaID] [int] NULL,
 CONSTRAINT [PK_AperturaCierre] PRIMARY KEY CLUSTERED 
(
	[AperturaCierreID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[AperturaCierreActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*Vistas*/
create VIEW [dbo].[AperturaCierreActivos] AS
select * from AperturaCierre where Estado = 1;
GO
/****** Object:  Table [dbo].[DetalleEntrega]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleEntrega](
	[DetelleEntregaID] [bigint] IDENTITY(1,1) NOT NULL,
	[EntregaID] [int] NOT NULL,
	[ProduccionID] [int] NOT NULL,
	[Paquetes] [int] NOT NULL,
	[Cantidad] [float] NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_DetalleEntrega] PRIMARY KEY CLUSTERED 
(
	[DetelleEntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Entregas]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entregas](
	[EntregaID] [int] IDENTITY(1,1) NOT NULL,
	[NumeroEntrega] [varchar](50) NOT NULL,
	[Fecha] [date] NULL,
	[UnidadesEntregadas] [int] NULL,
	[NumeroFactura] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pedidos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pedidos](
	[PedidoID] [int] IDENTITY(1,1) NOT NULL,
	[NumeroPedido] [varchar](50) NOT NULL,
	[ClienteID] [int] NOT NULL,
	[NumeroOrdenDeCompra] [varchar](50) NULL,
	[ProductoID] [int] NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[UnidadMedidaID] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Fecha] [date] NULL,
	[PrecioTotal] [money] NOT NULL,
	[Estado] [bit] NOT NULL,
	[EstadoID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PedidoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productos](
	[ProductoID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProducto] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[TipoProductoID] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Imagen] [varchar](max) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UnidadMedida]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnidadMedida](
	[UnidadMedidaID] [int] IDENTITY(1,1) NOT NULL,
	[Unidad] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UnidadMedidaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Produccion]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Produccion](
	[ProduccionID] [int] IDENTITY(1,1) NOT NULL,
	[PedidoID] [int] NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaFin] [datetime] NULL,
	[HorasTrabajadas] [varchar](100) NULL,
	[CantidadMerma] [float] NULL,
	[CantidadProduccionTotal] [float] NOT NULL,
	[CantidadPaquetesCajas] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProduccionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[DetalleEntregaActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create VIEW [dbo].[DetalleEntregaActivos] AS

Select Entregas.Fecha as FechaEntrega, Entregas.NumeroFactura as NumeroFactura ,Entregas.NumeroEntrega as NumeroEntrega,
		Pedidos.NumeroPedido as lote, Pedidos.NumeroOrdenDeCompra as NumeroOrdenCompra,
		Productos.Nombre as Producto,
		DetalleEntrega.Paquetes, DetalleEntrega.Cantidad,
		UnidadMedida.Unidad
from DetalleEntrega

join Entregas on DetalleEntrega.EntregaID = Entregas.EntregaID
join Produccion on  DetalleEntrega.ProduccionID = Produccion.ProduccionID
join Pedidos on Produccion.ProduccionID = Pedidos.PedidoID
join UnidadMedida on Pedidos.UnidadMedidaID = UnidadMedida.UnidadMedidaID
join Productos on Pedidos.ProductoID = Productos.ProductoID

where DetalleEntrega.Estado =1
GO
/****** Object:  Table [dbo].[MermaPorTurno]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MermaPorTurno](
	[MermaPorTurnoID] [int] IDENTITY(1,1) NOT NULL,
	[Cantidad] [float] NOT NULL,
	[AperturaCierreID] [bigint] NOT NULL,
	[TipoMermaID] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MermaPorTurnoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MermaPorTurnoActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[MermaPorTurnoActivos] as 
SELECT        *
FROM            MermaPorTurno
WHERE        Estado = 1
GO
/****** Object:  Table [dbo].[ProduccionPaquetesCajas]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProduccionPaquetesCajas](
	[EtiquetaID] [int] IDENTITY(1,1) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Cantidad] [float] NOT NULL,
	[AperturaCierreID] [bigint] NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[Turno] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_ProduccionPaquetesCajas] PRIMARY KEY CLUSTERED 
(
	[EtiquetaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ProduccionPaquetesCajaActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[ProduccionPaquetesCajaActivos] as
select * from ProduccionPaquetesCajas where Estado = 1;
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clientes](
	[ClienteID] [int] IDENTITY(1,1) NOT NULL,
	[Identificacion] [varchar](50) NOT NULL,
	[NombreCompleto] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ClienteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ClientesActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*Vistas*/
create VIEW [dbo].[ClientesActivos] AS
select * from Clientes where Estado = 1;
GO
/****** Object:  View [dbo].[EntregasActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create VIEW [dbo].[EntregasActivos] as
select * from Entregas where Estado = 1;
GO
/****** Object:  Table [dbo].[FichaTecnicaBotella]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FichaTecnicaBotella](
	[FichaTecnicaBotellaID] [int] IDENTITY(1,1) NOT NULL,
	[ProductoID] [int] NOT NULL,
	[Altura] [float] NOT NULL,
	[DiametroMayor] [float] NOT NULL,
	[DiametroMenor] [float] NOT NULL,
	[DiametroCuello] [float] NOT NULL,
	[CapacidadVolumetrica] [float] NOT NULL,
	[Tolerancia] [float] NULL,
	[ImagenCuerpo] [varchar](max) NULL,
	[ImagenFondo] [varchar](max) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FichaTecnicaBotellaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[FichaTecnicaBotellaActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[FichaTecnicaBotellaActivos] as
select * from FichaTecnicaBotella where Estado = 1;
GO
/****** Object:  View [dbo].[PedidosActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   view [dbo].[PedidosActivos] as
select * from Pedidos where Estado = 1;
GO
/****** Object:  View [dbo].[ProduccionActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[ProduccionActivos] as
select * from Produccion where Estado = 1;
GO
/****** Object:  View [dbo].[ClientesProduccion]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view 
[dbo].[ClientesProduccion] 
AS
select DISTINCT cliente.ClienteID
      ,cliente.Identificacion
      ,cliente.NombreCompleto
      ,cliente.Telefono
      ,cliente.Correo
      ,cliente.Direccion
      ,cliente.Estado
from ClientesActivos as cliente
join PedidosActivos as pedido on cliente.ClienteID= pedido.ClienteID
join  ProduccionActivos as produccion on pedido.PedidoID= produccion.PedidoID
GO
/****** Object:  Table [dbo].[FichaTecnicaTapa]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FichaTecnicaTapa](
	[FichaTecnicaTapaID] [int] IDENTITY(1,1) NOT NULL,
	[ProductoID] [int] NOT NULL,
	[PesoPieza] [float] NOT NULL,
	[Altura] [float] NOT NULL,
	[DiametroInterno] [float] NOT NULL,
	[DiametroExternoSelloOliva] [float] NOT NULL,
	[AlturaSelloOliva] [float] NOT NULL,
	[DiametroInternoBandaSeguridad] [float] NOT NULL,
	[EspesorPanel] [float] NOT NULL,
	[NumeroEstrias] [float] NOT NULL,
	[TorqueCierreRecomendado] [float] NOT NULL,
	[TorqueApertura] [float] NOT NULL,
	[Tolerancia] [float] NULL,
	[Imagen] [varchar](max) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FichaTecnicaTapaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[FichaTecnicaTapaActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[FichaTecnicaTapaActivos] as
select * from FichaTecnicaTapa where Estado = 1;
GO
/****** Object:  Table [dbo].[Maquinas]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Maquinas](
	[MaquinaID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoMaquina] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[TipoMaquinaID] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[ProduccionHora] [varchar](100) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MaquinaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MaquinasActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MaquinasActivos] as
select * from Maquinas where Estado = 1;
GO
/****** Object:  Table [dbo].[MateriaPrima]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MateriaPrima](
	[MateriaPrimaID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoMateriaPrima] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[ProveedorID] [int] NOT NULL,
	[TipoMateriaPrimaID] [int] NOT NULL,
	[UnidadMedidaID] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[PrecioUnitario] [money] NOT NULL,
	[PrecioTotal] [money] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MateriaPrimaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MateriaPrimaActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MateriaPrimaActivos] as
select * from MateriaPrima where Estado = 1;
GO
/****** Object:  View [dbo].[ProductosActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProductosActivos] as
select * from Productos where Estado = 1;
GO
/****** Object:  Table [dbo].[Proveedor]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proveedor](
	[ProveedorID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProveedor] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProveedorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ProveedorActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProveedorActivos] as
select * from Proveedor where Estado = 1;
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[UsuarioID] [int] IDENTITY(1,1) NOT NULL,
	[Identificacion] [varchar](50) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [varchar](100) NOT NULL,
	[NombreCompleto] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UsuarioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UC_Usuarios] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[UsuariosActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[UsuariosActivos] as
select * from Usuarios where Estado = 1;
GO
/****** Object:  Table [dbo].[VariablesMaquinaInyectora]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VariablesMaquinaInyectora](
	[VariablesMaquinaInyectoraID] [int] IDENTITY(1,1) NOT NULL,
	[MaquinaID] [int] NOT NULL,
	[ProductoID] [int] NOT NULL,
	[MateriaPrimaPrincipalID] [int] NOT NULL,
	[MateriaPrimaSecundariaID] [int] NULL,
	[MateriaPrimaTerciariaID] [int] NULL,
	[CantidadPrimaPrincipal] [float] NOT NULL,
	[CantidadPrimaSecundaria] [float] NULL,
	[CantidadPrimaTerciaria] [float] NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[VariablesMaquinaInyectoraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VariablesMaquinaInyectoraActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VariablesMaquinaInyectoraActivos] as
select * from VariablesMaquinaInyectora where Estado = 1;
GO
/****** Object:  Table [dbo].[VariablesMaquinaSopladora]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VariablesMaquinaSopladora](
	[VariablesMaquinaSopladoraID] [int] IDENTITY(1,1) NOT NULL,
	[MaquinaID] [int] NOT NULL,
	[ProductoID] [int] NOT NULL,
	[MateriaPrimaID] [int] NOT NULL,
	[RetardoPresoplado] [int] NOT NULL,
	[RetardoSoplado] [int] NOT NULL,
	[TiempoPresoplado] [int] NOT NULL,
	[TiempoRecuperacion] [int] NOT NULL,
	[TiempoSoplado] [int] NOT NULL,
	[TiempoEscape] [int] NOT NULL,
	[TiempoParoCadena] [int] NOT NULL,
	[TiempoSalidaPinzaPreforma] [int] NOT NULL,
	[FrecuenciaVentilador] [float] NOT NULL,
	[PresionEstirado] [float] NOT NULL,
	[Gancho] [bit] NOT NULL,
	[Relay] [bit] NOT NULL,
	[AnillosTornelas] [bit] NOT NULL,
	[Estado] [bit] NOT NULL,
	[Notas] [varchar](100) NULL,
	[CantidadZonas] [int] NOT NULL,
	[CantidadBancos] [int] NOT NULL,
	[PosicionBanco1Zona1] [varchar](100) NULL,
	[PosicionBanco1Zona2] [varchar](100) NULL,
	[PosicionBanco1Zona3] [varchar](100) NULL,
	[PosicionBanco1Zona4] [varchar](100) NULL,
	[PosicionBanco2Zona1] [varchar](100) NULL,
	[PosicionBanco2Zona2] [varchar](100) NULL,
	[PosicionBanco2Zona3] [varchar](100) NULL,
	[PosicionBanco2Zona4] [varchar](100) NULL,
	[ValorBanco1Zona1] [int] NULL,
	[ValorBanco1Zona2] [int] NULL,
	[ValorBanco1Zona3] [int] NULL,
	[ValorBanco1Zona4] [int] NULL,
	[ValorBanco2Zona1] [int] NULL,
	[ValorBanco2Zona2] [int] NULL,
	[ValorBanco2Zona3] [int] NULL,
	[ValorBanco2Zona4] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[VariablesMaquinaSopladoraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VariablesMaquinaSopladoraActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VariablesMaquinaSopladoraActivos] as
select * from VariablesMaquinaSopladora where Estado = 1;
GO
/****** Object:  View [dbo].[MaquinasSopladoraActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MaquinasSopladoraActivos] as
select * from Maquinas where TipoMaquinaID = 1 and Estado = 1;
GO
/****** Object:  View [dbo].[MaquinasInyectoraActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MaquinasInyectoraActivos] as
select * from Maquinas where TipoMaquinaID = 2 and Estado = 1;
GO
/****** Object:  View [dbo].[MateriaPrimaInyectoraActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MateriaPrimaInyectoraActivos] as
select * from MateriaPrima where TipoMateriaPrimaID = 2 or TipoMateriaPrimaID = 3 and Estado = 1
GO
/****** Object:  View [dbo].[MateriaPrimaSopladoraActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MateriaPrimaSopladoraActivos] as
select * from MateriaPrima where TipoMateriaPrimaID = 1 and Estado = 1
GO
/****** Object:  View [dbo].[ProductosBotellaActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProductosBotellaActivos] as
select * from Productos where TipoProductoID = 1 and Estado = 1;
GO
/****** Object:  View [dbo].[ProductosTapaActivos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ProductosTapaActivos] as
select * from Productos where TipoProductoID = 2 and Estado = 1;
GO
/****** Object:  Table [dbo].[Estados]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estados](
	[EstadoID] [int] IDENTITY(1,1) NOT NULL,
	[TipoEstado] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Estados] PRIMARY KEY CLUSTERED 
(
	[EstadoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Movimientos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movimientos](
	[PedidoID] [int] NOT NULL,
	[EntregaID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Movimientos] PRIMARY KEY CLUSTERED 
(
	[PedidoID] ASC,
	[EntregaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoMaquina]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoMaquina](
	[TipoMaquinaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMaquinaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoMateriaPrima]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoMateriaPrima](
	[TipoMateriaPrimaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMateriaPrimaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoMerma]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoMerma](
	[TipoMermaID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoMermaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoProducto]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoProducto](
	[TipoProductoID] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoProductoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsersInRoles]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersInRoles](
	[User_UserID] [int] NOT NULL,
	[Role_RoleID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.RoleUsers] PRIMARY KEY CLUSTERED 
(
	[Role_RoleID] ASC,
	[User_UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AperturaCierre] ADD  CONSTRAINT [DF_AperturaCierre_estado]  DEFAULT ((1)) FOR [Estado]
GO
ALTER TABLE [dbo].[DetalleEntrega] ADD  CONSTRAINT [DF_DetalleEntrega_Detalle]  DEFAULT ((1)) FOR [Estado]
GO
ALTER TABLE [dbo].[Entregas] ADD  CONSTRAINT [DF_Entregas_Estado]  DEFAULT ((1)) FOR [Estado]
GO
ALTER TABLE [dbo].[Pedidos] ADD  CONSTRAINT [DF_Pedidos_EstadoID]  DEFAULT ((1)) FOR [EstadoID]
GO
ALTER TABLE [dbo].[AperturaCierre]  WITH CHECK ADD  CONSTRAINT [FK_AperturaCierre_PedidoID] FOREIGN KEY([PedidoID])
REFERENCES [dbo].[Pedidos] ([PedidoID])
GO
ALTER TABLE [dbo].[AperturaCierre] CHECK CONSTRAINT [FK_AperturaCierre_PedidoID]
GO
ALTER TABLE [dbo].[DetalleEntrega]  WITH CHECK ADD  CONSTRAINT [FK_DetalleEntrega_Entregas] FOREIGN KEY([EntregaID])
REFERENCES [dbo].[Entregas] ([EntregaID])
GO
ALTER TABLE [dbo].[DetalleEntrega] CHECK CONSTRAINT [FK_DetalleEntrega_Entregas]
GO
ALTER TABLE [dbo].[DetalleEntrega]  WITH CHECK ADD  CONSTRAINT [FK_DetalleEntrega_Produccion] FOREIGN KEY([ProduccionID])
REFERENCES [dbo].[Produccion] ([ProduccionID])
GO
ALTER TABLE [dbo].[DetalleEntrega] CHECK CONSTRAINT [FK_DetalleEntrega_Produccion]
GO
ALTER TABLE [dbo].[FichaTecnicaBotella]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_FichaTecnicaBotella] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[FichaTecnicaBotella] CHECK CONSTRAINT [FK_ProductoID_FichaTecnicaBotella]
GO
ALTER TABLE [dbo].[FichaTecnicaTapa]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_FichaTecnicaTapa] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[FichaTecnicaTapa] CHECK CONSTRAINT [FK_ProductoID_FichaTecnicaTapa]
GO
ALTER TABLE [dbo].[Maquinas]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_Maquinas] FOREIGN KEY([TipoMaquinaID])
REFERENCES [dbo].[TipoMaquina] ([TipoMaquinaID])
GO
ALTER TABLE [dbo].[Maquinas] CHECK CONSTRAINT [FK_TipoID_Maquinas]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_ProveedorID_MateriaPrima] FOREIGN KEY([ProveedorID])
REFERENCES [dbo].[Proveedor] ([ProveedorID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_ProveedorID_MateriaPrima]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_MateriaPrima] FOREIGN KEY([TipoMateriaPrimaID])
REFERENCES [dbo].[TipoMateriaPrima] ([TipoMateriaPrimaID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_TipoID_MateriaPrima]
GO
ALTER TABLE [dbo].[MateriaPrima]  WITH CHECK ADD  CONSTRAINT [FK_UnidadMedidaID_MateriaPrima] FOREIGN KEY([UnidadMedidaID])
REFERENCES [dbo].[UnidadMedida] ([UnidadMedidaID])
GO
ALTER TABLE [dbo].[MateriaPrima] CHECK CONSTRAINT [FK_UnidadMedidaID_MateriaPrima]
GO
ALTER TABLE [dbo].[MermaPorTurno]  WITH CHECK ADD FOREIGN KEY([AperturaCierreID])
REFERENCES [dbo].[AperturaCierre] ([AperturaCierreID])
GO
ALTER TABLE [dbo].[MermaPorTurno]  WITH CHECK ADD FOREIGN KEY([TipoMermaID])
REFERENCES [dbo].[TipoMerma] ([TipoMermaID])
GO
ALTER TABLE [dbo].[Movimientos]  WITH CHECK ADD  CONSTRAINT [FK_EntregaID_Movimientos] FOREIGN KEY([EntregaID])
REFERENCES [dbo].[Entregas] ([EntregaID])
GO
ALTER TABLE [dbo].[Movimientos] CHECK CONSTRAINT [FK_EntregaID_Movimientos]
GO
ALTER TABLE [dbo].[Movimientos]  WITH CHECK ADD  CONSTRAINT [FK_PedidoID_Movimientos] FOREIGN KEY([PedidoID])
REFERENCES [dbo].[Pedidos] ([PedidoID])
GO
ALTER TABLE [dbo].[Movimientos] CHECK CONSTRAINT [FK_PedidoID_Movimientos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_ClienteID_Pedidos] FOREIGN KEY([ClienteID])
REFERENCES [dbo].[Clientes] ([ClienteID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_ClienteID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_Pedidos_Estados] FOREIGN KEY([EstadoID])
REFERENCES [dbo].[Estados] ([EstadoID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_Pedidos_Estados]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_Pedidos] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_ProductoID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_UnidadMedidaID_Pedidos] FOREIGN KEY([UnidadMedidaID])
REFERENCES [dbo].[UnidadMedida] ([UnidadMedidaID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_UnidadMedidaID_Pedidos]
GO
ALTER TABLE [dbo].[Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioID_Pedidos] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[Pedidos] CHECK CONSTRAINT [FK_UsuarioID_Pedidos]
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas]  WITH CHECK ADD  CONSTRAINT [FK_ProduccionPaquetesCajas_ProduccionPaquetesCajas] FOREIGN KEY([AperturaCierreID])
REFERENCES [dbo].[AperturaCierre] ([AperturaCierreID])
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas] CHECK CONSTRAINT [FK_ProduccionPaquetesCajas_ProduccionPaquetesCajas]
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas]  WITH CHECK ADD  CONSTRAINT [FK_ProduccionPaquetesCajas_Usuarios] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[ProduccionPaquetesCajas] CHECK CONSTRAINT [FK_ProduccionPaquetesCajas_Usuarios]
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [FK_TipoID_Productos] FOREIGN KEY([TipoProductoID])
REFERENCES [dbo].[TipoProducto] ([TipoProductoID])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [FK_TipoID_Productos]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK_Role_RoleID_UserInRoles] FOREIGN KEY([Role_RoleID])
REFERENCES [dbo].[Roles] ([RoleID])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [FK_Role_RoleID_UserInRoles]
GO
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK_User_UserID_UserInRoles] FOREIGN KEY([User_UserID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [FK_User_UserID_UserInRoles]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MaquinaID_VariablesMaquinaInyectora] FOREIGN KEY([MaquinaID])
REFERENCES [dbo].[Maquinas] ([MaquinaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MaquinaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaPrincipalID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaPrincipalID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaPrincipalID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaSecundariaID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaSecundariaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaSecundariaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaTerciariaID_VariablesMaquinaInyectora] FOREIGN KEY([MateriaPrimaTerciariaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_MateriaPrimaTerciariaID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_VariablesMaquinaInyectora] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[VariablesMaquinaInyectora] CHECK CONSTRAINT [FK_ProductoID_VariablesMaquinaInyectora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_MaquinaID_VariablesMaquinaSopladora] FOREIGN KEY([MaquinaID])
REFERENCES [dbo].[Maquinas] ([MaquinaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_MaquinaID_VariablesMaquinaSopladora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_MateriaPrimaID_VariablesMaquinaSopladora] FOREIGN KEY([MateriaPrimaID])
REFERENCES [dbo].[MateriaPrima] ([MateriaPrimaID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_MateriaPrimaID_VariablesMaquinaSopladora]
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora]  WITH CHECK ADD  CONSTRAINT [FK_ProductoID_VariablesMaquinaSopladora] FOREIGN KEY([ProductoID])
REFERENCES [dbo].[Productos] ([ProductoID])
GO
ALTER TABLE [dbo].[VariablesMaquinaSopladora] CHECK CONSTRAINT [FK_ProductoID_VariablesMaquinaSopladora]
GO
/****** Object:  StoredProcedure [dbo].[sp_DatosEtiqueta]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_DatosEtiqueta]
@Etiqueta int 

as
begin
		Select produccion.EtiquetaID, produccion.Fecha, produccion.Cantidad, produccion.Turno, usuario.NombreCompleto as Usuario,
		pedido.NumeroPedido, pedido.NumeroOrdenDeCompra, cliente.NombreCompleto as Cliente, cliente.Direccion, producto.Nombre as Producto,
		um.Unidad, maquina.Nombre as maquina

		from ProduccionPaquetesCajas as produccion
		join Usuarios as usuario on produccion.UsuarioID = usuario.UsuarioID
		join AperturaCierre as apertura on produccion.AperturaCierreID = apertura.AperturaCierreID
		join Pedidos as pedido on apertura.PedidoID = pedido.PedidoID
		join Clientes as cliente on pedido.ClienteID = cliente.ClienteID 
		join Productos as producto on pedido.ProductoID = producto.ProductoID		
		join UnidadMedida as um on pedido.UnidadMedidaID = um.UnidadMedidaID
		join Maquinas as maquina on apertura.MaquinaID = maquina.MaquinaID

		where EtiquetaID = @Etiqueta
end
GO
/****** Object:  StoredProcedure [dbo].[sp_FichaOperacion]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_FichaOperacion]
@Pedido int

as
begin

if (select  p.ProductoID from Pedidos p  join VariablesMaquinaInyectora vi on vi.ProductoID = p.ProductoID where p.PedidoID = @Pedido) > 0

select ('inyectora') as FichaOperacion , p.ProductoID 
from Pedidos p  join VariablesMaquinaInyectora vi on vi.ProductoID = p.ProductoID where p.PedidoID = @Pedido

 


else if (select  p.ProductoID from Pedidos p  join VariablesMaquinaSopladora vs on vs.ProductoID = p.ProductoID where p.PedidoID = @Pedido) > 0

select ('sopladora') as FichaOperacion, p.ProductoID from Pedidos p  join VariablesMaquinaSopladora vs on vs.ProductoID = p.ProductoID where p.PedidoID = @Pedido

else

select ('nada') as FichaOperacion ,p.ProductoID from Pedidos p  join VariablesMaquinaSopladora vs on vs.ProductoID = p.ProductoID where p.PedidoID = @Pedido

end
GO
/****** Object:  StoredProcedure [dbo].[sp_FichaOperacionInyectora]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_FichaOperacionInyectora]
@Pedido int

as
begin



Select p.NumeroPedido, p.NumeroOrdenDeCompra, p.Cantidad, p.Descripcion, p.Fecha, u.NombreCompleto as usuario,c.NombreCompleto, pd.Nombre as producto,
 m.Nombre, vi.CantidadPrimaPrincipal, vi.CantidadPrimaSecundaria, vi.CantidadPrimaTerciaria,
mp.Nombre as mp1, mps.Nombre as mp2, mpt.Nombre as mp3

from Pedidos p
join Usuarios u on p.UsuarioID = u.UsuarioID
join Clientes c on p.ClienteID = c.ClienteID
join Productos pd on p.ProductoID = pd.ProductoID
join VariablesMaquinaInyectora vi on vi.ProductoID = p.ProductoID
join Maquinas m on vi.MaquinaID = m.MaquinaID
join MateriaPrima mp on vi.MateriaPrimaPrincipalID = mp.MateriaPrimaID
join MateriaPrima mps on vi.MateriaPrimaSecundariaID = mps.MateriaPrimaID
join MateriaPrima mpt on vi.MateriaPrimaTerciariaID = mpt.MateriaPrimaID

where p.PedidoID = @Pedido;



end
GO
/****** Object:  StoredProcedure [dbo].[sp_FichaOperacionSopladora]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[sp_FichaOperacionSopladora]
@Pedido int

as
begin

Select p.NumeroPedido, p.NumeroOrdenDeCompra, p.Cantidad, p.Descripcion, p.Fecha, u.NombreCompleto as usuario,c.NombreCompleto as cliente, pd.Nombre as producto,
 m.Nombre as maquina, 
 mp.Nombre as MateriaPrima
      ,[RetardoPresoplado]
      ,[RetardoSoplado] 
      ,[TiempoPresoplado] 
      ,[TiempoRecuperacion] 
      ,[TiempoSoplado]
      ,[TiempoEscape]
      ,[TiempoParoCadena] 
      ,[TiempoSalidaPinzaPreforma] 
      ,[FrecuenciaVentilador] 
      ,[PresionEstirado] 
      ,[Gancho] 
      ,[Relay] 
      ,[AnillosTornelas] 
      ,[Notas] 
      ,[CantidadZonas] 
      ,[CantidadBancos] 
      ,[PosicionBanco1Zona1] 
      ,[PosicionBanco1Zona2]
      ,[PosicionBanco1Zona3]
      ,[PosicionBanco1Zona4] 
      ,[PosicionBanco2Zona1] 
      ,[PosicionBanco2Zona2] 
      ,[PosicionBanco2Zona3] 
      ,[PosicionBanco2Zona4] 
      ,[ValorBanco1Zona1] 
      ,[ValorBanco1Zona2] 
      ,[ValorBanco1Zona3]
      ,[ValorBanco1Zona4] 
      ,[ValorBanco2Zona1] 
      ,[ValorBanco2Zona2] 
      ,[ValorBanco2Zona3] 
      ,[ValorBanco2Zona4] 


from Pedidos p
join Usuarios u on p.UsuarioID = u.UsuarioID
join Clientes c on p.ClienteID = c.ClienteID
join Productos pd on p.ProductoID = pd.ProductoID
join VariablesMaquinaSopladora vs on vs.ProductoID = p.ProductoID
join Maquinas m on vs.MaquinaID = m.MaquinaID
join MateriaPrima mp on vs.MateriaPrimaID = mp.MateriaPrimaID


where p.PedidoID = @Pedido;



end
GO
/****** Object:  StoredProcedure [dbo].[sp_getRolesForUser]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*Procedimientos almacenados*/


create PROCEDURE [dbo].[sp_getRolesForUser]
	@userName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	select r.RoleName from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioID
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where u.UserName=@userName
END
GO
/****** Object:  StoredProcedure [dbo].[sp_getUsuariosRole]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_getUsuariosRole]
	
	@roleName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	select u.UserName from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioID
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where r.RoleName=@roleName 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GraficoParettoMerma]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_GraficoParettoMerma]
as
Select  TipoMerma.Tipo, sum(MermaPorTurno.Cantidad)as Cantidad
from MermaPorTurno
join TipoMerma on TipoMerma.TipoMermaID = MermaPorTurno.TipoMermaID

GROUP BY
TipoMerma.Tipo

order by cantidad desc
GO
/****** Object:  StoredProcedure [dbo].[sp_GraficoPedidos]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_GraficoPedidos] AS

select count(*) as PedidosProceso, (
select count(*) PedidosPendientes from PedidosActivos p 
 left join Produccion on p.PedidoID = Produccion.PedidoID where p.PedidoID not in (select PedidoID from Produccion))
 as PedidosPendientes
 ,(select count(*)

from PedidosActivos
 
join Produccion on PedidosActivos.PedidoID = Produccion.PedidoID
 
where PedidosActivos.PedidoID = Produccion.PedidoID and Produccion.CantidadProduccionTotal >= PedidosActivos.Cantidad
) as PedidosTerminados

from PedidosActivos
 
join Produccion on PedidosActivos.PedidoID = Produccion.PedidoID
 
where PedidosActivos.PedidoID = Produccion.PedidoID and Produccion.CantidadProduccionTotal >= 0 and Produccion.CantidadProduccionTotal < PedidosActivos.Cantidad


GO
/****** Object:  StoredProcedure [dbo].[sp_GraficoProduccion]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_GraficoProduccion] AS


SELECT DATEPART(month, Fecha) as mes, SUM(Cantidad) as total
FROM ProduccionPaquetesCajaActivos

GROUP BY DATEPART(month, Fecha)
GO
/****** Object:  StoredProcedure [dbo].[sp_isUserInRole]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_isUserInRole]
	@userName varchar(20),
	@roleName varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	declare @resultado bit =0;
	if exists(
	select * from UsersInRoles ur
	inner join Usuarios u on ur.User_UserId=u.UsuarioId
	inner join Roles r on ur.Role_RoleId = r.RoleId
	where r.RoleName=@roleName and u.UserName=@userName)
	set @resultado = 1
	 select @resultado
END


GO
/****** Object:  StoredProcedure [dbo].[sp_setSpecificRoleForUser]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_setSpecificRoleForUser]
	@UsuarioID int,
	@RoleID int
AS
BEGIN
	SET NOCOUNT ON;
	delete from UsersInRoles where User_UserID = @UsuarioID;
	insert into UsersInRoles values(@UsuarioID,@RoleID);
END

GO
/****** Object:  Trigger [dbo].[TRG_INS_Produccion]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRG_INS_Produccion]
ON [dbo].[AperturaCierre]
FOR INSERT
AS
BEGIN
		SET NOCOUNT ON;	

		IF NOT EXISTS( SELECT PedidoID FROM [Produccion] WHERE PedidoID =  (select top 1 PedidoID from AperturaCierre order by AperturaCierreID desc))
			begin
				INSERT INTO [dbo].[Produccion]
						   ([PedidoID]
						   ,[FechaInicio]
						   ,[FechaFin]
						   ,[HorasTrabajadas]
						   ,[CantidadMerma]
						   ,[CantidadProduccionTotal]
						   ,[CantidadPaquetesCajas]
						   ,[Estado])
					 VALUES
						   ((select top 1 PedidoID from AperturaCierre order by AperturaCierreID desc)
						   ,(select top 1 FechaApertura from AperturaCierre order by AperturaCierreID desc)
						   ,null
						   ,null
						   ,0
						   ,0
						   ,0
						   ,1)
			end


		END
GO
ALTER TABLE [dbo].[AperturaCierre] ENABLE TRIGGER [TRG_INS_Produccion]
GO
/****** Object:  Trigger [dbo].[TRG_INS_MermaProduccion]    Script Date: 25/11/2020 3:52:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create TRIGGER [dbo].[TRG_INS_MermaProduccion]
ON [dbo].[MermaPorTurno]
FOR INSERT
AS
BEGIN
		SET NOCOUNT ON;	

			begin
			
					UPDATE [dbo].[Produccion]
					   SET 
						  [CantidadMerma] = (select top 1 Cantidad from MermaPorTurno order by MermaPorTurnoID desc)
						  
					 WHERE Produccion.PedidoID = (

						select top 1 AperturaCierre.PedidoID 
						from AperturaCierre 
							join MermaPorTurno on  AperturaCierre.AperturaCierreID = MermaPorTurno.AperturaCierreID
						where AperturaCierre.AperturaCierreID = MermaPorTurno.AperturaCierreID
						 order by  AperturaCierre.AperturaCierreID desc
						)
				
end


	END
GO
ALTER TABLE [dbo].[MermaPorTurno] ENABLE TRIGGER [TRG_INS_MermaProduccion]
GO
USE [master]
GO
ALTER DATABASE [DIPREMA] SET  READ_WRITE 
GO
