﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Backend.Clases
{

   
    public static class BLLUtilitarios
    {
        public static string msj;
        public static StringBuilder coodigoToken; 

        #region Metodos/Funciones

        /// <summary>
        /// Funcion principal para convertir las contrasenas
        /// </summary>
        /// <param name="text">Texto de la clave de acceso</param>
        /// <returns>La conversion de la contrasena</returns>

        public static string getHashSha256(string text)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }
        public static float resta(float a, float b)
        {
            return a - b;
        }

            public static bool PasswordValida(string Password)
        {
            return PasswordPolicy.IsValid(Password);
        }

        public static string MensajePoliticasPassword()
        {
            PasswordPolicy p = new PasswordPolicy();
            return "Debe tener al menos "+p.Minimo()+
                " caracteres, "+p.Mayuscula()+" mayusculas, "+p.Minisculas()+" minusculas, "+p.NoAlfanumerico()+" no " +
                "alfanumericos";
        }

        #endregion Metodos/Funciones

        private class PasswordPolicy
        {
            public int Minimo() { return Minimum_Length; }
            public int Mayuscula() { return Upper_Case_length; }

            public int Minisculas() { return Lower_Case_length; }
            public int NoAlfanumerico() { return NonAlpha_length; }

            private static int Minimum_Length = 7;
            private static int Upper_Case_length = 1;
            private static int Lower_Case_length = 1;
            private static int NonAlpha_length = 1;
            private static int Numeric_length = 1;

            public static bool IsValid(string Password)
            {
                if (Password.Length < Minimum_Length)
                    return false;
                if (UpperCaseCount(Password) < Upper_Case_length)
                    return false;
                if (LowerCaseCount(Password) < Lower_Case_length)
                    return false;
                if (NumericCount(Password) < 1)
                    return false;
                if (NonAlphaCount(Password) < NonAlpha_length)
                    return false;
                return true;
            }

            private static int UpperCaseCount(string Password)
            {
                return Regex.Matches(Password, "[A-Z]").Count;
            }

            private static int LowerCaseCount(string Password)
            {
                return Regex.Matches(Password, "[a-z]").Count;
            }
            private static int NumericCount(string Password)
            {
                return Regex.Matches(Password, "[0-9]").Count;
            }
            private static int NonAlphaCount(string Password)
            {
                return Regex.Matches(Password, @"[^0-9a-zA-Z\._]").Count;
            }


           
        }
    }
}