﻿using Backend.DAL;
using Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.BLL
{
    public class BLLEntregas
    {
        public double cantidadRestante(DetalleEntrega detalleEntrega)
        {
            double cantidadEnProduccion = 0;
            List<Produccion> produccion = new List<Produccion>();
            using (UnidadDeTrabajo<Produccion> unidad = new UnidadDeTrabajo<Produccion>(new BDContext()))
            {
                produccion = unidad.genericDAL.GetAll().ToList();
            }
            foreach(var item in produccion)
            {
                if (detalleEntrega.ProduccionID == item.ProduccionID)
                {
                    cantidadEnProduccion += item.CantidadProduccionTotal;
                }
            }
            return cantidadEnProduccion - detalleEntrega.Cantidad;
        }
    }
}
