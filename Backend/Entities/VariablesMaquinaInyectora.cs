//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Backend.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class VariablesMaquinaInyectora
    {
        public int VariablesMaquinaInyectoraID { get; set; }
        public int MaquinaID { get; set; }
        public int ProductoID { get; set; }
        public int MateriaPrimaPrincipalID { get; set; }
        public Nullable<int> MateriaPrimaSecundariaID { get; set; }
        public Nullable<int> MateriaPrimaTerciariaID { get; set; }
        public double CantidadPrimaPrincipal { get; set; }
        public Nullable<double> CantidadPrimaSecundaria { get; set; }
        public Nullable<double> CantidadPrimaTerciaria { get; set; }
        public bool Estado { get; set; }
    
        public virtual Maquinas Maquinas { get; set; }
        public virtual MateriaPrima MateriaPrima { get; set; }
        public virtual MateriaPrima MateriaPrima1 { get; set; }
        public virtual MateriaPrima MateriaPrima2 { get; set; }
        public virtual Productos Productos { get; set; }
    }
}
