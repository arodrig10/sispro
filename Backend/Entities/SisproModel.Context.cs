﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Backend.Entities
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class BDContext : DbContext
    {
        public BDContext()
            : base("name=BDContext")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<AperturaCierre> AperturaCierre { get; set; }
        public virtual DbSet<Clientes> Clientes { get; set; }
        public virtual DbSet<DetalleEntrega> DetalleEntrega { get; set; }
        public virtual DbSet<Entregas> Entregas { get; set; }
        public virtual DbSet<Estados> Estados { get; set; }
        public virtual DbSet<FichaTecnicaBotella> FichaTecnicaBotella { get; set; }
        public virtual DbSet<FichaTecnicaTapa> FichaTecnicaTapa { get; set; }
        public virtual DbSet<Maquinas> Maquinas { get; set; }
        public virtual DbSet<MateriaPrima> MateriaPrima { get; set; }
        public virtual DbSet<MermaPorTurno> MermaPorTurno { get; set; }
        public virtual DbSet<Pedidos> Pedidos { get; set; }
        public virtual DbSet<Produccion> Produccion { get; set; }
        public virtual DbSet<ProduccionPaquetesCajas> ProduccionPaquetesCajas { get; set; }
        public virtual DbSet<Productos> Productos { get; set; }
        public virtual DbSet<Proveedor> Proveedor { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<TipoMaquina> TipoMaquina { get; set; }
        public virtual DbSet<TipoMateriaPrima> TipoMateriaPrima { get; set; }
        public virtual DbSet<TipoMerma> TipoMerma { get; set; }
        public virtual DbSet<TipoProducto> TipoProducto { get; set; }
        public virtual DbSet<UnidadMedida> UnidadMedida { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }
        public virtual DbSet<VariablesMaquinaInyectora> VariablesMaquinaInyectora { get; set; }
        public virtual DbSet<VariablesMaquinaSopladora> VariablesMaquinaSopladora { get; set; }
        public virtual DbSet<AperturaCierreActivos> AperturaCierreActivos { get; set; }
        public virtual DbSet<ClientesActivos> ClientesActivos { get; set; }
        public virtual DbSet<DetalleEntregaActivos> DetalleEntregaActivos { get; set; }
        public virtual DbSet<EntregasActivos> EntregasActivos { get; set; }
        public virtual DbSet<FichaTecnicaBotellaActivos> FichaTecnicaBotellaActivos { get; set; }
        public virtual DbSet<FichaTecnicaTapaActivos> FichaTecnicaTapaActivos { get; set; }
        public virtual DbSet<MaquinasActivos> MaquinasActivos { get; set; }
        public virtual DbSet<MaquinasInyectoraActivos> MaquinasInyectoraActivos { get; set; }
        public virtual DbSet<MaquinasSopladoraActivos> MaquinasSopladoraActivos { get; set; }
        public virtual DbSet<MateriaPrimaActivos> MateriaPrimaActivos { get; set; }
        public virtual DbSet<MateriaPrimaInyectoraActivos> MateriaPrimaInyectoraActivos { get; set; }
        public virtual DbSet<MateriaPrimaSopladoraActivos> MateriaPrimaSopladoraActivos { get; set; }
        public virtual DbSet<MermaPorTurnoActivos> MermaPorTurnoActivos { get; set; }
        public virtual DbSet<PedidosActivos> PedidosActivos { get; set; }
        public virtual DbSet<ProduccionActivos> ProduccionActivos { get; set; }
        public virtual DbSet<ProduccionPaquetesCajaActivos> ProduccionPaquetesCajaActivos { get; set; }
        public virtual DbSet<ProductosActivos> ProductosActivos { get; set; }
        public virtual DbSet<ProductosBotellaActivos> ProductosBotellaActivos { get; set; }
        public virtual DbSet<ProductosTapaActivos> ProductosTapaActivos { get; set; }
        public virtual DbSet<ProveedorActivos> ProveedorActivos { get; set; }
        public virtual DbSet<UsuariosActivos> UsuariosActivos { get; set; }
        public virtual DbSet<VariablesMaquinaInyectoraActivos> VariablesMaquinaInyectoraActivos { get; set; }
        public virtual DbSet<VariablesMaquinaSopladoraActivos> VariablesMaquinaSopladoraActivos { get; set; }
        public virtual DbSet<ClientesProduccion> ClientesProduccion { get; set; }
    
        public virtual ObjectResult<sp_DatosEtiqueta_Result> sp_DatosEtiqueta(Nullable<int> etiqueta)
        {
            var etiquetaParameter = etiqueta.HasValue ?
                new ObjectParameter("Etiqueta", etiqueta) :
                new ObjectParameter("Etiqueta", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_DatosEtiqueta_Result>("sp_DatosEtiqueta", etiquetaParameter);
        }
    
        public virtual ObjectResult<sp_FichaOperacion_Result> sp_FichaOperacion(Nullable<int> pedido)
        {
            var pedidoParameter = pedido.HasValue ?
                new ObjectParameter("Pedido", pedido) :
                new ObjectParameter("Pedido", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_FichaOperacion_Result>("sp_FichaOperacion", pedidoParameter);
        }
    
        public virtual ObjectResult<sp_FichaOperacionInyectora_Result> sp_FichaOperacionInyectora(Nullable<int> pedido)
        {
            var pedidoParameter = pedido.HasValue ?
                new ObjectParameter("Pedido", pedido) :
                new ObjectParameter("Pedido", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_FichaOperacionInyectora_Result>("sp_FichaOperacionInyectora", pedidoParameter);
        }
    
        public virtual ObjectResult<sp_FichaOperacionSopladora_Result> sp_FichaOperacionSopladora(Nullable<int> pedido)
        {
            var pedidoParameter = pedido.HasValue ?
                new ObjectParameter("Pedido", pedido) :
                new ObjectParameter("Pedido", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_FichaOperacionSopladora_Result>("sp_FichaOperacionSopladora", pedidoParameter);
        }
    
        public virtual ObjectResult<string> sp_getRolesForUser(string userName)
        {
            var userNameParameter = userName != null ?
                new ObjectParameter("userName", userName) :
                new ObjectParameter("userName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("sp_getRolesForUser", userNameParameter);
        }
    
        public virtual ObjectResult<string> sp_getUsuariosRole(string roleName)
        {
            var roleNameParameter = roleName != null ?
                new ObjectParameter("roleName", roleName) :
                new ObjectParameter("roleName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("sp_getUsuariosRole", roleNameParameter);
        }
    
        public virtual ObjectResult<sp_GraficoParettoMerma_Result> sp_GraficoParettoMerma()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GraficoParettoMerma_Result>("sp_GraficoParettoMerma");
        }
    
        public virtual ObjectResult<sp_GraficoPedidos_Result> sp_GraficoPedidos()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GraficoPedidos_Result>("sp_GraficoPedidos");
        }
    
        public virtual ObjectResult<sp_GraficoProduccion_Result> sp_GraficoProduccion()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GraficoProduccion_Result>("sp_GraficoProduccion");
        }
    
        public virtual ObjectResult<Nullable<bool>> sp_isUserInRole(string userName, string roleName)
        {
            var userNameParameter = userName != null ?
                new ObjectParameter("userName", userName) :
                new ObjectParameter("userName", typeof(string));
    
            var roleNameParameter = roleName != null ?
                new ObjectParameter("roleName", roleName) :
                new ObjectParameter("roleName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<bool>>("sp_isUserInRole", userNameParameter, roleNameParameter);
        }
    
        public virtual int sp_setSpecificRoleForUser(Nullable<int> usuarioID, Nullable<int> roleID)
        {
            var usuarioIDParameter = usuarioID.HasValue ?
                new ObjectParameter("UsuarioID", usuarioID) :
                new ObjectParameter("UsuarioID", typeof(int));
    
            var roleIDParameter = roleID.HasValue ?
                new ObjectParameter("RoleID", roleID) :
                new ObjectParameter("RoleID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_setSpecificRoleForUser", usuarioIDParameter, roleIDParameter);
        }
    }
}
