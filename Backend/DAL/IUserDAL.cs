﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.DAL
{
    public interface IUserDAL
    {
        bool insertar(int idRole, string login);
        bool insertar(string roleName, string login);
        Usuarios getUser(string userName);
        Usuarios getUser(string userName, string password);
        Usuarios getUser(int EmpleadoId);
        Usuarios get(int id);
        List<Usuarios> getAll();
        bool isUserInRole(string userName, string roleName);
        string[] gerRolesForUser(string userName);
        List<Usuarios> getUsuariosRole(string roleName);
        List<Roles> listarRoles();
        bool eliminar(string idRole, int idUsuario);
        Usuarios ObtenerPorID(int id);
    }
}
