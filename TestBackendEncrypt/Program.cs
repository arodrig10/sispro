﻿using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBackendEncrypt
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hash en SHA256: "+ BLLUtilitarios.getHashSha256("andres"));
            Console.WriteLine("Hash en SHA256: " + BLLUtilitarios.getHashSha256("hansel"));
            Console.WriteLine("Hash en SHA256: " + BLLUtilitarios.getHashSha256("123"));
            Console.ReadLine();
        }
    }
}
