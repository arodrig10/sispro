﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Backend.DAL;
using Backend.Entities;
using System.Linq.Expressions;
using System.Linq;

namespace TestBackend
{
    [TestClass]
    public class TestEntregaDAL
    {
        private UnidadDeTrabajo<Entregas> unidad;


        [TestMethod]
        public void TestAdd()
        {
            Entregas obj = new Entregas
            {
                NumeroEntrega = "TEST-01",
                Fecha = DateTime.Now,
                UnidadesEntregadas = 999,
                NumeroFactura = "TEST",
                Estado = true
            };
            using (unidad = new UnidadDeTrabajo<Entregas>(new BDContext()))
            {
                unidad.genericDAL.Add(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestGetByCode()
        {
            using (unidad = new UnidadDeTrabajo<Entregas>(new BDContext()))
            {
                Expression<Func<Entregas, bool>> consulta = (c => c.NumeroEntrega.Contains("TEST-01"));
                List<Entregas> lista = unidad.genericDAL.Find(consulta).ToList();
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestModify()
        {
            using (unidad = new UnidadDeTrabajo<Entregas>(new BDContext()))
            {
                Expression<Func<Entregas, bool>> consulta = (c => c.NumeroEntrega.Contains("TEST-01"));
                List<Entregas> lista = unidad.genericDAL.Find(consulta).ToList();
                Entregas obj = lista.FirstOrDefault();
                obj.NumeroFactura = "PRUEBAMODF";
                unidad.genericDAL.Update(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            using (unidad = new UnidadDeTrabajo<Entregas>(new BDContext()))
            {
                Expression<Func<Entregas, bool>> consulta = (c => c.NumeroEntrega.Contains("TEST-01"));
                List<Entregas> lista = unidad.genericDAL.Find(consulta).ToList();
                Entregas obj = lista.FirstOrDefault();
                unidad.genericDAL.Remove(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }
    }
}
