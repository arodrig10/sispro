﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Backend.DAL;
using Backend.Entities;
using System.Linq.Expressions;
using System.Linq;

namespace TestBackend
{
    [TestClass]
    public class TestPedidoDAL
    {
        private UnidadDeTrabajo<Pedidos> unidad;

        [TestMethod]
        public void TestAdd()
        {
            Pedidos obj = new Pedidos
            {
                NumeroPedido = "TEST-01",
                ClienteID = 21,
                NumeroOrdenDeCompra = null,
                ProductoID = 5,
                UsuarioID = 1014,
                UnidadMedidaID = 4,
                Cantidad = 2000,
                Descripcion = "Test insert",
                Fecha = DateTime.Now,
                PrecioTotal = 30000,
                Estado = true
            };
            using (unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
            {
                unidad.genericDAL.Add(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestGetByCode()
        {
            using (unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
            {
                Expression<Func<Pedidos, bool>> consulta = (c => c.NumeroPedido.Contains("TEST-01"));
                List<Pedidos> lista = unidad.genericDAL.Find(consulta).ToList();
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestModify()
        {
            using (unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
            {
                Expression<Func<Pedidos, bool>> consulta = (c => c.NumeroPedido.Contains("TEST-01"));
                List<Pedidos> lista = unidad.genericDAL.Find(consulta).ToList();
                Pedidos obj = lista.FirstOrDefault();
                obj.Descripcion = "Prueba modificar";
                unidad.genericDAL.Update(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            using (unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
            {
                Expression<Func<Pedidos, bool>> consulta = (c => c.NumeroPedido.Contains("TEST-01"));
                List<Pedidos> lista = unidad.genericDAL.Find(consulta).ToList();
                Pedidos obj = lista.FirstOrDefault();
                unidad.genericDAL.Remove(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }
    }
}
