﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Backend.DAL;
using Backend.Entities;
using System.Linq.Expressions;
using System.Linq;


namespace TestBackend
{
    [TestClass]
    public class TestMateriaPrimaDAL
    {
        private UnidadDeTrabajo<MateriaPrima> unidad;


        [TestMethod]
        public void TestAdd()
        {
            MateriaPrima obj = new MateriaPrima
            {
                CodigoMateriaPrima = "R-001",
                Nombre = "Resina HD",
                ProveedorID = 5,
                TipoMateriaPrimaID = 2,
                UnidadMedidaID = 2,
                Cantidad = 5,
                PrecioUnitario = 20,
                PrecioTotal = 10000,
                Descripcion = "Resina Alta densidad",
                Estado = true
            };
            using (unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
            {
                unidad.genericDAL.Add(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestGetByCode()
        {
            using (unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
            {
                Expression<Func<MateriaPrima, bool>> consulta = (c => c.CodigoMateriaPrima.Contains("R-001"));
                List<MateriaPrima> lista = unidad.genericDAL.Find(consulta).ToList();
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestModify()
        {
            using (unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
            {
                Expression<Func<MateriaPrima, bool>> consulta = (c => c.CodigoMateriaPrima.Contains("R-001"));
                List<MateriaPrima> lista = unidad.genericDAL.Find(consulta).ToList();
                MateriaPrima obj = lista.FirstOrDefault();
                obj.Descripcion = "Prueba modificar";
                unidad.genericDAL.Update(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            using (unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
            {
                Expression<Func<MateriaPrima, bool>> consulta = (c => c.CodigoMateriaPrima.Contains("R-001"));
                List<MateriaPrima> lista = unidad.genericDAL.Find(consulta).ToList();
                MateriaPrima obj = lista.FirstOrDefault();
                unidad.genericDAL.Remove(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }
    }
}
