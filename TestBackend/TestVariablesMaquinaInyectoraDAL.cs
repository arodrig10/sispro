﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Backend.DAL;
using Backend.Entities;
using System.Linq.Expressions;
using System.Linq;

namespace TestBackend
{
    [TestClass]
    public class TestVariablesMaquinaInyectoraDAL
    {
        private UnidadDeTrabajo<VariablesMaquinaInyectora> unidad;
        [TestMethod]
        public void TestAdd()
        {
            VariablesMaquinaInyectora obj = new VariablesMaquinaInyectora
            {
                MaquinaID = 12,
                ProductoID = 2,
                MateriaPrimaPrincipalID = 4,
                MateriaPrimaSecundariaID = null,
                MateriaPrimaTerciariaID = null,
                CantidadPrimaPrincipal = 1,
                CantidadPrimaSecundaria = null,
                CantidadPrimaTerciaria = null,
                Estado = true
            };
            using (unidad = new UnidadDeTrabajo<VariablesMaquinaInyectora>(new BDContext()))
            {
                unidad.genericDAL.Add(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestGetByID()
        {
            using (unidad = new UnidadDeTrabajo<VariablesMaquinaInyectora>(new BDContext()))
            {
                List<VariablesMaquinaInyectora> listaCompleta = unidad.genericDAL.GetAll().ToList();
                VariablesMaquinaInyectora obj = listaCompleta.Last();
                Expression<Func<VariablesMaquinaInyectora, bool>> consulta = (c => c.VariablesMaquinaInyectoraID == obj.VariablesMaquinaInyectoraID);
                List<VariablesMaquinaInyectora> lista = unidad.genericDAL.Find(consulta).ToList();
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestModify()
        {
            using (unidad = new UnidadDeTrabajo<VariablesMaquinaInyectora>(new BDContext()))
            {
                List<VariablesMaquinaInyectora> listaCompleta = unidad.genericDAL.GetAll().ToList();
                VariablesMaquinaInyectora obje = listaCompleta.Last();
                Expression<Func<VariablesMaquinaInyectora, bool>> consulta = (c => c.VariablesMaquinaInyectoraID == obje.VariablesMaquinaInyectoraID);
                List<VariablesMaquinaInyectora> lista = unidad.genericDAL.Find(consulta).ToList();
                VariablesMaquinaInyectora obj = lista.FirstOrDefault();
                obj.CantidadPrimaPrincipal = 9999;
                unidad.genericDAL.Update(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            using (unidad = new UnidadDeTrabajo<VariablesMaquinaInyectora>(new BDContext()))
            {
                List<VariablesMaquinaInyectora> listaCompleta = unidad.genericDAL.GetAll().ToList();
                VariablesMaquinaInyectora obje = listaCompleta.Last();
                Expression<Func<VariablesMaquinaInyectora, bool>> consulta = (c => c.VariablesMaquinaInyectoraID == obje.VariablesMaquinaInyectoraID);
                List<VariablesMaquinaInyectora> lista = unidad.genericDAL.Find(consulta).ToList();
                VariablesMaquinaInyectora obj = lista.FirstOrDefault();
                unidad.genericDAL.Remove(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }
    }
}
