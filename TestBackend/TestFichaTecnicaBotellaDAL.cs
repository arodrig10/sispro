﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Backend.DAL;
using Backend.Entities;
using System.Linq.Expressions;
using System.Linq;

namespace TestBackend
{
    [TestClass]
    public class TestFichaTecnicaBotellaDAL
    {
        private UnidadDeTrabajo<FichaTecnicaBotella> unidad;
        [TestMethod]
        public void TestAdd()
        {
            FichaTecnicaBotella obj = new FichaTecnicaBotella
            {
                ProductoID = 2,
                Altura = 9999,
                DiametroMayor = 9999,
                DiametroMenor = 9999,
                DiametroCuello = 9999,
                CapacidadVolumetrica = 9999,
                Tolerancia = null,
                ImagenCuerpo = null,
                ImagenFondo = null,
                Estado = true
            };
            using (unidad = new UnidadDeTrabajo<FichaTecnicaBotella>(new BDContext()))
            {
                unidad.genericDAL.Add(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestGetByID()
        {
            using (unidad = new UnidadDeTrabajo<FichaTecnicaBotella>(new BDContext()))
            {
                List<FichaTecnicaBotella> listaCompleta = unidad.genericDAL.GetAll().ToList();
                FichaTecnicaBotella obj = listaCompleta.Last();
                Expression<Func<FichaTecnicaBotella, bool>> consulta = (c => c.FichaTecnicaBotellaID == obj.FichaTecnicaBotellaID);
                List<FichaTecnicaBotella> lista = unidad.genericDAL.Find(consulta).ToList();
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestModify()
        {
            using (unidad = new UnidadDeTrabajo<FichaTecnicaBotella>(new BDContext()))
            {
                List<FichaTecnicaBotella> listaCompleta = unidad.genericDAL.GetAll().ToList();
                FichaTecnicaBotella obje = listaCompleta.Last();
                Expression<Func<FichaTecnicaBotella, bool>> consulta = (c => c.FichaTecnicaBotellaID == obje.FichaTecnicaBotellaID);
                List<FichaTecnicaBotella> lista = unidad.genericDAL.Find(consulta).ToList();
                FichaTecnicaBotella obj = lista.FirstOrDefault();
                obj.DiametroMayor = 9999999999;
                unidad.genericDAL.Update(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            using (unidad = new UnidadDeTrabajo<FichaTecnicaBotella>(new BDContext()))
            {
                List<FichaTecnicaBotella> listaCompleta = unidad.genericDAL.GetAll().ToList();
                FichaTecnicaBotella obje = listaCompleta.Last();
                Expression<Func<FichaTecnicaBotella, bool>> consulta = (c => c.FichaTecnicaBotellaID == obje.FichaTecnicaBotellaID);
                List<FichaTecnicaBotella> lista = unidad.genericDAL.Find(consulta).ToList();
                FichaTecnicaBotella obj = lista.FirstOrDefault();
                unidad.genericDAL.Remove(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }
    }
}
