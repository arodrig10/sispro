﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Backend.DAL;
using Backend.Entities;
using System.Linq.Expressions;
using System.Linq;

namespace TestBackend
{
    [TestClass]
    public class TestUsuarioDAL
    {
        private UnidadDeTrabajo<Usuarios> unidad;


        [TestMethod]
        public void TestAdd()
        {
            Usuarios obj = new Usuarios
            {
                Identificacion = "123456789",
                UserName = "usuarioprueba",
                Password = "de4ad303211af5bd002b69d07ce48d1fd8b0b537f7fc34a285eca4d0dea6ac50",
                NombreCompleto = "Hansel Murillo",
                Telefono = "61962809",
                Correo = "hmurillo@hotmail.com",
                Direccion = "100m este de empaques universal",
                Estado = true
            };
            using (unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
            {
                unidad.genericDAL.Add(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestGetByCode()
        {
            using (unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
            {
                Expression<Func<Usuarios, bool>> consulta = (c => c.UserName.Contains("usuarioprueba"));
                List<Usuarios> lista = unidad.genericDAL.Find(consulta).ToList();
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestModify()
        {
            using (unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
            {
                Expression<Func<Usuarios, bool>> consulta = (c => c.UserName.Contains("usuarioprueba"));
                List<Usuarios> lista = unidad.genericDAL.Find(consulta).ToList();
                Usuarios obj = lista.FirstOrDefault();
                obj.Direccion = "Prueba modificar";
                unidad.genericDAL.Update(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            using (unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
            {
                Expression<Func<Usuarios, bool>> consulta = (c => c.UserName.Contains("usuarioprueba"));
                List<Usuarios> lista = unidad.genericDAL.Find(consulta).ToList();
                Usuarios obj = lista.FirstOrDefault();
                unidad.genericDAL.Remove(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }
    }
}
