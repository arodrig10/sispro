﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Backend.DAL;
using Backend.Entities;
using System.Linq.Expressions;
using System.Linq;

namespace TestBackend
{
    [TestClass]
    public class TestMaquinaDAL
    {
        private UnidadDeTrabajo<Maquinas> unidad;

        [TestMethod]
        public void TestAdd()
        {
            Maquinas maquina = new Maquinas
            {
                CodigoMaquina = "INYEC01",
                Nombre = "Inyectora tapa",
                TipoMaquinaID = 2,
                Descripcion = "Maquina inyectora de tapa",
                ProduccionHora = "1234",
                Estado = true
            };
            using (unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
            {
                unidad.genericDAL.Add(maquina);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestGetByCode()
        {
            using (unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
            {
                Expression<Func<Maquinas, bool>> consulta = (c => c.CodigoMaquina.Contains("INYEC01"));
                List<Maquinas> lista = unidad.genericDAL.Find(consulta).ToList();
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestModify()
        {
            using (unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
            {
                Expression<Func<Maquinas, bool>> consulta = (c => c.CodigoMaquina.Contains("INYEC01"));
                List<Maquinas> lista = unidad.genericDAL.Find(consulta).ToList();
                Maquinas maquina = lista.FirstOrDefault();
                maquina.Descripcion = "Prueba modificar";
                unidad.genericDAL.Update(maquina);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            using (unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
            {
                Expression<Func<Maquinas, bool>> consulta = (c => c.CodigoMaquina.Contains("INYEC01"));
                List<Maquinas> lista = unidad.genericDAL.Find(consulta).ToList();
                Maquinas maquina = lista.FirstOrDefault();
                unidad.genericDAL.Remove(maquina);
                Assert.AreEqual(true, unidad.Complete());
            }
        }


    }
}
