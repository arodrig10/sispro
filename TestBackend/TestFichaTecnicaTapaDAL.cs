﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Backend.DAL;
using Backend.Entities;
using System.Linq.Expressions;
using System.Linq;

namespace TestBackend
{
    [TestClass]
    public class TestFichaTecnicaTapaDAL
    {
        private UnidadDeTrabajo<FichaTecnicaTapa> unidad;
        [TestMethod]
        public void TestAdd()
        {
            FichaTecnicaTapa obj = new FichaTecnicaTapa
            {
                ProductoID = 26,
                PesoPieza = 123,
                Altura = 123,
                DiametroInterno = 123,
                DiametroExternoSelloOliva = 123,
                AlturaSelloOliva = 123,
                DiametroInternoBandaSeguridad = 123,
                EspesorPanel = 123,
                NumeroEstrias = 123,
                TorqueCierreRecomendado = 123,
                TorqueApertura = 123,
                Tolerancia = null,
                Imagen = null,
                Estado = true
            };
            using (unidad = new UnidadDeTrabajo<FichaTecnicaTapa>(new BDContext()))
            {
                unidad.genericDAL.Add(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestGetByID()
        {
            using (unidad = new UnidadDeTrabajo<FichaTecnicaTapa>(new BDContext()))
            {
                List<FichaTecnicaTapa> listaCompleta = unidad.genericDAL.GetAll().ToList();
                FichaTecnicaTapa obj = listaCompleta.Last();
                Expression<Func<FichaTecnicaTapa, bool>> consulta = (c => c.FichaTecnicaTapaID == obj.FichaTecnicaTapaID);
                List<FichaTecnicaTapa> lista = unidad.genericDAL.Find(consulta).ToList();
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestModify()
        {
            using (unidad = new UnidadDeTrabajo<FichaTecnicaTapa>(new BDContext()))
            {
                List<FichaTecnicaTapa> listaCompleta = unidad.genericDAL.GetAll().ToList();
                FichaTecnicaTapa obje = listaCompleta.Last();
                Expression<Func<FichaTecnicaTapa, bool>> consulta = (c => c.FichaTecnicaTapaID == obje.FichaTecnicaTapaID);
                List<FichaTecnicaTapa> lista = unidad.genericDAL.Find(consulta).ToList();
                FichaTecnicaTapa obj = lista.FirstOrDefault();
                obj.NumeroEstrias = 9999;
                unidad.genericDAL.Update(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            using (unidad = new UnidadDeTrabajo<FichaTecnicaTapa>(new BDContext()))
            {
                List<FichaTecnicaTapa> listaCompleta = unidad.genericDAL.GetAll().ToList();
                FichaTecnicaTapa obje = listaCompleta.Last();
                Expression<Func<FichaTecnicaTapa, bool>> consulta = (c => c.FichaTecnicaTapaID == obje.FichaTecnicaTapaID);
                List<FichaTecnicaTapa> lista = unidad.genericDAL.Find(consulta).ToList();
                FichaTecnicaTapa obj = lista.FirstOrDefault();
                unidad.genericDAL.Remove(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }
    }
}
