﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Backend.DAL;
using Backend.Entities;
using System.Linq.Expressions;
using System.Linq;

namespace TestBackend
{
    [TestClass]
    public class TestProveedorDAL
    {
        private UnidadDeTrabajo<Proveedor> unidad;


        [TestMethod]
        public void TestAdd()
        {
            Proveedor obj = new Proveedor
            {
                CodigoProveedor = "310170410220",
                Nombre = "Entec",
                Telefono = "24386719",
                Correo = "entec@hotmail.com",
                Direccion = "100m este de empaques universal",
                Estado = true
            };
            using (unidad = new UnidadDeTrabajo<Proveedor>(new BDContext()))
            {
                unidad.genericDAL.Add(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestGetByCode()
        {
            using (unidad = new UnidadDeTrabajo<Proveedor>(new BDContext()))
            {
                Expression<Func<Proveedor, bool>> consulta = (c => c.CodigoProveedor.Contains("310170410220"));
                List<Proveedor> lista = unidad.genericDAL.Find(consulta).ToList();
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestModify()
        {
            using (unidad = new UnidadDeTrabajo<Proveedor>(new BDContext()))
            {
                Expression<Func<Proveedor, bool>> consulta = (c => c.CodigoProveedor.Contains("310170410220"));
                List<Proveedor> lista = unidad.genericDAL.Find(consulta).ToList();
                Proveedor obj = lista.FirstOrDefault();
                obj.Direccion = "Prueba modificar";
                unidad.genericDAL.Update(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            using (unidad = new UnidadDeTrabajo<Proveedor>(new BDContext()))
            {
                Expression<Func<Proveedor, bool>> consulta = (c => c.CodigoProveedor.Contains("310170410220"));
                List<Proveedor> lista = unidad.genericDAL.Find(consulta).ToList();
                Proveedor obj = lista.FirstOrDefault();
                unidad.genericDAL.Remove(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }
    }
}
