﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Backend.DAL;
using Backend.Entities;
using System.Linq.Expressions;
using System.Linq;

namespace TestBackend
{
    [TestClass]
    public class TestVariablesMaquinaSopladoraDAL
    {
        private UnidadDeTrabajo<VariablesMaquinaSopladora> unidad;
        [TestMethod]
        public void TestAdd()
        {
            VariablesMaquinaSopladora obj = new VariablesMaquinaSopladora
            {
                MaquinaID = 12,
                ProductoID = 2,
                MateriaPrimaID = 4,
                RetardoPresoplado = 1,
                RetardoSoplado = 1,
                TiempoPresoplado = 1,
                TiempoRecuperacion = 1,
                TiempoSoplado = 1,
                TiempoEscape = 1,
                TiempoParoCadena = 1,
                TiempoSalidaPinzaPreforma = 1,
                FrecuenciaVentilador = 1.0,
                PresionEstirado = 1.0,
                Gancho = true,
                Relay = false,
                AnillosTornelas = true,
                Estado = true,
                Notas = null,
                CantidadZonas = 4,
                CantidadBancos = 2,
                PosicionBanco1Zona1 = "test",
                PosicionBanco1Zona2 = "test",
                PosicionBanco1Zona3 = "test",
                PosicionBanco1Zona4 = "test",
                PosicionBanco2Zona1 = "test",
                PosicionBanco2Zona2 = "test",
                PosicionBanco2Zona3 = "test",
                PosicionBanco2Zona4 = "test",
                ValorBanco1Zona1 = null,
                ValorBanco1Zona2 = null,
                ValorBanco1Zona3 = null,
                ValorBanco1Zona4 = null,
                ValorBanco2Zona1 = null,
                ValorBanco2Zona2 = null,
                ValorBanco2Zona3 = null,
                ValorBanco2Zona4 = null
            };
            using (unidad = new UnidadDeTrabajo<VariablesMaquinaSopladora>(new BDContext()))
            {
                unidad.genericDAL.Add(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestGetByID()
        {
            using (unidad = new UnidadDeTrabajo<VariablesMaquinaSopladora>(new BDContext()))
            {
                List<VariablesMaquinaSopladora> listaCompleta = unidad.genericDAL.GetAll().ToList();
                VariablesMaquinaSopladora obj = listaCompleta.Last();
                Expression<Func<VariablesMaquinaSopladora, bool>> consulta = (c => c.VariablesMaquinaSopladoraID == obj.VariablesMaquinaSopladoraID);
                List<VariablesMaquinaSopladora> lista = unidad.genericDAL.Find(consulta).ToList();
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestModify()
        {
            using (unidad = new UnidadDeTrabajo<VariablesMaquinaSopladora>(new BDContext()))
            {
                List<VariablesMaquinaSopladora> listaCompleta = unidad.genericDAL.GetAll().ToList();
                VariablesMaquinaSopladora obje = listaCompleta.Last();
                Expression<Func<VariablesMaquinaSopladora, bool>> consulta = (c => c.VariablesMaquinaSopladoraID == obje.VariablesMaquinaSopladoraID);
                List<VariablesMaquinaSopladora> lista = unidad.genericDAL.Find(consulta).ToList();
                VariablesMaquinaSopladora obj = lista.FirstOrDefault();
                obj.ValorBanco1Zona1 = 9999;
                unidad.genericDAL.Update(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            using (unidad = new UnidadDeTrabajo<VariablesMaquinaSopladora>(new BDContext()))
            {
                List<VariablesMaquinaSopladora> listaCompleta = unidad.genericDAL.GetAll().ToList();
                VariablesMaquinaSopladora obje = listaCompleta.Last();
                Expression<Func<VariablesMaquinaSopladora, bool>> consulta = (c => c.VariablesMaquinaSopladoraID == obje.VariablesMaquinaSopladoraID);
                List<VariablesMaquinaSopladora> lista = unidad.genericDAL.Find(consulta).ToList();
                VariablesMaquinaSopladora obj = lista.FirstOrDefault();
                unidad.genericDAL.Remove(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }
    }
}
