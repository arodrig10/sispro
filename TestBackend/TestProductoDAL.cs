﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Backend.DAL;
using Backend.Entities;
using System.Linq.Expressions;
using System.Linq;

namespace TestBackend
{
    [TestClass]
    public class TestProductoDAL
    {

        private UnidadDeTrabajo<Productos> unidad;

        [TestMethod]
        public void TestAdd()
        {
            Productos obj = new Productos
            {
                CodigoProducto = "B-001",
                Nombre = "Botella 1000ml",
                TipoProductoID = 1,
                Descripcion = "Botella",
                Imagen = null,
                Estado = true
            };

            using (unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
            {
                unidad.genericDAL.Add(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestGetByCode()
        {
            using (unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
            {
                Expression<Func<Productos, bool>> consulta = (c => c.CodigoProducto.Contains("B-001"));
                List<Productos> lista = unidad.genericDAL.Find(consulta).ToList();
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestModify()
        {
            using (unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
            {
                Expression<Func<Productos, bool>> consulta = (c => c.CodigoProducto.Contains("B-001"));
                List<Productos> lista = unidad.genericDAL.Find(consulta).ToList();
                Productos obj = lista.FirstOrDefault();
                obj.Descripcion = "Prueba modificar";
                unidad.genericDAL.Update(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            using (unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
            {
                Expression<Func<Productos, bool>> consulta = (c => c.CodigoProducto.Contains("B-001"));
                List<Productos> lista = unidad.genericDAL.Find(consulta).ToList();
                Productos obj = lista.FirstOrDefault();
                unidad.genericDAL.Remove(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }
    }
}
