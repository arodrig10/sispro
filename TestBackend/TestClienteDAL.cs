﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Backend.DAL;
using Backend.Entities;
using System.Linq.Expressions;
using System.Linq;

namespace TestBackend
{
    [TestClass]
    public class TestClienteDAL
    {
        private UnidadDeTrabajo<Clientes> unidad;


        [TestMethod]
        public void TestAdd()
        {
            Clientes obj = new Clientes
            {
                Identificacion = "123456789",
                NombreCompleto = "Diprema S.A.",
                Telefono = "1",
                Correo = "diprema@hotmail.com",
                Direccion = "100",
                Estado = true
            };
            using (unidad = new UnidadDeTrabajo<Clientes>(new BDContext()))
            {
                unidad.genericDAL.Add(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestGetByCode()
        {
            using (unidad = new UnidadDeTrabajo<Clientes>(new BDContext()))
            {
                Expression<Func<Clientes, bool>> consulta = (c => c.Identificacion.Contains("123456789"));
                List<Clientes> lista = unidad.genericDAL.Find(consulta).ToList();
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestModify()
        {
            using (unidad = new UnidadDeTrabajo<Clientes>(new BDContext()))
            {
                Expression<Func<Clientes, bool>> consulta = (c => c.Identificacion.Contains("123456789"));
                List<Clientes> lista = unidad.genericDAL.Find(consulta).ToList();
                Clientes obj = lista.FirstOrDefault();
                obj.Direccion = "Prueba modificar";
                unidad.genericDAL.Update(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            using (unidad = new UnidadDeTrabajo<Clientes>(new BDContext()))
            {
                Expression<Func<Clientes, bool>> consulta = (c => c.Identificacion.Contains("123456789"));
                List<Clientes> lista = unidad.genericDAL.Find(consulta).ToList();
                Clientes obj = lista.FirstOrDefault();
                unidad.genericDAL.Remove(obj);
                Assert.AreEqual(true, unidad.Complete());
            }
        }
    }
}
