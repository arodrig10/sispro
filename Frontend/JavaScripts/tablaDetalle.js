﻿function AgregarDetalle() {

    var entrega = document.getElementById("NumeroEntrega").value;
    var fecha = document.getElementById("Fecha").value;
    var factura = document.getElementById("NumeroFactura").value;
    var pedido = document.getElementById("Pedidos").value;
    var paquetes = document.getElementById("Paquetes").value;
    var cantidad = document.getElementById("Cantidad").value;


    if (entrega != null && entrega.length > 0
        && factura != null && factura.length > 0
        && !isNaN(paquetes)
        && !isNaN(cantidad)
        && paquetes > 0 && cantidad >0)
    {
        $.ajax({

            url: "/Entregas/ValidarCantidad",
            type: "POST",
            data: JSON.stringify({
              
                vpedido: pedido,
                vcantidad: cantidad
            }),
            dataType: "json",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {

                if (data.status == "Success")
                {
                                
                    $.ajax({

                                    url: "/Entregas/SaveDetailedInfo",
                                    type: "POST",
                                    data: JSON.stringify({
                                        entrega: entrega,
                                        fecha: fecha,
                                        factura: factura,
                                        pedido: pedido,
                                        paquetes: paquetes,
                                        cantidad: cantidad
                                    }),
                                    dataType: "json",
                                    traditional: true,
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        if (data.status == "Success") {


                                            var tbl = $('#TableDetalleEntrega tbody');
                                            tbl.append('<tr>'
                                                + '<td>' + $("#Pedidos option:selected").text() + '</td>'
                                                + '<td>' + paquetes + '</td>'
                                                + '<td>' + cantidad + '</td>'
                                                + '</tr > ');


                                            document.getElementById("Pedidos").value = "";
                                            document.getElementById("Paquetes").value = "";
                                            document.getElementById("Cantidad").value = "";

                                            swal({
                                                title: "¡Se ha agregado un articulo correctamente!",
                                                icon: "success",
                                                button: "OK",
                                            });


                                        } else {
                                            swal({
                                                title: "¡Ha ocurrido un error en la comunicación con la base de datos, intente de nuevo!",
                                                icon: "error",
                                                button: "OK",
                                            });

                                        }
                                    },
                                    error: function () {
                                        swal({
                                            title: "¡Ha ocurrido un error durante el proceso, intente de nuevo y si el proble persiste comuniquese con el encargado!",
                                            icon: "error",
                                            button: "OK",
                                        });
                                    }
                                });
                }
                else
                {
                    swal({
                        title: data.message,
                        icon: "error",
                        button: "OK",
                    });
                }
            },
            error: function () {
                swal({
                    title: "error",
                    icon: "error",
                    button: "OK",
                });
            }
        });
               
    }
    else
    {
                swal({
                    title: "¡Debe de completar todos los campos!",
                    icon: "error",
                    button: "OK",
                });
    }
}