﻿using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Backend.MailServices;
using Frontend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Frontend.Controllers
{
    [Authorize]
    public class LoginController : Controller
    {
        private IUserDAL userDAL;
        [AllowAnonymous]
        public ActionResult Index()
        {
            UserViewModel userViewModel = new UserViewModel() { };
            if (/*User.Identity.IsAuthenticated || */Session["userID"] != null || Session["nombreCompleto"] != null || Session["userName"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View(userViewModel);
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Authorize(UserViewModel userModel)
        {
            try
            {
                userDAL = new UserDALImpl();
                if (userModel.Password != null)
                {
                    userModel.Password = BLLUtilitarios.getHashSha256(userModel.Password);
                }
                var userDetails = userDAL.getUser(userModel.UserName, userModel.Password);
                if (userDetails == null || !userDetails.Estado)
                {
                    TempData["TipoMensaje"] = "alert alert-danger";
                    userModel.LoginErrorMessage = "Nombre de Usuario o Password Incorrectos";
                    return View("Index", userModel);
                }
                else
                {
                    Session["userID"] = userDetails.UsuarioID;
                    Session["userName"] = userDetails.UserName;
                    Session["nombreCompleto"] = userDetails.NombreCompleto;
                    var authTicket = new FormsAuthenticationTicket(userDetails.UserName, true, 100000);
                    var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(authTicket));
                    Response.Cookies.Add(cookie);
                    var name = User.Identity.Name; // line 4
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                userModel.LoginErrorMessage = "Error de conexion, contacte al administrador";
                return View("Index", userModel);
            }
        }
        public ActionResult Logout()
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    //     int userId = (int)Session["userID"];
                    Session.Clear();
                    Session.Abandon();
                    // clear authentication cookie
                    HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                    cookie1.Expires = DateTime.Now.AddYears(-1);
                    Response.Cookies.Add(cookie1);
                    // clear session cookie
                    HttpCookie cookie2 = new HttpCookie("ASP.NET_SessionId", "");
                    cookie2.Expires = DateTime.Now.AddYears(-1);
                    Response.Cookies.Add(cookie2);
                }
                return RedirectToAction("Index", "Login");
            }
            catch (Exception)
            {
                return RedirectToAction("Index","Home");
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult EnviarCorreo(string correo)
        {
            try
            {
                int id = 1;

                List<UsuariosActivos> usuario;
                using (UnidadDeTrabajo<UsuariosActivos> Unidad = new UnidadDeTrabajo<UsuariosActivos>(new BDContext()))
                {
                    usuario = Unidad.genericDAL.GetAll().ToList();
                }

                const string alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
                StringBuilder token = new StringBuilder();
                Random rnd = new Random();

                for (int i = 0; i < 7; i++)
                {
                    int indice = rnd.Next(alfabeto.Length);
                    token.Append(alfabeto[indice]);
                }

                BLLUtilitarios.coodigoToken = token;

                //Recorrer el usuario y validar el correo
                foreach (var item in usuario)
                {
                    if (item.Correo.Equals(correo))
                    {
                        id = item.UsuarioID;
                        //para verificar que el correo existe   

                        var mailService = new SystemSupportMail();
                        mailService.sendMail(
                            subject: "SOPORTE SISPRO: Recuperación de contraseña",
                            body: "Hola " + item.NombreCompleto + ".\n" +
                            "\nRecibimos una solicitud para reestablecer la constraseña de tu cuenta de SisPro." +
                            "El código de verificación de SisPro es:\n" +
                            "\n                            " + token.ToString() +
                            "\n" +
                            "\nRecuerde que por su seguridad este código es de carácter personal.\n" +
                            "\nAtentamente.\n \nSoporte SisPro",
                            recipientMail: new List<string> { item.Correo }
                            );

                        //Mensaje de exito
                    }
                }
                return RedirectToAction("ValidarCodigo", new { id = id });
            }
            catch (Exception)
            {
                return RedirectToAction("Index","Login");
            }
        }
        [AllowAnonymous]
        public ActionResult RecuperarContrasennia()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult ValidarCodigo(int? id)
        {
            try
            {
                if (id != null)
                {
                    Usuarios usuario;
                    using (UnidadDeTrabajo<Usuarios> unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                    {
                        usuario = unidad.genericDAL.Get((int)id);
                        if (!usuario.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            //Usuario no activo
                        }
                    }
                    UsuarioViewModel usuarios = Convertir(usuario);
                    usuarios.Password = "";
                    //List De tipoa de usuario
                    return View(usuarios);
                }
                //Usuario no coincide o no existe
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index","Login");
            }
            
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ValidarCodigo(UsuarioViewModel usuarioViewModel)
        {
            try
            {
                if (usuarioViewModel.codigoVerificación.Equals(BLLUtilitarios.coodigoToken.ToString()))
                {
                    if (BLLUtilitarios.PasswordValida(usuarioViewModel.Password))
                    {
                        usuarioViewModel.Estado = true;
                        usuarioViewModel.Password = BLLUtilitarios.getHashSha256(usuarioViewModel.Password);
                        using (UnidadDeTrabajo<Usuarios> unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                        {
                            unidad.genericDAL.Update(Convertir(usuarioViewModel));
                            unidad.Complete();
                        }
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return RedirectToAction("ValidarCodigo", new { id = usuarioViewModel.UsuarioID });
                    }
                }
                else
                {
                    return RedirectToAction("ValidarCodigo", new { id = usuarioViewModel.UsuarioID });
                }
            }
            catch(Exception)
            {
                return RedirectToAction("ValidarCodigo", new { id = usuarioViewModel.UsuarioID });
            }

        }

        private UsuarioViewModel Convertir(UsuariosActivos usuario)
        {
            return new UsuarioViewModel
            {
                UsuarioID = usuario.UsuarioID,
                Identificacion = usuario.Identificacion,
                UserName = usuario.UserName,
                Password = usuario.Password,
                NombreCompleto = usuario.NombreCompleto,
                Telefono = usuario.Telefono,
                Correo = usuario.Correo,
                Direccion = usuario.Direccion,
                Estado = usuario.Estado
            };
        }


        private UsuarioViewModel Convertir(Usuarios usuario)
        {
            return new UsuarioViewModel
            {
                UsuarioID = usuario.UsuarioID,
                Identificacion = usuario.Identificacion,
                UserName = usuario.UserName,
                Password = usuario.Password,
                NombreCompleto = usuario.NombreCompleto,
                Telefono = usuario.Telefono,
                Correo = usuario.Correo,
                Direccion = usuario.Direccion,
                Estado = usuario.Estado
            };
        }
        private Usuarios Convertir(UsuarioViewModel usuarioViewModel)
        {
            return new Usuarios
            {
                UsuarioID = usuarioViewModel.UsuarioID,
                Identificacion = usuarioViewModel.Identificacion,
                UserName = usuarioViewModel.UserName,
                Password = usuarioViewModel.Password,
                NombreCompleto = usuarioViewModel.NombreCompleto,
                Telefono = usuarioViewModel.Telefono,
                Correo = usuarioViewModel.Correo,
                Direccion = usuarioViewModel.Direccion,
                Estado = usuarioViewModel.Estado
            };
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}