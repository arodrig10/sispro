﻿using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Frontend.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private UsuarioViewModel Convertir(UsuariosActivos usuario)
        {
            return new UsuarioViewModel
            {
                UsuarioID = usuario.UsuarioID,
                Identificacion = usuario.Identificacion,
                UserName = usuario.UserName,
                Password = usuario.Password,
                NombreCompleto = usuario.NombreCompleto,
                Telefono = usuario.Telefono,
                Correo = usuario.Correo,
                Direccion = usuario.Direccion,
                Estado = usuario.Estado
            };
        }
        private UsuarioViewModel Convertir(Usuarios usuario)
        {
            return new UsuarioViewModel
            {
                UsuarioID = usuario.UsuarioID,
                Identificacion = usuario.Identificacion,
                UserName = usuario.UserName,
                Password = usuario.Password,
                NombreCompleto = usuario.NombreCompleto,
                Telefono = usuario.Telefono,
                Correo = usuario.Correo,
                Direccion = usuario.Direccion,
                Estado = usuario.Estado
            };
        }
        private Usuarios Convertir(UsuarioViewModel usuarioViewModel)
        {
            return new Usuarios
            {
                UsuarioID = usuarioViewModel.UsuarioID,
                Identificacion = usuarioViewModel.Identificacion,
                UserName = usuarioViewModel.UserName,
                Password = usuarioViewModel.Password,
                NombreCompleto = usuarioViewModel.NombreCompleto,
                Telefono = usuarioViewModel.Telefono,
                Correo = usuarioViewModel.Correo,
                Direccion = usuarioViewModel.Direccion,
                Estado = usuarioViewModel.Estado
            };
        }


        public ActionResult Index()
        {
           return View();
        }
     /*   public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }*/

        public ActionResult MyAccount()
        {
            try
            {
                Usuarios usuario;
                using (UnidadDeTrabajo<Usuarios> unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                {
                    usuario = unidad.genericDAL.Get((int)Session["userID"]);
                    if (!usuario.Estado)
                    {
                        return RedirectToAction("Index");
                    }
                }
                UsuarioViewModel usuarios = Convertir(usuario);
                return View(usuarios);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener datos de usuario";
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        public ActionResult MyAccount(UsuarioViewModel usuarioViewModel)
        {
            try
            {
                Usuarios usuario;
                using (UnidadDeTrabajo<Usuarios> unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                {
                    usuario = unidad.genericDAL.Get(usuarioViewModel.UsuarioID);
                    usuario.Identificacion = usuarioViewModel.Identificacion;
                    usuario.NombreCompleto = usuarioViewModel.NombreCompleto;
                    usuario.Telefono = usuarioViewModel.Telefono;
                    usuario.Correo = usuarioViewModel.Correo;
                    usuario.Direccion = usuario.Direccion;
                    unidad.genericDAL.Update(usuario);
                    unidad.Complete();
                }
                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Datos actualizados";
                return RedirectToAction("Index");

            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al actualizar datos";
                return RedirectToAction("Index","Home");
            }
        }
        public ActionResult ChangePassword()
        {
            try
            {
                Usuarios usuario;
                using (UnidadDeTrabajo<Usuarios> unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                {
                    usuario = unidad.genericDAL.Get((int)Session["userID"]);
                    if (!usuario.Estado)
                    {
                        return RedirectToAction("Index");
                    }
                }
                UsuarioViewModel usuarios = Convertir(usuario);
                using (UnidadDeTrabajo<Roles> unidad = new UnidadDeTrabajo<Roles>(new BDContext()))
                {
                    usuarios.Roles = unidad.genericDAL.GetAll();
                    unidad.Complete();
                }
                usuarios.Password = "";
                //List De tipoa de usuario

                return View(usuarios);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al cambiar contraseña";
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        public ActionResult ChangePassword(UsuarioViewModel usuarioViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<Usuarios> unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                {
                    Usuarios usuario = unidad.genericDAL.Get(usuarioViewModel.UsuarioID);

                    if (BLLUtilitarios.getHashSha256(usuarioViewModel.Password) == usuario.Password)
                    {
                        if (usuarioViewModel.NewPassword == usuarioViewModel.RepeatPassword)
                        {
                            if (BLLUtilitarios.PasswordValida(usuarioViewModel.NewPassword))
                            {
                                usuario.Password = BLLUtilitarios.getHashSha256(usuarioViewModel.NewPassword);
                                unidad.genericDAL.Update(usuario);
                                unidad.Complete();
                                TempData["TipoMensaje"] = "alert alert-success";
                                TempData["Mensaje"] = "Cambio de contraseña correcto";
                                return RedirectToAction("Index");
                            }
                            TempData["TipoMensaje"] = "alert alert-danger";
                            TempData["Mensaje"] = "No cumple politicas: " + BLLUtilitarios.MensajePoliticasPassword();
                            return RedirectToAction("ChangePassword", new { @id = usuario.UsuarioID });
                        }
                        TempData["TipoMensaje"] = "alert alert-danger";
                        TempData["MensajeError"] = "Repita la contraseña";
                        return RedirectToAction("ChangePassword", new { @id = usuario.UsuarioID });
                    }
                    TempData["TipoMensaje"] = "alert alert-danger";
                    TempData["MensajeError"] = "Contraseña incorrecta";
                    return RedirectToAction("ChangePassword", new { @id = usuario.UsuarioID });
                }
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al cambiar contraseña";
                return RedirectToAction("Index", "Home");
            }
        }

      /*  protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }*/
    }
}