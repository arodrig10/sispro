﻿using Backend.Entities;
using Frontend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Frontend.Controllers
{
    [Authorize]
    public class GraficoParettoMermaController : Controller
    {
        // GET: GraficoParettoMerma
        public ActionResult _GraficoParettoMerma()
        {
            using (BDContext context = new BDContext())
            {
                var graficoMerma = context.sp_GraficoParettoMerma().ToList();

               GraficoParetoViewModel grafico = merma(graficoMerma);
                ViewBag.categoria = grafico.Categorias;

                return PartialView(grafico);
            }
        }

        private GraficoParetoViewModel merma(System.Collections.Generic.List<Backend.Entities.sp_GraficoParettoMerma_Result> graficoMerma)
        {
            string categoria="";
            string data="";
            List<string> categorias = new List<string>();
            foreach (var item in graficoMerma)
            {
                categoria = categoria + "'" + item.Tipo + "' ,";

                data = data + item.Cantidad + ",";
                categorias.Add(item.Tipo);
            }
            
            categoria = categoria.Remove(categoria.Length - 1);
            ViewBag.Lista = categorias;
            data = data.Remove(data.Length - 1);

            return new GraficoParetoViewModel
            {
                Categorias = categoria,
                Data = data

            };
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}