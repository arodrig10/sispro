﻿using Backend.DAL;
using Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Frontend.Controllers
{
    [Authorize]
    public class GraficosController : Controller
    {
        // GET: Graficos
        public ActionResult _GraficoDona()
        {

            using (BDContext context = new BDContext())
            {
                sp_GraficoPedidos_Result graficoPedidos = context.sp_GraficoPedidos().FirstOrDefault();
            

                return PartialView(graficoPedidos);
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }

    }
}