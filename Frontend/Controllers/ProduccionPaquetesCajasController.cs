﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;

namespace Frontend.Controllers
{
    [Authorize]
    public class ProduccionPaquetesCajasController : Controller
    {
        private ProduccionPaquetesCajasViewModel Convertir(ProduccionPaquetesCajaActivos produccionPaquetesCajaActivos)
        {
            return new ProduccionPaquetesCajasViewModel
            {
                EtiquetaID = produccionPaquetesCajaActivos.EtiquetaID,
                Fecha = produccionPaquetesCajaActivos.Fecha,
                Cantidad = produccionPaquetesCajaActivos.Cantidad,
                AperturaCierreID = produccionPaquetesCajaActivos.AperturaCierreID,
                UsuarioID = produccionPaquetesCajaActivos.UsuarioID,
                Turno = produccionPaquetesCajaActivos.Turno,
                Estado = produccionPaquetesCajaActivos.Estado
                
            };
        }


        private ProduccionPaquetesCajasViewModel Convertir(ProduccionPaquetesCajas produccionPaquetesCajas)
        {
            return new ProduccionPaquetesCajasViewModel
            {
                EtiquetaID = produccionPaquetesCajas.EtiquetaID,
                Fecha = produccionPaquetesCajas.Fecha,
                Cantidad = produccionPaquetesCajas.Cantidad,
                AperturaCierreID = produccionPaquetesCajas.AperturaCierreID,
                UsuarioID = produccionPaquetesCajas.UsuarioID,
                Turno = produccionPaquetesCajas.Turno,
                Estado = produccionPaquetesCajas.Estado
                
            };
        }

        private ProduccionPaquetesCajas Convertir(ProduccionPaquetesCajasViewModel produccionPaquetesCajasViewModel)
        {
            return new ProduccionPaquetesCajas
            {
                EtiquetaID = produccionPaquetesCajasViewModel.EtiquetaID,
                Fecha = produccionPaquetesCajasViewModel.Fecha,
                Cantidad = produccionPaquetesCajasViewModel.Cantidad,
                AperturaCierreID = produccionPaquetesCajasViewModel.AperturaCierreID,
                UsuarioID = produccionPaquetesCajasViewModel.UsuarioID,
                Turno = produccionPaquetesCajasViewModel.Turno,
                Estado = produccionPaquetesCajasViewModel.Estado
            };
        }


        public ActionResult Index()
        {
            List<ProduccionPaquetesCajaActivos> produccionPaquetesCajaActivos;

            using (UnidadDeTrabajo<ProduccionPaquetesCajaActivos> Unidad = new UnidadDeTrabajo<ProduccionPaquetesCajaActivos>(new BDContext()))
            {
                produccionPaquetesCajaActivos = Unidad.genericDAL.GetAll().ToList();
            }
            List<ProduccionPaquetesCajasViewModel> lista = new List<ProduccionPaquetesCajasViewModel>();
            ProduccionPaquetesCajasViewModel produccionPaquetesCajasViewModel;
            AperturaCierre aperturaCierre;

            foreach (var item in produccionPaquetesCajaActivos)
            {
                produccionPaquetesCajasViewModel = Convertir(item);
                
                using (UnidadDeTrabajo<AperturaCierre> Unidad = new UnidadDeTrabajo<AperturaCierre>(new BDContext()))
                {
                    produccionPaquetesCajasViewModel.AperturaCierre = Unidad.genericDAL.Get((int)produccionPaquetesCajasViewModel.AperturaCierreID);
                    aperturaCierre = Unidad.genericDAL.Get((int)produccionPaquetesCajasViewModel.AperturaCierreID);
                }

                //-------------------------------Para añadir la lista de pedidos--------------------------------------------
                using (UnidadDeTrabajo<Pedidos> Unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                {
                    produccionPaquetesCajasViewModel.PedidoCompleto = Unidad.genericDAL.Get((int)aperturaCierre.PedidoID);
                }
                //-------------------------------Para añadir la lista de pedidos--------------------------------------------

                //-------------------------------Para añadir la lista de usuarios--------------------------------------------
                using (UnidadDeTrabajo<Usuarios> Unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                {
                    produccionPaquetesCajasViewModel.Usuario = Unidad.genericDAL.Get((int)produccionPaquetesCajasViewModel.UsuarioID);
                }
                //-------------------------------Para añadir la lista de usuarios--------------------------------------------



                lista.Add(produccionPaquetesCajasViewModel);
            }
            //Mostrar mensajes

            return View(lista);
        }

        public ActionResult Create(int? pedido)
        {
            try
            {
                ProduccionPaquetesCajasViewModel produccion = new ProduccionPaquetesCajasViewModel { };
                produccion.Pedido = pedido.ToString();

                //-------------------------------Para añadir la lista de pedidos--------------------------------------------
                List<AperturaCierreActivos> aperturaCierres;
                List<PedidosActivos> pedidos;
                List<PedidosActivos> pedidosAbiertos = new List<PedidosActivos>();
                using (UnidadDeTrabajo<AperturaCierreActivos> Unidad = new UnidadDeTrabajo<AperturaCierreActivos>(new BDContext()))
                {
                    aperturaCierres = Unidad.genericDAL.GetAll().ToList();
                }

                using (UnidadDeTrabajo<PedidosActivos> Unidad = new UnidadDeTrabajo<PedidosActivos>(new BDContext()))
                {
                    pedidos = Unidad.genericDAL.GetAll().ToList();
                    
                }

                foreach (var apertura in aperturaCierres)
                {

                    foreach (var item in pedidos)
                    {

                        if(apertura.PedidoID == item.PedidoID)
                        {
                            pedidosAbiertos.Add(item);
                        }

                    }

                    produccion.PedidosActivos = pedidosAbiertos;
                }
                //-------------------------------Para añadir la lista de pedidos--------------------------------------------



                return View(produccion);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Create(ProduccionPaquetesCajasViewModel produccionPaquetesCajasViewModel)
        {
            //Para obtener los datos del pedido que se esta reportando
            Pedidos pedidoActual = null;

            try
            {
                bool flag = false;
               
                if (ModelState.IsValid)
                {
                    produccionPaquetesCajasViewModel.Fecha = DateTime.Now;
                    List<AperturaCierreActivos> aperturaCierres;
                    using (UnidadDeTrabajo<AperturaCierreActivos> Unidad = new UnidadDeTrabajo<AperturaCierreActivos>(new BDContext()))
                    {
                        aperturaCierres = Unidad.genericDAL.GetAll().ToList();
                    }
                    List<Pedidos> pedidos;
                    using (UnidadDeTrabajo<Pedidos> unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                    {
                        pedidos = unidad.genericDAL.GetAll().ToList();
                    }
                        foreach (var pedido in pedidos)
                        {
                            if (produccionPaquetesCajasViewModel.Pedido == pedido.NumeroPedido)
                                {
                                    foreach (var apertura in aperturaCierres)
                                    {
                                        if (apertura.PedidoID == pedido.PedidoID && apertura.Turno.Equals(produccionPaquetesCajasViewModel.Turno) ) 
                                        {
                                                    produccionPaquetesCajasViewModel.AperturaCierreID = (int)apertura.AperturaCierreID;
                                                    produccionPaquetesCajasViewModel.UsuarioID = (int)Session["userID"];
                                                    produccionPaquetesCajasViewModel.Turno = apertura.Turno;
                                                    produccionPaquetesCajasViewModel.Estado = true;
                                                    flag = true;

                                            //Para actualizar la produccion
                                            List<Produccion> produccion;
                                                using (UnidadDeTrabajo<Produccion> unidad = new UnidadDeTrabajo<Produccion>(new BDContext()))
                                                {
                                                    produccion = unidad.genericDAL.GetAll().ToList();

                                                    foreach (var prod in produccion)
                                                    {
                                                        if (prod.PedidoID == apertura.PedidoID)
                                                        {
                                                            prod.FechaFin = produccionPaquetesCajasViewModel.Fecha;
                                                            prod.CantidadProduccionTotal = prod.CantidadProduccionTotal + produccionPaquetesCajasViewModel.Cantidad;
                                                            prod.CantidadPaquetesCajas = prod.CantidadPaquetesCajas + 1;
                                                            prod.HorasTrabajadas = produccionPaquetesCajasViewModel.Fecha.Subtract(prod.FechaInicio).ToString();
                                              
                                                            unidad.genericDAL.Update(prod);
                                                            unidad.Complete();

                                                        }
                                                    }
                                                }

                                    pedidoActual = pedido;
                                    break;
                                        }
                                    }
                                }
                        }
                    
                    if (flag)
                    {
                        ProduccionPaquetesCajas produccion = Convertir(produccionPaquetesCajasViewModel);

                        using (UnidadDeTrabajo<ProduccionPaquetesCajas> unidad = new UnidadDeTrabajo<ProduccionPaquetesCajas>(new BDContext()))
                        {
                            unidad.genericDAL.Add(produccion);
                            unidad.Complete();
                        }

                        float produccionActual = 0; 
                        using (UnidadDeTrabajo<ProduccionActivos> unidad = new UnidadDeTrabajo<ProduccionActivos>(new BDContext()))
                        {
                            foreach (var item in unidad.genericDAL.GetAll().ToList())
                            {
                                if(item.PedidoID == pedidoActual.PedidoID)
                                {
                                    produccionActual = (float)item.CantidadProduccionTotal;
                                }
                            }
                        }
                        

                           /* BLLUtilitarios.msj = "Se ha agregado un nuevo reporte de producto 1";*/

                        TempData["TipoMensaje"] = "alert alert-success";
                        TempData["Mensaje"] = "Reporte con exito, el pedido: "+produccionPaquetesCajasViewModel.Pedido+" le quedan "+BLLUtilitarios.resta(pedidoActual.Cantidad, produccionActual) ;
                        return RedirectToAction("Create", "ProduccionPaquetesCajas", new {id = pedidoActual.PedidoID});
                    }
                    else
                    {
                        TempData["TipoMensaje"] = "alert alert-danger";
                        TempData["Mensaje"] = "No se pudo agregar el reporte, verifique que el turno este abierto o que sea el turno adecuado";
                        return RedirectToAction("Create");
                    }

                    
                   
                }
                return View("Create", pedidoActual.PedidoID);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al agregar reporte";
                return RedirectToAction("Create");
            }
        }

        public ActionResult Delete(int? id)
        {
            ProduccionPaquetesCajas produccion;

            using (UnidadDeTrabajo<ProduccionPaquetesCajas> unidad = new UnidadDeTrabajo<ProduccionPaquetesCajas>(new BDContext()))
            {
                produccion = unidad.genericDAL.Get((int)id);
                if (!produccion.Estado)
                {
                    return RedirectToAction("Index");
                    //No se pudo cerrar 
                }
            }
            TempData["TipoMensaje"] = "alert alert-success";
            TempData["Mensaje"] = "Reporte eliminado";
            ProduccionPaquetesCajasViewModel produccionViewModel = Convertir(produccion);
            Edit(produccionViewModel);
            return RedirectToAction("Index");
        }

        public void Edit(ProduccionPaquetesCajasViewModel produccionCierreViewModel)
        {
            List<AperturaCierreActivos> aperturaCierres;
                    using (UnidadDeTrabajo<AperturaCierreActivos> Unidad = new UnidadDeTrabajo<AperturaCierreActivos>(new BDContext()))
                    {
                        aperturaCierres = Unidad.genericDAL.GetAll().ToList();
                    }
            List<Produccion> produccion;
            foreach (var apertura in aperturaCierres)
            {

                using (UnidadDeTrabajo<Produccion> unidad = new UnidadDeTrabajo<Produccion>(new BDContext()))
                {
                    produccion = unidad.genericDAL.GetAll().ToList();

                    foreach (var prod in produccion)
                    {
                        if (prod.PedidoID == apertura.PedidoID)
                        {
                            prod.CantidadProduccionTotal = prod.CantidadProduccionTotal - produccionCierreViewModel.Cantidad;
                            prod.CantidadPaquetesCajas = prod.CantidadPaquetesCajas - 1;

                            unidad.genericDAL.Update(prod);
                            unidad.Complete();

                        }
                    }
                }
            }

            //Para actualizar la produccion
            produccionCierreViewModel.Estado = false;

            using (UnidadDeTrabajo<ProduccionPaquetesCajas> unidad = new UnidadDeTrabajo<ProduccionPaquetesCajas>(new BDContext()))
            {
                unidad.genericDAL.Update(Convertir(produccionCierreViewModel));
                unidad.Complete();
            }
            TempData["TipoMensaje"] = "alert alert-success";
            TempData["Mensaje"] = "Reporte eliminado";

        }

        public ActionResult Etiqueta(int? etiqueta)
        {
            Session["Numeroetiqueta"] = etiqueta;
            return View();
        }


        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}
