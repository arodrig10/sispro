﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;
using Microsoft.Ajax.Utilities;

namespace Frontend.Controllers
{
    [Authorize]
    public class VariablesMaquinaSopladoraController : Controller
    {
        private VariablesMaquinaSopladoraViewModel Convertir(VariablesMaquinaSopladoraActivos variablesMaquinaSopladora)
        {
            return new VariablesMaquinaSopladoraViewModel
            {
                VariablesMaquinaSopladoraID = variablesMaquinaSopladora.VariablesMaquinaSopladoraID,
                MaquinaID = variablesMaquinaSopladora.MaquinaID,
                ProductoID = variablesMaquinaSopladora.ProductoID,
                MateriaPrimaID = variablesMaquinaSopladora.MateriaPrimaID,
                RetardoPresoplado = variablesMaquinaSopladora.RetardoPresoplado,
                RetardoSoplado = variablesMaquinaSopladora.RetardoSoplado,
                TiempoPresoplado = variablesMaquinaSopladora.TiempoPresoplado,
                TiempoRecuperacion = variablesMaquinaSopladora.TiempoRecuperacion,
                TiempoSoplado = variablesMaquinaSopladora.TiempoSoplado,
                TiempoEscape = variablesMaquinaSopladora.TiempoEscape,
                TiempoParoCadena = variablesMaquinaSopladora.TiempoParoCadena,
                TiempoSalidaPinzaPreforma = variablesMaquinaSopladora.TiempoSalidaPinzaPreforma,
                FrecuenciaVentilador = variablesMaquinaSopladora.FrecuenciaVentilador,
                PresionEstirado = variablesMaquinaSopladora.PresionEstirado,
                Gancho = variablesMaquinaSopladora.Gancho,
 
                Relay = variablesMaquinaSopladora.Relay,
   
                AnillosTornelas = variablesMaquinaSopladora.AnillosTornelas,

                Estado = variablesMaquinaSopladora.Estado,
                Notas = variablesMaquinaSopladora.Notas,
                CantidadBancos = variablesMaquinaSopladora.CantidadBancos,
                CantidadZonas = variablesMaquinaSopladora.CantidadZonas,
                PosicionBanco1Zona1 = variablesMaquinaSopladora.PosicionBanco1Zona1,
                PosicionBanco1Zona2 = variablesMaquinaSopladora.PosicionBanco1Zona2,
                PosicionBanco1Zona3 = variablesMaquinaSopladora.PosicionBanco1Zona3,
                PosicionBanco1Zona4 = variablesMaquinaSopladora.PosicionBanco1Zona4,
                PosicionBanco2Zona1 = variablesMaquinaSopladora.PosicionBanco2Zona1,
                PosicionBanco2Zona2 = variablesMaquinaSopladora.PosicionBanco2Zona2,
                PosicionBanco2Zona3 = variablesMaquinaSopladora.PosicionBanco2Zona3,
                PosicionBanco2Zona4 = variablesMaquinaSopladora.PosicionBanco2Zona4,
                ValorBanco1Zona1 = variablesMaquinaSopladora.ValorBanco1Zona1,
                ValorBanco1Zona2 = variablesMaquinaSopladora.ValorBanco1Zona2,
                ValorBanco1Zona3 = variablesMaquinaSopladora.ValorBanco1Zona3,
                ValorBanco1Zona4 = variablesMaquinaSopladora.ValorBanco1Zona4,
                ValorBanco2Zona1 = variablesMaquinaSopladora.ValorBanco2Zona1,
                ValorBanco2Zona2 = variablesMaquinaSopladora.ValorBanco2Zona2,
                ValorBanco2Zona3 = variablesMaquinaSopladora.ValorBanco2Zona3,
                ValorBanco2Zona4 = variablesMaquinaSopladora.ValorBanco2Zona4
            };
        }

        private VariablesMaquinaSopladoraViewModel Convertir(VariablesMaquinaSopladora variablesMaquinaSopladora)
        {
            return new VariablesMaquinaSopladoraViewModel
            {
                VariablesMaquinaSopladoraID = variablesMaquinaSopladora.VariablesMaquinaSopladoraID,
                MaquinaID = variablesMaquinaSopladora.MaquinaID,
                ProductoID = variablesMaquinaSopladora.ProductoID,
                MateriaPrimaID = variablesMaquinaSopladora.MateriaPrimaID,
                RetardoPresoplado = variablesMaquinaSopladora.RetardoPresoplado,
                RetardoSoplado = variablesMaquinaSopladora.RetardoSoplado,
                TiempoPresoplado = variablesMaquinaSopladora.TiempoPresoplado,
                TiempoRecuperacion = variablesMaquinaSopladora.TiempoRecuperacion,
                TiempoSoplado = variablesMaquinaSopladora.TiempoSoplado,
                TiempoEscape = variablesMaquinaSopladora.TiempoEscape,
                TiempoParoCadena = variablesMaquinaSopladora.TiempoParoCadena,
                TiempoSalidaPinzaPreforma = variablesMaquinaSopladora.TiempoSalidaPinzaPreforma,
                FrecuenciaVentilador = variablesMaquinaSopladora.FrecuenciaVentilador,
                PresionEstirado = variablesMaquinaSopladora.PresionEstirado,
                Gancho = variablesMaquinaSopladora.Gancho,
                Relay = variablesMaquinaSopladora.Relay,
                AnillosTornelas = variablesMaquinaSopladora.AnillosTornelas,
                Estado = variablesMaquinaSopladora.Estado,
                Notas = variablesMaquinaSopladora.Notas,
                CantidadBancos = variablesMaquinaSopladora.CantidadBancos,
                CantidadZonas = variablesMaquinaSopladora.CantidadZonas,
                PosicionBanco1Zona1 = variablesMaquinaSopladora.PosicionBanco1Zona1,
                PosicionBanco1Zona2 = variablesMaquinaSopladora.PosicionBanco1Zona2,
                PosicionBanco1Zona3 = variablesMaquinaSopladora.PosicionBanco1Zona3,
                PosicionBanco1Zona4 = variablesMaquinaSopladora.PosicionBanco1Zona4,
                PosicionBanco2Zona1 = variablesMaquinaSopladora.PosicionBanco2Zona1,
                PosicionBanco2Zona2 = variablesMaquinaSopladora.PosicionBanco2Zona2,
                PosicionBanco2Zona3 = variablesMaquinaSopladora.PosicionBanco2Zona3,
                PosicionBanco2Zona4 = variablesMaquinaSopladora.PosicionBanco2Zona4,
                ValorBanco1Zona1 = variablesMaquinaSopladora.ValorBanco1Zona1,
                ValorBanco1Zona2 = variablesMaquinaSopladora.ValorBanco1Zona2,
                ValorBanco1Zona3 = variablesMaquinaSopladora.ValorBanco1Zona3,
                ValorBanco1Zona4 = variablesMaquinaSopladora.ValorBanco1Zona4,
                ValorBanco2Zona1 = variablesMaquinaSopladora.ValorBanco2Zona1,
                ValorBanco2Zona2 = variablesMaquinaSopladora.ValorBanco2Zona2,
                ValorBanco2Zona3 = variablesMaquinaSopladora.ValorBanco2Zona3,
                ValorBanco2Zona4 = variablesMaquinaSopladora.ValorBanco2Zona4
            };
        }
        private VariablesMaquinaSopladora Convertir(VariablesMaquinaSopladoraViewModel variablesMaquinaSopladoraViewModel)
        {
            return new VariablesMaquinaSopladora
            {
                VariablesMaquinaSopladoraID = variablesMaquinaSopladoraViewModel.VariablesMaquinaSopladoraID,
                MaquinaID = variablesMaquinaSopladoraViewModel.MaquinaID,
                ProductoID = variablesMaquinaSopladoraViewModel.ProductoID,
                MateriaPrimaID = variablesMaquinaSopladoraViewModel.MateriaPrimaID,
                RetardoPresoplado = variablesMaquinaSopladoraViewModel.RetardoPresoplado,
                RetardoSoplado = variablesMaquinaSopladoraViewModel.RetardoSoplado,
                TiempoPresoplado = variablesMaquinaSopladoraViewModel.TiempoPresoplado,
                TiempoRecuperacion = variablesMaquinaSopladoraViewModel.TiempoRecuperacion,
                TiempoSoplado = variablesMaquinaSopladoraViewModel.TiempoSoplado,
                TiempoEscape = variablesMaquinaSopladoraViewModel.TiempoEscape,
                TiempoParoCadena = variablesMaquinaSopladoraViewModel.TiempoParoCadena,
                TiempoSalidaPinzaPreforma = variablesMaquinaSopladoraViewModel.TiempoSalidaPinzaPreforma,
                FrecuenciaVentilador = variablesMaquinaSopladoraViewModel.FrecuenciaVentilador,
                PresionEstirado = variablesMaquinaSopladoraViewModel.PresionEstirado,
                Gancho = variablesMaquinaSopladoraViewModel.Gancho,
                Relay = variablesMaquinaSopladoraViewModel.Relay,
                AnillosTornelas = variablesMaquinaSopladoraViewModel.AnillosTornelas,
                Estado = variablesMaquinaSopladoraViewModel.Estado,
                Notas = variablesMaquinaSopladoraViewModel.Notas,
                CantidadBancos = variablesMaquinaSopladoraViewModel.CantidadBancos,
                CantidadZonas = variablesMaquinaSopladoraViewModel.CantidadZonas,
                PosicionBanco1Zona1 = variablesMaquinaSopladoraViewModel.PosicionBanco1Zona1,
                PosicionBanco1Zona2 = variablesMaquinaSopladoraViewModel.PosicionBanco1Zona2,
                PosicionBanco1Zona3 = variablesMaquinaSopladoraViewModel.PosicionBanco1Zona3,
                PosicionBanco1Zona4 = variablesMaquinaSopladoraViewModel.PosicionBanco1Zona4,
                PosicionBanco2Zona1 = variablesMaquinaSopladoraViewModel.PosicionBanco2Zona1,
                PosicionBanco2Zona2 = variablesMaquinaSopladoraViewModel.PosicionBanco2Zona2,
                PosicionBanco2Zona3 = variablesMaquinaSopladoraViewModel.PosicionBanco2Zona3,
                PosicionBanco2Zona4 = variablesMaquinaSopladoraViewModel.PosicionBanco2Zona4,
                ValorBanco1Zona1 = variablesMaquinaSopladoraViewModel.ValorBanco1Zona1,
                ValorBanco1Zona2 = variablesMaquinaSopladoraViewModel.ValorBanco1Zona2,
                ValorBanco1Zona3 = variablesMaquinaSopladoraViewModel.ValorBanco1Zona3,
                ValorBanco1Zona4 = variablesMaquinaSopladoraViewModel.ValorBanco1Zona4,
                ValorBanco2Zona1 = variablesMaquinaSopladoraViewModel.ValorBanco2Zona1,
                ValorBanco2Zona2 = variablesMaquinaSopladoraViewModel.ValorBanco2Zona2,
                ValorBanco2Zona3 = variablesMaquinaSopladoraViewModel.ValorBanco2Zona3,
                ValorBanco2Zona4 = variablesMaquinaSopladoraViewModel.ValorBanco2Zona4
            };
        }
        public ActionResult Index()
        {
            try
            {
                List<VariablesMaquinaSopladoraActivos> variablesMaquinaSopladoras;
                using (UnidadDeTrabajo<VariablesMaquinaSopladoraActivos> Unidad = new UnidadDeTrabajo<VariablesMaquinaSopladoraActivos>(new BDContext()))
                {
                    variablesMaquinaSopladoras = Unidad.genericDAL.GetAll().ToList();
                }
                List<VariablesMaquinaSopladoraViewModel> lista = new List<VariablesMaquinaSopladoraViewModel>();
                VariablesMaquinaSopladoraViewModel variablesMaquinaSopladoraViewModel;
                foreach (var item in variablesMaquinaSopladoras)
                {
                    variablesMaquinaSopladoraViewModel = Convertir(item);
                    using (UnidadDeTrabajo<Maquinas> Unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
                    {
                        variablesMaquinaSopladoraViewModel.Maquina = Unidad.genericDAL.Get(variablesMaquinaSopladoraViewModel.MaquinaID);
                    }
                    using (UnidadDeTrabajo<MateriaPrima> Unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                    {
                        variablesMaquinaSopladoraViewModel.MateriaPrima = Unidad.genericDAL.Get(variablesMaquinaSopladoraViewModel.MateriaPrimaID);
                    }
                    using (UnidadDeTrabajo<Productos> Unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        variablesMaquinaSopladoraViewModel.Producto = Unidad.genericDAL.Get(variablesMaquinaSopladoraViewModel.ProductoID);
                    }
                    lista.Add(variablesMaquinaSopladoraViewModel);
                }
                //Mostrar mensajes
                ViewBag.Message = BLLUtilitarios.msj;
                BLLUtilitarios.msj = null;
                return View(lista);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener lista";
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult Create()
        {
            try
            {
                VariablesMaquinaSopladoraViewModel variablesMaquinaSopladoraViewModel = new VariablesMaquinaSopladoraViewModel { };
                using (UnidadDeTrabajo<MaquinasSopladoraActivos> unidad = new UnidadDeTrabajo<MaquinasSopladoraActivos>(new BDContext()))
                {
                    variablesMaquinaSopladoraViewModel.Maquinas = unidad.genericDAL.GetAll().ToList();
                }
                using (UnidadDeTrabajo<MateriaPrimaSopladoraActivos> unidad = new UnidadDeTrabajo<MateriaPrimaSopladoraActivos>(new BDContext()))
                {
                    variablesMaquinaSopladoraViewModel.MateriaPrimas = unidad.genericDAL.GetAll().ToList();
                }
                using (UnidadDeTrabajo<ProductosBotellaActivos> unidad = new UnidadDeTrabajo<ProductosBotellaActivos>(new BDContext()))
                {
                    variablesMaquinaSopladoraViewModel.Productos = unidad.genericDAL.GetAll().ToList();
                }
                return View(variablesMaquinaSopladoraViewModel);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Create(VariablesMaquinaSopladoraViewModel variablesMaquinaSopladoraViewModel)
        {
            try
            {
                bool a;
                variablesMaquinaSopladoraViewModel.Estado = true;
                VariablesMaquinaSopladora variablesMaquinaSopladora = Convertir(variablesMaquinaSopladoraViewModel);
                using (UnidadDeTrabajo<VariablesMaquinaSopladora> unidad = new UnidadDeTrabajo<VariablesMaquinaSopladora>(new BDContext()))
                {
                    unidad.genericDAL.Add(variablesMaquinaSopladora);
                    a = unidad.Complete();
                }
                //Mostrar El mensaje 
               /* if (a)
                {
                    BLLUtilitarios.msj = "Se ha agregado las variables de la maquina sopladora 1";
                }
                else
                {

                    BLLUtilitarios.msj = "Error al agregar variables de maquina sopladora en la base de datos";
                }*/

                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Ficha de variables creada";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al crear variables de maquina";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    VariablesMaquinaSopladora variablesMaquinaSopladora;
                    using (UnidadDeTrabajo<VariablesMaquinaSopladora> unidad = new UnidadDeTrabajo<VariablesMaquinaSopladora>(new BDContext()))
                    {
                        variablesMaquinaSopladora = unidad.genericDAL.Get((int)id);
                        if (!variablesMaquinaSopladora.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    VariablesMaquinaSopladoraViewModel variablesMaquinaSopladoraViewModel = Convertir(variablesMaquinaSopladora);
                    List<ProductosBotellaActivos> productos;
                    using (UnidadDeTrabajo<ProductosBotellaActivos> unidad = new UnidadDeTrabajo<ProductosBotellaActivos>(new BDContext()))
                    {
                        productos = unidad.genericDAL.GetAll().ToList();
                    }
                    List<MaquinasSopladoraActivos> maquinas;
                    using (UnidadDeTrabajo<MaquinasSopladoraActivos> unidad = new UnidadDeTrabajo<MaquinasSopladoraActivos>(new BDContext()))
                    {
                        maquinas = unidad.genericDAL.GetAll().ToList();
                    }
                    List<MateriaPrimaSopladoraActivos> materiaPrimas;
                    using (UnidadDeTrabajo<MateriaPrimaSopladoraActivos> unidad = new UnidadDeTrabajo<MateriaPrimaSopladoraActivos>(new BDContext()))
                    {
                        materiaPrimas = unidad.genericDAL.GetAll().ToList();
                    }
                    variablesMaquinaSopladoraViewModel.Productos = productos;
                    variablesMaquinaSopladoraViewModel.MateriaPrimas = materiaPrimas;
                    variablesMaquinaSopladoraViewModel.Maquinas = maquinas;
                    return View(variablesMaquinaSopladoraViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Edit(VariablesMaquinaSopladoraViewModel variablesMaquinaSopladoraViewModel)
        {
            try
            {
                variablesMaquinaSopladoraViewModel.Estado = true;
                using (UnidadDeTrabajo<VariablesMaquinaSopladora> unidad = new UnidadDeTrabajo<VariablesMaquinaSopladora>(new BDContext()))
                {
                    unidad.genericDAL.Update(Convertir(variablesMaquinaSopladoraViewModel));
                    unidad.Complete();
                }
                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Ficha de variables modificada";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al editar variables de maquina";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Details(int? id)
        {
            try
            {
                if (id != null)
                {
                    VariablesMaquinaSopladora variablesMaquinaSopladora;
                    using (UnidadDeTrabajo<VariablesMaquinaSopladora> unidad = new UnidadDeTrabajo<VariablesMaquinaSopladora>(new BDContext()))
                    {
                        variablesMaquinaSopladora = unidad.genericDAL.Get((int)id);
                        if (!variablesMaquinaSopladora.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                        
                    }
                    VariablesMaquinaSopladoraViewModel variablesMaquinaSopladoraViewModel = Convertir(variablesMaquinaSopladora);
                    variablesMaquinaSopladoraViewModel.GanchoMensaje = variablesMaquinaSopladoraViewModel.Gancho ? "Aplica" : "No aplica";
                    variablesMaquinaSopladoraViewModel.RelayMensaje = variablesMaquinaSopladoraViewModel.Relay ? "Aplica" : "No aplica";
                    variablesMaquinaSopladoraViewModel.AnillosTornelasMensaje = variablesMaquinaSopladoraViewModel.AnillosTornelas ? "Aplica" : "No aplica";
                    using (UnidadDeTrabajo<Maquinas> Unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
                    {
                        variablesMaquinaSopladoraViewModel.Maquina = Unidad.genericDAL.Get(variablesMaquinaSopladoraViewModel.MaquinaID);
                    }
                    using (UnidadDeTrabajo<MateriaPrima> Unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                    {
                        variablesMaquinaSopladoraViewModel.MateriaPrima = Unidad.genericDAL.Get(variablesMaquinaSopladoraViewModel.MateriaPrimaID);
                    }
                    using (UnidadDeTrabajo<Productos> Unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        variablesMaquinaSopladoraViewModel.Producto = Unidad.genericDAL.Get(variablesMaquinaSopladoraViewModel.ProductoID);
                    }
                    return View(variablesMaquinaSopladoraViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver detalles";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    VariablesMaquinaSopladora variablesMaquinaSopladora;
                    using (UnidadDeTrabajo<VariablesMaquinaSopladora> unidad = new UnidadDeTrabajo<VariablesMaquinaSopladora>(new BDContext()))
                    {
                        variablesMaquinaSopladora = unidad.genericDAL.Get((int)id);
                    }
                    VariablesMaquinaSopladoraViewModel variablesMaquinaSopladoraViewModel = Convertir(variablesMaquinaSopladora);
                    using (UnidadDeTrabajo<Maquinas> Unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
                    {
                        variablesMaquinaSopladoraViewModel.Maquina = Unidad.genericDAL.Get(variablesMaquinaSopladoraViewModel.MaquinaID);
                    }
                    using (UnidadDeTrabajo<MateriaPrima> Unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                    {
                        variablesMaquinaSopladoraViewModel.MateriaPrima = Unidad.genericDAL.Get(variablesMaquinaSopladoraViewModel.MateriaPrimaID);
                    }
                    using (UnidadDeTrabajo<Productos> Unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        variablesMaquinaSopladoraViewModel.Producto = Unidad.genericDAL.Get(variablesMaquinaSopladoraViewModel.ProductoID);
                    }
                    return View(variablesMaquinaSopladoraViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Delete(VariablesMaquinaSopladoraViewModel variablesMaquinaSopladoraViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<VariablesMaquinaSopladora> unidad = new UnidadDeTrabajo<VariablesMaquinaSopladora>(new BDContext()))
                {
                    VariablesMaquinaSopladora variablesMaquinaSopladora = unidad.genericDAL.Get(variablesMaquinaSopladoraViewModel.VariablesMaquinaSopladoraID);
                    variablesMaquinaSopladora.Estado = false;
                    unidad.genericDAL.Update(variablesMaquinaSopladora);
                    unidad.Complete();
                }
                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Ficha de variables eliminada";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al eliminar variables de maquina";
                return RedirectToAction("Index");
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}
