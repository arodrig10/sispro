﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;

namespace Frontend.Controllers
{
    [Authorize]
    public class ProductosController : Controller
    {
        private ProductosViewModel Convertir(ProductosActivos producto)
        {
            return new ProductosViewModel
            {
                ProductoID = producto.ProductoID,
                CodigoProducto = producto.CodigoProducto,
                Nombre = producto.Nombre,
                TipoProductoID = producto.TipoProductoID,
                Descripcion = producto.Descripcion,
                Imagen = producto.Imagen,
                Estado = producto.Estado
            };
        }
        private ProductosViewModel Convertir(Productos producto)
        {
            return new ProductosViewModel
            {
                ProductoID = producto.ProductoID,
                CodigoProducto = producto.CodigoProducto,
                Nombre = producto.Nombre,
                TipoProductoID = producto.TipoProductoID,
                Descripcion = producto.Descripcion,
                Imagen = producto.Imagen,
                Estado = producto.Estado
            };
        }
        private Productos Convertir(ProductosViewModel productoViewModel)
        {
            return new Productos
            {
                ProductoID = productoViewModel.ProductoID,
                CodigoProducto = productoViewModel.CodigoProducto,
                Nombre = productoViewModel.Nombre,
                TipoProductoID = productoViewModel.TipoProductoID,
                Descripcion = productoViewModel.Descripcion,
                Imagen = productoViewModel.Imagen,
                Estado = productoViewModel.Estado
            };
        }
        public ActionResult Index()
        {
            try
            {
                List<ProductosActivos> productos;
                using (UnidadDeTrabajo<ProductosActivos> Unidad = new UnidadDeTrabajo<ProductosActivos>(new BDContext()))
                {
                    productos = Unidad.genericDAL.GetAll().ToList();
                }
                List<ProductosViewModel> lista = new List<ProductosViewModel>();
                ProductosViewModel productoViewModel;
                foreach (var item in productos)
                {
                    productoViewModel = Convertir(item);

                    using (UnidadDeTrabajo<TipoProducto> Unidad = new UnidadDeTrabajo<TipoProducto>(new BDContext()))
                    {
                        productoViewModel.TipoProducto = Unidad.genericDAL.Get(productoViewModel.TipoProductoID);
                    }
                    lista.Add(productoViewModel);
                }
                //Mostrar mensajes
                ViewBag.Message = BLLUtilitarios.msj;
                BLLUtilitarios.msj = null;
                return View(lista);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener lista";
                return RedirectToAction("Index", "Home");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            try
            {
                ProductosViewModel productos = new ProductosViewModel { };
                using (UnidadDeTrabajo<TipoProducto> unidad = new UnidadDeTrabajo<TipoProducto>(new BDContext()))
                {
                    productos.TipoProductos = unidad.genericDAL.GetAll().ToList();
                }
                return View(productos);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Create(ProductosViewModel productoViewModel)
        {
            try
            {
                bool a;
                productoViewModel.Estado = true;

                using (BDContext context = new BDContext())
                {
                    if (context.Productos.Where(c => c.CodigoProducto == productoViewModel.CodigoProducto).Count() > 0)
                    {
                        TempData["TipoMensaje"] = "alert alert-danger";
                        TempData["MensajeError"] = "Codigo de producto ya existe";
                        using (UnidadDeTrabajo<TipoProducto> unidad = new UnidadDeTrabajo<TipoProducto>(new BDContext()))
                        {
                            productoViewModel.TipoProductos = unidad.genericDAL.GetAll().ToList();
                        }
                        return View("Create", productoViewModel);
                    }
                }


                if (productoViewModel.ArchivoImagen != null)
                {
                    string name = Path.GetFileNameWithoutExtension(productoViewModel.ArchivoImagen.FileName);
                    string extension = Path.GetExtension(productoViewModel.ArchivoImagen.FileName);
                    name = name + DateTime.Now.ToString("yyyymmssfff") + extension;
                    productoViewModel.Imagen = "~/Archivos/Productos/" + name;
                    name = Path.Combine(Server.MapPath("~/Archivos/Productos/"), name);
                    productoViewModel.ArchivoImagen.SaveAs(name);
                }



                Productos producto = Convertir(productoViewModel);

                using (UnidadDeTrabajo<Productos> unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                {
                    unidad.genericDAL.Add(producto);
                    a = unidad.Complete();
                }
                //Mostrar El mensaje 
                /* if (a)
                 {
                     BLLUtilitarios.msj = "Se ha agregado un nuevo producto 1";
                 }
                 else
                 {

                     BLLUtilitarios.msj = "Error al agregar producto en la base de datos";
                 }*/
                 TempData["TipoMensaje"] = "alert alert-success";
                 TempData["Mensaje"] = "Product creado: " + productoViewModel.CodigoProducto;

               return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al crear producto: " + ex.Message;
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    Productos producto;
                    using (UnidadDeTrabajo<Productos> unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        producto = unidad.genericDAL.Get((int)id);
                        if (!producto.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    ProductosViewModel productos = Convertir(producto);

                    TipoProducto tipoProducto;
                    List<TipoProducto> tipoProductos;
                    using (UnidadDeTrabajo<TipoProducto> unidad = new UnidadDeTrabajo<TipoProducto>(new BDContext()))
                    {
                        tipoProductos = unidad.genericDAL.GetAll().ToList();
                        tipoProducto = unidad.genericDAL.Get(productos.TipoProductoID);
                    }
                    productos.TipoProductos = tipoProductos;
                    return View(productos);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Edit(ProductosViewModel productoViewModel)
        {
            try
            {
                productoViewModel.Estado = true;
                if (productoViewModel.ArchivoImagen != null)
                {
                    string name = Path.GetFileNameWithoutExtension(productoViewModel.ArchivoImagen.FileName);
                    string extension = Path.GetExtension(productoViewModel.ArchivoImagen.FileName);
                    name = name + DateTime.Now.ToString("yyyymmssfff") + extension;
                    productoViewModel.Imagen = "~/Archivos/Productos/" + name;
                    name = Path.Combine(Server.MapPath("~/Archivos/Productos/"), name);
                    productoViewModel.ArchivoImagen.SaveAs(name);
                }

                using (UnidadDeTrabajo<Productos> unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                {
                    unidad.genericDAL.Update(Convertir(productoViewModel));
                    unidad.Complete();
                }
                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Product editado: " + productoViewModel.CodigoProducto;
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al editar producto";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Details(int? id)
        {
            try
            {
                if (id != null)
                {
                    Productos producto;
                    using (UnidadDeTrabajo<Productos> unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        producto = unidad.genericDAL.Get((int)id);
                        if (!producto.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    ProductosViewModel productoViewModel = Convertir(producto);
                    using (UnidadDeTrabajo<TipoProducto> unidad = new UnidadDeTrabajo<TipoProducto>(new BDContext()))
                    {
                        productoViewModel.TipoProducto = unidad.genericDAL.Get(productoViewModel.TipoProductoID);
                    };
                    return View(productoViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver detalles";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    Productos producto;
                    using (UnidadDeTrabajo<Productos> unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        producto = unidad.genericDAL.Get((int)id);
                        if (!producto.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    ProductosViewModel productoViewModel = Convertir(producto);
                    using (UnidadDeTrabajo<TipoProducto> unidad = new UnidadDeTrabajo<TipoProducto>(new BDContext()))
                    {
                        productoViewModel.TipoProducto = unidad.genericDAL.Get(producto.TipoProductoID);
                    }
                    return View(productoViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Delete(ProductosViewModel productoViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<Productos> unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                {
                    Productos productos = unidad.genericDAL.Get(productoViewModel.ProductoID);
                    productos.Estado = false;
                    unidad.genericDAL.Update(productos);
                    unidad.Complete();
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Product eliminado: " + productos.CodigoProducto;
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al eliminar producto";
                return RedirectToAction("Index");
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}
