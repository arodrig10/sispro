﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;

namespace Frontend.Controllers
{
    [Authorize]
    public class MateriaPrimaController : Controller
    {
        private MateriaPrimaViewModel Convertir(MateriaPrimaActivos materiaPrima)
        {
            return new MateriaPrimaViewModel
            {
                MateriaPrimaID = materiaPrima.MateriaPrimaID,
                CodigoMateriaPrima = materiaPrima.CodigoMateriaPrima,
                Nombre = materiaPrima.Nombre,
                ProveedorID = materiaPrima.ProveedorID,
                TipoMateriaPrimaID = materiaPrima.TipoMateriaPrimaID,
                UnidadMedidaID = materiaPrima.UnidadMedidaID,
                Cantidad = materiaPrima.Cantidad,
                PrecioUnitario = materiaPrima.PrecioUnitario,
                PrecioTotal = materiaPrima.PrecioTotal,
                Descripcion = materiaPrima.Descripcion,
                Estado = materiaPrima.Estado
            };
        }
        private MateriaPrimaViewModel Convertir(MateriaPrima materiaPrima)
        {
            return new MateriaPrimaViewModel
            {
                MateriaPrimaID = materiaPrima.MateriaPrimaID,
                CodigoMateriaPrima = materiaPrima.CodigoMateriaPrima,
                Nombre = materiaPrima.Nombre,
                ProveedorID = materiaPrima.ProveedorID,
                TipoMateriaPrimaID = materiaPrima.TipoMateriaPrimaID,
                UnidadMedidaID = materiaPrima.UnidadMedidaID,
                Cantidad = materiaPrima.Cantidad,
                PrecioUnitario = materiaPrima.PrecioUnitario,
                PrecioTotal = materiaPrima.PrecioTotal,
                Descripcion = materiaPrima.Descripcion,
                Estado = materiaPrima.Estado
            };
        }
        private MateriaPrima Convertir(MateriaPrimaViewModel materiaPrimaViewModel)
        {
            return new MateriaPrima
            {
                MateriaPrimaID = materiaPrimaViewModel.MateriaPrimaID,
                CodigoMateriaPrima = materiaPrimaViewModel.CodigoMateriaPrima,
                Nombre = materiaPrimaViewModel.Nombre,
                ProveedorID = materiaPrimaViewModel.ProveedorID,
                TipoMateriaPrimaID = materiaPrimaViewModel.TipoMateriaPrimaID,
                UnidadMedidaID = materiaPrimaViewModel.UnidadMedidaID,
                Cantidad = materiaPrimaViewModel.Cantidad,
                PrecioUnitario = materiaPrimaViewModel.PrecioUnitario,
                PrecioTotal = materiaPrimaViewModel.PrecioTotal,
                Descripcion = materiaPrimaViewModel.Descripcion,
                Estado = materiaPrimaViewModel.Estado
            };
        }
        public ActionResult Index()
        {
            try
            {
                List<MateriaPrimaActivos> materiasPrimas;
                using (UnidadDeTrabajo<MateriaPrimaActivos> Unidad = new UnidadDeTrabajo<MateriaPrimaActivos>(new BDContext()))
                {
                    materiasPrimas = Unidad.genericDAL.GetAll().ToList();
                }
                List<MateriaPrimaViewModel> lista = new List<MateriaPrimaViewModel>();
                MateriaPrimaViewModel materiaPrimaViewModel;
                foreach (var item in materiasPrimas)
                {
                    materiaPrimaViewModel = Convertir(item);
                    using (UnidadDeTrabajo<Proveedor> Unidad = new UnidadDeTrabajo<Proveedor>(new BDContext()))
                    {
                        materiaPrimaViewModel.Proveedor = Unidad.genericDAL.Get(materiaPrimaViewModel.ProveedorID);
                    }
                    using (UnidadDeTrabajo<TipoMateriaPrima> Unidad = new UnidadDeTrabajo<TipoMateriaPrima>(new BDContext()))
                    {
                        materiaPrimaViewModel.TipoMateriaPrima = Unidad.genericDAL.Get(materiaPrimaViewModel.TipoMateriaPrimaID);
                    }
                    using (UnidadDeTrabajo<UnidadMedida> Unidad = new UnidadDeTrabajo<UnidadMedida>(new BDContext()))
                    {
                        materiaPrimaViewModel.UnidadMedida = Unidad.genericDAL.Get(materiaPrimaViewModel.UnidadMedidaID);
                    }
                    lista.Add(materiaPrimaViewModel);
                }
                //Mostrar mensajes
                ViewBag.Message = BLLUtilitarios.msj;
                BLLUtilitarios.msj = null;
                return View(lista);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener lista";
                return RedirectToAction("Index", "Home");
            }
        }
       [Authorize(Roles = "Administrador")]
       public ActionResult Create()
        {
            try
            {
                MateriaPrimaViewModel maquinas = new MateriaPrimaViewModel { };
                using (UnidadDeTrabajo<ProveedorActivos> unidad = new UnidadDeTrabajo<ProveedorActivos>(new BDContext()))
                {
                    maquinas.Proveedores = unidad.genericDAL.GetAll().ToList();
                }
                using (UnidadDeTrabajo<TipoMateriaPrima> unidad = new UnidadDeTrabajo<TipoMateriaPrima>(new BDContext()))
                {
                    maquinas.TipoMateriaPrimas = unidad.genericDAL.GetAll().ToList();
                }
                using (UnidadDeTrabajo<UnidadMedida> unidad = new UnidadDeTrabajo<UnidadMedida>(new BDContext()))
                {
                    maquinas.UnidadMedidas = unidad.genericDAL.GetAll().ToList();
                }
                return View(maquinas);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Create(MateriaPrimaViewModel materiaPrimaViewModel)
        {
            try
            {

                materiaPrimaViewModel.PrecioTotal = materiaPrimaViewModel.PrecioUnitario * materiaPrimaViewModel.Cantidad;
                bool a;
                materiaPrimaViewModel.Estado = true;

                using (BDContext context = new BDContext())
                {
                    if (context.MateriaPrima.Where(c => c.CodigoMateriaPrima == materiaPrimaViewModel.CodigoMateriaPrima).Count() > 0)
                    {
                        TempData["TipoMensaje"] = "alert alert-danger";
                        TempData["MensajeError"] = "Codigo materia ya existe";
                        using (UnidadDeTrabajo<ProveedorActivos> unidad = new UnidadDeTrabajo<ProveedorActivos>(new BDContext()))
                        {
                            materiaPrimaViewModel.Proveedores = unidad.genericDAL.GetAll().ToList();
                        }
                        using (UnidadDeTrabajo<TipoMateriaPrima> unidad = new UnidadDeTrabajo<TipoMateriaPrima>(new BDContext()))
                        {
                            materiaPrimaViewModel.TipoMateriaPrimas = unidad.genericDAL.GetAll().ToList();
                        }
                        using (UnidadDeTrabajo<UnidadMedida> unidad = new UnidadDeTrabajo<UnidadMedida>(new BDContext()))
                        {
                            materiaPrimaViewModel.UnidadMedidas = unidad.genericDAL.GetAll().ToList();
                        }

                        return View("Create", materiaPrimaViewModel);
                    }
                }

                    


                MateriaPrima materiaPrima = Convertir(materiaPrimaViewModel);
                using (UnidadDeTrabajo<MateriaPrima> unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                {
                    unidad.genericDAL.Add(materiaPrima);
                    a = unidad.Complete();
                }
                //Mostrar El mensaje 
              /*  if (a)
                {
                    BLLUtilitarios.msj = "Se ha agregado una nueva materia prima 1";
                }
                else
                {

                    BLLUtilitarios.msj = "Error al agregar materia prima en la base de datos";
                }*/

                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Materia prima creada: " + materiaPrimaViewModel.CodigoMateriaPrima;
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al agregar materia prima";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    MateriaPrima materiaPrima;
                    using (UnidadDeTrabajo<MateriaPrima> unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                    {
                        materiaPrima = unidad.genericDAL.Get((int)id);
                        if (!materiaPrima.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    MateriaPrimaViewModel materiaPrimaViewModel = Convertir(materiaPrima);
                    List<ProveedorActivos> proveedores;
                    using (UnidadDeTrabajo<ProveedorActivos> unidad = new UnidadDeTrabajo<ProveedorActivos>(new BDContext()))
                    {
                        proveedores = unidad.genericDAL.GetAll().ToList();
                    }
                    materiaPrimaViewModel.Proveedores = proveedores;
                    List<TipoMateriaPrima> tipoMateriaPrimas;
                    using (UnidadDeTrabajo<TipoMateriaPrima> unidad = new UnidadDeTrabajo<TipoMateriaPrima>(new BDContext()))
                    {
                        tipoMateriaPrimas = unidad.genericDAL.GetAll().ToList();
                    }
                    materiaPrimaViewModel.TipoMateriaPrimas = tipoMateriaPrimas;
                    List<UnidadMedida> unidadMedidas;
                    using (UnidadDeTrabajo<UnidadMedida> unidad = new UnidadDeTrabajo<UnidadMedida>(new BDContext()))
                    {
                        unidadMedidas = unidad.genericDAL.GetAll().ToList();
                    }
                    materiaPrimaViewModel.UnidadMedidas = unidadMedidas;
                    return View(materiaPrimaViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Edit(MateriaPrimaViewModel materiaPrimaViewModel)
        {
            try
            {
                materiaPrimaViewModel.Estado = true;
                materiaPrimaViewModel.PrecioTotal = materiaPrimaViewModel.PrecioUnitario * materiaPrimaViewModel.Cantidad;
                using (UnidadDeTrabajo<MateriaPrima> unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                {
                    unidad.genericDAL.Update(Convertir(materiaPrimaViewModel));
                    unidad.Complete();
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Materia prima modificada: " + materiaPrimaViewModel.CodigoMateriaPrima;
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al editar materia prima";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Details(int? id)
        {
            try
            {
                if (id != null)
                {
                    MateriaPrima materiaPrima;
                    using (UnidadDeTrabajo<MateriaPrima> unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                    {
                        materiaPrima = unidad.genericDAL.Get((int)id);
                        if (!materiaPrima.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    MateriaPrimaViewModel materiaPrimaViewModel = Convertir(materiaPrima);
                    using (UnidadDeTrabajo<Proveedor> unidad = new UnidadDeTrabajo<Proveedor>(new BDContext()))
                    {
                        materiaPrimaViewModel.Proveedor = unidad.genericDAL.Get(materiaPrima.ProveedorID);
                    }
                    using (UnidadDeTrabajo<TipoMateriaPrima> unidad = new UnidadDeTrabajo<TipoMateriaPrima>(new BDContext()))
                    {
                        materiaPrimaViewModel.TipoMateriaPrima = unidad.genericDAL.Get(materiaPrima.TipoMateriaPrimaID);
                    }
                    using (UnidadDeTrabajo<UnidadMedida> unidad = new UnidadDeTrabajo<UnidadMedida>(new BDContext()))
                    {
                        materiaPrimaViewModel.UnidadMedida = unidad.genericDAL.Get(materiaPrima.UnidadMedidaID);
                    }
                    return View(materiaPrimaViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver detalles";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    MateriaPrima materiaPrima;
                    using (UnidadDeTrabajo<MateriaPrima> unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                    {
                        materiaPrima = unidad.genericDAL.Get((int)id);
                        if (!materiaPrima.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    MateriaPrimaViewModel materiaPrimaViewModel = Convertir(materiaPrima);
                    using (UnidadDeTrabajo<Proveedor> unidad = new UnidadDeTrabajo<Proveedor>(new BDContext()))
                    {
                        materiaPrimaViewModel.Proveedor = unidad.genericDAL.Get(materiaPrima.ProveedorID);
                    }
                    using (UnidadDeTrabajo<TipoMateriaPrima> unidad = new UnidadDeTrabajo<TipoMateriaPrima>(new BDContext()))
                    {
                        materiaPrimaViewModel.TipoMateriaPrima = unidad.genericDAL.Get(materiaPrima.TipoMateriaPrimaID);
                    }
                    using (UnidadDeTrabajo<UnidadMedida> unidad = new UnidadDeTrabajo<UnidadMedida>(new BDContext()))
                    {
                        materiaPrimaViewModel.UnidadMedida = unidad.genericDAL.Get(materiaPrima.UnidadMedidaID);
                    }
                    return View(materiaPrimaViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Delete(MateriaPrimaViewModel materiaPrimaViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<MateriaPrima> unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                {
                    MateriaPrima materiaPrima = unidad.genericDAL.Get(materiaPrimaViewModel.MateriaPrimaID);
                    materiaPrima.Estado = false;
                    unidad.genericDAL.Update(materiaPrima);
                    unidad.Complete();
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Materia prima eliminada: " + materiaPrima.CodigoMateriaPrima;
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al eliminar materia prima";
                return RedirectToAction("Index");
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}
