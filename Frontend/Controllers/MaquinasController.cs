﻿using System;
using System.CodeDom;
using System.Collections.Generic;


using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;

namespace Frontend.Controllers
{
    [Authorize]
    public class MaquinasController : Controller
    {
        private MaquinasViewModel Convertir(MaquinasActivos maquina)
        {
            return new MaquinasViewModel
            {
                MaquinaID = maquina.MaquinaID,
                CodigoMaquina = maquina.CodigoMaquina,
                Nombre = maquina.Nombre,
                TipoMaquinaID = maquina.TipoMaquinaID,
                Descripcion = maquina.Descripcion,
                ProduccionHora = maquina.ProduccionHora,
                Estado = maquina.Estado
            };
        }

        private MaquinasViewModel Convertir(Maquinas maquina)
        {
            return new MaquinasViewModel
            {
                MaquinaID = maquina.MaquinaID,
                CodigoMaquina = maquina.CodigoMaquina,
                Nombre = maquina.Nombre,
                TipoMaquinaID = maquina.TipoMaquinaID,
                Descripcion = maquina.Descripcion,
                ProduccionHora = maquina.ProduccionHora,
                Estado = maquina.Estado
            };
        }
        private Maquinas Convertir(MaquinasViewModel maquinaViewModel)
        {
            return new Maquinas
            {
                MaquinaID = maquinaViewModel.MaquinaID,
                CodigoMaquina = maquinaViewModel.CodigoMaquina,
                Nombre = maquinaViewModel.Nombre,
                TipoMaquinaID = maquinaViewModel.TipoMaquinaID,
                Descripcion = maquinaViewModel.Descripcion,
                ProduccionHora = maquinaViewModel.ProduccionHora,
                Estado = maquinaViewModel.Estado
            };
        }
        public ActionResult Index()
        {
            try
            {
                List<MaquinasActivos> maquinas;
                using (UnidadDeTrabajo<MaquinasActivos> Unidad = new UnidadDeTrabajo<MaquinasActivos>(new BDContext()))
                {
                    maquinas = Unidad.genericDAL.GetAll().ToList();
                }
                List<MaquinasViewModel> lista = new List<MaquinasViewModel>();
                MaquinasViewModel maquinaViewModel;
                foreach (var item in maquinas)
                {
                    maquinaViewModel = Convertir(item);

                    using (UnidadDeTrabajo<TipoMaquina> Unidad = new UnidadDeTrabajo<TipoMaquina>(new BDContext()))
                    {
                        maquinaViewModel.TipoMaquina = Unidad.genericDAL.Get(maquinaViewModel.TipoMaquinaID);
                    }

                    lista.Add(maquinaViewModel);
                }
                //Mostrar mensajes
                ViewBag.Message = BLLUtilitarios.msj;
                BLLUtilitarios.msj = null;
                return View(lista);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener lista";
                return RedirectToAction("Index", "Home");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            try
            {
                MaquinasViewModel maquinas = new MaquinasViewModel { };
                using (UnidadDeTrabajo<TipoMaquina> unidad = new UnidadDeTrabajo<TipoMaquina>(new BDContext()))
                {
                    maquinas.TipoMaquinas = unidad.genericDAL.GetAll().ToList();
                }
                return View(maquinas);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Create(MaquinasViewModel maquinaViewModel)
        {
            try
            {
                bool a;
                maquinaViewModel.Estado = true;
                using (BDContext context = new BDContext())
                {
                    if (context.Maquinas.Where(c => c.CodigoMaquina== maquinaViewModel.CodigoMaquina).Count() > 0)
                    {
                        TempData["TipoMensaje"] = "alert alert-danger";
                        TempData["MensajeError"] = "Codigo maquina ya existe";
                        using (UnidadDeTrabajo<TipoMaquina> unidad = new UnidadDeTrabajo<TipoMaquina>(new BDContext()))
                        {
                            maquinaViewModel.TipoMaquinas = unidad.genericDAL.GetAll().ToList();
                        }
                        return View("Create", maquinaViewModel);
                    }
                }
                Maquinas maquina = Convertir(maquinaViewModel);
                using (UnidadDeTrabajo<Maquinas> unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
                {
                    unidad.genericDAL.Add(maquina);
                    a = unidad.Complete();

                }
                //Mostrar El mensaje 
              /*  if (a)
                {
                    BLLUtilitarios.msj = "Se ha agregado una nueva maquina 1";
                }
                else
                {

                    BLLUtilitarios.msj = "Error al agregar maquina en la base de datos";
                }*/

                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Maquina creada: " + maquinaViewModel.CodigoMaquina;
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al agregar maquina";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    Maquinas maquina;
                    using (UnidadDeTrabajo<Maquinas> unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
                    {
                        maquina = unidad.genericDAL.Get((int)id);
                        if (!maquina.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    MaquinasViewModel maquinas = Convertir(maquina);
                    TipoMaquina tipoMaquina;
                    List<TipoMaquina> tipoMaquinas;
                    using (UnidadDeTrabajo<TipoMaquina> unidad = new UnidadDeTrabajo<TipoMaquina>(new BDContext()))
                    {
                        tipoMaquinas = unidad.genericDAL.GetAll().ToList();
                        tipoMaquina = unidad.genericDAL.Get(maquinas.TipoMaquinaID);
                    }
                    maquinas.TipoMaquinas = tipoMaquinas;
                    return View(maquinas);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Edit(MaquinasViewModel maquinaViewModel)
        {
            try
            {
                maquinaViewModel.Estado = true;
                using (UnidadDeTrabajo<Maquinas> unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
                {
                    unidad.genericDAL.Update(Convertir(maquinaViewModel));
                    unidad.Complete();
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Maquina modificada: " + maquinaViewModel.CodigoMaquina;
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al editar maquina";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Details(int? id)
        {
            try
            {
                if (id != null)
                {
                    Maquinas maquina;
                    using (UnidadDeTrabajo<Maquinas> unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
                    {
                        maquina = unidad.genericDAL.Get((int)id);
                        if (!maquina.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    MaquinasViewModel maquinaViewModel = Convertir(maquina);
                    using (UnidadDeTrabajo<TipoMaquina> unidad = new UnidadDeTrabajo<TipoMaquina>(new BDContext()))
                    {
                        maquinaViewModel.TipoMaquina = unidad.genericDAL.Get(maquinaViewModel.TipoMaquinaID);
                    };
                    return View(maquinaViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver detalles";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    Maquinas maquina;
                    using (UnidadDeTrabajo<Maquinas> unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
                    {
                        maquina = unidad.genericDAL.Get((int)id);
                    }
                    MaquinasViewModel maquinaViewModel = this.Convertir(maquina);
                    using (UnidadDeTrabajo<TipoMaquina> unidad = new UnidadDeTrabajo<TipoMaquina>(new BDContext()))
                    {

                        maquinaViewModel.TipoMaquina = unidad.genericDAL.Get(maquina.TipoMaquinaID);
                    };
                    return View(maquinaViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Delete(MaquinasViewModel maquinaViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<Maquinas> unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
                {
                    Maquinas maquinas = unidad.genericDAL.Get(maquinaViewModel.MaquinaID);
                    maquinas.Estado = false;
                    unidad.genericDAL.Update(maquinas);
                    unidad.Complete();
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Maquina eliminada: " + maquinas.CodigoMaquina;
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al eliminar maquina";
                return RedirectToAction("Index");
            }
        }

        public ActionResult Reporte()
        {
            return View();
        }


        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }


}
