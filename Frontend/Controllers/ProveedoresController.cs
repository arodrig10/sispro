﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;

namespace Frontend.Controllers
{
    [Authorize]
    public class ProveedoresController : Controller
    {
        private ProveedorViewModel Convertir(ProveedorActivos proveedorViewModel)
        {
            return new ProveedorViewModel
            {
                ProveedorID = proveedorViewModel.ProveedorID,
                CodigoProveedor = proveedorViewModel.CodigoProveedor,
                Nombre = proveedorViewModel.Nombre,
                Telefono = proveedorViewModel.Telefono,
                Correo = proveedorViewModel.Correo,
                Direccion = proveedorViewModel.Direccion,
                Estado = proveedorViewModel.Estado
            };
        }

        private Proveedor Convertir(ProveedorViewModel proveedorViewModel)
        {
            return new Proveedor
            {
                ProveedorID = proveedorViewModel.ProveedorID,
                CodigoProveedor = proveedorViewModel.CodigoProveedor,
                Nombre = proveedorViewModel.Nombre,
                Telefono = proveedorViewModel.Telefono,
                Correo = proveedorViewModel.Correo,
                Direccion = proveedorViewModel.Direccion,
                Estado = proveedorViewModel.Estado
            };
        }
        private ProveedorViewModel Convertir(Proveedor proveedor)
        {
            return new ProveedorViewModel
            {
                ProveedorID = proveedor.ProveedorID,
                CodigoProveedor = proveedor.CodigoProveedor,
                Nombre = proveedor.Nombre,
                Telefono = proveedor.Telefono,
                Correo = proveedor.Correo,
                Direccion = proveedor.Direccion,
                Estado = proveedor.Estado
            };
        }
        public ActionResult Index()
        {
            try
            {
                List<ProveedorActivos> proveedores;
                using (UnidadDeTrabajo<ProveedorActivos> Unidad = new UnidadDeTrabajo<ProveedorActivos>(new BDContext()))
                {
                    proveedores = Unidad.genericDAL.GetAll().ToList();
                }
                List<ProveedorViewModel> lista = new List<ProveedorViewModel>();
                foreach (var item in proveedores)
                {
                    lista.Add(Convertir(item));
                }
                //Mostrar mensajes
                ViewBag.Message = BLLUtilitarios.msj;
                BLLUtilitarios.msj = null;
                return View(lista);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener lista";
                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize(Roles = "Administrador")]

        public ActionResult Create()
        {
            return View();
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Create(ProveedorViewModel proveedorViewModel)
        {
            try
            {
                bool a;
                proveedorViewModel.Estado = true;
                using (BDContext context = new BDContext())
                {
                    if (context.Proveedor.Where(c => c.CodigoProveedor == proveedorViewModel.CodigoProveedor).Count() > 0)
                    {
                        TempData["TipoMensaje"] = "alert alert-danger";
                        TempData["MensajeError"] = "Codigo proveedor ya existe";
                        return View("Create", proveedorViewModel);
                    }
                }


                    Proveedor proveedor = Convertir(proveedorViewModel);
                using (UnidadDeTrabajo<Proveedor> unidad = new UnidadDeTrabajo<Proveedor>(new BDContext()))
                {
                    unidad.genericDAL.Add(proveedor);
                    a = unidad.Complete();
                }
                //Mostrar El mensaje 
               /* if (a)
                {
                    BLLUtilitarios.msj = "Se ha agregado una nuevo proveedor 1";
                }
                else
                {

                    BLLUtilitarios.msj = "Error al agregar proveedor en la base de datos";
                }*/

                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Proveedor creado: " + proveedorViewModel.CodigoProveedor;
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al crear proveedor";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    Proveedor proveedor;
                    using (UnidadDeTrabajo<Proveedor> unidad = new UnidadDeTrabajo<Proveedor>(new BDContext()))
                    {
                        proveedor = unidad.genericDAL.Get((int)id);
                        if (!proveedor.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    return View(Convertir(proveedor));
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Edit(ProveedorViewModel proveedorViewModel)
        {
            try
            {
                proveedorViewModel.Estado = true;
                using (UnidadDeTrabajo<Proveedor> unidad = new UnidadDeTrabajo<Proveedor>(new BDContext()))
                {
                    unidad.genericDAL.Update(Convertir(proveedorViewModel));
                    unidad.Complete();
                }
                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Proveedor modificado: " + proveedorViewModel.CodigoProveedor;
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al editar proveedor";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Details(int? id)
        {
            try
            {
                if (id != null)
                {
                    Proveedor proveedor;
                    using (UnidadDeTrabajo<Proveedor> unidad = new UnidadDeTrabajo<Proveedor>(new BDContext()))
                    {
                        proveedor = unidad.genericDAL.Get((int)id);
                        if (!proveedor.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    return View(Convertir(proveedor));
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver detalles";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    Proveedor proveedor;
                    using (UnidadDeTrabajo<Proveedor> unidad = new UnidadDeTrabajo<Proveedor>(new BDContext()))
                    {
                        proveedor = unidad.genericDAL.Get((int)id);
                        if (!proveedor.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    return View(Convertir(proveedor));
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Delete(ProveedorViewModel proveedorViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<Proveedor> unidad = new UnidadDeTrabajo<Proveedor>(new BDContext()))
                {
                    Proveedor proveedor = unidad.genericDAL.Get(proveedorViewModel.ProveedorID);
                    proveedor.Estado = false;
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Proveedor eliminado: " + proveedor.CodigoProveedor;
                    unidad.genericDAL.Update(proveedor);
                    unidad.Complete();
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al eliminar producto";
                return RedirectToAction("Index");
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}
