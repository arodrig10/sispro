﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;

namespace Frontend.Controllers
{
    [Authorize]
    public class ClientesController : Controller
    {
        private ClientesViewModel Convertir(ClientesActivos cliente)
        {
            return new ClientesViewModel
            {
                ClienteID = cliente.ClienteID,
                Identificacion = cliente.Identificacion,
                NombreCompleto = cliente.NombreCompleto,
                Telefono = cliente.Telefono,
                Correo = cliente.Correo,
                Direccion = cliente.Direccion,
                Estado = cliente.Estado
            };
        }

        private ClientesViewModel Convertir(Clientes cliente)
        {
            return new ClientesViewModel
            {
                ClienteID = cliente.ClienteID,
                Identificacion = cliente.Identificacion,
                NombreCompleto = cliente.NombreCompleto,
                Telefono = cliente.Telefono,
                Correo = cliente.Correo,
                Direccion = cliente.Direccion,
                Estado = cliente.Estado
            };
        }
        private Clientes Convertir(ClientesViewModel clienteViewModel)
        {
            return new Clientes
            {
                ClienteID = clienteViewModel.ClienteID,
                Identificacion = clienteViewModel.Identificacion,
                NombreCompleto = clienteViewModel.NombreCompleto,
                Telefono = clienteViewModel.Telefono,
                Correo = clienteViewModel.Correo,
                Direccion = clienteViewModel.Direccion,
                Estado = clienteViewModel.Estado
            };
        }
        public ActionResult Index()
        {
            try
            {
                List<ClientesActivos> clientes;
                using (UnidadDeTrabajo<ClientesActivos> Unidad = new UnidadDeTrabajo<ClientesActivos>(new BDContext()))
                {
                    clientes = Unidad.genericDAL.GetAll().ToList();
                }
                List<ClientesViewModel> lista = new List<ClientesViewModel>();
                ClientesViewModel clienteViewModel;
                foreach (var item in clientes)
                {
                    clienteViewModel = Convertir(item);
                    lista.Add(clienteViewModel);
                }
                //Mostrar mensajes
                ViewBag.Message = BLLUtilitarios.msj;
                BLLUtilitarios.msj = null;
                return View(lista);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener lista";
                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            try
            {
                ClientesViewModel cliente = new ClientesViewModel { };
                return View(cliente);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Create(ClientesViewModel clienteViewModel)
        {
            try
            {

                clienteViewModel.Estado = true;
                using (BDContext context = new BDContext())
                {
                    if (context.Clientes.Where(c => c.Identificacion == clienteViewModel.Identificacion).Count() > 0)
                    {
                        TempData["TipoMensaje"] = "alert alert-danger";
                        TempData["MensajeError"] = "Numero de Identifacion ya existe";
                        return View("Create", clienteViewModel);
                    }
                }
                if (ModelState.IsValid)
                {
                    bool a;
                    Clientes cliente = Convertir(clienteViewModel);
                    using (UnidadDeTrabajo<Clientes> unidad = new UnidadDeTrabajo<Clientes>(new BDContext()))
                    {
                        unidad.genericDAL.Add(cliente);
                        a = unidad.Complete();
                    }
                    /*  if (a)
                      {
                          BLLUtilitarios.msj = "Se ha agregado un cliente 1";
                      }
                      else
                      {
                          BLLUtilitarios.msj = "Error al agregar cliente";
                      }*/
                      TempData["TipoMensaje"] = "alert alert-success";
                      TempData["Mensaje"] = "Cliente creado: " + clienteViewModel.NombreCompleto;

                   return RedirectToAction("Index");
                  }
                  return RedirectToAction("Create", clienteViewModel);
                
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al agregar cliente";
                return RedirectToAction("Create", clienteViewModel);
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    Clientes cliente;
                    using (UnidadDeTrabajo<Clientes> unidad = new UnidadDeTrabajo<Clientes>(new BDContext()))
                    {
                        cliente = unidad.genericDAL.Get((int)id);
                        if (!cliente.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    return View(Convertir(cliente));
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Edit(ClientesViewModel clienteViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    clienteViewModel.Estado = true;
                    using (UnidadDeTrabajo<Clientes> unidad = new UnidadDeTrabajo<Clientes>(new BDContext()))
                    {
                        unidad.genericDAL.Update(Convertir(clienteViewModel));
                        unidad.Complete();

                    }
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Cliente modificado: " + clienteViewModel.NombreCompleto;
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Edit", clienteViewModel);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al editar cliente";
                return RedirectToAction("Edit", clienteViewModel);
            }
        }
        public ActionResult Details(int? id)
        {
            try
            {
                if (id != null)
                {
                    Clientes clientes;
                    using (UnidadDeTrabajo<Clientes> unidad = new UnidadDeTrabajo<Clientes>(new BDContext()))
                    {
                        clientes = unidad.genericDAL.Get((int)id);
                        if (!clientes.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    ClientesViewModel clientesViewModel = Convertir(clientes);

                    return View(clientesViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver detalles";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    Clientes clientes;
                    using (UnidadDeTrabajo<Clientes> unidad = new UnidadDeTrabajo<Clientes>(new BDContext()))
                    {
                        clientes = unidad.genericDAL.Get((int)id);
                        if (!clientes.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    return View(Convertir(clientes));
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al eliminar cliente";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Delete(ClientesViewModel clienteViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<Clientes> unidad = new UnidadDeTrabajo<Clientes>(new BDContext()))
                {
                    Clientes clientes = unidad.genericDAL.Get(clienteViewModel.ClienteID);
                    clientes.Estado = false;
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Cliente eliminado: " + clientes.NombreCompleto;
                    unidad.genericDAL.Update(clientes);
                    unidad.Complete();
                }

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al eliminar cliente";
                return RedirectToAction("Index");
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }

    }
}
