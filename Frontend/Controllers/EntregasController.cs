﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;

namespace Frontend.Controllers
{
    [Authorize]
    public class EntregasController : Controller
    {

        private DetalleEntregaViewModel Convertir(DetalleEntrega obje)
        {
            return new DetalleEntregaViewModel
            {
                DetelleEntregaID = obje.DetelleEntregaID,
                EntregaID = obje.EntregaID,
                ProduccionID = obje.ProduccionID,
                Paquetes = obje.Paquetes,
                Cantidad = obje.Cantidad,
                Estado = obje.Estado
            };
        }


        public ActionResult SaveDetailedInfo(string entrega, DateTime fecha, string factura, int pedido, int paquetes, int cantidad)
        {
            bool flag = false;
            int idEntrega = 0;
            int idProduccion = 0;


            //---------------------Para Obtener el id de produccion--------------------------------------------------- 
            List<Produccion> produccion;
            using (UnidadDeTrabajo<Produccion> Unidad = new UnidadDeTrabajo<Produccion>(new BDContext()))
            {
                produccion = Unidad.genericDAL.GetAll().ToList();

            }
            foreach (var item in produccion)
            {
                if (item.PedidoID == pedido)
                {
                    idProduccion = item.ProduccionID;
                    flag = true;
                    break;
                }
                //--------------------FIn de agrgar detalle y aumentar las unidades entregadas al pedido General
            }
            if (!flag)
            {
                return Json(new { status = "Error", message = "Error" });

            }
            flag = false;
            //----------------------fin id produccion-------------


            try
            {
                List<EntregasActivos> entregas;
                using (UnidadDeTrabajo<EntregasActivos> Unidad = new UnidadDeTrabajo<EntregasActivos>(new BDContext()))
                {
                    entregas = Unidad.genericDAL.GetAll().ToList();

                }

                //Se crea el objeto view model para poder modificar la cantidad
                EntregaViewModel entregaViewModel;

                foreach (var item in entregas)
                {
                    //Validar si la entrega que se recibe esta almacenada si no se debe de crear entrega + detalle 
                    //Si es igual Hacer un update de cantidad entregada y un create en la de detalles
                    if (item.NumeroEntrega == entrega)
                    {
                        entregaViewModel = Convertir(item);
                        entregaViewModel.UnidadesEntregadas = entregaViewModel.UnidadesEntregadas + cantidad;

                        idEntrega = item.EntregaID;

                        using (UnidadDeTrabajo<Entregas> unidad = new UnidadDeTrabajo<Entregas>(new BDContext()))
                        {
                            unidad.genericDAL.Update(Convertir(entregaViewModel));
                            unidad.Complete();
                        }

                        flag = true;
                        break;
                    }
                    //--------------------FIn de agrgar detalle y aumentar las unidades entregadas al pedido General
                }

            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener lista";
                // devolvemos a la llamda del ajax un error para que nos diga que algo fallo 
                return Json(new { status = "Error", message = "Error" });
            }

            if (!flag)
            {
                //---------------------------crear una nueva entrega-----------------------------
                try
                {

                    //Se crea el objeto entregas para poder agregar  una nueva entrega
                    Entregas entregas = new Entregas
                    {
                        NumeroEntrega = entrega,
                        Fecha = fecha,
                        UnidadesEntregadas = cantidad,
                        NumeroFactura = factura,
                        Estado = true
                    };
                    //-----------------------Fin de entrega---------------


                    using (UnidadDeTrabajo<Entregas> unidad = new UnidadDeTrabajo<Entregas>(new BDContext()))
                    {
                        unidad.genericDAL.Add(entregas);
                        unidad.Complete();
                    }


                }
                catch (Exception)
                {
                    TempData["TipoMensaje"] = "alert alert-danger";
                    TempData["Mensaje"] = "Error al agregar entrega";
                    return Json(new { status = "Error", message = "Error" });
                }

                //--------------------------------fin de agregar entrega--------------------------


            }
            //Aca para siempre agregar el detalle si o si

            //-----------------------------------Agregar detalle-----------------------------------

            try
            {
                //Para obtener el id de la entrega
                List<EntregasActivos> entregas;
                using (UnidadDeTrabajo<EntregasActivos> Unidad = new UnidadDeTrabajo<EntregasActivos>(new BDContext()))
                {
                    entregas = Unidad.genericDAL.GetAll().ToList();

                }
                foreach (var item in entregas)
                {
                    //Validar si la entrega que se recibe esta almacenada si no se debe de crear entrega + detalle 
                    //Si es igual Hacer un update de cantidad entregada y un create en la de detalles
                    if (item.NumeroEntrega == entrega)
                    {
                        idEntrega = item.EntregaID;

                        break;
                    }
                }
                //------------------Fin obtener id de la entrega----------------------------

                bool a;
                //Se crea el objeto entregas para poder agregar  una nueva entrega
                DetalleEntrega detalleEntrega = new DetalleEntrega
                {
                    EntregaID = idEntrega,
                    ProduccionID = idProduccion,
                    Paquetes = paquetes,
                    Cantidad = cantidad,
                    Estado = true
                };
                //-----------------------Fin de entrega---------------
                using (UnidadDeTrabajo<DetalleEntrega> unidad = new UnidadDeTrabajo<DetalleEntrega>(new BDContext()))
                {
                    unidad.genericDAL.Add(detalleEntrega);
                    a = unidad.Complete();
                }

            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al agregar entrega";
                return Json(new { status = "Error", message = "Error" });
            }


            //---------------------------------fin de agregar detalle------------------------------

            return Json(new { status = "Success", message = "Success" });

        }

        //----------------------------------------Metodo para validar lo que se va entregar vrs lo entregado y queda producido-----------------------------------------------


        public ActionResult ValidarCantidad(int vpedido,  float vcantidad)
        {
            float produccionActual = 0;
            float entregadoActual = 0;
            try
            {
               

                //--------------------------------------------Para saber la cantidad de produccion-----------------------------------------------------------

                using (UnidadDeTrabajo<ProduccionActivos> unidad = new UnidadDeTrabajo<ProduccionActivos>(new BDContext()))
                {
                    foreach (var item in unidad.genericDAL.GetAll().ToList())
                    {
                        if (item.PedidoID == vpedido)
                        {
                            produccionActual = produccionActual + (float)item.CantidadProduccionTotal;
                        }
                    }
                }
                //------------------------------------------------fin de produccion------------------------------------------------------------------------------
                //--------------------------------------------Para saber la cantidad entregada-----------------------------------------------------------
               
                List<DetalleEntrega> entregas;
                using (UnidadDeTrabajo<DetalleEntrega> unidad = new UnidadDeTrabajo<DetalleEntrega>(new BDContext()))
                {

                    entregas = unidad.genericDAL.GetAll().ToList();

                }

                List<ProduccionActivos> produccionActivos;
                using (UnidadDeTrabajo<ProduccionActivos> unidad = new UnidadDeTrabajo<ProduccionActivos>(new BDContext()))
                {
                    produccionActivos = unidad.genericDAL.GetAll().ToList();

                }

               
                foreach (var item in produccionActivos)
                {
                    if (item.PedidoID == vpedido)
                    {
                        foreach (var entregasProduccion in entregas)
                        {
                            if (item.ProduccionID == entregasProduccion.ProduccionID)
                            {

                                entregadoActual = entregadoActual + (float)entregasProduccion.Cantidad;

                            }

                        }
                    }
                }

                //---------------------------------------Verificamoms que no se entregue más de lo que se produce
                if (BLLUtilitarios.resta(produccionActual, entregadoActual)< vcantidad )
                {
                    return Json(new { status = "Error", message = "¡No se pudo agregar detalle de entrega, excede la cantidad, solo hay: "+BLLUtilitarios.resta(produccionActual, entregadoActual)+" en existencia!" });
                }
                else
                {
                    return Json(new { status = "Success", message = "Success" });
                }


               
            //------------------------------------------------fin de produccion------------------------------------------------------------------------------

            //return Json(new { status = "Success", message = BLLUtilitarios.resta(produccionActual, entregadoActual) });
        }
            catch (Exception)
            {
                return Json(new { status = "Error", message = "¡Error al comunicarse con la Base de Datos!" } );
            }

            

        }

        //............................................................................fin..........................................................
        private EntregaViewModel Convertir(EntregasActivos entrega)
        {
            return new EntregaViewModel
            {
                EntregaID = entrega.EntregaID,
                NumeroEntrega = entrega.NumeroEntrega,
                Fecha = entrega.Fecha,
                UnidadesEntregadas = entrega.UnidadesEntregadas,
                NumeroFactura = entrega.NumeroFactura,
                Estado = entrega.Estado

            };
        }
        private EntregaViewModel Convertir(Entregas entrega)
        {
            return new EntregaViewModel
            {
                EntregaID = entrega.EntregaID,
                NumeroEntrega = entrega.NumeroEntrega,
                Fecha = entrega.Fecha,
                UnidadesEntregadas = entrega.UnidadesEntregadas,
                NumeroFactura = entrega.NumeroFactura,
                Estado = entrega.Estado

            };
        }

        private Entregas Convertir(EntregaViewModel entregaViewModel)
        {
            return new Entregas
            {
                EntregaID = entregaViewModel.EntregaID,
                NumeroEntrega = entregaViewModel.NumeroEntrega,
                Fecha = entregaViewModel.Fecha,
                UnidadesEntregadas = entregaViewModel.UnidadesEntregadas,
                NumeroFactura = entregaViewModel.NumeroFactura,
                Estado = entregaViewModel.Estado
            };
        }
        public ActionResult Index()
        {
            try
            {
                List<EntregasActivos> entregas;
                using (UnidadDeTrabajo<EntregasActivos> Unidad = new UnidadDeTrabajo<EntregasActivos>(new BDContext()))
                {
                    entregas = Unidad.genericDAL.GetAll().ToList();
                }

                List<EntregaViewModel> lista = new List<EntregaViewModel>();
                EntregaViewModel entregaViewModel;
                foreach (var item in entregas)
                {
                    entregaViewModel = Convertir(item);
                    lista.Add(entregaViewModel);
                }



                return View(lista);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener lista";
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult Create()
        {
            try
            {

                //---------------------para obtener los clientes que tienen produccion y pedidos activos----------------------------------------------------------


                EntregaViewModel entregas = new EntregaViewModel { };
                using (UnidadDeTrabajo<ClientesProduccion> unidad = new UnidadDeTrabajo<ClientesProduccion>(new BDContext()))
                {
                    entregas.ClientesProduccion = unidad.genericDAL.GetAll().ToList();
                }
                entregas.Fecha = DateTime.Now;

                return View(entregas);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Create(EntregaViewModel entregaViewModel)
        {
            try
            {
                using (BDContext context = new BDContext())
                {
                    if (context.Entregas.Where(c => c.NumeroEntrega == entregaViewModel.NumeroEntrega).Count() > 0)
                    {
                        TempData["TipoMensaje"] = "alert alert-danger";
                        TempData["MensajeError"] = "Numero entrega ya existe";
                        using (UnidadDeTrabajo<ClientesProduccion> unidad = new UnidadDeTrabajo<ClientesProduccion>(new BDContext()))
                        {
                            entregaViewModel.ClientesProduccion = unidad.genericDAL.GetAll().ToList();
                        }
                        return View("Create", entregaViewModel);
                    }
                }
                bool a;
                entregaViewModel.Estado = true;
                entregaViewModel.UnidadesEntregadas = 0;
                Entregas entregas = Convertir(entregaViewModel);

                using (UnidadDeTrabajo<Entregas> unidad = new UnidadDeTrabajo<Entregas>(new BDContext()))
                {
                    unidad.genericDAL.Add(entregas);
                    unidad.Complete();
                }

                using (UnidadDeTrabajo<Clientes> unidad = new UnidadDeTrabajo<Clientes>(new BDContext()))
                {
                    entregaViewModel.Cliente = unidad.genericDAL.Get(entregaViewModel.Cliente.ClienteID);
                   
                }

                TempData["TipoMensaje"] = "alert alert-success";
                TempData["MensajeError"] = "Entrega creada: " + entregas.NumeroEntrega;
                TempData["DetalleEntrega"] = "1";
                TempData["Cliente"] = entregaViewModel.Cliente.NombreCompleto;
                return RedirectToAction("CreateDetalle", new { id = entregas.EntregaID, cliente = entregaViewModel.Cliente.ClienteID });
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al agregar entrega";
                return RedirectToAction("Index");
            }
        }

        //--------------------------------------vista para mostrar solo la entrega--------------------------------------------------
        public ActionResult CreateDetalle(int? id, int? cliente)
        {

            try
            {
                EntregaViewModel entregas = new EntregaViewModel { };

                using (UnidadDeTrabajo<Entregas> unidad = new UnidadDeTrabajo<Entregas>(new BDContext()))
                {
                    entregas = Convertir(unidad.genericDAL.Get((int)id));

                }

                using (UnidadDeTrabajo<Clientes> unidad = new UnidadDeTrabajo<Clientes>(new BDContext()))
                {
                    entregas.Cliente = unidad.genericDAL.Get((int)cliente);

                }

                return View(entregas);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        //--------------------------------------vista para mostrar solo la entrega--------------------------------------------------
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    Entregas entrega;
                    using (UnidadDeTrabajo<Entregas> unidad = new UnidadDeTrabajo<Entregas>(new BDContext()))
                    {
                        entrega = unidad.genericDAL.Get((int)id);
                        if (!entrega.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    EntregaViewModel objeto = Convertir(entrega);
                    using (UnidadDeTrabajo<ClientesProduccion> unidad = new UnidadDeTrabajo<ClientesProduccion>(new BDContext()))
                    {
                        objeto.ClientesProduccion = unidad.genericDAL.GetAll().ToList();
                    }
                    using (UnidadDeTrabajo<PedidosActivos> unidad = new UnidadDeTrabajo<PedidosActivos>(new BDContext()))
                    {
                        objeto.Pedidos = unidad.genericDAL.GetAll().ToList();
                    }
                    objeto.DetalleEntregas = new List<DetalleEntregaViewModel>();
                    using (UnidadDeTrabajo<DetalleEntrega> unidad = new UnidadDeTrabajo<DetalleEntrega>(new BDContext()))
                    {
                        Expression<Func<DetalleEntrega, bool>> consulta = (c => c.EntregaID == entrega.EntregaID);
                        List<DetalleEntrega> listaDetalleEntrega = unidad.genericDAL.Find(consulta).ToList();
                        foreach (var item in listaDetalleEntrega)
                        {
                            DetalleEntregaViewModel obje = Convertir(item);
                            Produccion produccion;
                            using (UnidadDeTrabajo<Produccion> unidadProd = new UnidadDeTrabajo<Produccion>(new BDContext()))
                            {
                                produccion = unidadProd.genericDAL.Get(item.ProduccionID);
                            }
                            using (UnidadDeTrabajo<Pedidos> unidadProd = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                            {
                                obje.Pedido = unidadProd.genericDAL.Get(produccion.PedidoID);
                            }

                            objeto.DetalleEntregas.Add(obje);
                        }
                    }
                    if (objeto.DetalleEntregas.Count >0)
                    {
                        TempData["DetalleEntrega"] = "1";
                        ViewBag.ListaDetalle = objeto.DetalleEntregas;
                    //    return RedirectToAction("CreateDetalle", new { id = obje.EntregaID, cliente = obje.Cliente.ClienteID });
                    }

                    return View(objeto);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Edit(EntregaViewModel entregaViewModel)
        {
            try
            {
                entregaViewModel.Estado = true;
                using (UnidadDeTrabajo<Entregas> unidad = new UnidadDeTrabajo<Entregas>(new BDContext()))
                {
                    unidad.genericDAL.Update(Convertir(entregaViewModel));
                    unidad.Complete();
                }

               
                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Entrega editada";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al editar entrega";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Details(int? id)
        {
            try
            {
                if (id != null)
                {
                    Entregas entrega;
                    using (UnidadDeTrabajo<Entregas> unidad = new UnidadDeTrabajo<Entregas>(new BDContext()))
                    {
                        entrega = unidad.genericDAL.Get((int)id);
                        if (!entrega.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    EntregaViewModel entregaViewModel = Convertir(entrega);
                    entregaViewModel.DetalleEntregas = new List<DetalleEntregaViewModel>();
                    using (UnidadDeTrabajo<DetalleEntrega> unidad = new UnidadDeTrabajo<DetalleEntrega>(new BDContext()))
                    {
                        Expression<Func<DetalleEntrega, bool>> consulta = (c => c.EntregaID == entrega.EntregaID);
                        List<DetalleEntrega> listaDetalleEntrega = unidad.genericDAL.Find(consulta).ToList();
                        foreach (var item in listaDetalleEntrega)
                        {
                            DetalleEntregaViewModel obje = Convertir(item);
                            Produccion produccion;
                            using (UnidadDeTrabajo<Produccion> unidadProd = new UnidadDeTrabajo<Produccion>(new BDContext()))
                            {
                                produccion = unidadProd.genericDAL.Get(item.ProduccionID);
                            }
                            using (UnidadDeTrabajo<Pedidos> unidadProd = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                            {
                                obje.Pedido = unidadProd.genericDAL.Get(produccion.PedidoID);
                            }
                            
                            entregaViewModel.DetalleEntregas.Add(obje);
                        }
                    }

                    using (UnidadDeTrabajo<ClientesProduccion> unidad = new UnidadDeTrabajo<ClientesProduccion>(new BDContext()))
                    {
                        entregaViewModel.ClientesProduccion = unidad.genericDAL.GetAll().ToList();
                    }
                    return View(entregaViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver detalles";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    Entregas entrega;
                    using (UnidadDeTrabajo<Entregas> unidad = new UnidadDeTrabajo<Entregas>(new BDContext()))
                    {
                        entrega = unidad.genericDAL.Get((int)id);
                        if (!entrega.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    EntregaViewModel entregaViewModel = Convertir(entrega);

                    return View(entregaViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Delete(EntregaViewModel entregaViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<Entregas> unidad = new UnidadDeTrabajo<Entregas>(new BDContext()))
                {
                    Entregas entregas = unidad.genericDAL.Get(entregaViewModel.EntregaID);
                    entregas.Estado = false;
                    unidad.genericDAL.Update(entregas);
                    unidad.Complete();
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Entrega eliminada: " + entregas.NumeroEntrega;
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al eliminar entrega";
                return RedirectToAction("Index");
            }
        }


       /* protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }*/
    }
}
