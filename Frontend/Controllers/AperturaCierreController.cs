﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;

namespace Frontend.Controllers
{

    [Authorize]

    public class AperturaCierreController : Controller
    {

        private Pedidos Convertir(PedidosActivos pedido)
        {
            return new Pedidos
            {
                PedidoID = pedido.PedidoID,
                NumeroPedido = pedido.NumeroPedido,
                ClienteID = pedido.ClienteID,
                NumeroOrdenDeCompra = pedido.NumeroOrdenDeCompra,
                ProductoID = pedido.ProductoID,
                UsuarioID = pedido.UsuarioID,
                UnidadMedidaID = pedido.UnidadMedidaID,
                Cantidad = pedido.Cantidad,
                Descripcion = pedido.Descripcion,
                Fecha = pedido.Fecha,
                PrecioTotal = pedido.PrecioTotal,
                Estado = pedido.Estado,
                EstadoID = pedido.EstadoID
            };
        }
        private Maquinas Convertir(MaquinasActivos maquina)
        {
            return new Maquinas
            {
                MaquinaID = maquina.MaquinaID,
                CodigoMaquina = maquina.CodigoMaquina,
                Nombre = maquina.Nombre,
                TipoMaquinaID = maquina.TipoMaquinaID,
                Descripcion = maquina.Descripcion,
                ProduccionHora = maquina.ProduccionHora,
                Estado = maquina.Estado
            };
        }


        private AperturaCierreViewModel Convertir(AperturaCierreActivos aperturaCierreActivos)
        {
            return new AperturaCierreViewModel
            {
                AperturaCierreID = aperturaCierreActivos.AperturaCierreID,
                FechaApertura = aperturaCierreActivos.FechaApertura,
                FechaCierre = aperturaCierreActivos.FechaCierre,
                Turno = aperturaCierreActivos.Turno,
                PedidoID = aperturaCierreActivos.PedidoID,
                Estado = aperturaCierreActivos.Estado,
                MaquinaID = (int)aperturaCierreActivos.MaquinaID
            };
        }


        private AperturaCierreViewModel Convertir(AperturaCierre aperturaCierre)
        {
            return new AperturaCierreViewModel
            {
               
                AperturaCierreID = aperturaCierre.AperturaCierreID,
                FechaApertura =  aperturaCierre.FechaApertura,
                FechaCierre =  aperturaCierre.FechaCierre,
                Turno = aperturaCierre.Turno,
                PedidoID = aperturaCierre.PedidoID,
                Estado = aperturaCierre.Estado,
                MaquinaID = (int)aperturaCierre.MaquinaID
                
            };
        }

        private AperturaCierre Convertir(AperturaCierreViewModel aperturaCierreViewModel)
        {
            return new AperturaCierre
            {
                AperturaCierreID = aperturaCierreViewModel.AperturaCierreID,
                FechaApertura = aperturaCierreViewModel.FechaApertura,
                FechaCierre = aperturaCierreViewModel.FechaCierre,
                Turno = aperturaCierreViewModel.Turno,
                PedidoID = aperturaCierreViewModel.PedidoID,
                Estado = aperturaCierreViewModel.Estado,
                MaquinaID = aperturaCierreViewModel.MaquinaID
                       
                   };
        }


        public ActionResult Index()
        {
            List<AperturaCierreActivos> aperturaCierres;
            using (UnidadDeTrabajo<AperturaCierreActivos> Unidad = new UnidadDeTrabajo<AperturaCierreActivos>(new BDContext()))
            {
                aperturaCierres = Unidad.genericDAL.GetAll().ToList();
            }
            List<AperturaCierreViewModel> lista = new List<AperturaCierreViewModel>();
            AperturaCierreViewModel aperturaCierreViewModel;
            foreach (var item in aperturaCierres)
            {
                aperturaCierreViewModel = Convertir(item);

                using (UnidadDeTrabajo<Pedidos> UnidadPedidos = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                {
                    aperturaCierreViewModel.Pedido = UnidadPedidos.genericDAL.Get(aperturaCierreViewModel.PedidoID);
                }
                using (UnidadDeTrabajo<Maquinas> UnidadMaquina = new UnidadDeTrabajo<Maquinas>(new BDContext()))
                {
                    aperturaCierreViewModel.Maquina = UnidadMaquina.genericDAL.Get(aperturaCierreViewModel.MaquinaID);
                }

                lista.Add(aperturaCierreViewModel);
            }
            //Mostrar mensajes
            
            return View(lista);
        }


        public ActionResult Create()
        {
            AperturaCierreViewModel aperturaCierre = new AperturaCierreViewModel { };

            using (UnidadDeTrabajo<PedidosActivos> unidad = new UnidadDeTrabajo<PedidosActivos>(new BDContext()))
            {
                List<PedidosActivos> pedidosActivos = unidad.genericDAL.GetAll().ToList();
                List<Pedidos> pedidos = new List<Pedidos> ();
                 foreach(var item in pedidosActivos)
                {
                    if (item.EstadoID != 3) 
                    {
                        pedidos.Add(Convertir(item));
                    }
                }
                aperturaCierre.Pedidos = pedidos;


            }
            using (UnidadDeTrabajo<MaquinasActivos> unidad = new UnidadDeTrabajo<MaquinasActivos>(new BDContext()))
            {
                List<MaquinasActivos> maquinasActivos = unidad.genericDAL.GetAll().ToList();
                List<Maquinas> maquinas = new List<Maquinas>();
                foreach (var item in maquinasActivos)
                {
                    maquinas.Add(Convertir(item));
                }
               
                aperturaCierre.Maquinas = maquinas;
            }
            return View(aperturaCierre);
        }
        [HttpPost]
        public ActionResult Create(AperturaCierreViewModel aperturaCierreViewModel)
        {
            bool flag = false;
            List <AperturaCierreActivos> aperturaCierres;
            using (UnidadDeTrabajo<AperturaCierreActivos> Unidad = new UnidadDeTrabajo<AperturaCierreActivos>(new BDContext()))
            {
                aperturaCierres = Unidad.genericDAL.GetAll().ToList();
            }

         foreach (var item in aperturaCierres)
            {
                if(item.PedidoID == aperturaCierreViewModel.PedidoID)
                {
                    flag = true;
                    break;
                }
            }


            if (flag)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Este pedido tiene algun turno abierto, porfavor cierre el turno e intete de nuevo";
                return RedirectToAction("Create"); 
               
            }
            else
            {
                aperturaCierreViewModel.Estado = true;
                aperturaCierreViewModel.FechaCierre = null;
                AperturaCierre aperturaCierre = Convertir(aperturaCierreViewModel);

                using (UnidadDeTrabajo<AperturaCierre> unidad = new UnidadDeTrabajo<AperturaCierre>(new BDContext()))
                {
                    unidad.genericDAL.Add(aperturaCierre);
                    unidad.Complete();
                }

                Pedidos pedido;
                using (UnidadDeTrabajo<Pedidos> unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                {
                    pedido = unidad.genericDAL.Get(aperturaCierreViewModel.PedidoID);
                    pedido.EstadoID = 2;
                    unidad.genericDAL.Update(pedido);
                    unidad.Complete();
                    
                }




                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Se ha abierto el turno de con exito";
                return RedirectToAction("Index");
            }
        }


        //-------------------------------------Metodo Para cerrar Turnos---------------------------------------------
        public ActionResult CerrarTurno(int? id)
        {
            AperturaCierre apertura;

                using (UnidadDeTrabajo<AperturaCierre> unidad = new UnidadDeTrabajo<AperturaCierre>(new BDContext()))
                {
                    apertura = unidad.genericDAL.Get((int)id);
                    if (!apertura.Estado)
                    {
                        return RedirectToAction("Index");
                        //No se pudo cerrar 
                    }
                }
                AperturaCierreViewModel aperturaCierreViewModel = Convertir(apertura);
                Edit(aperturaCierreViewModel);


            return RedirectToAction("Index");
        }


        public void Edit(AperturaCierreViewModel aperturaCierreViewModel)
        {
            aperturaCierreViewModel.FechaCierre = DateTime.Now;
            aperturaCierreViewModel.Estado = false;

            using (UnidadDeTrabajo<AperturaCierre> unidad = new UnidadDeTrabajo<AperturaCierre>(new BDContext()))
            {
                unidad.genericDAL.Update(Convertir(aperturaCierreViewModel));
                unidad.Complete();


                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Se ha cerrado el turno de con exito";
            }
        }
        //------------------------fin de cerrar---------------------------------------------------------------------------
        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}
