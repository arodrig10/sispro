﻿using Backend.DAL;
using Backend.Entities;
using Frontend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Frontend.Controllers
{
    public class DetalleEntregaController : Controller
    {
        // GET: DetalleEntrega
        public ActionResult _DetalleEntrega(int? cliente)
        {
     

            try
            {

                //---------------------para obtener los clientes que tienen produccion y pedidos activos----------------------------------------------------------

                DetalleEntregaViewModel detalleEntregaViewModel = new DetalleEntregaViewModel();
                List <PedidosActivos> pedidos = new List<PedidosActivos>();
                List<ProduccionActivos> produccionActivos;
                List<PedidosActivos> lista = new List<PedidosActivos>();

                //usando linq  para acortar el codigo con where de cliente ID == q cliente
           
                using (UnidadDeTrabajo<ProduccionActivos> unidad = new UnidadDeTrabajo<ProduccionActivos>(new BDContext()))
                {

                    produccionActivos = unidad.genericDAL.GetAll().ToList();
                    
                }

                using (UnidadDeTrabajo<PedidosActivos> unidad = new UnidadDeTrabajo<PedidosActivos>(new BDContext()))
                {
                    pedidos = unidad.genericDAL.GetAll().ToList();
                }
                

                foreach (var item in pedidos)
                {
                    if (item.ClienteID == cliente)
                    {
                        foreach (var prod in produccionActivos)
                        {
                            if (prod.PedidoID == item.PedidoID)
                            {
                                lista.Add(item);
                            }

                        }
                    }
                }

                detalleEntregaViewModel.Pedidos = lista;
                return PartialView(detalleEntregaViewModel);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index", "Entregas");
            }

           
        }
    }
}