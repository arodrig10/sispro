﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing.Design;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;

namespace Frontend.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class UsuariosController : Controller
    {
        private UsuarioViewModel Convertir(UsuariosActivos usuario)
        {
            return new UsuarioViewModel
            {
                UsuarioID = usuario.UsuarioID,
                Identificacion = usuario.Identificacion,
                UserName = usuario.UserName,
                Password = usuario.Password,
                NombreCompleto = usuario.NombreCompleto,
                Telefono = usuario.Telefono,
                Correo = usuario.Correo,
                Direccion = usuario.Direccion,
                Estado = usuario.Estado
            };
        }
        private UsuarioViewModel Convertir(Usuarios usuario)
        {
            return new UsuarioViewModel
            {
                UsuarioID = usuario.UsuarioID,
                Identificacion = usuario.Identificacion,
                UserName = usuario.UserName,
                Password = usuario.Password,
                NombreCompleto = usuario.NombreCompleto,
                Telefono = usuario.Telefono,
                Correo = usuario.Correo,
                Direccion = usuario.Direccion,
                Estado = usuario.Estado
            };
        }
        private Usuarios Convertir(UsuarioViewModel usuarioViewModel)
        {
            return new Usuarios
            {
                UsuarioID = usuarioViewModel.UsuarioID,
                Identificacion = usuarioViewModel.Identificacion,
                UserName = usuarioViewModel.UserName,
                Password = usuarioViewModel.Password,
                NombreCompleto = usuarioViewModel.NombreCompleto,
                Telefono = usuarioViewModel.Telefono,
                Correo = usuarioViewModel.Correo,
                Direccion = usuarioViewModel.Direccion,
                Estado = usuarioViewModel.Estado
            };
        }
        public ActionResult Index()
        {
            try
            {
                List<UsuariosActivos> usuario;
                using (UnidadDeTrabajo<UsuariosActivos> Unidad = new UnidadDeTrabajo<UsuariosActivos>(new BDContext()))
                {
                    usuario = Unidad.genericDAL.GetAll().ToList();
                }
                List<UsuarioViewModel> lista = new List<UsuarioViewModel>();
                UsuarioViewModel usuarioViewModel;
                foreach (var item in usuario)
                {
                    usuarioViewModel = Convertir(item);
                    lista.Add(usuarioViewModel);
                }
                ViewBag.Message = BLLUtilitarios.msj;
                BLLUtilitarios.msj = null;
                return View(lista);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener lista";
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult Create()
        {
            try
            {
                UsuarioViewModel usuario = new UsuarioViewModel { };
                using (UnidadDeTrabajo<Roles> unidad = new UnidadDeTrabajo<Roles>(new BDContext()))
                {
                    usuario.Roles = unidad.genericDAL.GetAll();
                    unidad.Complete();
                }
                usuario.Estado = true;
                usuario.NewPassword = "null";
                usuario.codigoVerificación = "null";
                return View(usuario);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Create(UsuarioViewModel usuarioViewModel)
        {
            try
            {
                using (BDContext context = new BDContext())
                {
                    if (context.Usuarios.Where(c => c.UserName == usuarioViewModel.UserName).Count() == 0)
                    {
                        if (context.Usuarios.Where(c => c.Correo == usuarioViewModel.Correo).Count() == 0)
                        {
                            if (BLLUtilitarios.PasswordValida(usuarioViewModel.Password))
                            {
                                if (usuarioViewModel.Password == usuarioViewModel.RepeatPassword)
                                {
                                    usuarioViewModel.Password = BLLUtilitarios.getHashSha256(usuarioViewModel.Password);
                                    Usuarios usuario = Convertir(usuarioViewModel);
                                    Roles rol;
                                    int nose = Convert.ToInt16(Request.Form["Roles"].FirstOrDefault().ToString());
                                    usuario.Roles.Add(context.Roles.FirstOrDefault(c => c.RoleID == nose));
                                    usuario.Estado = true;
                                    context.Usuarios.Add(usuario);
                                    context.SaveChanges();

                                   /* BLLUtilitarios.msj = "Se ha agregado un nuevo usuario 1";*/

                                    TempData["TipoMensaje"] = "alert alert-success";
                                    TempData["Mensaje"] = "Usuario creado: " + usuarioViewModel.Identificacion;
                                    return RedirectToAction("Index");
                                }
                                else
                                {
                                    using (UnidadDeTrabajo<Roles> unidad = new UnidadDeTrabajo<Roles>(new BDContext()))
                                    {
                                        usuarioViewModel.Roles = unidad.genericDAL.GetAll();
                                        unidad.Complete();
                                    }
                                    TempData["TipoMensaje"] = "alert alert-danger";
                                    TempData["Mensaje"] = "Repita la contraseña";
                                    return View("Create", usuarioViewModel);
                                }
                            }
                            else
                            {
                                using (UnidadDeTrabajo<Roles> unidad = new UnidadDeTrabajo<Roles>(new BDContext()))
                                {
                                    usuarioViewModel.Roles = unidad.genericDAL.GetAll();
                                    unidad.Complete();
                                }
                                TempData["TipoMensaje"] = "alert alert-danger";
                                TempData["Mensaje"] = "No cumple politicas: " + BLLUtilitarios.MensajePoliticasPassword();
                                return View("Create", usuarioViewModel);
                            }
                        }
                        else
                        {
                            using (UnidadDeTrabajo<Roles> unidad = new UnidadDeTrabajo<Roles>(new BDContext()))
                            {
                                usuarioViewModel.Roles = unidad.genericDAL.GetAll();
                                unidad.Complete();
                            }
                            TempData["TipoMensaje"] = "alert alert-danger";
                            TempData["Mensaje"] = "Correo ya registrado";
                            return View("Create", usuarioViewModel);
                        }
                    }
                    else
                    {
                        using (UnidadDeTrabajo<Roles> unidad = new UnidadDeTrabajo<Roles>(new BDContext()))
                        {
                            usuarioViewModel.Roles = unidad.genericDAL.GetAll();
                            unidad.Complete();
                        }
                        TempData["TipoMensaje"] = "alert alert-danger";
                        TempData["Mensaje"] = "Usuario ya existe";
                        return View("Create", usuarioViewModel);
                    }
                }
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al agregar usuario";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    Usuarios usuario;
                    int RoleID;
                    using (UnidadDeTrabajo<Usuarios> unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                    {
                        usuario = unidad.genericDAL.Get((int)id);
                        if (!usuario.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                        RoleID = usuario.Roles.FirstOrDefault().RoleID;
                    }
                    UsuarioViewModel usuarios = Convertir(usuario);
                    using (UnidadDeTrabajo<Roles> unidad = new UnidadDeTrabajo<Roles>(new BDContext()))
                    {
                        usuarios.Roles = unidad.genericDAL.GetAll();
                    }
                    usuarios.RoleID = RoleID;
                    return View(usuarios);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult Edit(UsuarioViewModel usuarioViewModel)
        {
            try
            {
                Roles rol;
                Usuarios usuario;
                using (UnidadDeTrabajo<Usuarios> unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                {
                    usuario = unidad.genericDAL.Get(usuarioViewModel.UsuarioID);
                    usuario.Identificacion = usuarioViewModel.Identificacion;
                    usuario.NombreCompleto = usuarioViewModel.NombreCompleto;
                    usuario.Telefono = usuarioViewModel.Telefono;
                    usuario.Correo = usuarioViewModel.Correo;
                    usuario.Direccion = usuario.Direccion;
                    unidad.genericDAL.Update(usuario);
                    unidad.Complete();
                }
                using (BDContext context = new BDContext())
                {
                    rol = context.Set<Roles>().Find(usuarioViewModel.RoleID);
                    context.sp_setSpecificRoleForUser(usuarioViewModel.UsuarioID, rol.RoleID);
                    context.SaveChanges();
                }
                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Usuario modificado: " + usuarioViewModel.Identificacion;
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al editar usuario";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Details(int? id)
        {
            try
            {
                if (id != null)
                {
                    Usuarios usuario;
                    using (UnidadDeTrabajo<Usuarios> unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                    {
                        usuario = unidad.genericDAL.Get((int)id);
                        if (!usuario.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    UsuarioViewModel usuarioViewModel = Convertir(usuario);
                    return View(usuarioViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver detalles";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    Usuarios usuario;
                    using (UnidadDeTrabajo<Usuarios> unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                    {
                        usuario = unidad.genericDAL.Get((int)id);
                        if (!usuario.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    UsuarioViewModel usuarioViewModel = Convertir(usuario);
                    return View(usuarioViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Delete(UsuarioViewModel usuarioViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<Usuarios> unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                {
                    Usuarios usuarios = unidad.genericDAL.Get(usuarioViewModel.UsuarioID);
                    usuarios.Estado = false;
                    unidad.genericDAL.Update(usuarios);
                    unidad.Complete();
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Usuario eliminado: " + usuarios.Identificacion;
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        public ActionResult ChangePassword(int? id)
        {
            try
            {
                if (id != null)
                {
                    Usuarios usuario;
                    using (UnidadDeTrabajo<Usuarios> unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                    {
                        usuario = unidad.genericDAL.Get((int)id);
                        if (!usuario.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    UsuarioViewModel usuarios = Convertir(usuario);
                    using (UnidadDeTrabajo<Roles> unidad = new UnidadDeTrabajo<Roles>(new BDContext()))
                    {
                        usuarios.Roles = unidad.genericDAL.GetAll();
                        unidad.Complete();
                    }
                    usuarios.Password = "";
                    return View(usuarios);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult ChangePassword(UsuarioViewModel usuarioViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<Usuarios> unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                {
                    Usuarios usuario = unidad.genericDAL.Get(usuarioViewModel.UsuarioID);

                    if (BLLUtilitarios.getHashSha256(usuarioViewModel.Password) == usuario.Password)
                    {
                        if (usuarioViewModel.NewPassword == usuarioViewModel.RepeatPassword)
                        {
                            if (BLLUtilitarios.PasswordValida(usuarioViewModel.NewPassword))
                            {
                                usuario.Password = BLLUtilitarios.getHashSha256(usuarioViewModel.NewPassword);
                                unidad.genericDAL.Update(usuario);
                                unidad.Complete();
                                TempData["TipoMensaje"] = "alert alert-success";
                                TempData["Mensaje"] = "Cambio de contraseña correcto";
                                return RedirectToAction("Index");
                            }
                            TempData["TipoMensaje"] = "alert alert-danger";
                            TempData["MensajeError"] = "No cumple politicas: " + BLLUtilitarios.MensajePoliticasPassword();
                            // return RedirectToAction("ChangePassword", new { @id = usuario.UsuarioID });
                            return View("ChangePassword", usuarioViewModel);
                        }
                        TempData["TipoMensaje"] = "alert alert-danger";
                        TempData["MensajeError"] = "Repita la contraseña";
                        //  return RedirectToAction("ChangePassword", new { @id = usuario.UsuarioID });
                        return View("ChangePassword", usuarioViewModel);
                    }
                    TempData["TipoMensaje"] = "alert alert-danger";
                    TempData["MensajeError"] = "Contraseña incorrecta";
                    //  return RedirectToAction("ChangePassword", new { @id = usuario.UsuarioID });
                    return View("ChangePassword", usuarioViewModel);
                }

            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}
