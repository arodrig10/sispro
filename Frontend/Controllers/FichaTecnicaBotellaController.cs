﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;

namespace Frontend.Controllers
{
    [Authorize]
    public class FichaTecnicaBotellaController : Controller
    {
        private FichaTecnicaBotellaViewModel Convertir(FichaTecnicaBotellaActivos fichaTecnicaBotella)
        {
            return new FichaTecnicaBotellaViewModel
            {
                FichaTecnicaBotellaID = fichaTecnicaBotella.FichaTecnicaBotellaID,
                ProductoID = fichaTecnicaBotella.ProductoID,
                Altura = fichaTecnicaBotella.Altura,
                DiametroMayor = fichaTecnicaBotella.DiametroMayor,
                DiametroMenor = fichaTecnicaBotella.DiametroMenor,
                DiametroCuello = fichaTecnicaBotella.DiametroCuello,
                CapacidadVolumetrica = fichaTecnicaBotella.CapacidadVolumetrica,
                Tolerancia = fichaTecnicaBotella.Tolerancia,
                ImagenCuerpo = fichaTecnicaBotella.ImagenCuerpo,
                ImagenFondo = fichaTecnicaBotella.ImagenFondo,
                Estado = fichaTecnicaBotella.Estado
            };
        }


        private FichaTecnicaBotellaViewModel Convertir(FichaTecnicaBotella fichaTecnicaBotella)
        {
            return new FichaTecnicaBotellaViewModel
            {
                FichaTecnicaBotellaID = fichaTecnicaBotella.FichaTecnicaBotellaID,
                ProductoID = fichaTecnicaBotella.ProductoID,
                Altura = fichaTecnicaBotella.Altura,
                DiametroMayor = fichaTecnicaBotella.DiametroMayor,
                DiametroMenor = fichaTecnicaBotella.DiametroMenor,
                DiametroCuello = fichaTecnicaBotella.DiametroCuello,
                CapacidadVolumetrica = fichaTecnicaBotella.CapacidadVolumetrica,
                Tolerancia = fichaTecnicaBotella.Tolerancia,
                ImagenCuerpo = fichaTecnicaBotella.ImagenCuerpo,
                ImagenFondo = fichaTecnicaBotella.ImagenFondo,
                Estado = fichaTecnicaBotella.Estado
            };
        }
        private FichaTecnicaBotella Convertir(FichaTecnicaBotellaViewModel fichaTecnicaBotellaViewModel)
        {
            return new FichaTecnicaBotella
            {
                FichaTecnicaBotellaID = fichaTecnicaBotellaViewModel.FichaTecnicaBotellaID,
                ProductoID = fichaTecnicaBotellaViewModel.ProductoID,
                Altura = fichaTecnicaBotellaViewModel.Altura,
                DiametroMayor = fichaTecnicaBotellaViewModel.DiametroMayor,
                DiametroMenor = fichaTecnicaBotellaViewModel.DiametroMenor,
                DiametroCuello = fichaTecnicaBotellaViewModel.DiametroCuello,
                CapacidadVolumetrica = fichaTecnicaBotellaViewModel.CapacidadVolumetrica,
                Tolerancia = fichaTecnicaBotellaViewModel.Tolerancia,
                ImagenCuerpo = fichaTecnicaBotellaViewModel.ImagenCuerpo,
                ImagenFondo = fichaTecnicaBotellaViewModel.ImagenFondo,
                Estado = fichaTecnicaBotellaViewModel.Estado
            };
        }
        public ActionResult Index()
        {
            try
            {
                List<FichaTecnicaBotellaActivos> fichaTecnicaBotellas;
                using (UnidadDeTrabajo<FichaTecnicaBotellaActivos> Unidad = new UnidadDeTrabajo<FichaTecnicaBotellaActivos>(new BDContext()))
                {
                    fichaTecnicaBotellas = Unidad.genericDAL.GetAll().ToList();
                }
                List<FichaTecnicaBotellaViewModel> lista = new List<FichaTecnicaBotellaViewModel>();
                FichaTecnicaBotellaViewModel fichaTecnicaBotellaViewModel;
                foreach (var item in fichaTecnicaBotellas)
                {
                    fichaTecnicaBotellaViewModel = Convertir(item);
                    using (UnidadDeTrabajo<Productos> Unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        fichaTecnicaBotellaViewModel.Producto = Unidad.genericDAL.Get(fichaTecnicaBotellaViewModel.ProductoID);
                    }
                    lista.Add(fichaTecnicaBotellaViewModel);
                }
                //Mostrar mensajes
                ViewBag.Message = BLLUtilitarios.msj;
                BLLUtilitarios.msj = null;
                return View(lista);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener lista";
                return RedirectToAction("Index", "Home");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            try
            {
                FichaTecnicaBotellaViewModel fichaTecnicaBotellaViewModel = new FichaTecnicaBotellaViewModel { };
                using (UnidadDeTrabajo<ProductosBotellaActivos> unidad = new UnidadDeTrabajo<ProductosBotellaActivos>(new BDContext()))
                {
                    fichaTecnicaBotellaViewModel.Productos = unidad.genericDAL.GetAll().ToList();
                }
                return View(fichaTecnicaBotellaViewModel);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Create(FichaTecnicaBotellaViewModel fichaTecnicaBotellaViewModel)
        {
            try
            {
                bool a;
                fichaTecnicaBotellaViewModel.Estado = true;
                if (fichaTecnicaBotellaViewModel.ArchivoImagenCuerpo != null)
                {
                    string name = Path.GetFileNameWithoutExtension(fichaTecnicaBotellaViewModel.ArchivoImagenCuerpo.FileName);
                    string extension = Path.GetExtension(fichaTecnicaBotellaViewModel.ArchivoImagenCuerpo.FileName);
                    name = name + DateTime.Now.ToString("yyyymmssfff") + extension;
                    fichaTecnicaBotellaViewModel.ImagenCuerpo = "~/Archivos/FichaTecnicaBotella/" + name;
                    name = Path.Combine(Server.MapPath("~/Archivos/FichaTecnicaBotella/"), name);
                    fichaTecnicaBotellaViewModel.ArchivoImagenCuerpo.SaveAs(name);
                }
                if (fichaTecnicaBotellaViewModel.ArchivoImagenFondo != null)
                {
                    string name = Path.GetFileNameWithoutExtension(fichaTecnicaBotellaViewModel.ArchivoImagenFondo.FileName);
                    string extension = Path.GetExtension(fichaTecnicaBotellaViewModel.ArchivoImagenFondo.FileName);
                    name = name + DateTime.Now.ToString("yyyymmssfff") + extension;
                    fichaTecnicaBotellaViewModel.ImagenFondo = "~/Archivos/FichaTecnicaBotella/" + name;
                    name = Path.Combine(Server.MapPath("~/Archivos/FichaTecnicaBotella/"), name);
                    fichaTecnicaBotellaViewModel.ArchivoImagenFondo.SaveAs(name);
                }


                FichaTecnicaBotella fichaTecnicaBotella = Convertir(fichaTecnicaBotellaViewModel);
                using (UnidadDeTrabajo<FichaTecnicaBotella> unidad = new UnidadDeTrabajo<FichaTecnicaBotella>(new BDContext()))
                {
                    unidad.genericDAL.Add(fichaTecnicaBotella);
                    a = unidad.Complete();
                }
                //Mostrar El mensaje 
             /*  if (a)
                {
                 
                }
                else
                {

                  
                }*/

                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Ficha tecnica creada";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al agregar ficha tecnica";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    FichaTecnicaBotella fichaTecnicaBotella;
                    using (UnidadDeTrabajo<FichaTecnicaBotella> unidad = new UnidadDeTrabajo<FichaTecnicaBotella>(new BDContext()))
                    {
                        fichaTecnicaBotella = unidad.genericDAL.Get((int)id);
                        if (!fichaTecnicaBotella.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    FichaTecnicaBotellaViewModel fichaTecnicaBotellaViewModel = Convertir(fichaTecnicaBotella);
                    List<ProductosBotellaActivos> productos;
                    using (UnidadDeTrabajo<ProductosBotellaActivos> unidad = new UnidadDeTrabajo<ProductosBotellaActivos>(new BDContext()))
                    {
                        productos = unidad.genericDAL.GetAll().ToList();
                    }
                    fichaTecnicaBotellaViewModel.Productos = productos;
                    return View(fichaTecnicaBotellaViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Edit(FichaTecnicaBotellaViewModel fichaTecnicaBotellaViewModel)
        {
            try
            {
                fichaTecnicaBotellaViewModel.Estado = true;
                if (fichaTecnicaBotellaViewModel.ArchivoImagenCuerpo != null)
                {
                    string name = Path.GetFileNameWithoutExtension(fichaTecnicaBotellaViewModel.ArchivoImagenCuerpo.FileName);
                    string extension = Path.GetExtension(fichaTecnicaBotellaViewModel.ArchivoImagenCuerpo.FileName);
                    name = name + DateTime.Now.ToString("yyyymmssfff") + extension;
                    fichaTecnicaBotellaViewModel.ImagenCuerpo = "~/Archivos/FichaTecnicaBotella/" + name;
                    name = Path.Combine(Server.MapPath("~/Archivos/FichaTecnicaBotella/"), name);
                    fichaTecnicaBotellaViewModel.ArchivoImagenCuerpo.SaveAs(name);
                }
                if (fichaTecnicaBotellaViewModel.ArchivoImagenFondo != null)
                {
                    string name = Path.GetFileNameWithoutExtension(fichaTecnicaBotellaViewModel.ArchivoImagenFondo.FileName);
                    string extension = Path.GetExtension(fichaTecnicaBotellaViewModel.ArchivoImagenFondo.FileName);
                    name = name + DateTime.Now.ToString("yyyymmssfff") + extension;
                    fichaTecnicaBotellaViewModel.ImagenFondo = "~/Archivos/FichaTecnicaBotella/" + name;
                    name = Path.Combine(Server.MapPath("~/Archivos/FichaTecnicaBotella/"), name);
                    fichaTecnicaBotellaViewModel.ArchivoImagenFondo.SaveAs(name);
                }
                using (UnidadDeTrabajo<FichaTecnicaBotella> unidad = new UnidadDeTrabajo<FichaTecnicaBotella>(new BDContext()))
                {
                    unidad.genericDAL.Update(Convertir(fichaTecnicaBotellaViewModel));
                    unidad.Complete();
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Ficha tecnica modificada";
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al editar ficha tecnica";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Details(int? id)
        {
            try
            {
                if (id != null)
                {
                    FichaTecnicaBotella fichaTecnicaBotella;
                    using (UnidadDeTrabajo<FichaTecnicaBotella> unidad = new UnidadDeTrabajo<FichaTecnicaBotella>(new BDContext()))
                    {
                        fichaTecnicaBotella = unidad.genericDAL.Get((int)id);
                        if (!fichaTecnicaBotella.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    FichaTecnicaBotellaViewModel fichaTecnicaBotellaViewModel = Convertir(fichaTecnicaBotella);
                    using (UnidadDeTrabajo<Productos> unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        fichaTecnicaBotellaViewModel.Producto = unidad.genericDAL.Get(fichaTecnicaBotellaViewModel.ProductoID);
                    };
                    return View(fichaTecnicaBotellaViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver detalles";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    FichaTecnicaBotella fichaTecnicaBotella;
                    using (UnidadDeTrabajo<FichaTecnicaBotella> unidad = new UnidadDeTrabajo<FichaTecnicaBotella>(new BDContext()))
                    {
                        fichaTecnicaBotella = unidad.genericDAL.Get((int)id);
                        if (!fichaTecnicaBotella.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    FichaTecnicaBotellaViewModel fichaTecnicaBotellaViewModel = Convertir(fichaTecnicaBotella);
                    using (UnidadDeTrabajo<Productos> unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {

                        fichaTecnicaBotellaViewModel.Producto = unidad.genericDAL.Get(fichaTecnicaBotellaViewModel.ProductoID);
                    };
                    return View(fichaTecnicaBotellaViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Delete(FichaTecnicaBotellaViewModel fichaTecnicaBotellaViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<FichaTecnicaBotella> unidad = new UnidadDeTrabajo<FichaTecnicaBotella>(new BDContext()))
                {
                    FichaTecnicaBotella fichaTecnicaBotella = unidad.genericDAL.Get(fichaTecnicaBotellaViewModel.FichaTecnicaBotellaID);
                    fichaTecnicaBotella.Estado = false;
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Ficha tecnica eliminada";
                    unidad.genericDAL.Update(fichaTecnicaBotella);
                    unidad.Complete();
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al eliminar ficha tecnica";
                return RedirectToAction("Index");
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}
