﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;

namespace Frontend.Controllers
{
    [Authorize]
    public class FichaTecnicaTapaController : Controller
    {
        private FichaTecnicaTapaViewModel Convertir(FichaTecnicaTapaActivos fichaTecnicaTapa)
        {
            return new FichaTecnicaTapaViewModel
            {
                FichaTecnicaTapaID = fichaTecnicaTapa.FichaTecnicaTapaID,
                ProductoID = fichaTecnicaTapa.ProductoID,
                PesoPieza = fichaTecnicaTapa.PesoPieza,
                Altura = fichaTecnicaTapa.Altura,
                DiametroInterno = fichaTecnicaTapa.DiametroInterno,
                DiametroExternoSelloOliva = fichaTecnicaTapa.DiametroExternoSelloOliva,
                AlturaSelloOliva = fichaTecnicaTapa.AlturaSelloOliva,
                DiametroInternoBandaSeguridad = fichaTecnicaTapa.DiametroInternoBandaSeguridad,
                EspesorPanel = fichaTecnicaTapa.EspesorPanel,
                NumeroEstrias = fichaTecnicaTapa.NumeroEstrias,
                TorqueCierreRecomendado = fichaTecnicaTapa.TorqueCierreRecomendado,
                TorqueApertura = fichaTecnicaTapa.TorqueApertura,
                Tolerancia = fichaTecnicaTapa.Tolerancia,
                Imagen = fichaTecnicaTapa.Imagen,
                Estado = fichaTecnicaTapa.Estado
            };
        }
        private FichaTecnicaTapaViewModel Convertir(FichaTecnicaTapa fichaTecnicaTapa)
        {
            return new FichaTecnicaTapaViewModel
            {
                FichaTecnicaTapaID = fichaTecnicaTapa.FichaTecnicaTapaID,
                ProductoID = fichaTecnicaTapa.ProductoID,
                PesoPieza = fichaTecnicaTapa.PesoPieza,
                Altura = fichaTecnicaTapa.Altura,
                DiametroInterno = fichaTecnicaTapa.DiametroInterno,
                DiametroExternoSelloOliva = fichaTecnicaTapa.DiametroExternoSelloOliva,
                AlturaSelloOliva = fichaTecnicaTapa.AlturaSelloOliva,
                DiametroInternoBandaSeguridad = fichaTecnicaTapa.DiametroInternoBandaSeguridad,
                EspesorPanel = fichaTecnicaTapa.EspesorPanel,
                NumeroEstrias = fichaTecnicaTapa.NumeroEstrias,
                TorqueCierreRecomendado = fichaTecnicaTapa.TorqueCierreRecomendado,
                TorqueApertura = fichaTecnicaTapa.TorqueApertura,
                Tolerancia = fichaTecnicaTapa.Tolerancia,
                Imagen = fichaTecnicaTapa.Imagen,
                Estado = fichaTecnicaTapa.Estado
            };
        }
        private FichaTecnicaTapa Convertir(FichaTecnicaTapaViewModel fichaTecnicaTapaViewModel)
        {
            return new FichaTecnicaTapa
            {
                FichaTecnicaTapaID = fichaTecnicaTapaViewModel.FichaTecnicaTapaID,
                ProductoID = fichaTecnicaTapaViewModel.ProductoID,
                PesoPieza = fichaTecnicaTapaViewModel.PesoPieza,
                Altura = fichaTecnicaTapaViewModel.Altura,
                DiametroInterno = fichaTecnicaTapaViewModel.DiametroInterno,
                DiametroExternoSelloOliva = fichaTecnicaTapaViewModel.DiametroExternoSelloOliva,
                AlturaSelloOliva = fichaTecnicaTapaViewModel.AlturaSelloOliva,
                DiametroInternoBandaSeguridad = fichaTecnicaTapaViewModel.DiametroInternoBandaSeguridad,
                EspesorPanel = fichaTecnicaTapaViewModel.EspesorPanel,
                NumeroEstrias = fichaTecnicaTapaViewModel.NumeroEstrias,
                TorqueCierreRecomendado = fichaTecnicaTapaViewModel.TorqueCierreRecomendado,
                TorqueApertura = fichaTecnicaTapaViewModel.TorqueApertura,
                Tolerancia = fichaTecnicaTapaViewModel.Tolerancia,
                Imagen = fichaTecnicaTapaViewModel.Imagen,
                Estado = fichaTecnicaTapaViewModel.Estado
            };
        }
        public ActionResult Index()
        {
            try
            {
                List<FichaTecnicaTapaActivos> fichaTecnicaTapas;
                using (UnidadDeTrabajo<FichaTecnicaTapaActivos> Unidad = new UnidadDeTrabajo<FichaTecnicaTapaActivos>(new BDContext()))
                {
                    fichaTecnicaTapas = Unidad.genericDAL.GetAll().ToList();
                }
                List<FichaTecnicaTapaViewModel> lista = new List<FichaTecnicaTapaViewModel>();
                FichaTecnicaTapaViewModel fichaTecnicaTapaViewModel;
                foreach (var item in fichaTecnicaTapas)
                {
                    fichaTecnicaTapaViewModel = Convertir(item);
                    using (UnidadDeTrabajo<Productos> Unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        fichaTecnicaTapaViewModel.Producto = Unidad.genericDAL.Get(fichaTecnicaTapaViewModel.ProductoID);
                    }
                    lista.Add(fichaTecnicaTapaViewModel);
                }
                //Mostrar mensajes
                ViewBag.Message = BLLUtilitarios.msj;
                BLLUtilitarios.msj = null;
                return View(lista);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener lista";
                return RedirectToAction("Index", "Home");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            try
            {
                FichaTecnicaTapaViewModel fichaTecnicaTapaViewModel = new FichaTecnicaTapaViewModel { };
                using (UnidadDeTrabajo<ProductosTapaActivos> unidad = new UnidadDeTrabajo<ProductosTapaActivos>(new BDContext()))
                {
                    fichaTecnicaTapaViewModel.Productos = unidad.genericDAL.GetAll().ToList();
                }
                return View(fichaTecnicaTapaViewModel);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Create(FichaTecnicaTapaViewModel fichaTecnicaTapaViewModel)
        {
            try
            {
                bool a;
                fichaTecnicaTapaViewModel.Estado = true;
                if (fichaTecnicaTapaViewModel.ArchivoImagen != null)
                {
                    string name = Path.GetFileNameWithoutExtension(fichaTecnicaTapaViewModel.ArchivoImagen.FileName);
                    string extension = Path.GetExtension(fichaTecnicaTapaViewModel.ArchivoImagen.FileName);
                    name = name + DateTime.Now.ToString("yyyymmssfff") + extension;
                    fichaTecnicaTapaViewModel.Imagen = "~/Archivos/FichaTecnicaTapa/" + name;
                    name = Path.Combine(Server.MapPath("~/Archivos/FichaTecnicaTapa/"), name);
                    fichaTecnicaTapaViewModel.ArchivoImagen.SaveAs(name);
                }
                FichaTecnicaTapa fichaTecnicaTapa = Convertir(fichaTecnicaTapaViewModel);
                using (UnidadDeTrabajo<FichaTecnicaTapa> unidad = new UnidadDeTrabajo<FichaTecnicaTapa>(new BDContext()))
                {
                    unidad.genericDAL.Add(fichaTecnicaTapa);
                    a = unidad.Complete();
                }
                //Mostrar El mensaje 
              /*  if (a)
                {
                    BLLUtilitarios.msj = "Se ha agregado una nueva ficha tecnica de tapa 1";
                }
                else
                {

                    BLLUtilitarios.msj = "Error al agregar ficha tecnica de tapa en la base de datos";
                }*/

                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Ficha tecnica creada";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al agregar ficha tecnica";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    FichaTecnicaTapa fichaTecnicaTapa;
                    using (UnidadDeTrabajo<FichaTecnicaTapa> unidad = new UnidadDeTrabajo<FichaTecnicaTapa>(new BDContext()))
                    {
                        fichaTecnicaTapa = unidad.genericDAL.Get((int)id);
                        if (!fichaTecnicaTapa.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    FichaTecnicaTapaViewModel fichaTecnicaTapaViewModel = Convertir(fichaTecnicaTapa);
                    List<ProductosTapaActivos> productos;
                    using (UnidadDeTrabajo<ProductosTapaActivos> unidad = new UnidadDeTrabajo<ProductosTapaActivos>(new BDContext()))
                    {
                        productos = unidad.genericDAL.GetAll().ToList();
                    }
                    fichaTecnicaTapaViewModel.Productos = productos;
                    return View(fichaTecnicaTapaViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Edit(FichaTecnicaTapaViewModel fichaTecnicaTapaViewModel)
        {
            try
            {
                fichaTecnicaTapaViewModel.Estado = true;
                if (fichaTecnicaTapaViewModel.ArchivoImagen != null)
                {
                    string name = Path.GetFileNameWithoutExtension(fichaTecnicaTapaViewModel.ArchivoImagen.FileName);
                    string extension = Path.GetExtension(fichaTecnicaTapaViewModel.ArchivoImagen.FileName);
                    name = name + DateTime.Now.ToString("yyyymmssfff") + extension;
                    fichaTecnicaTapaViewModel.Imagen = "~/Archivos/FichaTecnicaTapa/" + name;
                    name = Path.Combine(Server.MapPath("~/Archivos/FichaTecnicaTapa/"), name);
                    fichaTecnicaTapaViewModel.ArchivoImagen.SaveAs(name);
                }
                using (UnidadDeTrabajo<FichaTecnicaTapa> unidad = new UnidadDeTrabajo<FichaTecnicaTapa>(new BDContext()))
                {
                    unidad.genericDAL.Update(Convertir(fichaTecnicaTapaViewModel));
                    unidad.Complete();
                }
                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Ficha tecnica modificada";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al editar ficha tecnica";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Details(int? id)
        {
            try
            {
                if (id != null)
                {
                    FichaTecnicaTapa fichaTecnicaTapa;
                    using (UnidadDeTrabajo<FichaTecnicaTapa> unidad = new UnidadDeTrabajo<FichaTecnicaTapa>(new BDContext()))
                    {
                        fichaTecnicaTapa = unidad.genericDAL.Get((int)id);
                        if (!fichaTecnicaTapa.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    FichaTecnicaTapaViewModel fichaTecnicaTapaViewModel = Convertir(fichaTecnicaTapa);
                    using (UnidadDeTrabajo<Productos> unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        fichaTecnicaTapaViewModel.Producto = unidad.genericDAL.Get(fichaTecnicaTapaViewModel.ProductoID);
                    };
                    return View(fichaTecnicaTapaViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver detalles";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    FichaTecnicaTapa fichaTecnicaTapa;
                    using (UnidadDeTrabajo<FichaTecnicaTapa> unidad = new UnidadDeTrabajo<FichaTecnicaTapa>(new BDContext()))
                    {
                        fichaTecnicaTapa = unidad.genericDAL.Get((int)id);
                    }
                    FichaTecnicaTapaViewModel fichaTecnicaTapaViewModel = Convertir(fichaTecnicaTapa);
                    using (UnidadDeTrabajo<Productos> unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {

                        fichaTecnicaTapaViewModel.Producto = unidad.genericDAL.Get(fichaTecnicaTapaViewModel.ProductoID);
                    };
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Ficha tecnica eliminada";
                    return View(fichaTecnicaTapaViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Delete(FichaTecnicaTapaViewModel fichaTecnicaTapaViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<FichaTecnicaTapa> unidad = new UnidadDeTrabajo<FichaTecnicaTapa>(new BDContext()))
                {
                    FichaTecnicaTapa fichaTecnicaTapa = unidad.genericDAL.Get(fichaTecnicaTapaViewModel.FichaTecnicaTapaID);
                    fichaTecnicaTapa.Estado = false;
                    unidad.genericDAL.Update(fichaTecnicaTapa);
                    unidad.Complete();
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al eliminar ficha tecnica";
                return RedirectToAction("Index");
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}
