﻿using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Frontend.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class ProduccionController : Controller
    {
        private ProduccionViewModel Convertir(ProduccionActivos produccion)
        {
            return new ProduccionViewModel
            {
                ProduccionID = produccion.ProduccionID,
                PedidoID = produccion.PedidoID,
                FechaInicio = produccion.FechaInicio,
                FechaFin = produccion.FechaFin,
                HorasTrabajadas = produccion.HorasTrabajadas,
                CantidadMerma = produccion.CantidadMerma,
                CantidadProduccionTotal = produccion.CantidadProduccionTotal,
                CantidadPaquetesCajas = produccion.CantidadPaquetesCajas,
                Estado = produccion.Estado
            };
        }

        private ProduccionViewModel Convertir(Produccion produccion)
        {
            return new ProduccionViewModel
            {
                ProduccionID = produccion.ProduccionID,
                PedidoID = produccion.PedidoID,
                FechaInicio = produccion.FechaInicio,
                FechaFin = produccion.FechaFin,
                HorasTrabajadas = produccion.HorasTrabajadas,
                CantidadMerma = produccion.CantidadMerma,
                CantidadProduccionTotal = produccion.CantidadProduccionTotal,
                CantidadPaquetesCajas = produccion.CantidadPaquetesCajas,
                Estado = produccion.Estado
            };
        }

        private Produccion Convertir(ProduccionViewModel produccion)
        {
            return new Produccion
            {
                ProduccionID = produccion.ProduccionID,
                PedidoID = produccion.PedidoID,
                FechaInicio = produccion.FechaInicio,
                FechaFin = produccion.FechaFin,
                HorasTrabajadas = produccion.HorasTrabajadas,
                CantidadProduccionTotal = produccion.CantidadProduccionTotal,
                CantidadPaquetesCajas = produccion.CantidadPaquetesCajas,
                Estado = produccion.Estado
            };
        }


        public ActionResult Index()
        {
            try
            {
                List<ProduccionActivos> produccion;
                using (UnidadDeTrabajo<ProduccionActivos> Unidad = new UnidadDeTrabajo<ProduccionActivos>(new BDContext()))
                {
                    produccion = Unidad.genericDAL.GetAll().ToList();
                }
                List<ProduccionViewModel> lista = new List<ProduccionViewModel>();
                ProduccionViewModel produccionViewModel;
                foreach (var item in produccion)
                {
                    produccionViewModel = Convertir(item);

                    using (UnidadDeTrabajo<Pedidos> UnidadCliente = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                    {
                        produccionViewModel.Pedido = UnidadCliente.genericDAL.Get(produccionViewModel.PedidoID);
                    }

                    lista.Add(produccionViewModel);
                }
                //Mostrar mensajes
                ViewBag.Message = BLLUtilitarios.msj;
                BLLUtilitarios.msj = null;
                return View(lista);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener lista";
                return RedirectToAction("Index", "Home");
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}