﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;
using Microsoft.Ajax.Utilities;

namespace Frontend.Controllers
{
    [Authorize]
    public class VariablesMaquinaInyectoraController : Controller
    {
        private VariablesMaquinaInyectoraViewModel Convertir(VariablesMaquinaInyectoraActivos variablesMaquinaInyectora)
        {
            return new VariablesMaquinaInyectoraViewModel
            {
                VariablesMaquinaInyectoraID = variablesMaquinaInyectora.VariablesMaquinaInyectoraID,
                MaquinaID = variablesMaquinaInyectora.MaquinaID,
                ProductoID = variablesMaquinaInyectora.ProductoID,
                MateriaPrimaPrincipalID = variablesMaquinaInyectora.MateriaPrimaPrincipalID,
                MateriaPrimaSecundariaID = variablesMaquinaInyectora.MateriaPrimaSecundariaID,
                CantidadPrimaPrincipal = variablesMaquinaInyectora.CantidadPrimaPrincipal,
                CantidadPrimaSecundaria = variablesMaquinaInyectora.CantidadPrimaSecundaria,
                CantidadPrimaTerciaria = variablesMaquinaInyectora.CantidadPrimaTerciaria,
                Estado = variablesMaquinaInyectora.Estado
            };
        }
        private VariablesMaquinaInyectoraViewModel Convertir(VariablesMaquinaInyectora variablesMaquinaInyectora)
        {
            return new VariablesMaquinaInyectoraViewModel
            {
                VariablesMaquinaInyectoraID = variablesMaquinaInyectora.VariablesMaquinaInyectoraID,
                MaquinaID = variablesMaquinaInyectora.MaquinaID,
                ProductoID = variablesMaquinaInyectora.ProductoID,
                MateriaPrimaPrincipalID = variablesMaquinaInyectora.MateriaPrimaPrincipalID,
                MateriaPrimaSecundariaID = variablesMaquinaInyectora.MateriaPrimaSecundariaID,
                CantidadPrimaPrincipal = variablesMaquinaInyectora.CantidadPrimaPrincipal,
                CantidadPrimaSecundaria = variablesMaquinaInyectora.CantidadPrimaSecundaria,
                CantidadPrimaTerciaria = variablesMaquinaInyectora.CantidadPrimaTerciaria,
                Estado = variablesMaquinaInyectora.Estado
            };
        }
        private VariablesMaquinaInyectora Convertir(VariablesMaquinaInyectoraViewModel variablesMaquinaInyectoraViewModel)
        {
            return new VariablesMaquinaInyectora
            {
                VariablesMaquinaInyectoraID = variablesMaquinaInyectoraViewModel.VariablesMaquinaInyectoraID,
                MaquinaID = variablesMaquinaInyectoraViewModel.MaquinaID,
                ProductoID = variablesMaquinaInyectoraViewModel.ProductoID,
                MateriaPrimaPrincipalID = variablesMaquinaInyectoraViewModel.MateriaPrimaPrincipalID,
                MateriaPrimaSecundariaID = variablesMaquinaInyectoraViewModel.MateriaPrimaSecundariaID,
                CantidadPrimaPrincipal = variablesMaquinaInyectoraViewModel.CantidadPrimaPrincipal,
                CantidadPrimaSecundaria = variablesMaquinaInyectoraViewModel.CantidadPrimaSecundaria,
                CantidadPrimaTerciaria = variablesMaquinaInyectoraViewModel.CantidadPrimaTerciaria,
                Estado = variablesMaquinaInyectoraViewModel.Estado
            };
        }
        public ActionResult Index()
        {
            try
            {
                List<VariablesMaquinaInyectoraActivos> variablesMaquinaInyectoras;
                using (UnidadDeTrabajo<VariablesMaquinaInyectoraActivos> Unidad = new UnidadDeTrabajo<VariablesMaquinaInyectoraActivos>(new BDContext()))
                {
                    variablesMaquinaInyectoras = Unidad.genericDAL.GetAll().ToList();
                }
                List<VariablesMaquinaInyectoraViewModel> lista = new List<VariablesMaquinaInyectoraViewModel>();
                VariablesMaquinaInyectoraViewModel variablesMaquinaInyectoraViewModel;
                foreach (var item in variablesMaquinaInyectoras)
                {
                    variablesMaquinaInyectoraViewModel = Convertir(item);
                    using (UnidadDeTrabajo<Maquinas> Unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
                    {
                        variablesMaquinaInyectoraViewModel.Maquina = Unidad.genericDAL.Get(variablesMaquinaInyectoraViewModel.MaquinaID);
                    }
                    using (UnidadDeTrabajo<Productos> Unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        variablesMaquinaInyectoraViewModel.Producto = Unidad.genericDAL.Get(variablesMaquinaInyectoraViewModel.ProductoID);
                    }
                    using (UnidadDeTrabajo<MateriaPrima> Unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                    {
                        variablesMaquinaInyectoraViewModel.MateriaPrima = Unidad.genericDAL.Get(variablesMaquinaInyectoraViewModel.MateriaPrimaPrincipalID);
                    }
                    if (variablesMaquinaInyectoraViewModel.MateriaPrimaSecundariaID != null)
                    {
                        using (UnidadDeTrabajo<MateriaPrima> Unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                        {
                            variablesMaquinaInyectoraViewModel.MateriaPrima1 = Unidad.genericDAL.Get((int)variablesMaquinaInyectoraViewModel.MateriaPrimaSecundariaID);
                        }
                    }
                    if (variablesMaquinaInyectoraViewModel.MateriaPrimaTerciariaID != null)
                    {
                        using (UnidadDeTrabajo<MateriaPrima> Unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                        {
                            variablesMaquinaInyectoraViewModel.MateriaPrima2 = Unidad.genericDAL.Get((int)variablesMaquinaInyectoraViewModel.MateriaPrimaTerciariaID);
                        }
                    }
                    lista.Add(variablesMaquinaInyectoraViewModel);
                }
                //Mostrar mensajes
                ViewBag.Message = BLLUtilitarios.msj;
                BLLUtilitarios.msj = null;
                return View(lista);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener lista";
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult Create()
        {
            try
            {
                VariablesMaquinaInyectoraViewModel variablesMaquinaInyectoraViewModel = new VariablesMaquinaInyectoraViewModel { };
                using (UnidadDeTrabajo<MaquinasInyectoraActivos> unidad = new UnidadDeTrabajo<MaquinasInyectoraActivos>(new BDContext()))
                {
                    variablesMaquinaInyectoraViewModel.Maquinas = unidad.genericDAL.GetAll().ToList();
                }
                using (UnidadDeTrabajo<ProductosTapaActivos> unidad = new UnidadDeTrabajo<ProductosTapaActivos>(new BDContext()))
                {
                    variablesMaquinaInyectoraViewModel.Productos = unidad.genericDAL.GetAll().ToList();
                }
                using (UnidadDeTrabajo<MateriaPrimaInyectoraActivos> unidad = new UnidadDeTrabajo<MateriaPrimaInyectoraActivos>(new BDContext()))
                {
                    variablesMaquinaInyectoraViewModel.MateriaPrimas = unidad.genericDAL.GetAll().ToList();
                }
                return View(variablesMaquinaInyectoraViewModel);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Create(VariablesMaquinaInyectoraViewModel variablesMaquinaInyectoraViewModel)
        {
            try
            {
                bool a;
                variablesMaquinaInyectoraViewModel.Estado = true;
                VariablesMaquinaInyectora variablesMaquinaInyectora = Convertir(variablesMaquinaInyectoraViewModel);
                using (UnidadDeTrabajo<VariablesMaquinaInyectora> unidad = new UnidadDeTrabajo<VariablesMaquinaInyectora>(new BDContext()))
                {
                    unidad.genericDAL.Add(variablesMaquinaInyectora);
                    a = unidad.Complete();
                }
                //Mostrar El mensaje 
               /* if (a)
                {
                    BLLUtilitarios.msj = "Se ha agregado las variables de la maquina inyectora 1";
                }
                else
                {

                    BLLUtilitarios.msj = "Error al agregar variables de maquina inyectora en la base de datos";
                }*/

                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Ficha de variables creada";

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al crear variables de maquina";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    VariablesMaquinaInyectora variablesMaquinaInyectora;
                    using (UnidadDeTrabajo<VariablesMaquinaInyectora> unidad = new UnidadDeTrabajo<VariablesMaquinaInyectora>(new BDContext()))
                    {
                        variablesMaquinaInyectora = unidad.genericDAL.Get((int)id);
                        if (!variablesMaquinaInyectora.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    VariablesMaquinaInyectoraViewModel variablesMaquinaInyectoraViewModel = Convertir(variablesMaquinaInyectora);
                    List<MaquinasInyectoraActivos> maquinas;
                    using (UnidadDeTrabajo<MaquinasInyectoraActivos> unidad = new UnidadDeTrabajo<MaquinasInyectoraActivos>(new BDContext()))
                    {
                        maquinas = unidad.genericDAL.GetAll().ToList();
                    }
                    List<ProductosTapaActivos> productos;
                    using (UnidadDeTrabajo<ProductosTapaActivos> unidad = new UnidadDeTrabajo<ProductosTapaActivos>(new BDContext()))
                    {
                        productos = unidad.genericDAL.GetAll().ToList();
                    }
                    List<MateriaPrimaInyectoraActivos> materiaPrimas;
                    using (UnidadDeTrabajo<MateriaPrimaInyectoraActivos> unidad = new UnidadDeTrabajo<MateriaPrimaInyectoraActivos>(new BDContext()))
                    {
                        materiaPrimas = unidad.genericDAL.GetAll().ToList();
                    }
                    variablesMaquinaInyectoraViewModel.Maquinas = maquinas;
                    variablesMaquinaInyectoraViewModel.Productos = productos;
                    variablesMaquinaInyectoraViewModel.MateriaPrimas = materiaPrimas;
                    return View(variablesMaquinaInyectoraViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Edit(VariablesMaquinaInyectoraViewModel variablesMaquinaInyectoraViewModel)
        {
            try
            {
                variablesMaquinaInyectoraViewModel.Estado = true;
                using (UnidadDeTrabajo<VariablesMaquinaInyectora> unidad = new UnidadDeTrabajo<VariablesMaquinaInyectora>(new BDContext()))
                {
                    unidad.genericDAL.Update(Convertir(variablesMaquinaInyectoraViewModel));
                    unidad.Complete();
                }
                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Ficha de variables modificada";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al editar variables de maquina";
                return RedirectToAction("Index");
            }
        }

        public ActionResult Details(int? id)
        {
            try
            {
                if (id != null)
                {
                    VariablesMaquinaInyectora variablesMaquinaInyectora;
                    using (UnidadDeTrabajo<VariablesMaquinaInyectora> unidad = new UnidadDeTrabajo<VariablesMaquinaInyectora>(new BDContext()))
                    {
                        variablesMaquinaInyectora = unidad.genericDAL.Get((int)id);
                        if (!variablesMaquinaInyectora.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    VariablesMaquinaInyectoraViewModel variablesMaquinaInyectoraViewModel = Convertir(variablesMaquinaInyectora);
                    using (UnidadDeTrabajo<Maquinas> Unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
                    {
                        variablesMaquinaInyectoraViewModel.Maquina = Unidad.genericDAL.Get(variablesMaquinaInyectoraViewModel.MaquinaID);
                    }
                    using (UnidadDeTrabajo<Productos> Unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        variablesMaquinaInyectoraViewModel.Producto = Unidad.genericDAL.Get(variablesMaquinaInyectoraViewModel.ProductoID);
                    }
                    using (UnidadDeTrabajo<MateriaPrima> Unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                    {
                        variablesMaquinaInyectoraViewModel.MateriaPrima = Unidad.genericDAL.Get(variablesMaquinaInyectoraViewModel.MateriaPrimaPrincipalID);
                    }
                    if (variablesMaquinaInyectoraViewModel.MateriaPrimaSecundariaID != null)
                    {
                        using (UnidadDeTrabajo<MateriaPrima> Unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                        {
                            variablesMaquinaInyectoraViewModel.MateriaPrima1 = Unidad.genericDAL.Get((int)variablesMaquinaInyectoraViewModel.MateriaPrimaSecundariaID);
                        }
                    }
                    if (variablesMaquinaInyectoraViewModel.MateriaPrimaTerciariaID != null)
                    {
                        using (UnidadDeTrabajo<MateriaPrima> Unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                        {
                            variablesMaquinaInyectoraViewModel.MateriaPrima2 = Unidad.genericDAL.Get((int)variablesMaquinaInyectoraViewModel.MateriaPrimaTerciariaID);
                        }
                    }
                    return View(variablesMaquinaInyectoraViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver detalles";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    VariablesMaquinaInyectora variablesMaquinaInyectora;
                    using (UnidadDeTrabajo<VariablesMaquinaInyectora> unidad = new UnidadDeTrabajo<VariablesMaquinaInyectora>(new BDContext()))
                    {
                        variablesMaquinaInyectora = unidad.genericDAL.Get((int)id);
                        if (!variablesMaquinaInyectora.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    VariablesMaquinaInyectoraViewModel variablesMaquinaInyectoraViewModel = Convertir(variablesMaquinaInyectora);
                    using (UnidadDeTrabajo<Maquinas> Unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
                    {
                        variablesMaquinaInyectoraViewModel.Maquina = Unidad.genericDAL.Get(variablesMaquinaInyectoraViewModel.MaquinaID);
                    }
                    using (UnidadDeTrabajo<Productos> Unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        variablesMaquinaInyectoraViewModel.Producto = Unidad.genericDAL.Get(variablesMaquinaInyectoraViewModel.ProductoID);
                    }
                    using (UnidadDeTrabajo<MateriaPrima> Unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                    {
                        variablesMaquinaInyectoraViewModel.MateriaPrima = Unidad.genericDAL.Get(variablesMaquinaInyectoraViewModel.MateriaPrimaPrincipalID);
                    }
                    if (variablesMaquinaInyectoraViewModel.MateriaPrimaSecundariaID != null)
                    {
                        using (UnidadDeTrabajo<MateriaPrima> Unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                        {
                            variablesMaquinaInyectoraViewModel.MateriaPrima1 = Unidad.genericDAL.Get((int)variablesMaquinaInyectoraViewModel.MateriaPrimaSecundariaID);
                        }
                    }
                    if (variablesMaquinaInyectoraViewModel.MateriaPrimaTerciariaID != null)
                    {
                        using (UnidadDeTrabajo<MateriaPrima> Unidad = new UnidadDeTrabajo<MateriaPrima>(new BDContext()))
                        {
                            variablesMaquinaInyectoraViewModel.MateriaPrima2 = Unidad.genericDAL.Get((int)variablesMaquinaInyectoraViewModel.MateriaPrimaTerciariaID);
                        }
                    }
                    return View(variablesMaquinaInyectoraViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Delete(VariablesMaquinaInyectoraViewModel variablesMaquinaInyectoraViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<VariablesMaquinaInyectora> unidad = new UnidadDeTrabajo<VariablesMaquinaInyectora>(new BDContext()))
                {
                    VariablesMaquinaInyectora variablesMaquinaInyectora = unidad.genericDAL.Get(variablesMaquinaInyectoraViewModel.VariablesMaquinaInyectoraID);
                    variablesMaquinaInyectora.Estado = false;
                    unidad.genericDAL.Update(variablesMaquinaInyectora);
                    unidad.Complete();
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Ficha de variables eliminada";
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al eliminar variables de maquina";
                return RedirectToAction("Index");
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}
