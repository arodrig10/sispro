﻿using Backend.Entities;
using Frontend.Models;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Frontend.Controllers
{
    [Authorize]
    public class GraficoLineasProduccionController : Controller
    {
        // GET: GraficoLineasProduccion
        public ActionResult _GraficoMensualProduccion()
        {

            using (BDContext context = new BDContext())
           {
              var graficoPRoduccion = context.sp_GraficoProduccion().ToList();

                ProduccionAnualViewModel obj = Produccion(graficoPRoduccion);
                return PartialView(obj);
            }
        }


        public ProduccionAnualViewModel Produccion(System.Collections.Generic.List<Backend.Entities.sp_GraficoProduccion_Result> lista)

        {
            ProduccionAnualViewModel obj = new ProduccionAnualViewModel();

            foreach (var item in lista)
            {
                switch (item.mes)
                {
                    case 1:
                        obj.Enero =(int) item.total;
                        break;
                    case 2:
                        obj.Febrero = (int)item.total;
                        break;
                    case 3:
                        obj.Marzo = (int)item.total;
                        break;
                    case 4:
                        obj.Abril = (int)item.total;
                        break;
                    case 5:
                        obj.Mayo = (int)item.total;
                        break;
                    case 6:
                        obj.Junio = (int)item.total;
                        break;
                    case 7:
                        obj.Julio = (int)item.total;
                        break;
                    case 8:
                        obj.Agosto = (int)item.total;
                        break;
                    case 9:
                        obj.Setiembre = (int)item.total;
                        break;
                    case 10:
                        obj.Octubre = (int)item.total;
                        break;
                    case 11:
                        obj.Noviembre = (int)item.total;
                        break;
                    case 12:
                        obj.Diciembre = (int)item.total;
                        break;
                }

            }
            return obj;
            
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}