﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;

namespace Frontend.Controllers
{
    [Authorize]
    public class MermaPorTurnoController : Controller
    {
        private AperturaCierreViewModel Convertir(AperturaCierreActivos aperturaCierreActivos)
        {
            return new AperturaCierreViewModel
            {
                AperturaCierreID = aperturaCierreActivos.AperturaCierreID,
                FechaApertura = aperturaCierreActivos.FechaApertura,
                FechaCierre = aperturaCierreActivos.FechaCierre,
                Turno = aperturaCierreActivos.Turno,
                PedidoID = aperturaCierreActivos.PedidoID,
                Estado = aperturaCierreActivos.Estado,
                MaquinaID = (int)aperturaCierreActivos.MaquinaID
            };
        }

        private AperturaCierreViewModel Convertir(AperturaCierre aperturaCierre)
        {
            return new AperturaCierreViewModel
            {

                AperturaCierreID = aperturaCierre.AperturaCierreID,
                FechaApertura = aperturaCierre.FechaApertura,
                FechaCierre = aperturaCierre.FechaCierre,
                Turno = aperturaCierre.Turno,
                PedidoID = aperturaCierre.PedidoID,
                Estado = aperturaCierre.Estado,
                MaquinaID = (int)aperturaCierre.MaquinaID

            };
        }

        private MermaPorTurnoViewModel Convertir(MermaPorTurnoActivos mermaPorTurnoActivos)
        {
            return new MermaPorTurnoViewModel
            {

                MermaPorTurnoID = mermaPorTurnoActivos.MermaPorTurnoID,

                Cantidad = mermaPorTurnoActivos.Cantidad,

                AperturaCierreID = (int)mermaPorTurnoActivos.AperturaCierreID,

                TipoMermaID = mermaPorTurnoActivos.TipoMermaID,

                Estado = mermaPorTurnoActivos.Estado
            };
        }
        private MermaPorTurnoViewModel Convertir(MermaPorTurno mermaPorTurno)
        {
            return new MermaPorTurnoViewModel
            {
                MermaPorTurnoID = mermaPorTurno.MermaPorTurnoID,

                Cantidad = mermaPorTurno.Cantidad,

                AperturaCierreID = (int)mermaPorTurno.AperturaCierreID,

                TipoMermaID = mermaPorTurno.TipoMermaID,

                Estado = mermaPorTurno.Estado
            };
        }
        private MermaPorTurno Convertir(MermaPorTurnoViewModel mermaPorTurnoViewModel)
        {
            return new MermaPorTurno
            {
                MermaPorTurnoID = mermaPorTurnoViewModel.MermaPorTurnoID,

                Cantidad = mermaPorTurnoViewModel.Cantidad,

                AperturaCierreID = mermaPorTurnoViewModel.AperturaCierreID,

                TipoMermaID = mermaPorTurnoViewModel.TipoMermaID,

                Estado = mermaPorTurnoViewModel.Estado
            };
        }
        public ActionResult Index()
        {
            try
            {
                List<MermaPorTurnoActivos> mermaPorTurno;
                using (UnidadDeTrabajo<MermaPorTurnoActivos> Unidad = new UnidadDeTrabajo<MermaPorTurnoActivos>(new BDContext()))
                {
                    mermaPorTurno = Unidad.genericDAL.GetAll().ToList();
                }
                List<MermaPorTurnoViewModel> lista = new List<MermaPorTurnoViewModel>();
                MermaPorTurnoViewModel mermaPorTurnoViewModel;
                foreach (var item in mermaPorTurno)
                {
                    mermaPorTurnoViewModel = Convertir(item);
                    using (UnidadDeTrabajo<AperturaCierre> Unidad = new UnidadDeTrabajo<AperturaCierre>(new BDContext()))
                    {
                        mermaPorTurnoViewModel.AperturaCierre = Unidad.genericDAL.Get(mermaPorTurnoViewModel.AperturaCierreID);
                        mermaPorTurnoViewModel.AperturaCierreViewModel = Convertir(mermaPorTurnoViewModel.AperturaCierre);
                    }
                    using (UnidadDeTrabajo<Pedidos> Unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                    {
                        mermaPorTurnoViewModel.AperturaCierreViewModel.Pedido = Unidad.genericDAL.Get(mermaPorTurnoViewModel.AperturaCierreViewModel.PedidoID);
                    }

                    using (UnidadDeTrabajo<TipoMerma> Unidad = new UnidadDeTrabajo<TipoMerma>(new BDContext()))
                    {
                        mermaPorTurnoViewModel.TipoMerma = Unidad.genericDAL.Get(mermaPorTurnoViewModel.TipoMermaID);
                    }
                    lista.Add(mermaPorTurnoViewModel);
                }

                //Mostrar mensajes
                ViewBag.Message = BLLUtilitarios.msj;
                BLLUtilitarios.msj = null;
                return View(lista);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener lista";
                return RedirectToAction("Index", "Home");
            }
        }
       public ActionResult Create()
        {
            try
            {
                MermaPorTurnoViewModel mermas = new MermaPorTurnoViewModel { };
                
                List<Pedidos> pedidos = new List<Pedidos>();
                using (UnidadDeTrabajo<AperturaCierreActivos> unidad = new UnidadDeTrabajo<AperturaCierreActivos>(new BDContext()))
                {
                    mermas.AperturaCierres = unidad.genericDAL.GetAll().ToList();
                }
                List<AperturaCierreViewModel> dpApert = new List<AperturaCierreViewModel>();
                foreach (var item in mermas.AperturaCierres)
                {
                    dpApert.Add(Convertir(item));
                    

                }

                foreach (var item in dpApert)
                {
                    using (UnidadDeTrabajo <Pedidos> unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                    {
                        item.Pedido = unidad.genericDAL.Get(item.PedidoID);
                    }
                }

                mermas.AperturaCierresViewModel = dpApert;

                using (UnidadDeTrabajo<TipoMerma> unidad = new UnidadDeTrabajo<TipoMerma>(new BDContext()))
                {
                    mermas.TipoMermas = unidad.genericDAL.GetAll().ToList();
                }
               
                return View(mermas);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Create(MermaPorTurnoViewModel mermaPorTurnoViewModel)
        {
            try
            {
                bool a;
                mermaPorTurnoViewModel.Estado = true;
                MermaPorTurno mermaPorTurno = Convertir(mermaPorTurnoViewModel);
                using (UnidadDeTrabajo<MermaPorTurno> unidad = new UnidadDeTrabajo<MermaPorTurno>(new BDContext()))
                {
                    unidad.genericDAL.Add(mermaPorTurno);
                    a = unidad.Complete();
                }
                //Mostrar El mensaje 
                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Merma reportada";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al agregar reporte";
                return RedirectToAction("Index");
            }
        }
       public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    MermaPorTurno mermaPorTurno;
                    using (UnidadDeTrabajo<MermaPorTurno> unidad = new UnidadDeTrabajo<MermaPorTurno>(new BDContext()))
                    {
                        mermaPorTurno = unidad.genericDAL.Get((int)id);
                        if (!mermaPorTurno.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    MermaPorTurnoViewModel mermaPorTurnoViewModel = Convertir(mermaPorTurno);
                    using (UnidadDeTrabajo<AperturaCierreActivos> unidad = new UnidadDeTrabajo<AperturaCierreActivos>(new BDContext()))
                    {
                        mermaPorTurnoViewModel.AperturaCierres = unidad.genericDAL.GetAll().ToList();
                    }
                    using (UnidadDeTrabajo<TipoMerma> unidad = new UnidadDeTrabajo<TipoMerma>(new BDContext()))
                    {
                        mermaPorTurnoViewModel.TipoMermas = unidad.genericDAL.GetAll().ToList();
                    }
                    List<AperturaCierreViewModel> dpApert = new List<AperturaCierreViewModel>();
                    foreach (var item in mermaPorTurnoViewModel.AperturaCierres)
                    {
                        dpApert.Add(Convertir(item));


                    }

                    foreach (var item in dpApert)
                    {
                        using (UnidadDeTrabajo<Pedidos> unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                        {
                            item.Pedido = unidad.genericDAL.Get(item.PedidoID);
                        }
                    }

                    mermaPorTurnoViewModel.AperturaCierresViewModel = dpApert;
                    return View(mermaPorTurnoViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Edit(MermaPorTurnoViewModel mermaPorTurnoViewModel)
        {
            try
            {
                mermaPorTurnoViewModel.Estado = true;
                using (UnidadDeTrabajo<MermaPorTurno> unidad = new UnidadDeTrabajo<MermaPorTurno>(new BDContext()))
                {
                    unidad.genericDAL.Update(Convertir(mermaPorTurnoViewModel));
                    unidad.Complete();
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Reporte Merma modificado";
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al editar reporte";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Details(int? id)
        {
            try
            {
                if (id != null)
                {
                    MermaPorTurno mermaPorTurno;
                    using (UnidadDeTrabajo<MermaPorTurno> unidad = new UnidadDeTrabajo<MermaPorTurno>(new BDContext()))
                    {
                        mermaPorTurno = unidad.genericDAL.Get((int)id);
                        if (!mermaPorTurno.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    MermaPorTurnoViewModel mermaPorTurnoViewModel = Convertir(mermaPorTurno);
                    using (UnidadDeTrabajo<AperturaCierre> unidad = new UnidadDeTrabajo<AperturaCierre>(new BDContext()))
                    {
                        mermaPorTurnoViewModel.AperturaCierre = unidad.genericDAL.Get(mermaPorTurnoViewModel.AperturaCierreID);
                    };
                    using (UnidadDeTrabajo<TipoMerma> unidad = new UnidadDeTrabajo<TipoMerma>(new BDContext()))
                    {
                        mermaPorTurnoViewModel.TipoMerma = unidad.genericDAL.Get(mermaPorTurnoViewModel.TipoMermaID);
                    };
                    return View(mermaPorTurnoViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver detalles";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    MermaPorTurno mermaPorTurno;
                    using (UnidadDeTrabajo<MermaPorTurno> unidad = new UnidadDeTrabajo<MermaPorTurno>(new BDContext()))
                    {
                        mermaPorTurno = unidad.genericDAL.Get((int)id);
                    }
                    MermaPorTurnoViewModel mermaPorTurnoViewModel = this.Convertir(mermaPorTurno);
                    using (UnidadDeTrabajo<AperturaCierre> unidad = new UnidadDeTrabajo<AperturaCierre>(new BDContext()))
                    {

                        mermaPorTurnoViewModel.AperturaCierre = unidad.genericDAL.Get(mermaPorTurnoViewModel.AperturaCierreID);
                    };
                    using (UnidadDeTrabajo<TipoMerma> unidad = new UnidadDeTrabajo<TipoMerma>(new BDContext()))
                    {

                        mermaPorTurnoViewModel.TipoMerma = unidad.genericDAL.Get(mermaPorTurnoViewModel.TipoMermaID);
                    };
                    return View(mermaPorTurnoViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Delete(MermaPorTurnoViewModel mermaPorTurnoViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<MermaPorTurno> unidad = new UnidadDeTrabajo<MermaPorTurno>(new BDContext()))
                {
                    MermaPorTurno mermaPorTurno = unidad.genericDAL.Get(mermaPorTurnoViewModel.MermaPorTurnoID);
                    mermaPorTurno.Estado = false;
                    unidad.genericDAL.Update(mermaPorTurno);
                    unidad.Complete();
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Reporte merma eliminado" ;
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al eliminar reporte";
                return RedirectToAction("Index");
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}
