﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Backend.Clases;
using Backend.DAL;
using Backend.Entities;
using Frontend.Models;

namespace Frontend.Controllers
{

    [Authorize]
    public class PedidosController : Controller
    {

        private PedidoViewModel Convertir(PedidosActivos pedido)
        {
            return new PedidoViewModel
            {
                PedidoID = pedido.PedidoID,
                NumeroPedido = pedido.NumeroPedido,
                ClienteID = pedido.ClienteID,
                NumeroOrdenDeCompra = pedido.NumeroOrdenDeCompra,
                ProductoID = pedido.ProductoID,
                UsuarioID = pedido.UsuarioID,
                UnidadMedidaID = pedido.UnidadMedidaID,
                Cantidad = pedido.Cantidad,
                Descripcion = pedido.Descripcion,
                Fecha = pedido.Fecha,
                PrecioTotal = pedido.PrecioTotal,
                Estado = pedido.Estado,
                EstadoID = pedido.EstadoID
            };
        }

        private PedidoViewModel Convertir(Pedidos pedido)
        {
            return new PedidoViewModel
            {
                PedidoID = pedido.PedidoID,
                NumeroPedido = pedido.NumeroPedido,
                ClienteID = pedido.ClienteID,
                NumeroOrdenDeCompra = pedido.NumeroOrdenDeCompra,
                ProductoID = pedido.ProductoID,
                UsuarioID = pedido.UsuarioID,
                UnidadMedidaID = pedido.UnidadMedidaID,
                Cantidad = pedido.Cantidad,
                Descripcion = pedido.Descripcion,
                Fecha = pedido.Fecha,
                PrecioTotal = pedido.PrecioTotal,
                Estado = pedido.Estado,
                EstadoID = pedido.EstadoID
            };
        }

        private Pedidos Convertir(PedidoViewModel pedidoViewModel)
        {
            return new Pedidos
            {
                PedidoID = pedidoViewModel.PedidoID,
                NumeroPedido = pedidoViewModel.NumeroPedido,
                ClienteID = pedidoViewModel.ClienteID,
                NumeroOrdenDeCompra = pedidoViewModel.NumeroOrdenDeCompra,
                ProductoID = pedidoViewModel.ProductoID,
                UsuarioID = pedidoViewModel.UsuarioID,
                UnidadMedidaID = pedidoViewModel.UnidadMedidaID,
                Cantidad = pedidoViewModel.Cantidad,
                Descripcion = pedidoViewModel.Descripcion,
                Fecha = pedidoViewModel.Fecha,
                PrecioTotal = pedidoViewModel.PrecioTotal,
                Estado = pedidoViewModel.Estado,
                EstadoID = pedidoViewModel.EstadoID
            };
        }
        public ActionResult Index()
        {
            try
            {
                List<PedidosActivos> pedidos;
                using (UnidadDeTrabajo<PedidosActivos> Unidad = new UnidadDeTrabajo<PedidosActivos>(new BDContext()))
                {
                    pedidos = Unidad.genericDAL.GetAll().ToList();
                }
                List<PedidoViewModel> lista = new List<PedidoViewModel>();
                PedidoViewModel pedidoViewModel;
                foreach (var item in pedidos)
                {
                    pedidoViewModel = Convertir(item);

                    using (UnidadDeTrabajo<Clientes> UnidadCliente = new UnidadDeTrabajo<Clientes>(new BDContext()))
                    {
                        pedidoViewModel.Cliente = UnidadCliente.genericDAL.Get(pedidoViewModel.ClienteID);
                    }
                    using (UnidadDeTrabajo<Productos> UnidadProducto = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        pedidoViewModel.Producto = UnidadProducto.genericDAL.Get(pedidoViewModel.ProductoID);
                    }
                    using (UnidadDeTrabajo<UnidadMedida> UnidadUnidadMedida = new UnidadDeTrabajo<UnidadMedida>(new BDContext()))
                    {
                        pedidoViewModel.UnidadMedida = UnidadUnidadMedida.genericDAL.Get(pedidoViewModel.UnidadMedidaID);
                    }
                    using (UnidadDeTrabajo<Estados>UnidadDeTrabajo = new UnidadDeTrabajo<Estados>(new BDContext()))
                    {
                        pedidoViewModel.EstadoPedido = UnidadDeTrabajo.genericDAL.Get((int)pedidoViewModel.EstadoID);
                    }
                        lista.Add(pedidoViewModel);
                }
                //Mostrar mensajes
                ViewBag.Message = BLLUtilitarios.msj;
                BLLUtilitarios.msj = null;
                return View(lista);
            }
            catch (Exception ex)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al obtener lista"+ ex;
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Create()
        {
            try
            {
                PedidoViewModel pedidos = new PedidoViewModel { };

                using (UnidadDeTrabajo<ClientesActivos> unidad = new UnidadDeTrabajo<ClientesActivos>(new BDContext()))
                {
                    pedidos.Clientes = unidad.genericDAL.GetAll().ToList();
                }
                using (UnidadDeTrabajo<ProductosActivos> unidad = new UnidadDeTrabajo<ProductosActivos>(new BDContext()))
                {
                    pedidos.Productos = unidad.genericDAL.GetAll().ToList();
                }
                using (UnidadDeTrabajo<UnidadMedida> unidad = new UnidadDeTrabajo<UnidadMedida>(new BDContext()))
                {
                    pedidos.UnidadMedidas = unidad.genericDAL.GetAll().ToList();
                }

                return View(pedidos);
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }


        [HttpPost]
        public ActionResult Create(PedidoViewModel pedidoViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<Usuarios> unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                {
                    pedidoViewModel.UsuarioID = unidad.genericDAL.Get((int)Session["userID"]).UsuarioID;
                }
                pedidoViewModel.Estado = true;
                pedidoViewModel.EstadoID = 1;
                using (BDContext context = new BDContext())
                {
                    if (context.Pedidos.Where(c => c.NumeroPedido == pedidoViewModel.NumeroPedido).Count() > 0)
                    {
                        TempData["TipoMensaje"] = "alert alert-danger";
                        TempData["MensajeError"] = "Numero pedido ya existe";
                        using (UnidadDeTrabajo<ClientesActivos> unidad = new UnidadDeTrabajo<ClientesActivos>(new BDContext()))
                        {
                            pedidoViewModel.Clientes = unidad.genericDAL.GetAll().ToList();
                        }
                        using (UnidadDeTrabajo<ProductosActivos> unidad = new UnidadDeTrabajo<ProductosActivos>(new BDContext()))
                        {
                            pedidoViewModel.Productos = unidad.genericDAL.GetAll().ToList();
                        }
                        using (UnidadDeTrabajo<UnidadMedida> unidad = new UnidadDeTrabajo<UnidadMedida>(new BDContext()))
                        {
                            pedidoViewModel.UnidadMedidas = unidad.genericDAL.GetAll().ToList();
                        }
                        return View("Create", pedidoViewModel);
                    }
                }
                bool a;

                Pedidos pedidos = Convertir(pedidoViewModel);
                using (UnidadDeTrabajo<Pedidos> unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                {
                    unidad.genericDAL.Add(pedidos);
                    a = unidad.Complete();
                }

                //Mostrar El mensaje 
               /* if (a)
                {
                    BLLUtilitarios.msj = "Se ha agregado una nuevo pedido 1";
                }
                else
                {
                    BLLUtilitarios.msj = "Error al agregar pedido en la base de datos";
                }*/


                TempData["TipoMensaje"] = "alert alert-success";
                TempData["Mensaje"] = "Pedido creado: " + pedidoViewModel.NumeroPedido;
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al crear pedido";
                return RedirectToAction("Index");
            }
        }


        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    Pedidos pedido;
                    using (UnidadDeTrabajo<Pedidos> unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                    {
                        pedido = unidad.genericDAL.Get((int)id);
                        if (!pedido.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    PedidoViewModel pedidos = Convertir(pedido);
                    Usuarios usuario;

                    List<ClientesActivos> cliente;
                    using (UnidadDeTrabajo<ClientesActivos> unidad = new UnidadDeTrabajo<ClientesActivos>(new BDContext()))
                    {
                        cliente = unidad.genericDAL.GetAll().ToList();

                    }
                    List<ProductosActivos> producto;
                    using (UnidadDeTrabajo<ProductosActivos> unidad = new UnidadDeTrabajo<ProductosActivos>(new BDContext()))
                    {
                        producto = unidad.genericDAL.GetAll().ToList();
                    }
                    List<UnidadMedida> unidadMedida;
                    using (UnidadDeTrabajo<UnidadMedida> unidad = new UnidadDeTrabajo<UnidadMedida>(new BDContext()))
                    {
                        unidadMedida = unidad.genericDAL.GetAll().ToList();
                    }
                    List<UsuariosActivos> usuarios;
                    using (UnidadDeTrabajo<UsuariosActivos> unidad = new UnidadDeTrabajo<UsuariosActivos>(new BDContext()))
                    {
                        usuarios = unidad.genericDAL.GetAll().ToList();
                    }

                    pedidos.Usuarios = usuarios;
                    pedidos.Clientes = cliente;
                    pedidos.Productos = producto;
                    pedidos.UnidadMedidas = unidadMedida;
                    return View(pedidos);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Edit(PedidoViewModel pedidoViewModel)
        {
            try
            {
                pedidoViewModel.EstadoID = 1;
                using (UnidadDeTrabajo<Usuarios> unidad = new UnidadDeTrabajo<Usuarios>(new BDContext()))
                {
                    pedidoViewModel.UsuarioID = unidad.genericDAL.Get((int)Session["userID"]).UsuarioID;
                }
                pedidoViewModel.Estado = true;
                using (UnidadDeTrabajo<Pedidos> unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                {
                    unidad.genericDAL.Update(Convertir(pedidoViewModel));
                    unidad.Complete();
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Pedido modificado: " + pedidoViewModel.NumeroPedido;
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al editar pedido";
                return RedirectToAction("Index");
            }
        }
       
        public ActionResult Cerrar(int? id)
        {
            PedidoViewModel pedidoViewModel = new PedidoViewModel();
            try
            {
                using (UnidadDeTrabajo<Pedidos> unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                {
                    pedidoViewModel = Convertir(unidad.genericDAL.Get((int)id));
                }



                    using (UnidadDeTrabajo<Pedidos> unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                {
                    pedidoViewModel.EstadoID = 3;
                    unidad.genericDAL.Update(Convertir(pedidoViewModel));
                    unidad.Complete();
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Se ha cerrado el: " + pedidoViewModel.NumeroPedido+" con éxito";
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al cerrar pedido";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Details(int? id)
        {
            try
            {
                if (id != null)
                {
                    Pedidos pedido;
                    using (UnidadDeTrabajo<Pedidos> unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                    {
                        pedido = unidad.genericDAL.Get((int)id);
                        if (!pedido.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    PedidoViewModel pedidoViewModel = Convertir(pedido);

                    using (UnidadDeTrabajo<Clientes> unidad = new UnidadDeTrabajo<Clientes>(new BDContext()))
                    {
                        pedidoViewModel.Cliente = unidad.genericDAL.Get(pedidoViewModel.ClienteID);
                    };
                    using (UnidadDeTrabajo<Productos> unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        pedidoViewModel.Producto = unidad.genericDAL.Get(pedidoViewModel.ProductoID);
                    };
                    using (UnidadDeTrabajo<UnidadMedida> unidad = new UnidadDeTrabajo<UnidadMedida>(new BDContext()))
                    {
                        pedidoViewModel.UnidadMedida = unidad.genericDAL.Get(pedidoViewModel.UnidadMedidaID);
                    };
                    using (UnidadDeTrabajo<Estados> UnidadDeTrabajo = new UnidadDeTrabajo<Estados>(new BDContext()))
                    {
                        pedidoViewModel.EstadoPedido = UnidadDeTrabajo.genericDAL.Get((int)pedidoViewModel.EstadoID);
                    }


                    List<ProduccionActivos> produccionActivos;
                    using (UnidadDeTrabajo<ProduccionActivos> unidad = new UnidadDeTrabajo<ProduccionActivos>(new BDContext()))
                    {
                        produccionActivos = unidad.genericDAL.GetAll().ToList();
                        foreach (var item in produccionActivos)
                        {
                            if(item.PedidoID == pedidoViewModel.PedidoID)
                            {
                                pedidoViewModel.produccionActual = (float)item.CantidadProduccionTotal;
                            }
                        }
                    }

                    List<DetalleEntrega> entregas;
                    using (UnidadDeTrabajo<DetalleEntrega> unidad = new UnidadDeTrabajo<DetalleEntrega>(new BDContext()))
                    {

                        entregas = unidad.genericDAL.GetAll().ToList();

                    }

                    foreach (var item in produccionActivos)
                    {
                        if (item.PedidoID == pedidoViewModel.PedidoID)
                        {
                            foreach (var entregasProduccion in entregas)
                            {
                                if (item.ProduccionID == entregasProduccion.ProduccionID)
                                {

                                    pedidoViewModel.entregadoActual = pedidoViewModel.entregadoActual + (float)entregasProduccion.Cantidad;

                                }

                            }
                        }
                    }



                    return View(pedidoViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver detalles";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    Pedidos pedido;
                    using (UnidadDeTrabajo<Pedidos> unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                    {
                        pedido = unidad.genericDAL.Get((int)id);
                        if (!pedido.Estado)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                    PedidoViewModel pedidoViewModel = Convertir(pedido);

                    using (UnidadDeTrabajo<Clientes> unidad = new UnidadDeTrabajo<Clientes>(new BDContext()))
                    {
                        pedidoViewModel.Cliente = unidad.genericDAL.Get(pedidoViewModel.ClienteID);
                    };
                    using (UnidadDeTrabajo<Productos> unidad = new UnidadDeTrabajo<Productos>(new BDContext()))
                    {
                        pedidoViewModel.Producto = unidad.genericDAL.Get(pedidoViewModel.ProductoID);
                    };
                    using (UnidadDeTrabajo<UnidadMedida> unidad = new UnidadDeTrabajo<UnidadMedida>(new BDContext()))
                    {
                        pedidoViewModel.UnidadMedida = unidad.genericDAL.Get(pedidoViewModel.UnidadMedidaID);
                    };
                    return View(pedidoViewModel);
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al ver formulario";
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult Delete(PedidoViewModel pedidoViewModel)
        {
            try
            {
                using (UnidadDeTrabajo<Pedidos> unidad = new UnidadDeTrabajo<Pedidos>(new BDContext()))
                {
                    Pedidos pedidos = unidad.genericDAL.Get(pedidoViewModel.PedidoID);
                    pedidos.Estado = false;
                    unidad.genericDAL.Update(pedidos);
                    unidad.Complete();
                    TempData["TipoMensaje"] = "alert alert-success";
                    TempData["Mensaje"] = "Pedido eliminado: " + pedidos.NumeroPedido;
                }
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["TipoMensaje"] = "alert alert-danger";
                TempData["Mensaje"] = "Error al eliminar pedido";
                return RedirectToAction("Index");
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            //Log the error!!

            //Redirect to action
            filterContext.Result = RedirectToAction("Index", "Error");
        }

    }
}