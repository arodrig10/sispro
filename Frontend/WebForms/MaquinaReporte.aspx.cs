﻿using Backend.DAL;
using Backend.Entities;
using Frontend.Models;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Frontend
{
    public partial class MaquinaReporte : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["userID"] != null && Session["userName"] != null)
            {
                if (!IsPostBack)
                {
                    List<Maquinas> maquinas;
                    using (UnidadDeTrabajo<Maquinas> Unidad = new UnidadDeTrabajo<Maquinas>(new BDContext()))
                    {
                        maquinas = Unidad.genericDAL.GetAll().ToList();
                    }
                    List<MaquinasViewModel> lista = new List<MaquinasViewModel>();
                    MaquinasViewModel maquinaViewModel;
                    foreach (var item in maquinas)
                    {
                        maquinaViewModel = Convertir(item);

                        using (UnidadDeTrabajo<TipoMaquina> Unidad = new UnidadDeTrabajo<TipoMaquina>(new BDContext()))
                        {
                            maquinaViewModel.TipoMaquina = Unidad.genericDAL.Get(maquinaViewModel.TipoMaquinaID);
                        }

                        lista.Add(maquinaViewModel);
                    }

                    ReportDataSource rptds = new ReportDataSource("MaquinaDataSet", lista);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.LocalReport.DataSources.Add(rptds);
                }
            }
            else
            {
                Response.Redirect("/");
            }
        }

        private MaquinasViewModel Convertir(Maquinas maquina)
        {
            return new MaquinasViewModel
            {
                MaquinaID = maquina.MaquinaID,
                CodigoMaquina = maquina.CodigoMaquina,
                Nombre = maquina.Nombre,
                TipoMaquinaID = maquina.TipoMaquinaID,
                Descripcion = maquina.Descripcion,
                ProduccionHora = maquina.ProduccionHora,
                Estado = maquina.Estado
            };
        }
        private Maquinas Convertir(MaquinasViewModel maquinaViewModel)
        {
            return new Maquinas
            {
                MaquinaID = maquinaViewModel.MaquinaID,
                CodigoMaquina = maquinaViewModel.CodigoMaquina,
                Nombre = maquinaViewModel.Nombre,
                TipoMaquinaID = maquinaViewModel.TipoMaquinaID,
                Descripcion = maquinaViewModel.Descripcion,
                ProduccionHora = maquinaViewModel.ProduccionHora,
                Estado = maquinaViewModel.Estado
            };
        }
    }
}