﻿using Backend.Entities;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Frontend.WebForms
{
    public partial class WFInyectora : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["userID"] != null && Session["userName"] != null)
            {
                if (!IsPostBack)
                {
                    List<sp_FichaOperacionInyectora_Result> fichaInyectora;
                    ReportParameter[] reportParameters = new ReportParameter[15];


                        using (BDContext context = new BDContext())
                        {
                                    fichaInyectora = context.sp_FichaOperacionInyectora((int)Session["FichaOperacion"]).ToList();

                                    foreach (var i in fichaInyectora)
                                    {

                                        //Añadir los valores a los parametros del reporte
                                        reportParameters[0] = new ReportParameter("NumeroPedido", i.NumeroPedido.ToString(), false);
                                        reportParameters[1] = new ReportParameter("OrdenCompra", i.NumeroOrdenDeCompra, false);
                                        reportParameters[2] = new ReportParameter("Cantidad", i.Cantidad.ToString(), false);
                                        reportParameters[3] = new ReportParameter("fecha", i.Fecha.ToString(), false);
                                        reportParameters[4] = new ReportParameter("Cliente", i.NombreCompleto, false);
                                        reportParameters[5] = new ReportParameter("Producto", i.Nombre, false);
                                        reportParameters[6] = new ReportParameter("Maquina", i.Nombre, false);
                                        reportParameters[7] = new ReportParameter("MP1", i.mp1, false);
                                        reportParameters[8] = new ReportParameter("MP2", i.mp2, false);
                                        reportParameters[9] = new ReportParameter("MP3", i.mp3, false);
                                        reportParameters[10] = new ReportParameter("CantidadMP1", i.CantidadPrimaPrincipal.ToString(), false);
                                        reportParameters[11] = new ReportParameter("CantidadMP2", i.CantidadPrimaSecundaria.ToString(), false);
                                        reportParameters[12] = new ReportParameter("CantidadMP2", i.CantidadPrimaTerciaria.ToString(), false);
                                        reportParameters[13] = new ReportParameter("Descripcion", i.Descripcion, false);
                                        reportParameters[14] = new ReportParameter("Usuario", i.usuario, false);

                                       
                            
                                    }
                        RVInyectora.LocalReport.SetParameters(reportParameters);
                        }
                }
            }
            else
            {
                Response.Redirect("/");
            }


        }
    }
}