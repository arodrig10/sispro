﻿using Backend.DAL;
using Backend.Entities;
using Frontend.Models;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Frontend.WebForms
{
    public partial class Etiqueta : System.Web.UI.Page
    {
      

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["userID"] != null && Session["userName"] != null)
            {
                if (!IsPostBack)
                {

                    string etiquetaID = "";
                    string fecha = "";
                    string cliente = "";
                    string ordenCompra= "";
                    string lote = "";
                    string direccion= "";
                    string producto = "";
                    string maquina= "";
                    string um= "";
                    string cantidad= "";
                    string operador= "";
                    string turno= "";

                    using (BDContext context = new BDContext())
                    {
                       
                        var a = context.sp_DatosEtiqueta((int?)Session["Numeroetiqueta"]);
                        
                        foreach (var i in a)
                        {
                            etiquetaID = i.EtiquetaID.ToString();
                            fecha = i.Fecha.ToString();
                             cliente = i.Cliente.ToString();
                            if (i.NumeroOrdenDeCompra != null)
                            {
                                ordenCompra = i.NumeroOrdenDeCompra.ToString();
                            }
                            else
                            {
                                ordenCompra = "-";
                            }

                             
                             lote = i.NumeroPedido.ToString();
                             direccion = i.Direccion.ToString();
                             producto = i.Producto.ToString();
                             maquina = i.maquina.ToString();
                             um = i.Unidad.ToString();
                             cantidad = i.Cantidad.ToString();
                            operador = i.Usuario.ToString();
                            turno = i.Turno.ToString();
                        }
                        
                        ReportParameter[] reportParameters = new ReportParameter[12];
                        reportParameters[0] = new ReportParameter("EtiquetaID", etiquetaID, false);
                        reportParameters[1] = new ReportParameter("fecha", fecha, false);
                        reportParameters[2] = new ReportParameter("Cliente", cliente, false);
                        reportParameters[3] = new ReportParameter("OrdenCompra", ordenCompra, false);
                        reportParameters[4] = new ReportParameter("Lote", lote, false);
                        reportParameters[5] = new ReportParameter("Direccion", direccion, false);
                        reportParameters[6] = new ReportParameter("Producto", producto, false);
                        reportParameters[7] = new ReportParameter("Maquina", maquina, false);
                        reportParameters[8] = new ReportParameter("UM", um, false);
                        reportParameters[9] = new ReportParameter("Cantidad", cantidad, false);
                        reportParameters[10] = new ReportParameter("Operador", operador, false);
                        reportParameters[11] = new ReportParameter("Turno", turno, false);


                        RVEtiqueta.LocalReport.SetParameters(reportParameters);
                        
                    }


                }
            }
            else
            {
                Response.Redirect("/");
            }
        }
    }
}