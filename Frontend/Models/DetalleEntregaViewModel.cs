﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class DetalleEntregaViewModel
    {
        public long DetelleEntregaID { get; set; }
        public int EntregaID { get; set; }
        public int ProduccionID { get; set; }
        public int Paquetes { get; set; }
        public double Cantidad { get; set; }
        public bool Estado { get; set; }


        public Pedidos Pedido { get; set; }
        public IEnumerable<PedidosActivos> Pedidos { get; set; }

    }
}