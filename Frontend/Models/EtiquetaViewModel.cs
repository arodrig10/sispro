﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class EtiquetaViewModel
    {
        public int EtiquetaID { get; set; }
        public System.DateTime Fecha { get; set; }
        public double Cantidad { get; set; }
        public string Turno { get; set; }
        public string NombreCompleto { get; set; }
        public string NumeroPedido { get; set; }
        public string NumeroOrdenDeCompra { get; set; }
        public string NombreCompleto1 { get; set; }
        public string Direccion { get; set; }
        public string Nombre { get; set; }
        public string Unidad { get; set; }
        public string Nombre1 { get; set; }
    }
}