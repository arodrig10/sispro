﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Frontend.Models
{
    public class ProveedorViewModel
    {
        [Key]
        [Display(Name = "ID")]
        public int ProveedorID { get; set; }
        [Display(Name = "Codigo de proveedor")]
        [Required(ErrorMessage = "Debe ingresar el codigo")]
        [MinLength(3)]
        public string CodigoProveedor { get; set; }
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Debe ingresar el nombre")]
        [MinLength(5)]
        public string Nombre { get; set; }
        [Display(Name = "Telefono")]
        public string Telefono { get; set; }
        [Display(Name = "Correo")]
        public string Correo { get; set; }
        [Display(Name = "Direccion")]
        public string Direccion { get; set; }
        [Display(Name = "Estado")]
        public bool Estado { get; set; }

    }
}
