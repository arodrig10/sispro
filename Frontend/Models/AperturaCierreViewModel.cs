﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class AperturaCierreViewModel
    {
        [Key]
        [Display(Name = "ID")]
        public long AperturaCierreID { get; set; }
        [Display(Name = "Fecha apertura")]
        [DataType(DataType.DateTime)]
        public System.DateTime FechaApertura { get; set; }
        [Display(Name = "Fecha cierre")]
        public Nullable<System.DateTime> FechaCierre { get; set; }
        [Display(Name = "Turno")]
        public string Turno { get; set; }
        [Display(Name = "Pedido")]
        public int PedidoID { get; set; }
        [Display(Name = "Estado")]
        public bool Estado { get; set; }
        public int MaquinaID { get; set; }
        //Objeto de tipo Pedido para poder mostrar los pedidos en una lista desplegable ,se usará una lista IENUMERABLE
        public Pedidos Pedido { get; set; }
        public IEnumerable<Pedidos> Pedidos { get; set; }

        //-------------------------------------------------FIN DE Pedidos----------------------------------------------
        //Objeto de tipo Pedido para poder mostrar los pedidos en una lista desplegable ,se usará una lista IENUMERABLE
        public Maquinas Maquina { get; set; }
        public IEnumerable<Maquinas> Maquinas { get; set; }

        public string Descripcion => string.Format("{0} {1} {2}", Pedido.NumeroPedido, Turno, FechaApertura);

        //-------------------------------------------------FIN DE Pedidos----------------------------------------------
    }
}