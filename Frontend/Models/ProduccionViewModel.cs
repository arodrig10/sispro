﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class ProduccionViewModel
    {
        [Key]
        public int ProduccionID { get; set; }
        [Display(Name = "Pedido")]
        public int PedidoID { get; set; }
        [Display(Name = "Fecha Inicio")]
        public System.DateTime FechaInicio { get; set; }
        [Display(Name = "Fecha Fin")]
        public Nullable<System.DateTime> FechaFin { get; set; }
        [Display(Name = "Horas Trabajadas")]
        public string HorasTrabajadas { get; set; }
        [Display(Name = "Cantidad Merma")]
        public Nullable<double> CantidadMerma { get; set; }
        [Display(Name = "Cantidad produccion total")]
        public double CantidadProduccionTotal { get; set; }
        [Display(Name = "Cantidad paquetes/cajas")]
        public int CantidadPaquetesCajas { get; set; }
        [Display(Name = "Estado")]
        public bool Estado { get; set; }
        public Pedidos Pedido { get; set; }

    }
}