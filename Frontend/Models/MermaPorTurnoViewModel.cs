﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class MermaPorTurnoViewModel
    {
        [Key]
        [Display(Name = "Merma por turno ID")]
        public int MermaPorTurnoID { get; set; }
        [Display(Name = "Cantidad")]
        [Required(ErrorMessage = "Debe ingresar la cantidad")]
        public double Cantidad { get; set; }
        [Display(Name = "Apertura cierre")]
        [Required(ErrorMessage = "Debe ingresar la apertura de cierre")]
        public int AperturaCierreID { get; set; }
        [Display(Name = "Tipo de merma")]
        [Required(ErrorMessage = "Debe ingresar el tipo de merma")]
        public int TipoMermaID { get; set; }
        [Display(Name = "Estado")]
        public bool Estado { get; set; }

        public AperturaCierre AperturaCierre { get; set; }

        public AperturaCierreViewModel AperturaCierreViewModel { get; set; }
        public TipoMerma TipoMerma { get; set; }

        public IEnumerable <AperturaCierreActivos> AperturaCierres { get; set; }
        public IEnumerable<AperturaCierreViewModel> AperturaCierresViewModel { get; set; }
        public IEnumerable <TipoMerma> TipoMermas { get; set; }
    }
}