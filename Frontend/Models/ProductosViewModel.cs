﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class ProductosViewModel
    {
        [Key]
        [Display(Name = "ID")]
        public int ProductoID { get; set; }
        [Display(Name = "Codigo de producto")]
        [Required(ErrorMessage = "Debe ingresar el codigo")]
        [MinLength(3)]
        public string CodigoProducto { get; set; }
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Debe ingresar el nombre")]
        [MinLength(5)]
        public string Nombre { get; set; }
        [Display(Name = "Tipo de producto")]
        [Required(ErrorMessage = "Debe ingresar el tipo")]
        public int TipoProductoID { get; set; }
        [Display(Name = "Descripcion")]
        [DataType(DataType.MultilineText)]
        public string Descripcion { get; set; }
        [Display(Name = "Imagen")]
        public string Imagen { get; set; }
        [Display(Name = "Estado")]
        public bool Estado { get; set; }
        public HttpPostedFileBase ArchivoImagen { get; set; }

        public TipoProducto TipoProducto { get; set; }
        public IEnumerable<TipoProducto> TipoProductos { get; set; }
        
    }
}