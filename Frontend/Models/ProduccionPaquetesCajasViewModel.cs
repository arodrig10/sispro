﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class ProduccionPaquetesCajasViewModel
    {
        [Key]
        [Display(Name = "# etiqueta")]
        public int EtiquetaID { get; set; }
        [Display(Name = "Fecha")]
        public System.DateTime Fecha { get; set; }
        [Display(Name = "Cantidad")]
        public double Cantidad { get; set; }
        [Display(Name = "Apertura cierre")]
        public long AperturaCierreID { get; set; }
        [Display(Name = "Operario")]
        public int UsuarioID { get; set; }
        [Display(Name = "Turno")]
        public string Turno { get; set; }
        [Display(Name = "Estado")]
        public bool Estado { get; set; }
        [Display(Name = "Pedido")]
        public string Pedido { get; set; }

        //Objeto de tipo Pedido para poder mostrar los pedidos en una lista desplegable ,se usará una lista IENUMERABLE
        public AperturaCierre AperturaCierre { get; set; }
        public IEnumerable<AperturaCierre> aperturaCierres { get; set; }

        public Pedidos PedidoCompleto { get; set; }
        public IEnumerable<PedidosActivos> PedidosActivos { get; set; }

        public Usuarios Usuario { get; set; }

        //-------------------------------------------------FIN DE Pedidos----------------------------------------------
    }
}