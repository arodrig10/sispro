﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class MateriaPrimaViewModel
    {
        [Key]
        [Display(Name = "ID")]
        public int MateriaPrimaID { get; set; }
        [Display(Name = "Codigo")]
        [Required(ErrorMessage = "Debe ingresar el codigo")]
        [MinLength(3)]
        public string CodigoMateriaPrima { get; set; }
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Debe ingresar el nombre")]
        [MinLength(5)]
        public string Nombre { get; set; }
        [Display(Name = "Proveedor")]
        [Required(ErrorMessage = "Debe ingresar el proveedor")]
        public int ProveedorID { get; set; }
        [Display(Name = "Tipo")]
        [Required(ErrorMessage = "Debe ingresar el tipo")]
        public int TipoMateriaPrimaID { get; set; }
        [Display(Name = "Unidad de medida")]
        [Required(ErrorMessage = "Debe ingresar la unidad de medida")]
        public int UnidadMedidaID { get; set; }
        [Display(Name = "Cantidad (segun unidad)")]
        [Required(ErrorMessage = "Debe ingresar la cantidad")]
        public int Cantidad { get; set; }
        [Display(Name = "Precio unitario (colones)")]
        [Required(ErrorMessage = "Debe ingresar el precio unitario")]
        public decimal PrecioUnitario { get; set; }
        [Display(Name = "Precio total (colones)")]
        [Required(ErrorMessage = "Debe ingresar el total")]
        public decimal PrecioTotal { get; set; }
        [Display(Name = "Descripcion")]
        [DataType(DataType.MultilineText)]
        public string Descripcion { get; set; }
        [Display(Name = "Estado")]
        public bool Estado { get; set; }
        public Proveedor Proveedor { get; set; }
        public TipoMateriaPrima TipoMateriaPrima { get; set; }
        public UnidadMedida UnidadMedida { get; set; }
        public IEnumerable<ProveedorActivos> Proveedores { get; set; }
        public IEnumerable<TipoMateriaPrima> TipoMateriaPrimas { get; set; }
        public IEnumerable<UnidadMedida> UnidadMedidas { get; set; }
    }
}