﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class ClientesViewModel
    {
        [Key]
        [Display(Name = "ID")]
        public int ClienteID { get; set; }
        [Required(ErrorMessage = "Debe ingresar el numero de telefono")]
        [MinLength(9)]
        [Display(Name = "Identificacion")]
        public string Identificacion { get; set; }
        [Display(Name ="Nombre completo")]
        [MinLength(10)]
        [Required(ErrorMessage = "Debe ingresar el nombre completo")]
        public string NombreCompleto { get; set; }
        [MinLength(8)]
        [Required(ErrorMessage = "Debe ingresar el telefono")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Telefono")]
        public string Telefono { get; set; }
        [MinLength(5)]
        [Required(ErrorMessage = "Debe ingresar el correo")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Correo")]
        public string Correo { get; set; }
        [Display(Name = "Direccion")]
        [DataType(DataType.MultilineText)]
        public string Direccion { get; set; }
        [Display(Name = "Estado")]
        public bool Estado { get; set; }
    }
}