﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class EntregaViewModel
    {
        [Key]
        [Display(Name = "ID")]
        public int EntregaID { get; set; }
        [Display(Name ="# entrega")]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Debe ingresar el numero de entrega")]
        public string NumeroEntrega { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha")]
        [Required(ErrorMessage = "Debe ingresar la fecha de entrega")]
        public Nullable<System.DateTime> Fecha { get; set; }
        [Display(Name = "Unidades entregadas")]
        [Required(ErrorMessage = "Debe ingresar la cantidad de la entrega")]
        public Nullable<int>  UnidadesEntregadas { get; set; }
        [Display(Name = "# factura")]
        [Required(ErrorMessage = "Debe ingresar el numero de factura")]
        public string NumeroFactura { get; set; }
        [Display(Name = "Estado")]
        public bool Estado { get; set; }

        //Objeto de tipo Pedido para poder mostrar los pedidos en una lista desplegable ,se usará una lista IENUMERABLE
        public Pedidos Pedido { get; set; }
        public IEnumerable<PedidosActivos> Pedidos { get; set; }

        //-------------------------------------------------FIN DE Pedidos----------------------------------------------
        //Objeto de tipo Clientes para poder mostrar los pedidos en una lista desplegable ,se usará una lista IENUMERABLE
        public Clientes Cliente { get; set; }
        public IEnumerable<Clientes> Clientes { get; set; }

        public IEnumerable<ClientesProduccion> ClientesProduccion { get; set; }
        //-------------------------------------------------FIN DE Pedidos----------------------------------------------
        public List<DetalleEntregaViewModel> DetalleEntregas { get; set; }

    }
}