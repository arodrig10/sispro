﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class UsuarioViewModel
    {
        [Key]
        [Display(Name = "ID")]
        public int UsuarioID { get; set; }
        [Display(Name = "Identificacion")]
        [MinLength(9)]
        [Required(ErrorMessage = "Escriba Identificacion")]
        public string Identificacion { get; set; }
        [Required (ErrorMessage = "Escriba el nombre de usuario")]
        [MinLength(5)]
        [Display(Name = "Nombre de usuario")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "El campo no puede estar vacío, por favor ingrese la contraseña")]
        [Display(Name = "Contraseña")]
        [MinLength(8)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(ErrorMessage = "El campo no puede estar vacío, por favor repita la contraseña")]
        [Display(Name = "Repetir contraseña")]
        [MinLength(8)]
        [DataType(DataType.Password)]
        public string RepeatPassword { get; set; }
        [Required(ErrorMessage = "El campo no puede estar vacio, por favor escriba la nueva contraseña")]
        [Display(Name = "Nueva contraseña")]
        [MinLength(8)]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }
        [Required(ErrorMessage = "Escriba el nombre")]
        [Display(Name = "Nombre completo")]
        [MinLength(10)]
        public string NombreCompleto { get; set; }
        [Required]
        [Display(Name = "Telefono")]
        [MinLength(8)]
        [DataType(DataType.PhoneNumber)]
        public string Telefono { get; set; }
        [DataType(DataType.EmailAddress)]
        [MinLength(5)]
        [Required]
        [Display(Name = "Correo")]
        public string Correo { get; set; }
        [Display(Name = "Direccion")]
        [DataType(DataType.MultilineText)]
        public string Direccion { get; set; }
        [Display(Name = "Estado")]
        public bool Estado { get; set; }


        [Required(ErrorMessage = "EL campo no puede estar vacio, ingrese el código de verificación")]
        [Display(Name = "Código de verificación")]
        public string codigoVerificación { get; set; }
        public IEnumerable<Roles> Roles { get; set; }
        public int RoleID { get; set; }
    }
}