﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class FichaTecnicaBotellaViewModel
    {
        [Key]
        [Display(Name = "ID")]
        public int FichaTecnicaBotellaID { get; set; }
        [Display(Name = "Producto")]
        [Required(ErrorMessage = "Debe ingresar el producto")]
        public int ProductoID { get; set; }
        [Display(Name = "Altura (milimetros)")]
        [Required(ErrorMessage = "Debe ingresar la altura")]
        public double Altura { get; set; }
        [Display(Name = "Diametro mayor (milimetros)")]
        [Required(ErrorMessage = "Debe ingresar el diametro mayor")]
        public double DiametroMayor { get; set; }
        [Display(Name = "Diametro menor (milimetros)")]
        [Required(ErrorMessage = "Debe ingresar el diametro menor")]
        public double DiametroMenor { get; set; }
        [Display(Name = "Diametro cuello (milimetros)")]
        [Required(ErrorMessage = "Debe ingresar el diametro del cuello")]
        public double DiametroCuello { get; set; }
        [Display(Name = "Capacidad volumetrica (mililitros)")]
        [Required(ErrorMessage = "Debe ingresar la capacidad volumetrica")]
        public double CapacidadVolumetrica { get; set; }
        [Display(Name = "Tolerancia")]
        public Nullable<double> Tolerancia { get; set; }
        [Display(Name = "Imagen cuerpo")]
     //   [DataType(DataType.ImageUrl)]
        public string ImagenCuerpo { get; set; }
        [Display(Name = "Imagen fondo")]
     //   [DataType(DataType.ImageUrl)]
        public string ImagenFondo { get; set; }
        [Display(Name = "Estado")]
        public bool Estado { get; set; }
        public HttpPostedFileBase ArchivoImagenCuerpo { get; set; }
        public HttpPostedFileBase ArchivoImagenFondo { get; set; }
        public Productos Producto { get; set; }
        public IEnumerable<ProductosBotellaActivos> Productos { get; set; }
    }
}