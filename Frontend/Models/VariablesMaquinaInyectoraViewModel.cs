﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class VariablesMaquinaInyectoraViewModel
    {
        [Key]
        [Display(Name = "ID")]
        public int VariablesMaquinaInyectoraID { get; set; }
        [Display(Name = "Maquina")]
        [Required(ErrorMessage = "Debe ingresar la maquina")]
        public int MaquinaID { get; set; }
        [Display(Name = "Producto")]
        [Required(ErrorMessage = "Debe ingresar el producto")]
        public int ProductoID { get; set; }
        [Display(Name = "Materia prima principal")]
        [Required(ErrorMessage = "Debe ingresar la materia prima principal")]
        public int MateriaPrimaPrincipalID { get; set; }
        [Display(Name = "Materia prima secundaria")]
        public Nullable<int> MateriaPrimaSecundariaID { get; set; }
        [Display(Name = "Materia prima terciaria")]
        public Nullable<int> MateriaPrimaTerciariaID { get; set; }
        [Display(Name = "Cantidad materia prima principal (gramos)")]
        [Required(ErrorMessage = "Debe ingresar la cantidad de materia prima principal")]
        public double CantidadPrimaPrincipal { get; set; }
        [Display(Name = "Cantidad materia prima secundaria (gramos)")]
        public Nullable<double> CantidadPrimaSecundaria { get; set; }
        [Display(Name = "Cantidad materia prima terciaria (gramos)")]
        public Nullable<double> CantidadPrimaTerciaria { get; set; }
        [Display(Name = "Estado")]
        public bool Estado { get; set; }

        public Maquinas Maquina { get; set; }
        public MateriaPrima MateriaPrima { get; set; }
        public MateriaPrima MateriaPrima1 { get; set; }
        public MateriaPrima MateriaPrima2 { get; set; }
        public Productos Producto { get; set; }
        public IEnumerable<MaquinasInyectoraActivos> Maquinas { get; set; }
        public IEnumerable<MateriaPrimaInyectoraActivos> MateriaPrimas { get; set; }
        public IEnumerable<ProductosTapaActivos> Productos { get; set; }
    }
}