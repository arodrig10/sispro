﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class VariablesMaquinaSopladoraViewModel
    {
        [Key]
        [Display(Name = "ID")]
        public int VariablesMaquinaSopladoraID { get; set; }
        [Display(Name = "Maquina")]
        [Required(ErrorMessage = "Debe ingresar la maquina")]
        public int MaquinaID { get; set; }
        [Display(Name = "Producto")]
        [Required(ErrorMessage = "Debe ingresar el producto")]
        public int ProductoID { get; set; }
        [Display(Name = "Materia Prima")]
        [Required(ErrorMessage = "Debe ingresar la materia prima")]
        public int MateriaPrimaID { get; set; }
        [Display(Name = "Retardo presoplado (ms)")]
        [Required(ErrorMessage = "Debe ingresar el retardo de presoplado")]
        public int RetardoPresoplado { get; set; }
        [Display(Name = "Retardo soplado (ms)")]
        [Required(ErrorMessage = "Debe ingresar el retardo de soplado")]
        public int RetardoSoplado { get; set; }
        [Display(Name = "Tiempo presoplado (ms)")]
        [Required(ErrorMessage = "Debe ingresar el tiempo de presoplado")]
        public int TiempoPresoplado { get; set; }
        [Display(Name = "Tiempo recuperacion (ms)")]
        [Required(ErrorMessage = "Debe ingresar el tiempo de recuperacion")]
        public int TiempoRecuperacion { get; set; }
        [Display(Name = "Tiempo soplado (ms)")]
        [Required(ErrorMessage = "Debe ingresar el tiempo de soplado")]
        public int TiempoSoplado { get; set; }
        [Display(Name = "Tiempo escape (ms)")]
        [Required(ErrorMessage = "Debe ingresar el tiempo de escape")]
        public int TiempoEscape { get; set; }
        [Display(Name = "Tiempo paro cadena (ms)")]
        [Required(ErrorMessage = "Debe ingresar el tiempo de paro de cadena")]
        public int TiempoParoCadena { get; set; }
        [Display(Name = "Tiempo salida pinza preforma (ms)")]
        [Required(ErrorMessage = "Debe ingresar el tiempo de salida de la pinza de preforma")]
        public int TiempoSalidaPinzaPreforma { get; set; }
        [Display(Name = "Frecuencia ventilador (hz)")]
        [Required(ErrorMessage = "Debe ingresar la frecuencia del ventilador")]
        public double FrecuenciaVentilador { get; set; }
        [Display(Name = "Presion estirado (bar)")]
        [Required(ErrorMessage = "Debe ingresar la presion de estirado")]
        public double PresionEstirado { get; set; }
        [Display(Name = "Gancho")]
        [Required]
        public bool Gancho { get; set; }
        public string GanchoMensaje { get; set; }

        [Display(Name = "Relay")]
        [Required]
        public bool Relay { get; set; }
        public string RelayMensaje { get; set; }
        [Display(Name = "Anillos tornelas")]
        [Required]
        public bool AnillosTornelas { get; set; }
        public string AnillosTornelasMensaje { get; set; }
        [Display(Name = "Estado")]
        public bool Estado { get; set; }
        [Display(Name = "Notas")]
        [DataType(DataType.MultilineText)]
        public string Notas { get; set; }
        [Display(Name = "Cantidad de zonas")]
        [Required(ErrorMessage = "Debe ingresar la cantidad de zonas")]
        public int CantidadZonas { get; set; }
        [Display(Name = "Cantidad de bancos")]
        [Required(ErrorMessage = "Debe ingresar la cantidad de bancos")]
        public int CantidadBancos { get; set; }
        [Display(Name = "Posicion Banco 1 Zona 1")]
        public string PosicionBanco1Zona1 { get; set; }
        [Display(Name = "Posicion Banco 1 Zona 2")]
        public string PosicionBanco1Zona2 { get; set; }
        [Display(Name = "Posicion Banco 1 Zona 3")]
        public string PosicionBanco1Zona3 { get; set; }
        [Display(Name = "Posicion Banco 1 Zona 4")]
        public string PosicionBanco1Zona4 { get; set; }
        [Display(Name = "Posicion Banco 2 Zona 1")]
        public string PosicionBanco2Zona1 { get; set; }
        [Display(Name = "Posicion Banco 2 Zona 2")]
        public string PosicionBanco2Zona2 { get; set; }
        [Display(Name = "Posicion Banco 2 Zona 3")]
        public string PosicionBanco2Zona3 { get; set; }
        [Display(Name = "Posicion Banco 2 Zona 4")]
        public string PosicionBanco2Zona4 { get; set; }
        [Display(Name = "Valor Banco 1 Zona 1")]
        public Nullable<int> ValorBanco1Zona1 { get; set; }
        [Display(Name = "Valor Banco 1 Zona 2")]
        public Nullable<int> ValorBanco1Zona2 { get; set; }
        [Display(Name = "Valor Banco 1 Zona 3")]
        public Nullable<int> ValorBanco1Zona3 { get; set; }
        [Display(Name = "Valor Banco 1 Zona 4")]
        public Nullable<int> ValorBanco1Zona4 { get; set; }
        [Display(Name = "Valor Banco 2 Zona 1")]
        public Nullable<int> ValorBanco2Zona1 { get; set; }
        [Display(Name = "Valor Banco 2 Zona 2")]
        public Nullable<int> ValorBanco2Zona2 { get; set; }
        [Display(Name = "Valor Banco 2 Zona 3")]
        public Nullable<int> ValorBanco2Zona3 { get; set; }
        [Display(Name = "Valor Banco 2 Zona 4")]
        public Nullable<int> ValorBanco2Zona4 { get; set; }

        public Maquinas Maquina { get; set; }
        public MateriaPrima MateriaPrima { get; set; }
        public Productos Producto { get; set; }
        public IEnumerable<MaquinasSopladoraActivos> Maquinas { get; set; }
        public IEnumerable<MateriaPrimaSopladoraActivos> MateriaPrimas { get; set; }
        public IEnumerable<ProductosBotellaActivos> Productos { get; set; }
    }
}