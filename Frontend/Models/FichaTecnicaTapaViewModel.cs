﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class FichaTecnicaTapaViewModel
    {
        [Key]
        [Display(Name = "ID")]
        public int FichaTecnicaTapaID { get; set; }
        [Display(Name = "Producto")]
        [Required(ErrorMessage = "Debe ingresar el producto")]
        public int ProductoID { get; set; }
        [Display(Name = "Peso de pieza (gramos)")]
        [Required(ErrorMessage = "Debe ingresar el peso de la pieza")]
        public double PesoPieza { get; set; }
        [Display(Name = "Altura (milimetros)")]
        [Required(ErrorMessage = "Debe ingresar la altura")]
        public double Altura { get; set; }
        [Display(Name = "Diametro interno (milimetros)")]
        [Required(ErrorMessage = "Debe ingresar el diametro interno")]
        public double DiametroInterno { get; set; }
        [Display(Name = "Diametro externo sello oliva (milimetros)")]
        [Required(ErrorMessage = "Debe ingresar el diametro externo del sello oliva")]
        public double DiametroExternoSelloOliva { get; set; }
        [Display(Name = "Altura sello oliva (milimetros)")]
        [Required(ErrorMessage = "Debe ingresar la altura del sello oliva")]
        public double AlturaSelloOliva { get; set; }
        [Display(Name = "Diametro interno banda de seguridad (milimetros)")]
        [Required(ErrorMessage = "Debe ingresar el diametro interno de la banda de seguridad")]
        public double DiametroInternoBandaSeguridad { get; set; }
        [Display(Name = "Espesor panel (milimetros)")]
        [Required(ErrorMessage = "Debe ingresar el espesor del panel")]
        public double EspesorPanel { get; set; }
        [Display(Name = "Numero de estrias")]
        [Required(ErrorMessage = "Debe ingresar el numero de estrias")]
        public double NumeroEstrias { get; set; }
        [Display(Name = "Torque cierre recomendado (in/lbs)")]
        [Required(ErrorMessage = "Debe ingresar el torque de cierre recomendado")]
        public double TorqueCierreRecomendado { get; set; }
        [Display(Name = "Torque apertura (in/lbs)")]
        [Required(ErrorMessage = "Debe ingresar el torque de apertura")]
        public double TorqueApertura { get; set; }
        [Display(Name = "Tolerancia")]
        public Nullable<double> Tolerancia { get; set; }
        [Display(Name = "Imagen")]
        public string Imagen { get; set; }
        [Display(Name = "Estado")]
        public HttpPostedFileBase ArchivoImagen { get; set; }
        public bool Estado { get; set; }
        public Productos Producto { get; set; }
        public IEnumerable<ProductosTapaActivos> Productos { get; set; }
    }
}