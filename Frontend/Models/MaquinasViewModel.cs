﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Frontend.Models
{
    public class MaquinasViewModel
    {
        [Key]
        [Display(Name = "ID")]
        public int MaquinaID { get; set; }
        [Required(ErrorMessage = "Debe ingresar el codigo")]
       
        [Display(Name = "Codigo de Maquina")]
        [MinLength(3)]
        public string CodigoMaquina { get; set; }
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Debe ingresar el nombre")]
        [MinLength(5)]
        public string Nombre { get; set; }
        [Display(Name = "Tipo de maquina")]
        [Required(ErrorMessage = "Debe ingresar el tipo de maquina")]
        public int TipoMaquinaID { get; set; }
        [Display(Name = "Descripcion")]
        [DataType(DataType.MultilineText)]
        public string Descripcion { get; set; }
        [Display(Name = "Produccion por hora (unidades)")]
        public string ProduccionHora { get; set; }
        [Display(Name = "Estado")]
        public bool Estado { get; set; }

        public TipoMaquina TipoMaquina { get; set; }

        public IEnumerable<TipoMaquina> TipoMaquinas { get; set; }

       /* public IEnumerable<VariablesMaquinaInyectora> VariablesMaquinaInyectora { get; set; }
        public IEnumerable<VariablesMaquinaSopladora> VariablesMaquinaSopladora { get; set; }*/
    }
}
