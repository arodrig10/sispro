﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class PedidoViewModel
    {
        [Key]
        [Display(Name = "ID")]
        public int PedidoID { get; set; }
        [Display(Name = "# de pedido")]
        [Required(ErrorMessage = "Debe ingresar el numero de pedido")]
        [MinLength(3)]
        public string NumeroPedido { get; set; }
        //iD TOMADO DE LA LISTA DESPLEGABLE DE CLIENTES
        [Display(Name = "Cliente")]
        [Required(ErrorMessage = "Debe ingresar el cliente")]
        public int ClienteID { get; set; }
        [Display(Name = "# de OC")]
        public string NumeroOrdenDeCompra { get; set; }
        [Display(Name = "Producto")]
        [Required(ErrorMessage = "Debe ingresar el producto")]
        //ID TOMADO DE LA LISTA DESPLEGABLE DE PRODUCTOS
        public int ProductoID { get; set; }
        [Display(Name = "Usuario")]
        //Usuario se deberá tomar de la sesion actual
        public int UsuarioID { get; set; }
        //ID TOMADO DE LA LISTA DESPLEGABLE DE PRODUCTOS
        [Display(Name = "Unidad de medida")]
        [Required(ErrorMessage = "Debe ingresar la unidad de medida")]
        public int UnidadMedidaID { get; set; }
        [Display(Name = "Cantidad (segun unidad)")]
        [Required(ErrorMessage = "Debe ingresar la cantidad")]
        public int Cantidad { get; set; }
        [Display(Name = "Descripcion")]
        [DataType(DataType.MultilineText)]
        public string Descripcion { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha de entrega")]
        public Nullable<System.DateTime> Fecha { get; set; }
        [Display(Name = "Precio total (colones)")]
        [Required(ErrorMessage = "Debe ingresar el precio total")]
        public decimal PrecioTotal { get; set; }
        [Display(Name = "Estado")]
        public bool Estado { get; set; }
        public Nullable<int> EstadoID { get; set; }


        public float produccionActual { get; set; }
        public float entregadoActual {get; set;}


        //Objeto de tipo cliente para poder mostrar los clientes en una lista desplegable, se usará una lista IENUMERABLE
        public Clientes Cliente { get; set; }
        public IEnumerable<ClientesActivos> Clientes { get; set; }

        //-------------------------------------------------FIN DE CLIENTE-----------------------------------------------

        //Objeto de tipo PRODUCTOS para poder mostrar los PRODUCTOS en una lista desplegable ,se usará una lista IENUMERABLE
        public Productos Producto { get; set; }
        public IEnumerable<ProductosActivos> Productos { get; set; }

        //-------------------------------------------------FIN DE Productos-----------------------------------------------
        //Objeto de tipo Unidad de Medida para poder mostrar las unidades de medida en una lista desplegable ,se usará una lista IENUMERABLE
        public UnidadMedida UnidadMedida { get; set;}
        public IEnumerable<UnidadMedida> UnidadMedidas { get; set; }

        public Usuarios Usuario { get; set; }
        public IEnumerable<UsuariosActivos> Usuarios { get; set; }

        //-------------------------------------------------FIN DE Unidad de Medida-----------------------------------------------

        //Objeto de tipo Estado para poder mostrar los en una lista desplegable ,se usará una lista IENUMERABLE
        public Estados EstadoPedido { get; set; }
        public IEnumerable<UnidadMedida> EstadosPedido { get; set; }


        //-------------------------------------------------FIN DE Unidad de Medida-----------------------------------------------


    }
}